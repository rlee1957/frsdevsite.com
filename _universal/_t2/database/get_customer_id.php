<?php

$_SESSION["order"]["test"] .= "get_customer_id.php";

foreach($_SESSION["order"]["addresses"] as $idx => $address)
	{
	$customer_id = -1;
	$address_id = -1;
	$phone_id = -1;
	$email_id = -1;
	include($path."../_t2/database/get_address_id.php");
	$_SESSION["order"]["addresses"][$idx]["address_id"] = $address_id;
	$first_name = $address["fn"];
	$last_name = $address["ln"];
	
	if($idx == 0)
		{
		include($path."../_t2/database/get_phone_id.php");
		include($path."../_t2/database/get_email_id.php");
		$_SESSION["order"]["addresses"][$idx]["phone_id"] = $phone_id;
		$_SESSION["order"]["addresses"][$idx]["email_id"] = $email_id;
		$sql = "
select 
	customer_id
from 
	contact_data 
where 
	upper(trim(first_name)) = ? and 
	upper(trim(last_name)) = ? and 
	(address_id = ? or
	phone_id = ? or 
	email_id = ?)
";
		$params = array();
		$params[count($params)] = strtoupper(trim($first_name));
		$params[count($params)] = strtoupper(trim($last_name));
		$params[count($params)] = $address_id;
		$params[count($params)] = $phone_id;
		$params[count($params)] = $email_id;
		}
	else
		{
		$sql = "
select 
	customer_id
from 
	contact_data 
where 
	upper(trim(first_name)) = ? and 
	upper(trim(last_name)) = ? and 
	address_id = ?
";	
		$params = array();
		$params[count($params)] = strtoupper(trim($first_name));
		$params[count($params)] = strtoupper(trim($last_name));
		$params[count($params)] = $address_id;
		}
	$results = sql_shell($sql, $params, $path);
	if((isset($results["rowcount"]))&&($results["rowcount"] > 0))
		{
		$customer_id = $results["recordset"][0]["customer_id"];	
		}
	else
		{
		$sql = "
insert into customers 
	(first_name, middle_name, last_name, full_name, date_created) 
values 
	(?, ?, ?, ?, ?) 
returning customer_id
";	
		$params = array();
		$params[count($params)] = ucwords(trim($address["fn"]));
		$params[count($params)] = ucwords(trim($address["mn"]));
		$params[count($params)] = ucwords(trim($address["ln"]));
		$params[count($params)] = ucwords(get_full_name(trim($address["fn"]), trim($address["mn"]), trim($address["ln"])));
		$params[count($params)] = $date_created;
		$results = sql_shell($sql, $params, $path);
		$customer_id = $results["recordset"][0]["customer_id"];
		/*echo($sql."<br />");
		echo("<textarea style='width: 100%; height: 350px;'>");
		print_r($results);
		echo("</textarea>");*/
		$sql = "
insert into contact_info 
	(customer_id, info_type, info_id, date_created) 
values 
	(?, ?, ?, ?)
";
		$params = array();
		$params[count($params)] = $customer_id;
		$params[count($params)] = "address";
		$params[count($params)] = $address_id;
		$params[count($params)] = $date_created;
		$results = exe_shell($sql, $params, $path);
		if($idx == 0)
			{
			$params = array();
			$params[count($params)] = $customer_id;
			$params[count($params)] = "phone";
			$params[count($params)] = $phone_id;
			$params[count($params)] = $date_created;
			$results = exe_shell($sql, $params, $path);
			$params = array();
			$params[count($params)] = $customer_id;
			$params[count($params)] = "email";
			$params[count($params)] = $email_id;
			$params[count($params)] = $date_created;
			$results = exe_shell($sql, $params, $path);
			}
		}
	$_SESSION["order"]["addresses"][$idx]["customer_id"] = $customer_id;
	
	}
$_SESSION["order"]["info"]["customer_id"] = $customer_id;

function get_full_name($fn, $mn, $ln)
{
$ret = trim($fn);
if(trim($mn) != "")	{ $ret .= " ".trim($mn); }
$ret .= " ".trim($ln);
return $ret;	
}

?>