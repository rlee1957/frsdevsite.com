<?php

# 17 parameters
$params = array();
$params[count($params)] = $campaign_id;			// [0]
$params[count($params)] = $campaign_name;		// [1]
$params[count($params)] = $campaign_db_name ;	// [2]
$params[count($params)] = $order_id ;			// [3]
$params[count($params)] = $datetimestamp ;		// [4]
$params[count($params)] = $author_first_name ;	// [5]
$params[count($params)] = $author_last_name ;	// [6]
$params[count($params)] = $author_phone ;		// [7]
$params[count($params)] = $author_email  ;		// [8]
$params[count($params)] = $story_type ;			// [9]
$params[count($params)] = $category ;			// [10]
$params[count($params)] = $details ;			// [11]
$params[count($params)] = $has_photo ;			// [12]
$params[count($params)] = $photo_file_name ;	// [13]
$params[count($params)] = $story_text ;			// [14]
$params[count($params)] = $story_html ;			// [15]
$params[count($params)] = $story_other ;		// [16]

?>