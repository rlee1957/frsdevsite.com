<?php

$gc = 0;
if(!isset($value["gift_certificate"])){ $gc = 0; }
else
	{
	if($value["gift_certificate"] == "yes"){ $gc = 1; }
	else
		{ 
		if(is_numeric($value["gift_certificate"]))
			{ 
			if(intval($value["gift_certificate"]) > 0){ $gc = 1; }
			} 	
		}
	}
# values for parameters
$o_order_no = $o_order_no;            			// [0]
$li_line  = $key;								// [1]
$li_bktype  = 1;								// [2]
$li_bkdesc  = $value["description"];			// [3]
$li_bklines  = $value["max_lines"];				// [4]
$li_bkchar  = $value["max_characters"];			// [5]
$li_bkrship  = 0;    							// [6]
$li_ins1  = $inscription[1];					// [7]
$li_ins2  = $inscription[2]; 					// [8]
$li_ins3  = $inscription[3]; 					// [9]
$li_ins4  = $inscription[4];					// [10]
$li_ins5  = $inscription[5];					// [11]
$li_ins6  = $inscription[6];					// [12]
$li_ins7  = $inscription[7];					// [13]
$li_ins8  = $inscription[8];					// [14]
$li_ins9  = $inscription[9];					// [15]
$li_ins10 = $inscription[10];					// [16]
$li_ins11 = $inscription[11]; 					// [17]
$li_ins12 = $inscription[12]; 					// [18]
$li_ins13 = $inscription[13]; 					// [19]
$li_ins14 = $inscription[14]; 					// [20]
$li_ins15 = $inscription[15]; 					// [21]
$li_batch = NULL;								// [22]
$li_logonam  = ""; 								// [23]
$li_logono  = ""; 								// [24]
$li_pbkno  = $value["item_no"]; 				// [25]
$li_rbkno  = $opt["replica1"]["bkno"]; 			// [26]
$li_r2bkno  = $opt["replica2"]["bkno"]; 		// [27]
$li_dcbkno  = $opt["display_case1"]["bkno"];	// [28]
$li_dc2bkno  = $opt["display_case2"]["bkno"];	// [29]
$li_pqty  = 1; 									// [30]
$li_rqty  = $opt["replica1"]["quantity"];		// [31]
$li_r2qty  = $opt["replica2"]["quantity"];		// [32]
$li_dcqty  = $opt["display_case1"]["quantity"];	// [33]
$li_dc2qty  = $opt["display_case2"]["quantity"];// [34]
$li_bkpcost  = $value["price"];					// [35]
$li_bkrcost  = $opt["replica1"]["price"]; 		// [36]
$li_bkr2cost  = $opt["replica2"]["price"]; 		// [37]
$li_dccost  = $opt["display_case1"]["price"];	// [38]
$li_dc2cost  = $opt["display_case2"]["price"];	// [39]
$li_gc  = $gc;									// [40] smallint
$li_location  = "Not Applicable";				// [41] character(250)

?>