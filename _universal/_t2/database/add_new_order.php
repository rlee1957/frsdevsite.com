<?php

$_SESSION["order"]["test"] .= "add_new_order.php
";
$sql = "
insert into orders (
	campaign_number,
	purchase_id,
	status,
	status_date,
	transaction_date,
	transaction_amount,
	tax_total,
	shipping_total,
	convenience_fee,
	greek_fee,
	other_fee1,
	other_fee1_description,
	other_fee2,
	other_fee2_description,
	other_fee3,
	other_fee3_description,
	promotional_discount,
	promotional_code,
	cart_total,
	customer_id,
	non_profit,
	batch,
	credit_card_month,
	credit_card_year,
	credit_card_owner,
	credit_card_type,
	authorization_number,
	response_code,
	result_code,
	result_message,
	misc,
	marketing,
	promotion,
	extra,
	entered_by
	) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning order_id
";

$misc = get_misc();	
$marketing = get_marketing();
$promotion = get_promotion();
$extra = get_extra();
$params = array();
$params[count($params)] = $campaign_number;										// campaign number
$params[count($params)] = "purchase_id update";									// purchase ID
$params[count($params)] = "status update";										// order status
$params[count($params)] = "status_date update";									// date of order status
$params[count($params)] = $date_created;										// transaction date
$params[count($params)] = $_SESSION["order"]["info"]["amount"]["order_total"];	// transaction amount
$params[count($params)] = get_tax_total($path);									// tax total
$params[count($params)] = get_shipping_total($path);							// shipping total
$params[count($params)] = get_convenience_fee($path);							// convenience fee
$params[count($params)] = get_greek_fee($path);									// greek_fee
$params[count($params)] = get_other_fee($path, 1, "fee");						// other fee 1
$params[count($params)] = get_other_fee($path, 1, "description");				// other fee 1 description
$params[count($params)] = get_other_fee($path, 2, "fee");						// other fee 2
$params[count($params)] = get_other_fee($path, 2, "description");				// other fee 2 description
$params[count($params)] = get_other_fee($path, 3, "fee");						// other fee 3
$params[count($params)] = get_other_fee($path, 3, "description");				// other fee 3 description
$params[count($params)] = get_promotional_discount($path);						// promotional discount
$params[count($params)] = get_promotion_code($path);							// promotion code
$params[count($params)] = get_cart_total($path);								// cart total
$params[count($params)] = $_SESSION["order"]["info"]["customer_id"];			// customer ID
$params[count($params)] = NULL;													// is customer a non prophet organization?
$params[count($params)] = NULL;													// batch identifier
$params[count($params)] = $_SESSION["order"]["info"]["cc"]["cc_month"];			// credit card month
$params[count($params)] = $_SESSION["order"]["info"]["cc"]["cc_year"];			// credit card year
$params[count($params)] = $_SESSION["order"]["info"]["cc"]["cc_name"];			// credit card owner
$params[count($params)] = $_SESSION["order"]["info"]["cc"]["cc_type"];			// credit card type
$params[count($params)] = NULL;													// authorization number
$params[count($params)] = NULL;													// response code
$params[count($params)] = NULL;													// result code
$params[count($params)] = NULL;													// result message
$params[count($params)] = $misc;												// misc
$params[count($params)] = $marketing;											// marketing
$params[count($params)] = $promotion;											// promotion
$params[count($params)] = $extra;												// extra
$params[count($params)] = "Web Order";											// entered by
/*echo($sql."<br />");										// entered by
echo("The parameters<br />");
echo("<textarea style='width: 100%; height: 350px;>");
print_r($params);
echo("</textarea>");*/
$results = sql_shell($sql, $params, $path);
/*echo($sql."<br />");
echo("<textarea style='width: 100%; height: 350px;>");
print_r($results);
echo("</textarea>");*/
// $order_id = -1;
if((isset($results["rowcount"]))&&($results["rowcount"] > 0))
	{
	$order_id = $results["recordset"][0]["order_id"];	
	}
$_SESSION["order"]["info"]["order_id"] = $order_id;
$_SESSION["order"]["info"]["purchase_id"] = get_purchase_id($path);
$_SESSION["order"]["info"]["status"] = "";
$_SESSION["order"]["info"]["status_date"] = "";
	
?>