<?php

# 22 Fields
$sql = "
insert into w_shipto 
	( 
	s_order_no ,
	s_line ,
	s_shipline ,
	s_r1qty ,
	s_r2qty ,
	s_dc1qty ,
	s_dc2qty ,
	s_qty ,
	s_fname ,
	s_mname ,
	s_lname ,
	s_fullname ,
	s_company ,
	s_addr1 ,
	s_addr2 ,
	s_city ,
	s_state ,
	s_zip ,
	s_country ,
	s_phone ,
	s_email ,
	s_batch 
	)
values
	(
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	? 
	)
";

?>