<?php

# 5 fields; 6 Parameters
$sql = "
update orders 
set
	purchase_id = ?,
	status = ?,
	status_date = ?,
	authorization_number = ?,
	response_code = ?,
	result_code = ?,
	result_message = ?
where
	order_id = ?
";

?>