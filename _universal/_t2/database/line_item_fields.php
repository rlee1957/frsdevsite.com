<?php

$fields = "li_order_no, ";		// [0]
$fields .= "li_line, ";			// [1]
$fields .= "li_bktype, ";		// [2]
$fields .= "li_bkdesc, ";		// [3]
$fields .= "li_bklines, ";		// [4]
$fields .= "li_bkchar, ";		// [5]
$fields .= "li_bkrship, ";		// [6]
$fields .= "li_ins1, ";			// [7]
$fields .= "li_ins2, ";			// [8]
$fields .= "li_ins3, ";			// [9]
$fields .= "li_ins4, ";			// [10]
$fields .= "li_ins5, ";			// [11]
$fields .= "li_ins6, ";			// [12]
$fields .= "li_ins7, ";			// [13]
$fields .= "li_ins8, ";			// [14]
$fields .= "li_ins9, ";			// [15]
$fields .= "li_ins10, ";		// [16]
$fields .= "li_ins11, ";		// [17]
$fields .= "li_ins12, ";		// [18]
$fields .= "li_ins13, ";		// [19]
$fields .= "li_ins14, ";		// [20]
$fields .= "li_ins15, ";		// [21]
$fields .= "li_batch, ";		// [22]
$fields .= "li_logonam, ";		// [23]
$fields .= "li_logono, ";		// [24]
$fields .= "li_pbkno, ";		// [25]
$fields .= "li_rbkno, ";		// [26]
$fields .= "li_r2bkno, ";		// [27]
$fields .= "li_dcbkno, ";		// [28]
$fields .= "li_dc2bkno, ";		// [29]
$fields .= "li_pqty, ";			// [30]
$fields .= "li_rqty, ";			// [31]
$fields .= "li_r2qty, ";		// [32]
$fields .= "li_dcqty, ";		// [33]
$fields .= "li_dc2qty, ";		// [34]
$fields .= "li_bkpcost, ";		// [35]
$fields .= "li_bkrcost, ";		// [36]
$fields .= "li_bkr2cost, ";		// [37]
$fields .= "li_dccost, ";		// [38]
$fields .= "li_dc2cost, ";		// [39]
$fields .= "li_gc, ";			// [40] smallint
$fields .= "li_location";		// [41] character(250)

?>