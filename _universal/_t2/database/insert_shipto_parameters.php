<?php

# 22 parameters
$params = array();
$params[count($params)] = $s_order_no;			// [0]
$params[count($params)] = $s_line;				// [1]
$params[count($params)] = $s_shipline ;			// [2]
$params[count($params)] = $s_r1qty ;			// [3]
$params[count($params)] = $s_r2qty ;			// [4]
$params[count($params)] = $s_dc1qty ;			// [5]
$params[count($params)] = $s_dc2qty ;			// [6]
$params[count($params)] = $s_qty ;				// [7]
$params[count($params)] = $s_fname  ;			// [8]
$params[count($params)] = $s_mname ;			// [9]
$params[count($params)] = $s_lname ;			// [10]
$params[count($params)] = $s_fullname ;			// [11]
$params[count($params)] = $s_company ;			// [12]
$params[count($params)] = $s_addr1 ;			// [13]
$params[count($params)] = $s_addr2 ;			// [14]
$params[count($params)] = $s_city ;				// [15]
$params[count($params)] = $s_state ;			// [16]
$params[count($params)] = $s_zip ;				// [17]
$params[count($params)] = $s_country ;			// [18]
$params[count($params)] = $s_phone ;			// [19]
$params[count($params)] = $s_email ;			// [20]
$params[count($params)] = $s_batch ;			// [21]

?>