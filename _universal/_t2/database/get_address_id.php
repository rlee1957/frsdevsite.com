<?php

$_SESSION["order"]["test"] .= "get_address_id.php
";
$address_id = -1;
$sql = "
select 
	address_id 
from 
	addresses 
where 
	upper(address1) = ? and
	upper(city) = ? and
	upper(state_prov) = ? and
	upper(postal_code) = ? and
	upper(country) = ?
";
$params = array();
$params[count($params)] = strtoupper(trim($address["address1"]));
$params[count($params)] = strtoupper(trim($address["city"]));
$params[count($params)] = strtoupper(trim($address["state_prov"]));
$params[count($params)] = strtoupper(trim($address["postal_code"]));
$params[count($params)] = strtoupper(trim($address["country"]));
$results = sql_shell($sql, $params, $path);
if((isset($results["rowcount"]))&&($results["rowcount"] > 0))
	{
	$address_id = $results["recordset"][0]["address_id"];
	}
else
	{
	$sql = "
insert into addresses 
	(
	company, 
	address1, 
	address2, 
	city, 
	state_prov, 
	postal_code, 
	country, 
	verified, 
	verified_3rd_party, 
	verified_by_name, 
	date_created
	) 
values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) 
returning address_id
";
	$params = array();
	$params[count($params)] = ucwords(trim($address["company"]));
	$params[count($params)] = strtoupper(trim($address["address1"]));
	$params[count($params)] = ucwords(trim($address["address2"]));
	$params[count($params)] = strtoupper(trim($address["city"]));
	$params[count($params)] = strtoupper(trim($address["state_prov"]));
	$params[count($params)] = strtoupper(trim($address["postal_code"]));
	$params[count($params)] = strtoupper(trim($address["country"]));
	$params[count($params)] = $date_created;
	$params[count($params)] = 't';
	$params[count($params)] = 'FedEx';
	$params[count($params)] = $date_created;
	$results = sql_shell($sql, $params, $path);
	if((isset($results["rowcount"]))&&($results["rowcount"] > 0))
		{
		$address_id = $results["recordset"][0]["address_id"];
		}	
	}
$_SESSION["order"]["addresses"][$idx]["address_id"] = $address_id;
$_SESSION["order"]["info"]["address_id"] = $_SESSION["order"]["addresses"][0]["address_id"];
	
?>