<?php

# 17 Fields
$sql = "
insert into stories
	( 
	campaign_id,
	campaign_name,
	campaign_db_name,
	order_id,
	datetimestamp,
	author_first_name,
	author_last_name,
	author_phone,
	author_email,
	story_type,
	category,
	details,
	has_photo,
	photo_file_name,
	story_text,
	story_html,
	story_other
	)
values
	(
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	?,
	? 
	)
";

?>