<?php


$_SESSION["order"]["test"] .= "add_new_lineitem.php
";
$sql = "
insert into line_items (
	order_id,
	item_id,
	amount,
	tax,
	shipping,
	shipping_cost,
	shipping_id,
	inscription_id,
	batch,
	gift_certificate,
	gift_certificate_id,
	location
	) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
";

$params = array();
$params[count($params)] = $order_id;											// order ID
$params[count($params)] = $item_id;												// catalog id for the item
$params[count($params)] = $amount;												// amount charged for this item
$params[count($params)] = $tax;													// tax charged for this item
$params[count($params)] = $shipping;											// is shipping calculated?
$params[count($params)] = $shipping_cost;										// amount charged for shipping
$params[count($params)] = $shipping_id;											// shipping ID							
$params[count($params)] = $inscription_id;										// inscription ID
$params[count($params)] = $batch;												// batch identifier
$params[count($params)] = $gift_certificate;									// is this item included in a gift certificate?
$params[count($params)] = $gift_certificate_id;									// gift certificate ID
$params[count($params)] = $location;											// status date
/*echo("The parameters<br />");
echo("<textarea style='width: 100%; height: 350px;>");
print_r($params);
echo("</textarea>");*/
$results = exe_shell($sql, $params, $path);
/*echo($sql."<br />");
echo("<textarea style='width: 100%; height: 350px;>");
print_r($results);
echo("</textarea>");*/
	
?>