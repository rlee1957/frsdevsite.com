<?php

$_SESSION["order"]["test"] .= "get_email_id.php
";
$email_id = -1;
$sql = "select email_id from email where upper(address) = ?";
$params = array();
$params[count($params)] = strtoupper(trim($_SESSION["order"]["addresses"][0]["email"]));
$results = sql_shell($sql, $params, $path);
if((isset($results["rowcount"]))&&($results["rowcount"] > 0))
	{
	$email_id = $results["recordset"][0]["email_id"];
	}

if($email_id == -1)
	{
	$sql = "
insert into email 
	(address, date_created) 
values 
	(?, ?) 
returning email_id
";	
	$params = array();
	$params[count($params)] = trim($_SESSION["order"]["addresses"][0]["email"]);
	$params[count($params)] = $date_created;
	$results = sql_shell($sql, $params, $path);
	$email_id = $results["recordset"][0]["email_id"];
	}
$_SESSION["order"]["info"]["email_id"] = $email_id;
$_SESSION["order"]["addresses"][0]["email_id"] = $email_id;

?>