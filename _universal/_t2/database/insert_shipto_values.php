<?php

# Pre Calculations
$addresses = $_SESSION["order"]["addresses"];
$ship_fn = trim($addresses[$aid]["fn"]); 		// see ../_t2/includes/billing.php
$ship_mn = trim($addresses[$aid]["mn"]);		// see ../_t2/includes/billing.php
$ship_ln = trim($addresses[$aid]["ln"]);		// see ../_t2/includes/billing.php
$ship_fullname = $ship_fn;
if(trim($ship_mn) != ""){ $ship_fullname .= " ".$ship_mn; }
$ship_fullname .= " ".$ship_ln;

# values for parameters (22)
$s_order_no  = $o_order_no;            					// [0]
$s_line  = $key;										// [1]
$s_shipline  = $ct;										// [2]
$s_r1qty  = $ship_addresses[$aid]["r1qty"];				// [3]
$s_r2qty  = $ship_addresses[$aid]["r2qty"];				// [4]
$s_dc1qty  = $ship_addresses[$aid]["dc1qty"];			// [5]
$s_dc2qty  = $ship_addresses[$aid]["dc2qty"];    		// [6]
$s_qty  = $s_r1qty + $s_r2qty + $s_dc1qty + $s_dc2qty;	// [7]
$s_fname  = $ship_fn; 									// [8]
$s_mname  = $ship_mn; 									// [9]
$s_lname  = $ship_ln;									// [10]
$s_fullname  = $ship_fullname;							// [11]
$s_company  = trim($addresses[$aid]["company"]);		// [12]
$s_addr1  = trim($addresses[$aid]["address1"]);			// [13]
$s_addr2  = trim($addresses[$aid]["address2"]);			// [14]
$s_city  = trim($addresses[$aid]["city"]);				// [15]
$s_state  = trim($addresses[$aid]["state_prov"]);			// [16]
$s_zip  = trim($addresses[$aid]["postal_code"]); 				// [17]
$s_country  = trim($addresses[$aid]["country"]); 		// [18]
$s_phone  = NULL; 										// [19]
$s_email  = NULL; 										// [20]
$s_batch  = NULL; 										// [21]

?>