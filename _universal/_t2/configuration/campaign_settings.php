<?php

if(!isset($path)){ $path = ""; }
include($path."configuration/environment_settings.php");
include($path."configuration/payment_processing_settings.php");
include($path."configuration/postgres_database_settings.php");
include($path."configuration/option_settings.php");
include($path."configuration/default_values.php");
include($path."configuration/color_settings.php");
include($path."configuration/image_settings.php");
include($path."configuration/contact_settings.php");
include($path."configuration/gift_certificate_settings.php");
include($path."configuration/tax_settings.php");
include($path."configuration/google_analytics_settings.php");
include($path."configuration/social_networking_settings.php");
include($path."configuration/fee_settings.php");
include($path."configuration/misc_settings.php");
include($path."configuration/story_settings.php"); # added 08/13/2015

?>