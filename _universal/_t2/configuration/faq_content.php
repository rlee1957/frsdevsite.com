<!-- Insert FAQ content here -->
<p class="exhead">INSCRIPTION / ENGRAVING</p>
<div class="questions">
    <h3>What are other people putting on their bricks?</h3>
    <div>
        <p>Please click 
		<a href='<?php echo($lang['set_iyb_link']); ?>' target='_blank'>
			<span style='font-weight: bold; font-style:normal; font-size: 10pt; color: <?php echo($color["link"]); ?>'>
				here
			</span>
		</a>
		to create your personal message on a virtual version of your brick. Examples of personal messages created by other fans are also provided.</p>
    </div>
    <h3>Can I put a personal image, graphic or symbol on my brick?</h3>
    <div>
        <p>No; only numbers, letters and/or punctuation marks found on a standard English keyboard are permitted.</p>
    </div>
    <h3>Are advertisements allowed?</h3>
    <div>
        <p>No; websites, phone numbers, twitter handles, hashtags, symbols, commercialization and/or manifestos are also not acceptable. The Georgia World Congress Center Authority reserves the right to approve all inscriptions prior to production. Should any message be deemed unsuitable, you will be contacted and allowed to submit an alternate inscription.</p>
    </div>
    <h3>Can I have my personal message engraved in italics for special emphasis?</h3>
    <div>
        <p>No; all inscriptions will be centered and engraved in UPPERCASE font for uniformity.</p>
    </div>
    <h3>How many characters fit on each line of a brick?</h3>
    <div>
        <p>Each line of text can be engraved with up to 14 characters per line. Characters can consist of letters, numbers, blank spaces, and punctuation marks found on a standard English keyboard.</p>
    </div>
    <h3>How many lines of text can I have on a brick?</h3>
    <div>
        <p>4&#148; x 8&#148; bricks can be engraved with up to two lines of text.</p>
        <p>8&#148; x 8&#148; bricks can be engraved with up to six four lines of text.</p>
    </div>
    <h3>Can I change my inscription if I change my mind?  Is there a fee?</h3>
    <div>
        <p>Yes; however, a $10 reprocessing fee will apply if those changes completely alter the original inscription submitted, requiring it to be resubmitted. A $10 reprocessing fee may also apply if changes are requested AFTER the proof time has expired on your &#147;Inscription Verification Letter,&#148; and additional redo fees will apply if the brick is already in production. No fees will apply for corrections made within the proof/verification letter timeframe. Inscription verification letters are typically sent out within three to four weeks of your order being processed and approved by The Georgia World Congress Center Authority.</p>
    </div>
    <h3>Can I have multiple bricks spell out one message?</h3>
    <div>
        <p>No; we cannot guarantee that multiple bricks purchased together will be placed next to one another.</p>
    </div>
    <h3>Can I replace a brick that is currently laid in Centennial Olympic Park?</h3>
    <div>
        <p>No; bricks currently in Centennial Olympic Park cannot be edited, amended, replaced or changed.</p>
    </div>
</div>
<p class="exhead">QUANTITY / LOCATION</p>
<div class="questions">
    <h3>How many bricks are available?</h3>
    <div>
        <p>A limited number of personalized engraved bricks are available and will be sold on a first come, first served basis.</p>
    </div>
    <h3>Can I choose where my brick is placed?</h3>
    <div>
        <p>No; all bricks will be installed at the discretion of The Georgia World Congress Center Authority within the Centennial Olympic Park.</p>
    </div>
    <h3>How will I know where my brick is installed?</h3>
    <div>
        <p>After the bricks have been installed, a locator map will be available in both Centennial Olympic Park&#146;s Visitor Center and online to help you find your brick.</p>
    </div>
    <h3>When and where will the bricks be installed?</h3>
    <div>
        <p>All bricks are scheduled to be installed in Spring 2017. The bricks purchased during this fundraiser will be installed along Andrew Young International Boulevard, raising the former road to pedestrian level.</p>
    </div>
    <h3>Can I have multiple bricks placed together?</h3>
    <div>
        <p>You can purchase multiple bricks at one time, however, we cannot guarantee that bricks purchased together will be placed next to one another.</p>
    </div>
	<h3>How long will my brick be at Centennial Olympic Park?</h3>
    <div>
        <p>Engraved bricks are not insured to last forever, but are constructed to withstand normal wear and tear. Centennial Olympic Park takes great pride in the upkeep of the facilities and takes every measure to limit damage, but is not able to replace engraved bricks due to damage.</p>
    </div>
</div>
<p class="exhead">ORDER / PAYMENT</p>
<div class="questions">
    <h3>What is the last date I can order?</h3>
    <div>
        <p>Bricks are available in limited quantities and will be sold on a first-come, first-served basis.</p>
    </div>
    <h3>Have you received my order?</h3>
    <div>
        <p>If you ordered online or over the phone, you will receive an automatic payment receipt, which will be followed by an email order confirmation within two to five business days of your order being processed.</p>
        <p>If you mailed your order, it usually takes five to seven business days to process once we receive it. An email order confirmation will follow if an email address was provided on your submitted form.</p>
        <p>Always check your spam or junk folder if this is your first time receiving anything from the Centennial Olympic Park Brick Program. You may also call to speak with a customer service representative during normal business hours at (844) 496-BRICK (844-496-2742).</p>
    </div>
    <h3>Can I purchase a gift certificate for myself or a friend?</h3>
    <div>
        <p>Yes; any brick option can be converted into a gift certificate, which does not require you to enter an inscription prior to checkout. They are the perfect gift! It&#146;s also a great way to secure your own placement while allowing additional time to personalize your own brick.</p>
        <p>Gift certificates will be sent within five to seven business days from receipt of your order. For any questions or concerns, please contact us at (844) 496-BRICK (844-496-2742).</p>
    </div>
    <h3>Is my brick tax deductible?</h3>
    <div>
        <p>Yes; a portion of your purchase is tax deductible to the extent allowed by law through the Geo. L. Smith II Georgia World Congress Center Authority.</p>
    </div>
    <h3>Is there sales tax?</h3>
    <div>
        <p>Yes; replicas and display cases will be charged 8% sales tax based on shipping to the State of Georgia.</p>
    </div>
    <h3>Can I pay by check?</h3>
    <div>
        <p>Yes; please make checks payable to: Centennial Olympic Park Brick Program</p>
    </div>
    <h3>Where do I mail my check?</h3>
    <div>
        <p>Mail payment to: Centennial Olympic Park Brick Program &#149; Fulfillment Center &#149; P.O. Box 9267 &#149; Boise, ID 83707</p>
    </div>
    <h3>Where will the proceeds from the bricks go?</h3>
    <div>
        <p>Proceeds from the brick program will benefit the efforts to re-envision Centennial Olympic Park in celebration of its 20th Anniversary. Proceeds will support five transformative projects that will improve the Park for visitors and residents alike.</p>
    </div>
</div>
<p class="exhead">KEEPSAKE REPLICAS & DISPLAY CASES</p>
<div class="questions">
    <h3>What is the replica made of?</h3>
    <div>
        <p>The replica is made of The same material as your installed brick with a commemorative plaque affixed to The top. Colors may vary slightly due to natural variances, and we cannot guarantee your replica will be an exact color match of The installed brick at The Centennial Olympic Park.</p>
    </div>
    <h3>Can a replica be shipped to a different address?</h3>
    <div>
        <p>Yes, with the exception of P.O. or APO boxes.</p>
    </div>
    <h3>How long does it take to get my replica?</h3>
    <div>
        <p>Replica bricks are expected to be engraved and shipped within 10-12 weeks from the time your order is processed and approved by The Georgia World Congress Center Authority.</p>
    </div>
    <h3>Can I pay to have my replica expedited?</h3>
    <div>
        <p>Yes; please call (844) 496-BRICK (844-496-2742) to speak with a customer service representative for shipping quotes.</p>
    </div>
    <h3>Where is my replica brick?</h3>
    <div>
        <p>If you placed your order more than 12 weeks ago and have not received an email shipment tracking notification, please call (844) 496-BRICK (844-496-2742) or email olympicpark@fundraisersltd.com.</p>
    </div>
</div>

<!-- End FAQ content -->