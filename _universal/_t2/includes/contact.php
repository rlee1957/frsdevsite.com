<?php

include("configuration/contact_settings.php");

?>
<div class="container first contact">
        <h1 class="text-center contact-title"><?php echo($lang["page"]["contact"]["title"]); ?></h1>
        <p class="text-center t-15 mb-15"><?php echo($lang["page"]["contact"]["phone-label"].$lang['published_telephone']); ?></p>
        <p class="text-center t-15"><?php echo($lang["page"]["contact"]["email-label"].$lang["contact-mailto"]); ?></p>
        <div class="row">
            <div class="col-sm-6 border-success text-center">
            	<br />
				<img src="images/<?php echo($image["contact-address"]); ?>" style="height: 65px;" />
				<br />
				<br />
                <h4 class="contact-label"><?php echo($lang["page"]["contact"]["address-label"]); ?></h4>
                <p class="t-15 mb-15"><?php echo($lang['published_campaign_name']); ?></p>
                <p class="t-15 mb-15"><?php echo($lang["page"]["contact"]["address"]); ?></p>
                <p class="t-15 mb-15"><?php echo($lang["page"]["contact"]["state-zip"]); ?></p>
            </div>
            <div class="col-sm-6 text-center">
            	<br />
            	<img src="images/<?php echo($image["contact-hours"]); ?>" style="height: 65px;" />
            	<br />
            	<br />
                <h4 class="contact-label"><?php echo($lang["page"]["contact"]["hours-label"]); ?></h4>
                <p class="t-15 mb-15"><?php echo($lang["page"]["contact"]["days-times"]); ?></p>
                <p class="t-15 mb-15"><?php echo($lang["page"]["contact"]["saturday"]); ?></p>
                <p class="t-15 mb-15"><?php echo($lang["page"]["contact"]["sunday"]); ?></p>
            </div>
        </div>
    </div>
<div style="height: 200px;">&nbsp;</div>