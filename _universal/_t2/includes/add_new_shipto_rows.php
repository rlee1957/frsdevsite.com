<?php

include($path."includes/order_functions.php");
$_SESSION["order"]["test"] .= "add_new_order.php
";
$sql = "
insert into orders (
	campaign_number,
	purchase_id,
	status,
	status_date,
	transaction_date,
	transaction_amount,
	tax_total,
	shipping_total,
	fees_total,
	customer_id,
	address_id,
	phone_id,
	email_id,
	non_profit,
	batch,
	credit_card_month,
	credit_card_year,
	credit_card_owner,
	credit_card_type,
	authorization_number,
	response_code,
	result_code,
	result_message,
	misc,
	marketing,
	promotion,
	extra,
	entered_by
	) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning order_id
";
$shipping_total = get_shipping_total();
$fees_total = get_fees_total($path);
$misc = get_misc();	
$marketing = get_marketing();
$promotion = get_promotion();
$extra = get_extra();
$params = array();
$params[count($params)] = $campaign_number;										// campaign number
$params[count($params)] = "purchase_id update";									// purchase ID
$params[count($params)] = "status update";										// order status
$params[count($params)] = "status_date update";									// date of order status
$params[count($params)] = $date_created;										// transaction date
$params[count($params)] = $_SESSION["order"]["info"]["amount"]["order_total"];	// transaction amount
$params[count($params)] = $_SESSION["order"]["info"]["amount"]["sales_tax"];	// tax total
$params[count($params)] = get_shipping_total();									// shipping total
$params[count($params)] = get_fees_total($path);								// fees total
$params[count($params)] = $_SESSION["order"]["info"]["customer_id"];			// customer ID
$params[count($params)] = $_SESSION["order"]["info"]["address_id"];			    // address ID
$params[count($params)] = $_SESSION["order"]["info"]["phone_id"];				// phone ID
$params[count($params)] = $_SESSION["order"]["info"]["email_id"];				// email ID
$params[count($params)] = NULL;													// is customer a non prophet organization?
$params[count($params)] = NULL;													// batch identifier
$params[count($params)] = $_SESSION["order"]["info"]["cc"]["cc_month"];			// credit card month
$params[count($params)] = $_SESSION["order"]["info"]["cc"]["cc_year"];			// credit card year
$params[count($params)] = $_SESSION["order"]["info"]["cc"]["cc_name"];			// credit card owner
$params[count($params)] = $_SESSION["order"]["info"]["cc"]["cc_type"];			// credit card type
$params[count($params)] = NULL;													// authorization number
$params[count($params)] = NULL;													// response code
$params[count($params)] = NULL;													// result code
$params[count($params)] = NULL;													// result message
$params[count($params)] = $misc;												// misc
$params[count($params)] = $marketing;											// marketing
$params[count($params)] = $promotion;											// promotion
$params[count($params)] = $extra;												// extra
$params[count($params)] = "Web Order";											// entered by
echo("The parameters<br />");
echo("<textarea style='width: 100%; height: 350px;>");
print_r($params);
echo("</textarea>");
$results = sql_shell($sql, $params, $path);
echo($sql."<br />");
echo("<textarea style='width: 100%; height: 350px;>");
print_r($results);
echo("</textarea>");
$order_id = -1;
if((isset($results["rowcount"]))&&($results["rowcount"] > 0))
	{
	$order_id = $results["recordset"][0]["order_id"];	
	}
$_SESSION["order"]["info"]["order_id"] = $order_id;
$_SESSION["order"]["info"]["purchase_id"] = get_purchase_id($path);
$_SESSION["order"]["info"]["status"] = "";
$_SESSION["order"]["info"]["status_date"] = "";
	
?>