<?php

include($path."configuration/send_mail_settings.php");
include($path."../_t2/includes/sendgrid.php");

if($mail_results["success"])
	{
	$retval = '
	<div class="alert alert-success">
	Order receipt sent to '.$email_to.'.  Please check your email inbox.
	</div>';	
	}
else
	{
	$retval = "<div class='alert alert-error'>Email was not sent (".$mail_results["message"].")!</div>";		
	}
	
?>