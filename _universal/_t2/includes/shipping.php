<?php
 
$cart["ship_quantity"] = 0;
$ship_count = 0;
$c = $_SESSION["order"]["cart"];
foreach($c as $key => $item)
	{
	$o = $item["options"];
	foreach($o as $oid => $oarr){ $ship_count += $oarr["quantity"]; }
	}
$ship_method = "";
$ship_bill = "";
$ship_other = "";
$ship_different = "";
$ship_method = $_SESSION["order"]["info"]["ship_method"];
if($ship_method == "bill"){ $ship_bill = " checked"; }
if($ship_method == "other"){ $ship_other = " checked"; }
if($ship_method == "different"){ $ship_different = " checked"; }
if($ship_count > 0)
	{
	// include($path."../_t2/includes/separator.php");
?>
<h3 class="t-20"><?php echo($lang["cart_billing_information"]); ?></h3>
<div class="form-group mb-40">
    <div class="checkbox-item">
        <input type=radio
               name=ship_method
               id=ship_to_bill
               onclick="save_shipto('bill');"
            <?php echo($ship_bill); ?>
               onfocus="save_billing(); save_credit_card();"
               value="bill"
               class="ship-option-rdo" />
        <label for=ship_to_bill class="ship-option-label">&nbsp;&nbsp;&nbsp;<?php echo($lang["shipping_to_billing"]); ?> </label>
    </div>
    <div class="checkbox-item">
        <input type=radio
               name=ship_method
               id=ship_to_other
               onclick="ship_other(this);"
            <?php echo($ship_other); ?>
               onfocus="save_billing(); save_credit_card();"
               value="other"
               class="ship-option-rdo" />
        <label for=ship_to_other class="ship-option-label">&nbsp;&nbsp;&nbsp;<?php echo($lang["shipping_to_other"]); ?> </label>
    </div>
    <?php if($ship_count > 1) { ?>
        <div class="checkbox-item">
            <input type=radio
                   name=ship_method
                   id=ship_to_different
                   onclick="ship_different(this);"
                <?php echo($ship_different); ?>
                   onfocus="save_billing(); save_credit_card();"
                   value="different"
                   class="ship-option-rdo" />
            <label for=ship_to_different class="ship-option-label">&nbsp;&nbsp;&nbsp;<?php echo($lang["shipping_to_different"]); ?> </label>
        </div>
    <?php } ?>

</div>
    <?php
    }
?>