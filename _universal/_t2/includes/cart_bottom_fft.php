<!--<h2 class=cart-title><?php echo($lang["total_summary"]); ?></h2>-->

<?php
include($path."configuration/calculate_promo_discount.php");
$subtotal = $_SESSION["order"]["info"]["amount"]["sub_total"];
$convenience_fee = get_convenience_fee($path);
$_SESSION["order"]["info"]["amount"]["convenience_fee"] = $convenience_fee;
$tax = 0.00;
$_SESSION["order"]["info"]["amount"]["sales_tax"] = $tax;
$order_total = $subtotal + $convenience_fee + $tax - $promo_discount;
$_SESSION["order"]["info"]["amount"]["order_total"] = $order_total;


if($subtotal != $order_total)
	{
        echo '<div class="total">'.$lang['cart_subtotal'].'<span class="wrap"><span class="pull-left">'.$lang["currency_symbol"].'</span>'.sprintf("%2.2f", $subtotal).'</span></div>';

	if($options["promo"])
		{
            echo '<div class="total">'.$lang['promotional_discount'].'<span class="wrap"><span class="pull-left">'.$lang["currency_symbol"].'</span>'.sprintf("%2.2f", $promo_discount).'</span></div>';
		}
	if($options["web_convenience_fee"])
		{
            echo '<div class="total">'.$lang['convenience_fee'].'<span class="wrap"><span class="pull-left">'.$lang["currency_symbol"].'</span>'.sprintf("%2.2f", $convenience_fee).'</span></div>';
		}
	if($options["apply_sales_tax"])
		{
            echo '<div class="total">'.$lang['sales_tax'].'<span class="wrap"><span class="pull-left">'.$lang["currency_symbol"].'</span>'.sprintf("%2.2f", $tax).'</span></div>';
		}
	}
echo '<strong class="subtotal">'.$lang['order_total'].'<span>'.$lang["currency_symbol"].'</span>'.sprintf("%2.2f", $order_total).'</strong>';


function get_sub_total()
{
	
$sub_total = 0;
foreach($_SESSION["order"]["cart"] as $key => $val)
	{
	$sub_total += $val["price"];
	foreach($val["options"] as $oid => $oarr)
		{
		$quantity = $oarr["quantity"];
		$price = $oarr["price"];
		$total = $quantity * $price;
		$sub_total += $total;	
		}	
	}
return $sub_total;
	
}

function get_convenience_fee($path)
{

$convenience_fee = 0;
include($path."configuration/fee_settings.php");
$convenience_fee = $_SESSION["convenience_fee"];
return $convenience_fee;
	
}

?>