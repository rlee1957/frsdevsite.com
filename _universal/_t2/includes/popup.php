<div id="popup" class="popup_bg lvl-5" style="display: none;"></div>
<div id="popup_content" class="generic_container centered lvl-9 msg" style="display: none;">
	<div id="popup_button_container" class=popup-button-container style="display: block;">
		<img id="popup_close" src="images/delete.png" onclick="hide_popup()" alt=exit class="popup-close lvl-top no-select" />
	</div>
	<div id=popup_head_spacer class=popup-head-spacer style="display: none;">&nbsp;<br /></div>
	<div id=popup_label class=popup-label style="display: none"></div>
		<div class=popup-window>
		<div id="popup_image_row" style="display: none;">
			<div class=popup-table-spacer></div>
			<img id=popup_image src="images/2_computers.gif" />
		</div>
		<div id=popup_table_instruction class=popup-table-instruction>
			<input type=hidden id=popup_next />
			<div class=popup-table-spacer></div>
			<span id=popup_instruction class=popup-instruction><br /><br /><br /></span>
		</div>
	</div>
	<div class=popup-table-spacer></div>
	<div class=popup-table-spacer></div>
	<div class=popup-table-spacer></div>
</div>

<table id="ays" class="centered msg lvl-9" style="display: none; font-size: 14pt;" cellspacing=25 cellpadding=20 border=0>
	<tr> <!-- space -->
		<td>
			<div class=popup-button-container style="display: block;">
				<img src="images/delete.png" onclick="hide_ays();" alt=exit class="popup-close no-select lvl-top" />
			</div>
		</td>
	</tr>
	<tr> <!-- Popup Label -->
		<td align=center valign=middle>
			<span id=ays_label 
				  style="font-size: 20pt; font-weight: bold; color: #606060;">
				<?php echo($lang["page"]["cart_details"]["remove-popup"]["head"]); ?>
			</span>
		</td>
	</tr>
	<tr> <!-- space -->
		<td>&nbsp;<br />&nbsp;</td>
	</tr>
	<tr> <!-- Popup Instruction -->
		<td align=center valign=middle>
			<span id=ays_message>
				<?php echo($lang["page"]["cart_details"]["remove-popup"]["msg"]); ?>
			</span>
		</td>
	</tr>
	<tr> <!-- space -->
		<td>&nbsp;<br />&nbsp;</td>
	</tr>
	<tr> <!-- Popup Close -->
		<td align=right valign=bottom>
			<a id=ays_remove href="javascript: remove_item();" class=add-to-cart>
				<b><?php echo($lang["page"]["cart_details"]["remove-popup"]["remove"]); ?></b>
			</a>
		</td>
	</tr>
	<tr> <!-- space -->
		<td>
			&nbsp;
			<input type=hidden id=ays_next />
		</td>
	</tr>
</table>

<table id="ays_fft" class="centered msg lvl-9" style="display: none; font-size: 14pt;" cellspacing=25 cellpadding=20 border=0>
	<tr> <!-- space -->
		<td>
			<div class=popup-button-container style="display: block;">
				<img src="images/delete.png" onclick="hide_ays_fft();" alt=exit class="popup-close no-select lvl-top" />
			</div>
		</td>
	</tr>
	<tr> <!-- Popup Label -->
		<td align=center valign=middle>
			<span id=ays_label_fft 
				  style="font-size: 20pt; font-weight: bold; color: #606060;">
				<?php echo($lang["page"]["cart_details"]["remove-popup"]["head"]); ?>
			</span>
		</td>
	</tr>
	<tr> <!-- space -->
		<td>&nbsp;<br />&nbsp;</td>
	</tr>
	<tr> <!-- Popup Instruction -->
		<td align=center valign=middle>
			<span id=ays_message_fft>
				<?php echo($lang["page"]["cart_details"]["remove-popup"]["msg"]); ?>
			</span>
		</td>
	</tr>
	<tr> <!-- space -->
		<td>&nbsp;<br />&nbsp;</td>
	</tr>
	<tr> <!-- Popup Close -->
		<td align=right valign=bottom>
			<a id=ays_remove_fft href="javascript: remove_item_fft();" class=add-to-cart>
				<b><?php echo($lang["page"]["cart_details"]["remove-popup"]["remove"]); ?></b>
			</a>
		</td>
	</tr>
	<tr> <!-- space -->
		<td>
			&nbsp;
			<input type=hidden id=ays_next_fft />
		</td>
	</tr>
</table>

