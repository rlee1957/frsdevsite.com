<?php

if($options["promo"])
	{
	include($path."configuration/calculate_promo_discount.php");
	$promo_code = "";
	if($_SESSION["order"]["info"]["promotion_code"] !== "")
		{
		$promo_code = $_SESSION["order"]["info"]["promotion_code"];	
		}
?>

        <div class="promo form-group">
            <label for="code"><?php echo($lang["page"]["cart_details"]["promo"]["title"]); ?></label>
           <input type=text
                   id=promotional_code_id
                   name=promotional_code_id
                   class="form-control"
                   placeholder="<?php echo($lang["page"]["cart_details"]["promo"]["placeholder"]); ?>"
                   value="<?php echo($promo_code); ?>" />
            <input type=hidden id=promotion_amount />
            <a class="btn btn-primary btn-sm" href="javascript: promo_code_test('<?php echo($page); ?>');"><?php echo($lang["page"]["cart_details"]["promo"]["button"]); ?></a>
        </div>


<?php
	}
	
?>
