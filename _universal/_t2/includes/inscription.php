			<tr>
				<td>
<?php
if($dev_environment)
	{
?>
<h3>inscription.php</h3>
<?php
	}
?>
					<div style="text-align: left;"><?php echo($lang['cart_inscription_guide_title']); ?></div>
<?php
$characters_allowed = $cart["max_characters"];
$lines_allowed = $cart["max_lines"];
if(count($cart["inscription"]) == $lines_allowed)
	{
	$inscription = $cart["inscription"];
	}
else
	{
	$inscription = array();
	for($ll=1;$ll<=$lines_allowed;$ll++)  
		{
		$inscription[$ll] = "";
		}		
	}
?>

					<center>
						<table cellspacing=0 cellpadding=0 border=0>
							<tr>
								<td>&nbsp;</td>
								<td align=center>
									<strong><?php echo($characters_allowed.$lang["allowed_chrs_line"]); ?></strong>
								</td>
								<td>&nbsp;</td>
							</tr>
<?php
			  
for($ll=1;$ll<=$lines_allowed;$ll++)  
	{
	$line = "line".$ll;
	$r_line = htmlspecialchars(trim($inscription[$ll]));	
	echo '
							<tr>
								<td>
									<strong style="font-size:20px;">'.$lang['cart_inscription_line'].' '.$ll.':&nbsp;</strong>
								</td>
								<td>
									<input id="'.$line.'" style="font-size: 19px; text-transform:uppercase;" class="large" name="'.$line.'" maxlength="'.$characters_allowed.'" value="'.$r_line.'"
									onblur="sanitize_sql(this);"
									onchange="sanitize_sql(this);" />
								</td>
';
	include($path."configuration/option_settings.php");
	if($options["greek_lettering"])
		{
		echo'
								<td>
									<script type="text/javascript">
									 $(function () {$(\'#'.$line.'\').keypad({showOn: \'button\',
									 buttonImageOnly: true, keypadOnly: false, buttonImage: \'img/omega.png\',separator: \'|\', layout: [\'Α|Β|Γ|Δ|Ε\',
									 \'Ζ|Η|Θ|Ι|Κ\', \'Λ|Μ|Ν|Ξ|Ο\', \'Π|Ρ|Σ|Τ|Υ\', \'Φ|Χ|Ψ|Ω\',
									 $.keypad.CLOSE +\'|\'+ $.keypad.CLEAR], 											prompt: \'Add these Greek letters\'});
									 });
									 </script>&nbsp;
								</td>
';
		}
	echo'
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td align=center><div id="'.$ll.'_num"></div></td>
								<td>&nbsp;</td>
							</tr>
';
	}
echo '
						</table>
					</center>
					<i style="text-align: left;">'.$lang['cart_inscription_footer'].'</i>
';
// Javascript
echo '
<script type="text/javascript">       
$(document).ready(function(){';
for($ll=1;$ll<= $lines_allowed;$ll++)  
	{
	$line = "line".$ll;
	echo '
		$("#'.$line.'").keyup(function () {
			var ta = '.$characters_allowed.';
			$("#'.$ll.'_num").html(\''.$lang["spaces_used"].' (\' + $(\'#'.$line.'\').val().length + \') '.$lang["spaces_remaining"].' (\' + (ta - $(\'#'.$line.'\').val().length) +\')\');
		});
		
		$("#'.$line.'").change(function () {
    		var ta = '.$characters_allowed.';
    		$("#'.$ll.'_num").html(\''.$lang["spaces_used"].' (\' + $(\'#'.$line.'\').val().length + \') '.$lang["spaces_remaining"].' (\' + (ta - $(\'#'.$line.'\').val().length) +\')\');
		});
	';
	}
echo '
});
</script>';

?>
				</td>
			</tr>
<hr />