			<tr>
				<td>
<?php
if($show_page_name)
	{
	echo("<h3>iyb_inscription.php</h3>");
	}
$characters_allowed = $cart["max_characters"];
$lines_allowed = $cart["max_lines"];
if(count($cart["inscription"]) == $lines_allowed)
	{
	$inscription = $cart["inscription"];
	}
else
	{
	$inscription = array();
	for($ll=1;$ll<=$lines_allowed;$ll++)  
		{
		$inscription[$ll] = "";
		}		
	}
?>
					<center>
						<table cellspacing=0 cellpadding=0 border=0>
							<!--<tr>
								<td>&nbsp;</td>
								<td align=center>
									<strong><?php echo($characters_allowed.$lang["allowed_chrs_line"]); ?></strong>
								</td>
								<td>&nbsp;</td>
							</tr>-->
	<?php
include($path."configuration/option_settings.php");
for($x=1;$x<$paver["max_lines"]+1;$x++)
	{
	$line = "";
	if(isset($paver["inscription"][$x])){ $line = $paver["inscription"][$x]; }
?>						
							<tr>
								<td>
									<strong style="font-size:20px;"><?php echo($lang['cart_inscription_line'].' '.$x); ?>:&nbsp;</strong>
								</td>
								<td>
									<input class="input-medium form-control" 
										   onkeyup="enter_text(<?php echo($x); ?>, 
															   <?php echo($paver["max_characters"]); ?>, 
															   '<?php echo($lang["page"]["iyb"]["spaces-used"]); ?>', 
															   '<?php echo($lang["page"]["iyb"]["remaining"]); ?>', 
															   '<?php echo($lang["page"]["iyb"]["chars-finish"]); ?>',
															   true,
															   <?php echo($paver["max_lines"]); ?>);
															   sanitize(this);"
										   onchange="enter_text(<?php echo($x); ?>, 
															    <?php echo($paver["max_characters"]); ?>, 
																'<?php echo($lang["page"]["iyb"]["spaces-used"]); ?>', 
																'<?php echo($lang["page"]["iyb"]["remaining"]); ?>', 
																'<?php echo($lang["page"]["iyb"]["chars-finish"]); ?>',
																true,
																<?php echo($paver["max_lines"]); ?>); 
																sanitize(this);"
										   height="16" 
										   type="text" 
										   name="line<?php echo($x); ?>" 
										   id="line<?php echo($x); ?>" 
										   value="<?php echo($line); ?>" 
										   size="16" 
										   maxlength="<?php echo($paver["max_characters"]); ?>"
										   onblur="enter_text(<?php echo($x); ?>, 
															   <?php echo($paver["max_characters"]); ?>, 
															   '<?php echo($lang["page"]["iyb"]["spaces-used"]); ?>', 
															   '<?php echo($lang["page"]["iyb"]["remaining"]); ?>', 
															   '<?php echo($lang["page"]["iyb"]["chars-finish"]); ?>',
															   true,
															   <?php echo($paver["max_lines"]); ?>);
															   sanitize(this);"
										   style="text-transform:uppercase;"/>							
								</td>
<?php
	if($options["greek_lettering"])
		{
?>
								<td>
									<script type="text/javascript">
									$(function () {$(\'#'.$line.'\').keypad({showOn: \'button\',
									buttonImageOnly: true, keypadOnly: false, buttonImage: \'img/omega.png\',separator: \'|\', layout: [\'Α|Β|Γ|Δ|Ε\',
									\'Ζ|Η|Θ|Ι|Κ\', \'Λ|Μ|Ν|Ξ|Ο\', \'Π|Ρ|Σ|Τ|Υ\', \'Φ|Χ|Ψ|Ω\',
									$.keypad.CLOSE +\'|\'+ $.keypad.CLEAR], 											prompt: \'Add these Greek letters\'});
									});
									</script>&nbsp;
								</td>
<?php	
		}
	else
		{
?>
								<td></td>
<?php
		}
?>
							</tr>
							<tr>
								<td colspan=3><div id="<?php echo($x); ?>_num" style="margin-top:5px; display: none;" >&nbsp;</div></td>
							</tr>
								
<?php
	}
?>	
						</table>
						<input type="hidden" name="brick_type" value="<?php echo($paver["description"]); ?>" />
						<input type="hidden" name="remote" value="true" />
						<input type="hidden" name="bktype" value="<?php echo($paver["item_number"]); ?>" />
						<input type="hidden" name="optype" value="<?php echo($paver["item_type"]); ?>" />             
					</center>
<?php
echo '
<script type="text/javascript">       
$(document).ready(function(){';
for($ll=1;$ll<=$paver["max_lines"];$ll++)  
	{
	$line = "line".$ll;
	echo '
		$("#'.$line.'").keyup(function () {
			var ta = '.$paver["max_characters"].';
			$("#'.$ll.'_num").html(\''.$lang["spaces-used"].' (\' + $(\'#'.$line.'\').val().length + \') '.$lang["spaces-remaining"].' (\' + (ta - $(\'#'.$line.'\').val().length) +\')\');
		});
		
		$("#'.$line.'").change(function () {
    		var ta = '.$paver["max_characters"].';
    		$("#'.$ll.'_num").html(\''.$lang["spaces-used"].' (\' + $(\'#'.$line.'\').val().length + \') '.$lang["spaces-remaining"].' (\' + (ta - $(\'#'.$line.'\').val().length) +\')\');
		});
	';
	}
echo '
});
</script>';

?>
					
				</td>
			</tr>
			<tr>
				<td>
					<div style="text-align: left; width: 800px;">
						<?php echo($lang['cart_inscription_guide_title']); ?>
					</div>
				</td>
			</tr>
		<table>
	</div>
</center>
<center>
	<div style='width: 800px; border-width: 2px;'>
		<table>