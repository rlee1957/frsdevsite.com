<!--<h2 class=cart-title><?php echo($lang["total_summary"]); ?></h2>-->
<div style="text-align: right;">
<?php
include($path."configuration/calculate_promo_discount.php");
$subtotal = get_sub_total();
$_SESSION["order"]["info"]["amount"]["sub_total"] = $subtotal;
$convenience_fee = get_convenience_fee($path);
$_SESSION["order"]["info"]["amount"]["convenience_fee"] = $convenience_fee;
$tax = 0.00;
$_SESSION["order"]["info"]["amount"]["sales_tax"] = $tax;
$order_total = $subtotal + $convenience_fee + $tax - $promo_discount;
$_SESSION["order"]["info"]["amount"]["order_total"] = $order_total;
echo("
<table class=checkout-base cellpadding=0 cellspacing=0>
	<tr>
		<td style='width: 1000px'>&nbsp;</td>
		<td class=cost-label align=right>".$lang['cart_subtotal']."&nbsp;&nbsp;&nbsp;</td>
		<td class=cost-currency>".$lang["currency_symbol"]."</td>
		<td class=cost-price align=right>");
printf("%2.2f", $subtotal);
echo("</td>
	</tr>
");
if($options["promo"])
	{
	echo("
	<tr>
		<td style='width: 1000px'>&nbsp;</td>
		<td class=cost-label align=right>".$lang["promotional_discount"]."&nbsp;&nbsp;&nbsp;</td>
		<td class=cost-currency>".$lang["currency_symbol"]."</td>
		<td class=cost-price align=right>");
printf("%2.2f", $promo_discount);
echo("</td>
	</tr>
");
	}
if($options["web_convenience_fee"])
	{
	echo("
	<tr>
		<td style='width: 1000px'>&nbsp;</td>
		<td class=cost-label align=right>".$lang["convenience_fee"]."&nbsp;&nbsp;&nbsp;</td>
		<td class=cost-currency>".$lang["currency_symbol"]."</td>
		<td class=cost-price align=right>");
printf("%2.2f", $convenience_fee);
echo("</td>
	</tr>
");
	}
if($options["apply_sales_tax"])
	{
	echo("
	<tr>
		<td style='width: 1000px'>&nbsp;</td>
		<td class=cost-label align=right>".$lang["sales_tax"]."&nbsp;&nbsp;&nbsp;</td>
		<td class=cost-currency>".$lang["currency_symbol"]."</td>
		<td class=cost-price align=right>");
printf("%2.2f", $tax);
echo("</td>
	</tr>
");
	}
echo("
	<tr>
		<td style='width: 1000px'>&nbsp;</td>
		<td class=cost-label align=right><b>".$lang["order_total"]."</b>&nbsp;&nbsp;&nbsp;</td>
		<td class=cost-currency><b>".$lang["currency_symbol"]."</b></td>
		<td class=cost-price><b>");
printf("%2.2f", $order_total);
echo("</b></td>
	</tr>
</table>
</div>
");

function get_sub_total()
{
	
$sub_total = 0;
foreach($_SESSION["order"]["cart"] as $key => $val)
	{
	$sub_total += $val["price"];
	foreach($val["options"] as $oid => $oarr)
		{
		$quantity = $oarr["quantity"];
		$price = $oarr["price"];
		$total = $quantity * $price;
		$sub_total += $total;	
		}	
	}
return $sub_total;
	
}

function get_convenience_fee($path)
{

$convenience_fee = 0;
include($path."configuration/fee_settings.php");
$convenience_fee = $_SESSION["convenience_fee"];
return $convenience_fee;
	
}

?>