<?php
 
$cart["ship_quantity"] = 0;
$ship_count = 0;
$c = $_SESSION["order"]["cart"];
foreach($c as $key => $item){ $ship_count += $item["quantity"]; }
$ship_method = "";
$ship_bill = "";
$ship_other = "";
$ship_different = "";
$ship_method = $_SESSION["order"]["info"]["ship_method"];
if($ship_method == "bill"){ $ship_bill = " checked"; }
if($ship_method == "other"){ $ship_other = " checked"; }
if($ship_method == "different"){ $ship_different = " checked"; }
if($ship_count > 0)
	{
?>
        <h3 class="cart-title"><?php echo($lang["shipping_information"]); ?></h3>
        <div class="form-group mb-40">

            <div class="checkbox-item">
                <input type=radio
                       name=ship_method
                       id=ship_to_bill
                       onclick="save_shipto_fft('bill');"
                    <?php echo($ship_bill); ?>
                       onfocus="save_billing(); save_credit_card();"
                       value="bill"
                       class="check-style" />
                <label for=ship_to_bill ><?php echo($lang["shipping_to_billing"]); ?></label>
            </div>
            <div class="checkbox-item">
                <input type=radio
                       name=ship_method
                       id=ship_to_other
                       onclick="ship_other_fft(this);"
                    <?php echo($ship_other); ?>
                       onfocus="save_billing(); save_credit_card();"
                       value="other"
                       class="check-style" />
                <label for=ship_to_other ><?php echo($lang["shipping_to_other"]); ?> </label>
            </div>
            <?php
            if($ship_count > 1)
            {
                ?>
            <div class="checkbox-item">

                <td align=left valign=top>
                    <input type=radio
                           name=ship_method
                           id=ship_to_different
                           onclick="ship_different_fft(this);"
                        <?php echo($ship_different); ?>
                           onfocus="save_billing(); save_credit_card();"
                           value="different"
                           class="check-style" />
                </td>
                <label for=ship_to_different ><?php echo($lang["shipping_to_different"]); ?> </label>
            </div>
            <?php } ?>
<?php
	}
?>


            </div>