<?php
include($path."configuration/iyb_settings.php");
$ar = $iyb[$paver["item_number"]];
?>
			<tr>
				<td align=center>
					<table cellpadding=0 cellspacing=0 border=0>
						<tr>
							<td valign=bottom 
								align=center 
								style='width: 400px;'>
								<style type="text/css">				
								.imagePaver 
								{
								padding-top: 0px;
								width: <?php echo($ar["p-width"]); ?>px;
								height: <?php echo($ar["p-height"]); ?>px;
								z-index:100;
								background-repeat: no-repeat;
								font-weight: bold;
								font-size:<?php echo($ar["p-font-size"]); ?>px;
								color:#000000;
								font-family:Arial;
								position:relative;
								vertical-align: middle;
								}
								
								.imageText
								{
								position:absolute;
								display: table-cell;
								margin-top: <?php echo($ar["margin-top"]); ?>px;
								vertical-align: middle;
								margin-left: <?php echo($ar["margin-left"]); ?>px;
								font-family: '<?php echo($ar["font"]); ?>', Arial, sans-serif;
								}
								
								input[type="text"] 
								{
								height:1.2em;
								padding:2px;
								border:1px solid #ddd;
								font:1em/1.2 "Helvetica neue", Arial, sans-serif;
								font-size:18px;
								}
								</style>
								<div style="width: <?php echo($ar["p-width"]); ?>px; position: relative; left: 0; top: 0; margin-top: 0px;">
									<img class='pull-left imgBase' 
										 src='images/<?php echo($img); ?>' 
										 style='width: <?php echo($ar['p-width']); ?>' />
									<div class='imageText' 
										 style='width: <?php echo($ar['p-width']); ?>px; margin-top: <?php echo($ar['p-start-top']); ?>px;'>
	<?php
	for($x=1;$x<$paver["max_lines"]+1;$x++)
		{
		$line = "";
		if(isset($paver["inscription"][$x])){ $line = $paver["inscription"][$x]; }
	?>
										<div style="margin-top: <?php echo($ar["p-spacing"]); ?>px; 
													margin-bottom: 0px;
													margin-left: 0px; 
													width: <?php echo($ar["p-width"]); ?>px; 
													font-size: <?php echo($ar["p-font-size"]); ?>pt; 
													color: <?php echo($ar["font-color"]); ?>;
													text-transform:uppercase;" 
											 id="<?php echo($x); ?>line" 
											 class="sifr">
											<?php echo($line); ?>
										</div>
	<?php
		}
	?>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</center>
<hr class="hr-color" />
<center>
	<div style='width: 800px; border-width: 2px;'>
		<table>
			<tr>
				<td>
					<table cellpadding=0 cellspacing=0 border=0>
						<tr style='height: 30px;'>
							<td valign=middle align=center style='height: 30px;'>
								<span class="brick-description"><?php echo($paver["description"]); ?></span>
							</td>
						</tr>
					</table>
				</td>
			</tr>