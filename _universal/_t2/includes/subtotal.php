<?php

$sub_total = 0;
foreach($_SESSION["order"]["cart"] as $key => $val)
	{
	$sub_total += $val["price"];
	foreach($val["options"] as $oid => $oarr)
		{
		$quantity = $oarr["quantity"];
		$price = $oarr["price"];
		$total = $quantity * $price;
		$sub_total += $total;	
		}	
	}

?>
<div style="text-align: right;" class=general-text>
<?php echo($lang['cart_subtotal']); ?>&nbsp;&nbsp;<?php echo($lang["currency_symbol"]); ?>&nbsp;<?php printf("%2.2f", $sub_total); ?>
<br />
<span style="font-size: smaller;"><?php echo($lang["shipping_and_tax"]); ?></span
<br />
</div>