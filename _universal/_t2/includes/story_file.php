<?php


$campaign_name = $lang['published_campaign_name_short'];
$order_id = time();
if(isset($_SESSION["go_orderid"])){ $order_id = $_SESSION["go_orderid"]; }
$t_path = $path;
if((isset($parent))&&($parent == "home")){ $t_path = ""; }
?>
		<tr>
			<td align=left colspan=2>
				<br />
				<div class="control-group">
					<div class="controls">
						<form action="<?php echo($t_path); ?>actions/upload_file.php" 
							  method="post" 
							  enctype="multipart/form-data" 
							  target="work">
							<input type="file" 
								   name="upload_file" 
								   id="upload_file" 
								   accept=".gif, .jpg, .jpeg, .png"
								   onchange="inspect_file(this)"
								   style="position: absolute; left: -1000; top: -1000; height: 0px; width: 0px;" />
							<div style="float: left;">
								<input type="button" class="story_button image_btn" 
									   value="<?php echo($lang["story_photo_select"]); ?>"
									   onclick="document.getElementById('upload_file').click();"
									   id="btn_select_image" />
								&nbsp;&nbsp;
							
							</div>
							<div style="float: left;">
							
								<input type="submit" 
									   value="<?php echo($lang["story_photo_upload"]); ?>" 
									   name="submit" 
									   id="image_submit_button" 
									   style="display: none;";
									   class="story_button image_btn"/>
								<input type=button 
									   class="story_button submit_btn" 
									   value="<?php echo($lang["story_submit_story"]); ?>"
									   onclick="submit_story('<?php echo($t_path); ?>');" />
							
							</div>
							<div style="clear: both;"></div>
							<input type=hidden id="suffix" name="suffix" value="" />
							<input type=hidden id="campaign_name" name="campaign_name" value="<?php echo($campaign_name); ?>" />
							<input type=hidden id="order_id" name="order_id" value="<?php echo($order_id); ?>" />
						</form>
					</div>
				</div>
			</td>
		</tr>