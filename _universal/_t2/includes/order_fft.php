<?php  

$description = $item["description"];
$image_file = $item["item_image_default"];
$show_text = false;
include($path."configuration/image_settings.php");
if($item["gift_certificate"] == "yes")
	{ 
	$image_file = $image['gift_certificate']; 
	}
$price = $item["price"];
$qty = $item["quantity"];
$remove = "";
if($page == "cart")
	{
	$remove = "<img src='".$image['remove-paver']."' alt='Remove' onclick='ays_fft(".$key.");' style='width: 20px; cursor: pointer;' class='remove-ic' />";
	}
$image_code = "<img src='images/".$image_file."' style='width: 125px;' />";


$description_code = "
			<strong>
				".$description."
			</strong>
";
if($page == "cart")
	{
	$description_code = "
			<strong>
				".$description."
			</strong>
				".$lang["cart_price_each"]."&nbsp;&nbsp;".$lang["currency_symbol"]."&nbsp;".number_format($price, 2, '.', ',')."

";
	}
$subtotal = $price * $qty;
$align = " align=left";
if($page == "cart"){ $align = "center"; }
$class = "cart-table-item-separater";
if(!$head){ include($path."../_t2/includes/item_separator.php"); }
$head = false;
$control_name = $page."_option_".$item["item_id"];
$refresh = "show_cart_detail_fft();";
$control_pm_height = 25;
$control_pm_width = 25;
$control_plus_top = 0;
$control_minus_top = 0;
$control_value_left = $control_pm_width + 1;
$control_value_top= 0;
$control_value_height = 25;
$control_value_width = 32;
$control_height	= 35;
$control_width = ((($control_pm_width * 2) + $control_value_width) + 7);
$quantity = $qty;
$quantities = $del.$qty;
$del = "|";
# Return value is stored in $quantity_control variable
include($path."../_t2/elements/quantity_control_fft.php");
$qc = $qty;
if($page == "cart"){ $qc = $quantity_control; }
?>

<div class="tb">
    <div class="row">
        <div class="col-sm-3">
            <?php echo($image_code); ?>

        </div>
        <div class="col-sm-9">
            <div class="row tr">
                <div class="col-xs-5 col-md-6">
                    <?php
                        echo($description_code);
                    ?>
                </div>
                <div class="col-xs-3 text-center">
                    <div class="wrap-count">
                        <?php
                        echo($qc);
                        ?>


                    </div>
                </div>
                <?php if($page == "cart"){ ?><div class="col-xs-1"><?php echo($remove);?></div><?php } ?>
                <div class="col-xs-3  <?php if($page == "cart"){ ?>col-md-2<?php } ?> text-right">
                    <span class="pull-left"><?php echo($lang["currency_symbol"]); ?></span>
                    <?php echo(number_format($subtotal, 2, '.', ',')); ?>
                </div>

            </div>


        </div>
    </div>
</div>


