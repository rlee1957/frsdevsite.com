<?php
 
# default values (EMPTY)
$fn = "";
$mn = "";
$ln = "";;
$country = $default["country"];
$company = "";;
$address1 = "";
$address2 = "";
$city = "";
$state = "";
$zip = "";
$phone = "";
$email = "";
$validated_javascript = "";
$validated_fedex = "";
$validated_overall = "";
# prefill values (EXISTING)
if((isset($_SESSION["order"]))&&(isset($_SESSION["order"]["addresses"]))&&(isset($_SESSION["order"]["addresses"][0])))
	{
	$adr = $_SESSION["order"]["addresses"][0];
	$fn = $adr["fn"];
	$mn = $adr["mn"];
	$ln = $adr["ln"];
	$country = $adr["country"];
	$company = $adr["company"];
	$address1 = $adr["address1"];
	$address2 = $adr["address2"];
	$city = $adr["city"];
	$state = $adr["state_prov"];
	$zip = $adr["postal_code"];
	$phone = $adr["phone"];
	$email = $adr["email"];
	$validated_javascript = $adr["validated_javascript"];
	$validated_fedex = $adr["validated_fedex"];
	$validated_overall = $adr["validated_overall"];
	}

?>