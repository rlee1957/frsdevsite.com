<?php

$cc_owner = "";
$cc_type = "";
$cc_number = "";
$cc_month = "";
$cc_year = "";
if(isset($_SESSION["order"]["info"]["cc"]["cc_name"])){ $cc_owner = $_SESSION["order"]["info"]["cc"]["cc_name"]; }
if(isset($_SESSION["order"]["info"]["cc"]["cc_type"])){ $cc_type = $_SESSION["order"]["info"]["cc"]["cc_type"]; }
if(isset($_SESSION["order"]["info"]["cc"]["cc_number"])){ $cc_number = $_SESSION["order"]["info"]["cc"]["cc_number"]; }
if(isset($_SESSION["order"]["info"]["cc"]["cc_month"])){ $cc_month = $_SESSION["order"]["info"]["cc"]["cc_month"]; }
if(isset($_SESSION["order"]["info"]["cc"]["cc_year"])){ $cc_year = $_SESSION["order"]["info"]["cc"]["cc_year"]; }
 	
?>
<h3 class="cart-title"><?php echo($lang['cart_credit_card_information']); ?></h3>
<div class="form-group mb-40">
<?php
include($path."../_t2/elements/credit_card_input.php");

function get_month($idx)
{

$value = "";	
switch($idx)	
	{
	case 1:
		{
		$value = "(01) January";
		break;		
		}
	case 2:
		{
		$value = "(02) February";
		break;		
		}
	case 3:
		{
		$value = "(03) March";
		break;		
		}
	case 4:
		{
		$value = "(04) April";
		break;		
		}
	case 5:
		{
		$value = "(05) May";
		break;		
		}
	case 6:
		{
		$value = "(06) June";
		break;		
		}
	case 7:
		{
		$value = "(07) July";
		break;		
		}
	case 8:
		{
		$value = "(08) August";
		break;		
		}
	case 9:
		{
		$value = "(09) September";
		break;		
		}
	case 10:
		{
		$value = "(10) October";
		break;		
		}
	case 11:
		{
		$value = "(11) November";
		break;		
		}
	case 12:
		{
		$value = "(12) December";
		break;		
		}
	}
return $value;
	
}

?>
</div>