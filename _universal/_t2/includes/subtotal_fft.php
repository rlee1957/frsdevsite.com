<?php

$sub_total = 0;
foreach($_SESSION["order"]["cart"] as $key => $val)
	{
	$sub_total += ($val["price"] * $val["quantity"]);
	}
$_SESSION["order"]["info"]["amount"]["sub_total"] = $sub_total;
?>


<div class="text-right">
    <?php include($path."../_t2/includes/promotion_code.php"); ?>

    <strong class="t-22 subtotal"><?php echo($lang['cart_subtotal']); ?>: <?php echo($lang["currency_symbol"]); ?><?php printf("%2.2f", $sub_total); ?></strong>
    <p class="note"><?php echo($lang["shipping_and_tax"]); ?></p>
</div>


