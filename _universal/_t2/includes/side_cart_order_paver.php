<?php  

$option_count = count($item["options"]);
$rowspan = ($option_count * 2) + 1;
$description = $item["description"];
$image = $item["item_image_blank"];
$edit = "
			<br />
			<a href=\"javascript: edit_inscription('".$key."', '".$refresh."');\">".$lang['edit_inscription']."</a>
";
if($item["gift_certificate"] == "yes")
	{ 
	$image = $item["item_image_default"]; 
	$edit = "";
	$rowspan++;
	}
$price = $item["price"];
$remove = "<button onclick='remove_item(\"".$key."\", \"".$refresh."\");' class=increment> Remove </button>";
$subtotal = $price;
$align = " align=left";
if($page == "cart"){ $align = "center"; }
$class = "cart-table-item-separater";
if(!$head){ include($path."../_t2/includes/item_separator.php"); }
$head = false;
?>
	<tr>
		<td align=center valign=middle>
			<input type=hidden id=ok />
			<span class="cart-description"><b><?php echo("&nbsp;&nbsp;".$description."&nbsp;&nbsp;"); ?></b></span><br />
			<span class="cart-price">
				<?php 
				echo($lang["cart_price_each"]);
				echo($lang["currency_symbol"]); 
				printf("%2.2f", $price); 
				?>
			</span>
		</td>
		<td align=center valign=middle>1</td>
		<td>&nbsp;</td>
		<td align=right valign=middle><?php echo($lang["currency_symbol"]); printf("%2.2f", $price); ?></td>
		<td align=center valign=middle rowspan=<?php echo($rowspan); ?>>
			<?php include($path."../_t2/includes/paver_inscription.php"); ?>
		</td>
	</tr>