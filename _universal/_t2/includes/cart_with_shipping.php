<?php

$cart_ship = array();
foreach($_SESSION["order"]["cart"] as $iid => $paver )
	{
	$ip = array();
	$ip["item_number"] = $paver["item_number"];
	$ip["description"] = $paver["description"];
	$ip["quantity"] = 1;
	$ip["inscription"] = get_inscription($paver);
	$ip["image"] = get_image($paver, $path);
	$ip["options"] = get_options($paver);
	$ip["row_count"] = get_row_count($ip["options"]) + 1;
	$ip["iyb"] = is_iyb($paver);
	$cart_ship[count($cart_ship)] = $ip;
	}
$htm .= "
<table cellspacing=0 cellpadding=7 class=conf-table>
	<tr>
		<td class=conf-table-product-head>
			".$lang["page"]["email"]["product-label"]."	
		</td>
		<td class=conf-table-quantity-head align=center>
			".$lang["page"]["email"]["quantity-label"]."
		</td>
		<td class=conf-table-description-head>
			".$lang["page"]["email"]["description-label"]."	
		</td>
		<td class=conf-table-address-head>
			".$lang["page"]["email"]["address-label"]."	
		</td>
	</tr>
";
$begin = true;
foreach($cart_ship as $pid => $paver)
	{
	$is_paver = true;
	$image_code = "<img src='".$paver["image"]."' style='width: 125px;' />";
	if($paver["iyb"])
		{
		include($path."configuration/iyb_settings.php");
		$ar = $iyb[$paver["item_number"]];
		$iyb_width = $ar['t-width'];
		$iyb_height = $ar['t-height'];;
		$iyb_image = $paver["image"];
		$iyb_page = $page;
		$iyb_paver_id = $pid;
		$iyb_option_id = 999;
		$iyb_line_spacing = $ar['t-spacing'];
		$iyb_start_top = $ar['t-start-top'];
		$iyb_font_size = $ar['t-font-size'];
		$iyb_font_color = $ar["font-color"];
		$inscription = $paver["inscription"];
		$iyb_default_inscription = $inscription;
		include($path."../_t2/elements/iyb_thumbnail_control.php");	
		$image_code = $iyb_thumbnail_control;		
		}

	$htm .= "
	<tr>
		<td rowspan=".$paver["row_count"]." align=center valign=top>
			<div class=conf-table-spacer1></div>
			".$image_code."	
		</td>
		<td valign=top align=center>
			<div class=conf-table-spacer1></div>
			1	
		</td>
		<td valign=top>
			<div class=conf-table-spacer1></div>
			".$paver["description"]."	
		</td>
		<td valign=top>
			<div class=conf-table-spacer1></div>
			&nbsp;
		</td>
	</tr>
";
	foreach($paver["options"] as $pid => $ar)	
		{
		foreach($paver["options"][$pid] as $aid => $sop)
			{
			$htm .= "
	<tr>
		<td valign=top align=center>
			".$sop["count"]."	
		</td>
		<td valign=top>
			".$sop["description"]."	
		</td>
		<td valign=top style='text-transform: capitalize;'>
			".get_address($aid)."
		</td>
	</tr>
";				
			}
		}
	$htm .= "
	<tr style='height: 7px;'>
		<td colspan=4 class=conf-table-line></td>
	</tr>
";
	}
$htm .= "
</table>
";

function get_inscription($paver)
{
$ins = "";
$del = "";
foreach($paver["inscription"] as $line => $txt)
	{
	$ins .= $del.$txt;
	$del = "<br />";
	}	
return "<center>".$ins."</center>";
}

function get_image($paver, $path)
{
$image_file = $paver["item_image_blank"];
if($paver["gift_certificate"] == "yes")	
	{
	include($path."configuration/image_settings.php");
	$image_file = $image['gift_certificate'];	
	}
include($path."configuration/misc_settings.php");
$link = "https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
if(strpos($link, "DEVELOPMENT"))
	{
	$image_file = "https://www.brickorder.com/DEVELOPMENT/template2/".$_SESSION["store_folder"]."/images/".$image_file;	
	}
else
	{
	$image_file = "https://www.brickorder.com/".$_SESSION["store_folder"]."/images/".$image_file;		
	}
return $image_file;
}

function get_options($paver)
{
$options = array();
foreach($paver["options"] as $oid => $option)	
	{
	if($option["quantity"] > 0)
		{
		foreach($option["shipto_addresses"] as $idx => $aid)	
			{
			if(!isset($options[$oid]))	
				{
				$options[$oid] = array();
				$opt = array();
				$opt[$aid]	= array();
				$opt[$aid]["description"] = $option["description"];
				$opt[$aid]["count"] = 1;
				$options[$oid] = $opt;
				}
			else
				{
				if(!isset($options[$oid][$aid]))
					{
					$opt = array();
					$opt["description"] = $option["description"];
					$opt["count"] = 1;
					$options[$oid][$aid] = $opt;
					}
				else
					{
					$options[$oid][$aid]["count"]++;
					}					
				}
			}
		}
	}
return $options;
}

function is_iyb($paver)
{
$iyb = true;
if($paver["gift_certificate"] == "yes" ){ $iyb = false; }
return $iyb;
}

function get_row_count($options)
{
$count = 0;
foreach($options as $oid => $aid)	
	{
	$count += count($aid);
	}
return $count;	
}

function get_address($aid)
{
$address = $_SESSION["order"]["addresses"][$aid]["fn"]."&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["ln"].",&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["address1"].",&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["city"].",&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["state_prov"]."&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["postal_code"];
return $address;
}

?>