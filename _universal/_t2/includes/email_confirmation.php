<?php

$page="email_confirmation";

/*include($path."configuration/environment_settings.php");
include($path."../_t2/configuration/should_errors_display.php");
include($path."../_t2/includes/session_start.php");*/
include($path."../_t2/configuration/configuration.php");
include($path."../_t2/configuration/set_defaults.php");
include($path."../_t2/includes/language_check.php");
include($path."configuration/contact_settings.php");
$info_label_class = "width: 33.33%; font-size: 18pt; font-family: arial;";
$htm = "
<body style='font-size: 14pt; color: #606060; font-family: Calibri, Arial, Optima;'>
<table style='table-layout: fixed; width: 100%;'>
	<tr>
		<td style='width: 100%;' align=center>
			<table style='table-layout: fixed; width: 750px;'>
				<tr>
					<td style='width: 750px;' align=left>
						<div><!-- Logo -->
							<img src='".$image["head_published"]."' />
						</div>
						<div style='font-weight: bold; font-size: 18pt;'><!-- Header -->
							".$lang["page"]["email"]["head"]."
						</div>
						<div><!-- Inscriptions -->
							".$lang["page"]["email"]["inscriptions"]."
						</div>
						<div><!-- Call Us -->
							".$lang["page"]["email"]["call"]."
						</div>
						<div><!-- Certificate -->
							".$lang["page"]["email"]["certificate"]."
						</div>
		<div style='border-style: none none solid none; border-color: #5B7F18; border-width: 2px; height: 6px;'></div>
		<div>&nbsp;</div>
		<div><!-- Information -->
			<table style='table-layout: fixed; width: 100%;' cellpadding=0 cellspacing=0 border=0>
				<tr>
					<td style='".$info_label_class."'>
						".$lang["page"]["email"]["billing-info-head"]."
					</td>
					<td style='".$info_label_class."'>
						".$lang["page"]["email"]["order-info-head"]."
					</td>
					<td style='".$info_label_class."'>
						".$lang["page"]["email"]["payment-info-head"]."
					</td>
				</tr>
				<tr>
					<td align=left valign=top>
";
include($path."../_t2/includes/billing_information.php");
$htm .= "
					</td>
					<td align=left valign=top>
";
include($path."../_t2/includes/order_information.php");
$htm .= "
					</td>
					<td align=right valign=top>
";
include($path."../_t2/includes/payment_information.php");
$htm .= "
					</td>
				</tr>
			</table>
		</div>
		<div style='border-style: none none solid none; border-color: #5B7F18; border-width: 2px; height: 6px;'></div>
		<div>&nbsp;</div>
		<div><!-- Cart -->
";
include($path."../_t2/includes/cart_with_inscription.php");
$htm .= "
		</div>
	</td></tr></table>
</td></tr></table>
</body>
";
//echo($htm);

?>