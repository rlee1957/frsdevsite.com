<?php

include($path."../_t2/includes/bill_session.php");
$address_type = "bill";
$onchange = " onchange='record_bill_address();' ";
?>
<h3 class="cart-title"><?php echo($lang["cart_billing_information"]); ?></h3>
<div class="form-group mb-40">
    <?php
    include($path."../_t2/includes/address_only.php");
    include($path."../_t2/elements/phone.php");
    include($path."../_t2/elements/email.php");
    ?>
</div>