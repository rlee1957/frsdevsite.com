<?php


?>
<div class="container first checkout">
    <div class="row">
        <div class="col-md-6 pr-20">
            <?php
            $address_type = "bill";
            $address_title = $lang["cart_billing_information"];
            $address_index = "";
            include($path."../_t2/includes/billing.php");
            // include($path."../_t2/includes/separator.php");
            include($path."../_t2/includes/credit_card.php");
            $address_type = "bill";
            $address_title = $lang["cart_billing_information"];
            $address_index = "0";
            include($path."../_t2/includes/shipping.php");
            ?>
        </div>
        <div class="col-md-6 border-success">
            <h3 class="t-20">Shopping Cart</h3>
            <div class="th hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-md-3">PRODUCT</div>
                    <div class="col-xs-9">
                        <div class="row">
                            <div class="col-xs-7">DESCRIPTION</div>
                            <div class="col-md-2 text-center">QTY</div>
                            <div class="col-xs-3 text-right">SUBTOTAL</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tb">
                <div class="row">
                    <div class="col-sm-3">
                        <img src="images/bricks02.png" alt="">
                    </div>
                    <div class="col-sm-9">
                        <div class="row tr">
                            <div class="col-xs-7">
                                <strong>4" x 8" BRICK</strong>
                            </div>
                            <div class="col-xs-2 text-center">
                                1
                            </div>
                            <div class="col-xs-3 text-right">
                                <span class="pull-left">$</span>
                                65.00
                            </div>
                        </div>
                        <div class="row tr">
                            <div class="col-xs-7">
                                <strong>4" x 8" REPLICA & DISPLAY CASE</strong>
                            </div>
                            <div class="col-xs-2 text-center">
                                2
                            </div>
                            <div class="col-xs-3 text-right">
                                <span class="pull-left">$</span>
                                195.00
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tb">
                <div class="row">
                    <div class="col-sm-3">
                        <img src="images/bricks03.png" alt="">
                    </div>
                    <div class="col-sm-9">
                        <div class="row tr">
                            <div class="col-xs-7">
                                <strong>8" x 8" BRICK</strong>
                            </div>
                            <div class="col-xs-2 text-center">
                                1
                            </div>
                            <div class="col-xs-3 text-right">
                                <span class="pull-left">$</span>
                                125.00
                            </div>
                        </div>
                        <div class="row tr">
                            <div class="col-xs-7">
                                <strong>8" x 8" REPLICA</strong>
                            </div>
                            <div class="col-xs-2 text-center">
                                1
                            </div>
                            <div class="col-xs-3 text-right">
                                <span class="pull-left">$</span>
                                75.00
                            </div>
                        </div>
                        <div class="row tr">
                            <div class="col-xs-7">
                                <strong>8" x 8" REPLICA & DISPLAY CASE</strong>
                            </div>
                            <div class="col-xs-2 text-center">
                                1
                            </div>
                            <div class="col-xs-3 text-right">
                                <span class="pull-left">$</span>
                                145.00
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <div class="promo form-group">
                    <label for="code">Promotional Code:</label>
                    <input class="form-control" type="text" placeholder="Enter Code">
                    <button class="btn btn-primary btn-sm">Apply</button>
                </div>
                <div class="total">Subtotal: <span class="wrap"><span class="pull-left">$</span>455.00</span></div>
                <div class="total">Promotional Discount: <span class="wrap"><span class="pull-left">$</span>0.00</span></div>
                <div class="total">Sales Tax: <span class="wrap"><span class="pull-left">$</span>0.00</span></div>
                <div class="total">Web Convenience Fee: <span class="wrap"><span class="pull-left">$</span>6.00</span></div>
                <strong class="subtotal">ORDER TOTAL:<span>$</span>461.00</strong>
            </div>
            <div class="text-center">
                <div class="checkbox-item">
                    <input type="checkbox" id="terms"/>
                    <label for="terms">Accept </label>
                </div>
                <a class="text-primary text-underline" href="#">Terms & Conditions</a><br>
                <button class="btn btn-primary blue btn-lg">PLACE YOUR ORDER</button>
            </div>
        </div>
    </div>
</div>
    *****************************************************************************************************
<table style="table-layout: fixed; width: 100%;">
	<tr>
		<td valign=top align=right
			style="width: 49%; padding: 20px 60px 20px 20px;">
<?php
$address_type = "bill";
$address_title = $lang["cart_billing_information"];
$address_index = "";
include($path."../_t2/includes/billing.php");
// include($path."../_t2/includes/separator.php");
include($path."../_t2/includes/credit_card.php");
$address_type = "bill";
$address_title = $lang["cart_billing_information"];
$address_index = "0";
include($path."../_t2/includes/shipping.php");
echo("
<div id='div_marketing' style='display: none;'>
");
include($path."../_t2/includes/marketing.php");
echo("
</div>
");
$div_width = "750";
if($page == "cart")
	{
	$wfull = 840;
	$w1 = 200;
	$w2 = 225;
	$w3 = 125;
	$w4 = 125;
	$w5 = 125;
	}
else
	{
	$wfull = 640;
	$w1 = 200;
	$w2 = 260;
	$w3 = 30;
	$w4 = 10;
	$w5 = 100;
	}
?>
			<div style="clear: both;"></div>
		</td>
		<td class=divider>&nbsp;</td>
		<td valign=top
			style="width: 49%; padding: 20px;"
			align=left>
				<div style="width: <?php echo($div_width); ?>px;" id="cart_div">
					<h2 class='cart-title' style="border-style: none;"><?php echo($lang["cart-title"]); ?></h2>
					<?php include($path."../_t2/includes/cart_no_image.php"); ?>
					<div style="width: 650px; overflow: hidden;">
					<?php include($path."../_t2/includes/separator.php"); ?>
					</div>
				</div>
<?php

if($options["promo"])
	{
	echo("<div style='width: 650px;'>");
	include($path."../_t2/includes/promotion_code.php");
	echo("</div>");
	}
echo("
				<div style='width: 650px; text-align: right;'>
");
include($path."../_t2/includes/cart_bottom.php");
echo("
				</div>
");
?>
					<div style="width: 650px; overflow: hidden;">
					<?php include($path."../_t2/includes/separator.php"); ?>
					</div>
<?php
include($path."../_t2/includes/terms.php");

include($path."../_t2/includes/process_order_button.php");
?>


			<div style="clear: both;"></div>
			<div style="width: <?php echo($div_width); ?>px;">
			<?php include($path."../_t2/includes/tnc_container.php"); ?>
			</div>
		</td>
	</tr>
</table>
<?php
if($options["foot_spacer"])
	{
	include($path."../_t2/elements/foot_spacer.php");
	}
?>