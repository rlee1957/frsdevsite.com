<?php
	include($path."configuration/iyb_settings.php");
	$show_text = false;
	if($options["iyb"])
		{ 
		if(isset($iyb[$paver["item_number"]]["options"][$row["item_number"]]))
			{
			$image = $row["item_image_blank"]; 
			$show_text_items .= $stt_del.$page."_p_".$id."_o_".$i;
			$show_text = true;
			$stt_del = "|";
			}
		else
			{
			$image = $row["item_image_default"];
			}
		}
	else{ $image = $row["item_image_default"]; }
	if($show_text){ $ar = $iyb[$paver["item_number"]]["options"][$row["item_number"]]; }
	if($_GET["gift_certificate"] == "yes"){ $image = $row["item_image_default"]; }
	$htm .= "
	<td valign=bottom style='padding: 0 40px 0 0;'>
		<table cellspacing=0 cellpadding=20>
			<tr>				
";
	
	if($show_text)
		{
		# What's expected by the iyb thumbnail control
		#-----------------------------------------------------
		$iyb_width = "250";
		$iyb_height = "250";
		$iyb_image = "images/".$image;
		$iyb_page = $page;
		$iyb_paver_id = $id;
		$iyb_option_id = $i;
		$iyb_line_spacing = $ar['spacing'];
		$iyb_start_top = $ar['start-top'];
		$iyb_font_size = $ar['font-size'];
		$iyb_font_color = $ar['color'];
		$iyb_default_inscription = "";
		# Return value is stored in $iyb_thumbnail_control variable
		include($path."../_t2/elements/iyb_thumbnail_control.php");
		$htm .= "
				<td colspan=3 class=option-image align=center valign=bottom style='width: 300px;'>
";
		$htm .= $iyb_thumbnail_control;
		}
	else
		{
		$htm .= "
				<td colspan=3 class=option-image align=center valign=bottom style='width: 300px;'>
					<div style='width: 250px;'>
						<img src='images/".$image."' style='width: 250px;' />
					</div>
";	
		}
	$htm .= "
				</td>
			</tr><tr>
				<td colspan=3 class=option-desc align=center>".$row["description"]."</td>
			</tr><tr>
				<td class=option-price align=right style='width: 110px;'>
					".$lang["currency_symbol"].number_format($row["price"], 2)."&nbsp;
				</td>	
				<td class=option-qty-label align=right style='width: 40px;'>
					".$lang['cart_quantity']."&nbsp;&nbsp;&nbsp;
				</td>
				<td class=option-qty-amt align=left style='width: 100px;'>
";
	$control_name = $page."_option_".$id."_".$i;
	$refresh = "do_nothing();";
	$control_pm_height = 25;
	$control_pm_width = 25;
	$control_plus_top = 0;
	$control_minus_top = 0;
	$control_value_left = $control_pm_width + 1;
	$control_value_top= 0;
	$control_value_height = 25;
	$control_value_width = 32;
	$control_height	= 35;
	$control_width = ((($control_pm_width * 2) + $control_value_width) + 7);
	$quantity = 0;
	# Return value is stored in $quantity_control variable
	include($path."../_t2/elements/quantity_control.php");
	$htm .= $quantity_control;
	$htm .= "
				</td>	
			</tr>
		</table>
	</td>
";

?>