<?php

$_SESSION["g_ordertotal"] = $_SESSION["order"]["info"]["amount"]["order_total"];
$_SESSION["go_orderid"]	= $_SESSION["order"]["info"]["purchase_id"];
$_SESSION["go_total"] = (string) sprintf("%2.2f", $_SESSION["order"]["info"]["amount"]["order_total"]);
$_SESSION["go_cname"] = (string) trim($_SESSION["order"]["info"]["cc"]["cc_type"]);
$_SESSION["go_cno"]	 = (string) trim($_SESSION["order"]["info"]["cc"]["cc_number"]);
$_SESSION["go_cexp"] = (string) trim($_SESSION["order"]["info"]["cc"]["cc_month"].$_SESSION["order"]["info"]["cc"]["cc_year"] );
$_SESSION["go_cvv2"] = '';
$_SESSION["go_oname"] = (string) trim($_SESSION["order"]["info"]["cc"]["cc_name"]) ;
$_SESSION["go_ostreet"] = (string) trim($_SESSION["order"]["addresses"][0]["address1"]);
$_SESSION["go_ocity"] = (string) trim($_SESSION["order"]["addresses"][0]["city"]);
$_SESSION["go_ostate"] = (string) trim($_SESSION["order"]["addresses"][0]["state_prov"]);
$_SESSION["go_ozip"] = (string) trim($_SESSION["order"]["addresses"][0]["postal_code"]);
$aryResult = array();

?>