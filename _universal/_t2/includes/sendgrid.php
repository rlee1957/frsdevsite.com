<?php

# ---------------------------------------
# Passed variables
# 
# $email_from_name
# $email_from
# $email_to_name
# $email_to
# $email_bcc_name
# $email_bcc
# $subject
# $body

include($path."../_t2/includes/should_errors_display.php");
require($path."../_sendgrid/sendgrid-php.php");
include($path."../_common/configuration/sendgrid.php");

$mail_results = array();
$mail_results["success"] = true;
$mail_results["number"] = 202;
$mail_results["message"] = "Success";

$type = "text/html";

$from = new SendGrid\Email($email_from_name, $email_from);

$to = new SendGrid\Email($email_to_name, $email_to);

$bcc = new SendGrid\Email($email_bcc_name, $email_bcc);

$content = new SendGrid\Content($type, $body);

$bccsettings = new sendGrid\BccSettings();
$bccsettings -> setEnable(true);

$mailsettings = new SendGrid\MailSettings();
$mailsettings -> setBccSettings($bccsettings);

$personalization = new SendGrid\Personalization();
$personalization -> addTo($to);
$personalization -> addBcc($bcc);

$mail = new SendGrid\Mail();
$mail -> setMailSettings($mailsettings);
$mail -> setFrom($from);
$mail -> setReplyTo($from);
$mail -> addPersonalization($personalization);
$mail -> setSubject($subject);
$mail -> addContent($content);

$sg = new \SendGrid($sendgrid_api_key);

$response = $sg->client->mail()->send()->post($mail);

if($response->statusCode() != 202)
	{ 
	$mail_results["success"] = false; 
	$mail_results["message"] = $response->headers();
	}
else
	{
	$mail_results["message"] .= ": ".$response->headers();	
	}
	
$mail_results["number"] = $response->statusCode();

?>