<?php

$htm = "";
$ct = 0;
foreach($_SESSION["cart"] as $key => $item)
	{
	if(is_numeric($key))
		{
		if((isset($item["replica1_quantity"]))&&($item["replica1_quantity"] > 0))
			{
			for($x=0;$x<$item["replica1_quantity"];$x++)
				{
				$htm .= get_option($key, "replica1_addresses", $item, $x, $ct);
				$ct++;
				}
			}
		if((isset($item["display_case1_quantity"]))&&($item["display_case1_quantity"] > 0))
			{
			for($x=0;$x<$item["display_case1_quantity"];$x++)
				{
				$htm .= get_option($key, "display_case1_addresses", $item, $x, $ct);
				$ct++;
				}
			}
		if((isset($item["replica2_quantity"]))&&($item["replica2_quantity"] > 0))
			{
			for($x=0;$x<$item["replica2_quantity"];$x++)
				{
				$htm .= get_option($key, "replica2_addresses", $item, $x, $ct);
				$ct++;
				}
			}
		if((isset($item["display_case2_quantity"]))&&($item["display_case2_quantity"] > 0))
			{
			for($x=0;$x<$item["display_case2_quantity"];$x++)
				{
				$htm .= get_option($key, "display_case2_addresses", $item, $x, $ct);
				$ct++;
				}
			}
		}
	}
$htm .= "
<input type=hidden id=option_count name=option_count value='".$ct."' />
<input type=hidden id=selected_option name=selected_option value='' />
";
echo($htm);

function get_option($item_id, $opt_type, $item, $opt_id, $ct)
{
$elid = $item_id."|".$opt_type."|".$opt_id;
$htm = "
<div onclick='select_order_item(".$ct.");' class=list-item>
	<table style='table-layout: fixed; width: 100%;'>
		<tr>
			<td align=center>
				".$item[$opt_type."_description"]."
				<input type=hidden id=list_item".$ct." value='".$elid."' />
				<div class=list-inscription>";
$del = "";
foreach($item["inscription"] as $line => $text)
	{
	if(trim($text) != "")
		{
		$htm .= $del.$text;
		$del = "<br />";
		}
	}
$htm .= "
				</div>
			</td>
			<td valign=middle align=center style='width: 25px;'>
";
if(isset($item["address"])){ $aid = $item["address"]; }
else{ $aid = 0; }
$htm .= "
				<input type=text readOnly id=item_list_address".$ct." value=".$aid." style='width: 25px;' />
			</td>
		</tr>
	</table>	
</div>
";
return $htm;
}
?>