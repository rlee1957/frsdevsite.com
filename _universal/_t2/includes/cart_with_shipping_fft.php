<?php

$cs = array();
foreach($_SESSION["order"]["cart"] as $iid => $item )
	{
	if($item["quantity"] > 0)
		{	
		$ip = array();
		$ip["item_number"] = $item["item_number"];
		$ip["description"] = $item["description"];
		$ip["quantity"] = $item["quantity"];
		$ip["image"] = $item["item_image_default"];
		$add = array();
		for($x=0;$x<$item["quantity"];$x++)
			{
			$aix = $item["shipto_addresses"][$x];
			if(!isset($add[$aix])){ $add[$aix] = 1; }
			else{ $add[$aix]++; }	
			}
		$ip["addresses"] = $add;
		$cs[$iid] = $ip;
		}
	}


$htm .= "

<div class='th hidden-xs'>
            <div class='row'>
                <div class='col-sm-3'>".$lang["page"]["email"]["product-label"]."</div>
                <div class='col-xs-9'>
                    <div class='row'>
                        <div class='col-xs-3 text-center'>".$lang["page"]["email"]["quantity-label"]."</div>
                        <div class='col-xs-5'>".$lang["page"]["email"]["description-label"]."</div>
                        <div class='col-xs-4'>".$lang["page"]["email"]["address-label"]."</div>
                    </div>
                </div>                          
            </div>
        </div>


";
$begin = true;
foreach($cs as $idx => $fft)
	{
	$is_paver = true;
	$image_code = "<img src='".get_image($fft["image"], $path)."' style='width: 125px;' />";
	$rowcount = count($fft["addresses"]);
//
//	$htm .= "
//	<tr>
//		<td rowspan=".$rowcount." align=center valign=top>
//			<div>&nbsp;</div>
//			".$image_code."
//		</td>
//";
        $description = $fft["description"];
        $address_ ='';
	foreach($fft["addresses"] as $aid => $qt)
		{
		$description = $fft["description"];
		$address = get_address($aid, $path);
            $address_ .=ucwords($address);

//	$htm .= "
//		<td valign=top align=center>
//			<div class=conf-table-spacer1></div>
//			(".$qt.")
//		</td>
//		<td valign=top>
//			<div class=conf-table-spacer1></div>
//			".$description."
//		</td>
//		<td valign=top>
//			<div class=conf-table-spacer1></div>
//			".ucwords($address)."
//		</td>
//	</tr>
//";
//		$first = false;
		}
        
     $htm.="
     <div class='tb'>
            <div class='row'>
                <div class='col-sm-3'>
                    ".$image_code."

    </div>
                <div class='col-sm-9'>
                    <div class='row tr'>
                        <div class='col-xs-3 text-center'>
                          ".$qt."

    </div>
                        <div class='col-xs-9 col-md-5'>
                            <strong>".$description."</strong>
                        </div>
                        <div class='col-md-4'>
                          ".$address_."
                        </div>
                    </div>
                </div>
            </div>
        </div>
     ";
        
        

	}




function get_inscription($paver)
{
$ins = "";
$del = "";
foreach($paver["inscription"] as $line => $txt)
	{
	$ins .= $del.$txt;
	$del = "<br />";
	}	
return "<center>".$ins."</center>";
}

function get_image($image, $path)
{
$image_file = $image;
include($path."configuration/misc_settings.php");
$link = "https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
if(strpos($link, "DEVELOPMENT"))
	{
	$image_file = "https://www.brickorder.com/DEVELOPMENT/template2/".$_SESSION["store_folder"]."/images/".$image_file;	
	}
else
	{
	$image_file = "https://www.brickorder.com/".$_SESSION["store_folder"]."/images/".$image_file;		
	}
// echo($image_file."<br />");
return $image_file;
}

function get_options($paver)
{
$options = array();
foreach($paver["options"] as $oid => $option)	
	{
	if($option["quantity"] > 0)
		{
		foreach($option["shipto_addresses"] as $idx => $aid)	
			{
			if(!isset($options[$oid]))	
				{
				$options[$oid] = array();
				$opt = array();
				$opt[$aid]	= array();
				$opt[$aid]["description"] = $option["description"];
				$opt[$aid]["count"] = 1;
				$options[$oid] = $opt;
				}
			else
				{
				if(!isset($options[$oid][$aid]))
					{
					$opt = array();
					$opt["description"] = $option["description"];
					$opt["count"] = 1;
					$options[$oid][$aid] = $opt;
					}
				else
					{
					$options[$oid][$aid]["count"]++;
					}					
				}
			}
		}
	}
return $options;
}

function is_iyb($paver)
{
$iyb = true;
if($paver["gift_certificate"] == "yes" ){ $iyb = false; }
return $iyb;
}

function get_row_count($options)
{
$count = 0;
foreach($options as $oid => $aid)	
	{
	$count += count($aid);
	}
return $count;	
}

function get_address($aid)
{
$address = $_SESSION["order"]["addresses"][$aid]["fn"]."&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["ln"].",&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["address1"].",&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["city"].",&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["state_prov"]."&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["postal_code"];
return $address;
}

?>