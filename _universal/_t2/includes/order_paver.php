<?php  

$option_count = count($item["options"]);
$rowspan = ($option_count) + 1;
$description = $item["description"];
$image_file = $item["item_image_blank"];
$edit = "";
if($page == "cart")
	{
	$edit = "
			<br />
			<a href=\"javascript: edit_inscription('".$key."', '".$refresh."');\">".$lang['edit_inscription']."</a>
";
	}
$show_text = false;
include($path."configuration/image_settings.php");
if($item["gift_certificate"] == "yes")
	{ 
	$image_file = $image['gift_certificate']; 
	$edit = "";
	}
$price = $item["price"];
$remove = "";
if($page == "cart")
	{
	$remove = "<img src='".$image['remove-paver']."' alt='Remove' onclick='ays(".$key.");' style='width: 20px; cursor: pointer;' />";
	}
$image_code = "<img src='images/".$image_file."' style='width: 125px;' />";

include($path."configuration/iyb_settings.php");
$ar = $iyb[$item["item_number"]];
$iyb_width = $ar['t-width'];
$iyb_height = $ar['t-height'];;
$iyb_image = "images/".$image_file;
$iyb_page = $page;
$iyb_paver_id = $key;
$iyb_option_id = 999;
$iyb_line_spacing = $ar['t-spacing'];
$iyb_start_top = $ar['t-start-top'];
$iyb_font_size = $ar['t-font-size'];
$iyb_font_color = $ar["font-color"];
$inscription = $_SESSION["order"]["cart"][$key]["inscription"];
$iyb_default_inscription = "
<center>";
$del = "";
foreach($inscription as $ln => $insc)
	{
	$iyb_default_inscription .= $del.$insc;
	$del = "<br />";	
	}
$iyb_default_inscription .= "
</center>
";
if($item["gift_certificate"] == "no")
	{
	include($path."../_t2/elements/iyb_thumbnail_control.php");	
	$image_code = $iyb_thumbnail_control;
	}
$description_code = "
			".$spc."
			<span class='".$cls."'>
				".$description."&nbsp;&nbsp;
			</span>
";
if($page == "cart")
	{
	$description_code = "
			".$spc."
			<span class='".$cls."'>
				".$description."
			</span><br />
			<span class='cart-price'>
				".$lang["cart_price_each"]."&nbsp;&nbsp;".$lang["currency_symbol"]."&nbsp;".number_format($price, 2, '.', ',')."
			</span>
";
	}
$subtotal = $price;
$align = " align=left";
if($page == "cart"){ $align = "center"; }
$class = "cart-table-item-separater";
if(!$head){ include($path."../_t2/includes/item_separator.php"); }
$head = false;
?>
	<tr>
		<td rowspan=<?php echo($rowspan); ?> align=left valign=middle>
<?php
echo($spc);
echo($image_code);
echo($edit);
echo($spc);
?>
		</td> 
		<td valign=top align=left>
			<?php echo($description_code); ?>
		</td>
		<td align=center valign=top>
			<span<?php echo($ts); ?>><?php echo($spc); ?>1<?php echo($qp); ?></span>
		</td>
		<td valign=top align=center>
			<?php echo($spc.$remove); ?>
		</td>
		<td align=right valign=top>
			<?php echo($spc.$lang["currency_symbol"]); ?>
		</td>
		<td align=right valign=top>
			<?php echo($spc.number_format($price, 2, '.', ',')); ?>
		</td>
	</tr>