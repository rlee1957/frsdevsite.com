<?php 

# default settings
$description = "
			".$spc."
			<div class='".$cls."'>
				".$oarr["description"]."&nbsp;
			</div>
";

$quantity_code = "
			".$spc."
			<span>
				".$oarr["quantity"]."
			</span>
";
# cart page settings
$price = $oarr["price"];
if($page == "cart")
	{
	$description = "
			".$spc."
			<div class='".$cls."'>".$oarr["description"]."</div>
			<div class='cart-price'>
				".$lang["cart_price_each"]."&nbsp;&nbsp;".$lang["currency_symbol"]."&nbsp;".number_format($price, 2, '.', ',')." 
			</div>
";	
	# quantity control settings
	$control_name = $page."_option_".$key."_".$oid;
	$refresh = "do_nothing();";
	$control_pm_height = 25;
	$control_pm_width = 25;
	$control_plus_top = 0;
	$control_minus_top = 0;
	$control_value_left = $control_pm_width + 1;
	$control_value_top= 0;
	$control_value_height = 25;
	$control_value_width = 32;
	$control_height	= 35;
	$control_width = ((($control_pm_width * 2) + $control_value_width) + 7);
	$quantity = $oarr["quantity"];
	# Return value is stored in $quantity_control variable
	include($path."../_t2/elements/quantity_control.php");
	$quantity_code = $quantity_control;
	}
$image = $oarr["item_image_blank"];
if($item["gift_certificate"] == "yes"){ $image = $oarr["item_image_default"]; }

$id = $oarr["item_number"];
$quantity = $oarr["quantity"];
$ship_address = $oarr["ship_address"];	
$ref = $key."_".$oid;
$subtotal = $quantity * $price;
$class = "cart-table-option-separater";
//if(!$head){ include($path."../_t2/includes/item_separator.php"); }
?>

	<tr>
		<td align=left valign=top>
			<?php echo($description); ?>
		</td>
		<td align=center valign=top>
			<?php echo($spc.$quantity_code); ?>
		</td>
		<td>&nbsp;</td>
		<td align=right valign=top>
			<?php echo($spc.$lang["currency_symbol"]); ?>
		</td>
		<td align=right valign=top>
			<?php echo($spc.number_format($subtotal, 2, '.', ',')); ?>
		</td>
	</tr>