<div id=content1 style="display: <?php echo($display[1]); ?>;" class=landing-container>

<?php

if($options["head_spacer"])
	{
	include("../_t2/elements/head_spacer.php");	
	}
if($show_page_name)
	{
	echo("<h3>content1.php</h3>");
	}
include("../_t2/database/get_pavers.php");
?>
<div style="height: 50px;"></div>
<center>
	<div class=shop-title><?php echo $lang['shop-title']; ?></div>
	<br />
	<input type="hidden" id=bktype name=bktype />
	<table>
		<tr>
<?php
$htm = "";						
foreach($pavers as $i => $row)
	{
	$htm .= "
			<td>
				<div onclick='select_item(".$row["item_number"].");' class=select-item>
					<table>
						<tr style='height: 275px'>
							<td class=item-image valign=bottom>
								<img style='width: 250px;'  src='images/".$row["item_image_default"]."' />
								<br />
							</td>
						</tr><tr>
							<td class=item-desc><br />".$row["description"]."</td>
						</tr><tr>
							<td class=item-desc2>".$row["description2"]."</td>
						</tr><tr>
							<td class=item-price>".$lang["currency_symbol"].number_format($row["price"],2)."</td>	
						</tr>
					</table>
				</div>
			</td>
";
    }
if($options["gift_certificates"])
	{
	$htm .= "
			<td>
				<div onclick='gift_certificate();' class=select-item>
					<table>
						<tr style='height: 275px'>
							<td class=item-image valign=bottom>
								<img style='width: 250px;'  src='images/".$image['gift_certificate']."' />
								<br />
							</td>
						</tr><tr>
							<td class=item-desc><br />".$lang["gift_certificate"]."</td>
						</tr><tr>
							<td class=item-desc2>&nbsp;</td>
						</tr><tr>
							<td class=item-price>&nbsp;</td>	
						</tr>
					</table>
				</div>
			</td>
";		
	}
echo($htm);
?>  
		</tr>
	</table>
</center>
<br /><br /><br /><br /><br />
<?php
if($show_session)
	{
	$label = "Session Variables";
	$var_array = $_SESSION;
	include($path."../_t2/includes/show_array.php");
	}
if($show_order)
	{
	$label = "Order Variables";
	$var_array = $_SESSION["order"];
	include($path."../_t2/includes/show_array.php");
	}
if($options["foot_spacer"])
	{
	include("../_t2/elements/foot_spacer.php");	
	}
?>
</div>