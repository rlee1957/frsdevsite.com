<?php   
$head = true;
if($page == "cart")
	{
	$wfull = 840;
	$w1 = 200;
	$w2 = 300;
	$w3 = 125;
	$w4 = 50;
	$w5 = 105;
	$w6 = 20;
	}
else
	{
	$wfull = 640;
	$w1 = 200;
	$w2 = 255;
	$w3 = 30;
	$w4 = 1;
	$w5 = 100;
	$w6 = 14;
	}
$q = "&nbsp;";
$qp = "";
$ts = "";
$cls = "cart-description";
$spc = "<div style='height: 5px;'></div>";
if($page == "cart")
	{ 
	$q = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"; 
	$qp = "&nbsp;&nbsp;&nbsp;";
	$ts = " style='font-weight: bold; font-size: 14pt;'";
	$cls = "sum-description";
	$spc = "<div style='height: 10px;'></div>";
	}
?>

<div class="th hidden-xs">
    <div class="row">
        <div class="col-sm-3"><?php echo($lang["cart_label_image"]); ?></div>
        <div class="col-xs-9">
            <div class="row">
                <div class="col-xs-5 col-md-6"><?php echo($lang["cart_label_description"]); ?></div>
                <div class="col-xs-3 text-center"><?php
                    echo($q);
                    echo($lang["cart_label_quantity"]);
                    ?></div>
                <?php if($page == "cart"){ ?><div class="col-xs-1"></div><?php } ?>
                <div class="col-xs-3 col-md-2 text-right"><?php echo($lang["cart_label_price"]); ?></div>
            </div>
        </div>
    </div>
</div>

