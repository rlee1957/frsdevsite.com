<div class="popup_bg" 
	 id="tnc_bg" 
	 style="display: none; cursor: pointer;"
	 onclick="hide_tnc();">
</div>
<div class="tnc_container centered msg" id="tnc_container" style="display: none;">
	<table style="table-layout: fixed; height: 100%; width: 100%;">
		<tr style="height: 7%">
			<td align="center" valign="middle">
				<h1>
					<b>
						<span style='font-size: 30pt; color: #0000FF;'>
							<?php echo $lang["page"]["tnc"]["header"]; ?><o:p></o:p>
						</span>
					</b>
				</h1>
			</td>
		</tr>
		<tr style="height: 7%">
			<td align="center" valign="middle">
				<a href="#cancellations">
					<span style='font-size:14.0pt;mso-bidi-font-size:10.0pt'>
						<?php echo $lang["page"]["tnc"]["cancellations_header"]; ?>
					</span>
				</a>
				<!--&nbsp;&nbsp;&nbsp;
				<a href="#inscription">
					<span style='font-size:14.0pt;mso-bidi-font-size:10.0pt'>
						<?php echo $lang["page"]["tnc"]["inscriptions_header"]; ?>
					</span>
				</a>-->
				&nbsp;&nbsp;&nbsp;
				<a href="#payment">
					<span style='font-size:14.0pt;mso-bidi-font-size:10.0pt'>
						<?php echo $lang["page"]["tnc"]["transaction_header"]; ?>
					</span>
				</a>
				&nbsp;&nbsp;&nbsp;
				<a href="#privacy">
					<span style='font-size:14.0pt;mso-bidi-font-size:10.0pt'>
						<?php echo $lang["page"]["tnc"]["privacy_header"]; ?>
					</span>
				</a>
			</td>
		</tr>
		<tr style="height: 79%">
			<td style="height: 100%;">
				<div style="overflow: auto; height: 100%;" class="tnc_content">
<!-- Cancellations -->
					<a name="cancellations" >
						<h2 style="text-transform:uppercase;">
							<?php echo $lang["page"]["tnc"]["cancellations_header"]; ?>
						</h2>
					</a>
					<p id="tnc_returns" style='font-size:11.0pt;mso-bidi-font-size:7.0pt'>
						<?php echo $lang["page"]["tnc"]["cancellations_text"]; ?>
					</p>
<!-- Inscription 
					<a name="inscription">
						<h2 style="text-transform:uppercase;">
							<?php echo $lang["page"]["tnc"]["inscriptions_header"]; ?>
						</h2>
					</a>
					<p id="tnc_inscription" style='font-size:11.0pt;mso-bidi-font-size:7.0pt'>
						<?php echo $lang["page"]["tnc"]["inscriptions_text"]; ?>
					</p> -->
<!-- Payment -->
					<a name="payment">
						<h2 style="text-transform:uppercase;">
							<?php echo $lang["page"]["tnc"]["transaction_header"]; ?>
						</h2>
					</a>
					<p id="tnc_payment" style='font-size:11.0pt;mso-bidi-font-size:7.0pt'>
						<?php echo $lang["page"]["tnc"]["transaction_text"]; ?>
					</p>
<!-- Privacy -->
					<a name="privacy">
						<h2 style="text-transform:uppercase;">
							<?php echo $lang["page"]["tnc"]["privacy_header"]; ?>
						</h2>
					</a>
					<p id="tnc_privacy" style='font-size:11.0pt;mso-bidi-font-size:7.0pt'>
						<?php echo $lang["page"]["tnc"]["privacy_text"]; ?>
					</p>
				</div>
			</td>
		</tr>
		<tr style="height: 7px;">
			<td align="center" valign="middle">
				<br />
				<?php echo $lang["page"]["tnc"]["important_step"]; ?>
				<br />
				<?php echo $lang["page"]["tnc"]["questions"]; ?>
				<br />
				<span style='font-size: 20pt; color: #0000FF;'>
					<?php echo $lang["page"]["tnc"]["close_instructions"]; ?>
				</span>
				<br />
			</td>
		</tr>
	</table>
</div>
