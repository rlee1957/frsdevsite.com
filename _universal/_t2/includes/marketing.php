<?php

include($path."configuration/marketing_settings.php");

$del = "";
if(isset($marketing["how_u_heard"]))
	{
	$default = "";	
	if(isset($_SESSION["order"]["info"]["marketing"]["how_u_heard"]))
		{ 
		$default = $_SESSION["order"]["info"]["marketing"]["how_u_heard"]; 
		}
	$htm = $del.get_code("how_u_heard", $marketing["how_u_heard"], $default);
	echo($htm);
	$del = "";
	}
if(isset($marketing["buying_as"]))
	{
	$default = "";	
	// echo($_SESSION["order"]["info"]["marketing"]["buying_as"]."<br />");
	if(isset($_SESSION["order"]["info"]["marketing"]["buying_as"]))
		{ 
		$default = $_SESSION["order"]["info"]["marketing"]["buying_as"]; 
		}
	$htm = $del.get_code("buying_as", $marketing["buying_as"], $default);
	echo($htm);
	$del = "";
	}
if(isset($marketing["opt_in"]))
	{
	$default = "";	
	if(isset($_SESSION["order"]["info"]["marketing"]["opt_in"]))
		{ 
		$default = $_SESSION["order"]["info"]["marketing"]["opt_in"]; 
		}
	$htm = $del.get_code("opt_in", $marketing["opt_in"], $default);
	echo($htm);
	}
if(isset($marketing["group_code"]))
	{
	$default = "";	
	if(isset($_SESSION["order"]["info"]["marketing"]["group_code"]))
		{ 
		$default = $_SESSION["order"]["info"]["marketing"]["group_code"]; 
		}
	$htm = get_code("group_code", $marketing["group_code"], $default);
	echo($htm);
	$del = "<br /><br />";
	}
	
	
function get_code($name, $ar, $default)
{
$type = $ar["type"];

switch($type)
	{
	case "drop down":
		{
		return get_drop_down($name, $ar, $default);
		break;	
		}
	case "checkbox group":
		{
		return get_checkbox_group($name, $ar, $default);
		break;	
		}
	case "text input":
		{
		return get_text_input($name, $ar, $default);
		break;	
		}	
	}

}

function get_drop_down($name, $ar, $default)
{

// initialize variables
$oc = "";
$value = "";
$display = "none";
$options = "";
$exceptions = "";
$show_exceptions = false;
$exception_value = "";
$exception_elements = array();
// check for existing values
if($_SESSION["order"]["info"]["marketing"]["how_u_heard"] != "")
	{
	$va = explode("~", $_SESSION["order"]["info"]["marketing"]["how_u_heard"]);
	$value = $va[0];
	if(count($va) > 1){ $exception_value = $va[1]; }
	}
// set value to default if value is empty
if(($default != "") && ($value == "")){ $value = $default; }
// add prompting option if value is empty
if($value == "")
	{
	$options .= "
					<option value = '' selected disabled>".$ar["prompt"]."</option>";	
	}
// get options
$opts = $ar["options"];
// sort options
if($ar["sorted"]){ sort($opts); }
$del = "";
$match_value = $value;
foreach($opts as $key => $val)
	{ 
	$selected = "";
	if($val == $match_value){ $selected = " selected"; }
	$options .= "
					<option value = '".$val."'".$selected.">".$val."</option>";
	if(isset($ar["exceptions"][$val]))
		{
		// initialize variables
		$start = "";
		$end = "";
		$ele = "";
		$valu = "";
		
		// check for existing value
		if($value == $val)
			{
			$xval = str_replace('"', '\"', $exception_value);
			$xval = str_replace("'", "\'", $xval);
			$valu = " value=\"".$xval."\"";	
			$display = "block";
			$show_exceptions = true;
			}
		$eleid = $name."_".$ar["exceptions"][$val]["id"]."_value";
		$container_start = "<div class='form-group t-17'  id='".$name."_".$ar["exceptions"][$val]["id"]."_container' style='display: ".$display.";>";
		// check if label should be displayed or not
		if($ar["exceptions"][$val]["label"] != "")	
			{
			$start = "<label for='".$eleid."' class='sm-17' >". $ar["exceptions"][$val]["label"] ."</label>";
			$end = "";
			}
		// create exception element
		$ele = "<input type='text' id='".$eleid."' name='".$name."' placeholder='".$ar["exceptions"][$val]["placeholder"]."' ".$valu." onchange=\"marketing_dd_exception('".$name."', '".$ar["id"]."', this.id);\" class='form-control control-inline' />
";
		// add to exceptions
		$exceptions .= $del.$val."~".$ar["exceptions"][$val]["id"];
		// add to exception elements
		$exception_elements[count($exception_elements)] = $container_start.$start.$ele.$end."</div>";
		$display = "none";		
		$del = "|";
		}
	}	
$oc = "marketing_dd_change(this, '".$name."', '".$exceptions."');";
$htm = "
<div class='form-group t-17 pl-335'>

				<label for='".$ar["id"]."' class='sm-17'>
					".one_string($ar["label"])."
				</label>

				<div class='select-wrap'>

				<select id='".$name."' name='".$name."'  class='form-control' value='".$value."' onchange=\"".$oc."\" >
";
if($show_exceptions){ $display = "block"; }
else{ $display = "none"; }
$htm .= $options."
				</select>
				</div>
				<input type='hidden' id='".$name."_warning' value='".$ar["warning"]."' />
				<input type='hidden' id='".$name."_warning_label' value='".$ar["warning_label"]."' />
				<input type='hidden' id='".$name."_required' value='".$ar["required"]."' />
				<input type='hidden' id='".$name."_exceptions' value='".$exceptions."' />
				<input type='hidden' id='".$name."_value' value='".$default."' />
				<div id='".$name."_exception_elements' style='display: ".$display.";'>
";
// add exception elements				

$htm .= "
				</div>
</div>

";
    foreach($exception_elements as $i => $e)
    {
        $htm .= "
					".$e;
    }
return $htm;	
	
}

function get_text_input($name, $ar, $default)
{

$value = "";
if($default != ""){ $value = $default; }
$htm = "
<div class='form-group t-17'>

				<label for='".$ar["id"]."' class='sm-17'>
					".one_string($ar["label"])."
				</label>

				<input type='hidden' id='".$name."_required' value='".$ar["required"]."' />
				<input type='hidden' id='".$name."_warning' value='".$ar["warning"]."' />
				<input type='hidden' id='".$name."_warning_label' value='".$ar["warning_label"]."' />
				<input type='hidden' id='".$name."_value' value='".$default."' />

</div>

";
return $htm;	
	
}

function get_checkbox_group($name, $ar, $default)
{
	

$options = "";
$exceptions = "";
$e = "";
$del = "";
// create json from options
$options_json = json_encode($ar["options"]);
$options = str_replace('"', '^', $options_json);
// identify current values
$value = $_SESSION["order"]["info"]["marketing"][$name];
$current = array();
$vals = explode("|", $value);
foreach($vals as $key => $content)
	{
	$props = explode("~", $content);
	$propkey = $props[0];
	$propval = "";
	if(count($props) > 1)
		{
		$propval = $props[1];	
		}
	$current[$propkey] = $propval;
	}
/*echo("<textarea style='width: 99%; height: 350px;'>");
print_r($current);
echo("</textarea>");*/

$htm = "
<div class='form-group t-17 group-inline'>

				<span  class='label sm-17'>
					".one_string($ar["label"])."
				</span>

				<div class ='checkbox-item'>
";	
$opts = $ar["options"];
if($ar["sorted"]){ sort($opts); }

foreach($opts as $key => $arra)
	{ 
	$checked = "";
	$display = "none";
	$label = $arra["label"];
	$id = $arra["id"];
	$exception = $arra["exception"];
	$exlabel = $arra["explabel"];
	$placeholder = $arra["placeholder"];
	$xval = "";
	$valu = "";
	$e = "";
	if(isset($current[$label]))
		{ 
		if($exception > 0)
			{
			if(strlen($current[$label]) > 0)
				{
				$xval = str_replace('"', '\"', $current[$label]);
				$xval = str_replace("'", "\'", $xval);
				$valu = " value=\"".$xval."\"";	
				}
			$display = "inline";
			}
		$checked = " checked"; 
		}
	if($exception > 0)
		{
		// initialize variables
		$start = "";
		$end = "";
		$ele = "";
		$eleid = $name."_".$id."_value";
		$container_start = "<span id='".$name."_".$id."_container' style='display: ".$display.";'>";
		// check if label should be displayed or not
		if($exlabel != "")	
			{
			$start = "

			<label for='".$eleid."' clas='marketing-text chk-desc'>".$label."</label>
		";
			$end = "
	";
			}
		// create exception element
		$master = "document.getElementById('chk_".$name."_".$id."')";
		$oc = "onchange=\"marketing_checkbox_group_change('".$name."', ".$master.", '".$name."_options')\"";
		$ele = "<input type='text' 
					   id='".$eleid."' 
					   name='".$name."' 
					   placeholder='".$placeholder."' 
					   ".$valu." 
					   class='form-control ib chk-inpt'
					   ".$oc." />
";
		$e = $container_start.$start.$ele.$end."</span>";
		}
		
	$htm .= "
					<div class='checkbox-item'>

									<input type='checkbox'
									       class='check-style'
										   value='".$label."'
										   ".$checked." 
										   id='chk_".$name."_".$id."' 
										   onclick=\"marketing_checkbox_group_change('".$name."', this, '".$name."_options')\" />

									<label for='chk_".$name."_".$id."' >".$label."</label>

									".$e."


					</div>
";
	}
$htm .= "
					<div style='clear: both; display: none;'></div>
						<input type='hidden' id='".$ar["id"]."_required' value='".$ar["required"]."' />
						<input type='hidden' id='".$ar["id"]."_warning' value='".$ar["warning"]."' />
						<input type='hidden' id='".$ar["id"]."_warning_label' value='".$ar["warning_label"]."' />
						<input type='hidden' id='".$name."_value' value='".$default."' />
					<div style='clear: both; display: none;'></div>
					<div style='width: 10px;'></div>
				</div>

	<input type=\"hidden\" id=\"".$name."_options\" value=\"".$options."\" />
</div>
";
//exit();
return $htm;

}

function one_string($text)
{
$subject = $text;
$search = " ";
$replace = "&nbsp;";
return str_replace($search, $replace, $subject);	
	
}

?>