<?php   
$head = true;
$wfull = 600;
$w1 = 250;
$w2 = 75;
$w3 = 75;
$w4 = 75;
$w5 = 125;
?>
<center><h2><?php echo($lang["order_info"]); ?></h2></center>
<table style="table-layout: fixed; width: <?php echo($wfull); ?>px;" cellspacing="0" cellpadding="15">
	<tr>
		<td class=cart-table-head align=center style="width: <?php echo($w1); ?>px;"><?php echo($lang["cart_label_description"]); ?></td>
		<td class=cart-table-head align=center style="width: <?php echo($w2); ?>px;"><?php echo($lang["cart_label_quantity"]); ?></td>
		<td class=cart-table-head align=center style="width: <?php echo($w3); ?>px;">&nbsp;</td>
		<td class=cart-table-head align=center style="width: <?php echo($w4); ?>px;"><?php echo($lang["cart_label_price"]); ?></td>
		<td class=cart-table-head align=center style="width: <?php echo($w5); ?>px;"><?php echo($lang["cart_label_inscription"]); ?></td>
	</tr>