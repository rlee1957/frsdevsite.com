<?php

$fn = "";
$mn = "";
$ln = "";
$country = "";
$company = "";
$address1 = "";
$address2 = "";
$city = "";
$state = "";
$zip = "";
$phone = "";
$email = "";

if(isset($_SESSION["cart"]))
	{
	if(isset($_SESSION["cart"]["addresses"]))
		{
		if(isset($_SESSION["cart"]["addresses"][0]))
			{
			$fn = $_SESSION["cart"]["addresses"][0]["fn"];
			$mn = $_SESSION["cart"]["addresses"][0]["mn"];
			$ln = $_SESSION["cart"]["addresses"][0]["ln"];
			$country = $_SESSION["cart"]["addresses"][0]["country"];
			$company = $_SESSION["cart"]["addresses"][0]["company"];
			$address1 = $_SESSION["cart"]["addresses"][0]["address1"];
			$address2 = $_SESSION["cart"]["addresses"][0]["address2"];
			$city = $_SESSION["cart"]["addresses"][0]["city"];
			$state = $_SESSION["cart"]["addresses"][0]["state"];
			$zip = $_SESSION["cart"]["addresses"][0]["zip"];
			$phone = $_SESSION["cart"]["addresses"][0]["phone"];
			$email = $_SESSION["cart"]["addresses"][0]["email"];	
			}
		}		
	}
?>
<h2><?php echo($address_title); ?></h2>
<div style="width: 99%;"><!-- First, Middle, Last Name -->
	<div class="name" style="float: left; width: 45%;"><!-- First Name -->
		<input type=text	
			   id=<?php echo($address_type); ?>_fn<?php echo($address_index); ?>
			   name=<?php echo($address_type); ?>_fn<?php echo($address_index); ?>
			   placeholder="<?php echo($lang["fn_placeholder"]); ?>"
			   required
			   maxlength=50
			   style="width: 100%;"
			   value="<?php echo($fn); ?>" />
	</div>
	<div class="mname" style="float: left; width: 9%;"><!-- Middle Name -->
		<input type=text	
			   id=<?php echo($address_type); ?>_mn<?php echo($address_index); ?>
			   name=<?php echo($address_type); ?>_mn<?php echo($address_index); ?>
			   placeholder="<?php echo($lang["mn_placeholder"]); ?>"
			   required
			   maxlength=50 
			   style="width: 100%;"
			   value="<?php echo($mn); ?>" />
	</div>
	<div class="name" style="float: left; width: 45%;"><!-- Last Name -->
		<input type=text	
			   id=<?php echo($address_type); ?>_ln<?php echo($address_index); ?>
			   name=<?php echo($address_type); ?>_ln<?php echo($address_index); ?>
			   placeholder="<?php echo($lang["ln_placeholder"]); ?>"
			   required
			   maxlength=50 
			   style="width: 100%;"
			   value="<?php echo($ln); ?>" />
	</div>
	<div style="clear: both;"></div>
</div>
<div style="width: 99%;"><!-- Country -->
	<select id=<?php echo($address_type); ?>_country<?php echo($address_index); ?> 
			name=<?php echo($address_type); ?>_country<?php echo($address_index); ?>  
			style="width: 100%;" 
			required 
			onchange="country_change('<?php echo($address_type); ?>', '<?php echo($address_index); ?>');"
<?php
if($country == "")
	{
	echo("
			value='".$default["country"]."'>
");	
	}
else
	{
	echo("
			value='".$country."'>
");	
	}
?>
		<option value="" disabled><?php echo($lang["country_placeholder"]); ?></option>
<?php
if($country == ""){ $default = $default["country"]; }
else{ $default = $country; }
$selected = "";
include($path."../_t2/configuration/country_list.php");	
foreach($country_list as $key => $country)	
	{
	if($key == $default){ $selected = " selected "; }
	else{ $selected = ""; }
	echo("
		<option value='".$key."'".$selected.">".$country."</option>
");
	}		
?>
	</select>
</div>
<div style="width: 99%;"><!-- Company -->
	<input type=text	
		   id=<?php echo($address_type); ?>_company<?php echo($address_index); ?> 
		   name=<?php echo($address_type); ?>_company<?php echo($address_index); ?> 
		   placeholder="<?php echo($lang['cart_shipping_company_placeholder']); ?>"
		   maxlength=255 
		   style="width: 99%;" />
</div>
<div style="width: 99%;"><!-- Address 1 -->
	<input type=text	
		   id=<?php echo($address_type); ?>_address1<?php echo($address_index); ?> 
		   name=<?php echo($address_type); ?>_address1<?php echo($address_index); ?> 
		   placeholder="<?php echo($lang["addr1_placeholder"]); ?>"
		   required
		   maxlength=255 
		   style="width: 99%;" />
</div>
<div style="width: 99%;"><!-- Address 2 -->
	<input type=text	
		   id=<?php echo($address_type); ?>_address2<?php echo($address_index); ?> 
		   name=<?php echo($address_type); ?>_address2<?php echo($address_index); ?> 
		   placeholder="<?php echo($lang["addr2_placeholder"]); ?>"
		   maxlength=255 
		   style="width: 99%;" />
</div>
<div style="width: 99%;"><!-- City, State, Zip -->
	<div style="float: left; width: 40%;"><!-- City -->
		<input type=text	
			   id=<?php echo($address_type); ?>_city<?php echo($address_index); ?>
			   name=<?php echo($address_type); ?>_city<?php echo($address_index); ?>
			   placeholder="<?php echo($lang["city_placeholder"]); ?>"
			   required
			   maxlength=255 
			   style="width: 100%;" />
	</div>
	<div style="float: left; width: 35%;"><!-- State -->
<?php
include($path."configuration/us_states.php");
$htm = "";
foreach($us_states as $key => $ar)
	{
	$htm .= "
						<option value='".$ar["abbreviation"]."'>".$ar["english"]."</option>
";	
	}
?>					
				<div id=<?php echo($address_type); ?>_state_container<?php echo($address_index); ?>>
					<select id=<?php echo($address_type); ?>_state_prov<?php echo($address_index); ?> 
							name=<?php echo($address_type); ?>_state_prov<?php echo($address_index); ?> 
							style="width: 100%;" 
							required>
						<option value="" disabled selected><?php echo($lang["state_placeholder"]); ?></option>
<?php
echo($htm);
?>								
					</select>						
				</div>
				<textarea id=<?php echo($address_type); ?>_us_states<?php echo($address_index); ?> style="display: none;">
					<select id=<?php echo($address_type); ?>_state_prov<?php echo($address_index); ?> 
							name=<?php echo($address_type); ?>_state_prov<?php echo($address_index); ?> 
							style="width: 100%;" 
							required>
						<option value="" disabled selected><?php echo($lang["state_placeholder"]); ?></option>
<?php
echo($htm);
?>								
					</select>
				</textarea>
				<textarea id=<?php echo($address_type); ?>_canada_provinces<?php echo($address_index); ?> style="display: none;">
					<select id=<?php echo($address_type); ?>_state_prov 
							name=<?php echo($address_type); ?>_state_prov 
							style="width: 100%;" 
							required>
						<option value="" disabled selected><?php echo($lang["province_placeholder"]); ?></option>
<?php
include($path."configuration/canada_provinces.php");
$htm = "";
foreach($canada_provinces as $key => $ar)
	{
	$htm .= "
						<option value='".$ar["abbreviation"]."'>".$ar["english"]."</option>
";	
	}
echo($htm);
?>								
					</select>
				</textarea>
				<textarea id=<?php echo($address_type); ?>_other_divisions<?php echo($address_index); ?> style="display: none;">
					<input type=text 
						   id=<?php echo($address_type); ?>_state_prov<?php echo($address_index); ?> 
						   name=<?php echo($address_type); ?>_state_prov<?php echo($address_index); ?> 
						   style="width: 100%;" 
						   required
						   placeholder="<?php echo($lang["division_placeholder"]); ?>">
				</textarea>
	</div>
	<div style="float: left; width: 24%;"><!-- Zip -->
				<div id=<?php echo($address_type); ?>_postal_code_container<?php echo($address_index); ?>>
					<input type=text	
						   id=<?php echo($address_type); ?>_postal_code<?php echo($address_index); ?>
						   name=<?php echo($address_type); ?>_postal_code<?php echo($address_index); ?>
						   placeholder="<?php echo($lang["zip_placeholder"]); ?>"
						   required
						   maxlength=5 
						   style="width: 100%;" />
				</div>
				<textarea id=<?php echo($address_type); ?>_us_zip<?php echo($address_index); ?> style="display: none;">
				<input type=text	
					   id=<?php echo($address_type); ?>_postal_code<?php echo($address_index); ?>
					   name=<?php echo($address_type); ?>_postal_code<?php echo($address_index); ?>
					   placeholder="<?php echo($lang["zip_placeholder"]); ?>"
					   required
					   maxlength=5 
					   style="width: 100%;" />
				</textarea>
				<textarea id=<?php echo($address_type); ?>_canada_postal_code<?php echo($address_index); ?> style="display: none;">
				<input type=text	
					   id=<?php echo($address_type); ?>_postal_code<?php echo($address_index); ?>
					   name=<?php echo($address_type); ?>_postal_code<?php echo($address_index); ?>
					   placeholder="<?php echo($lang["zip_placeholder"]); ?>"
					   required
					   maxlength=7 
					   style="width: 100%;" />
				</textarea>
				<textarea id=<?php echo($address_type); ?>_other_postal_code<?php echo($address_index); ?> style="display: none;">
				<input type=text	
				   id=<?php echo($address_type); ?>_postal_code<?php echo($address_index); ?>
				   name=<?php echo($address_type); ?>_postal_code<?php echo($address_index); ?>
				   placeholder="<?php echo($lang["zip_placeholder"]); ?>"
				   required
				   maxlength=25 
				   style="width: 100%;" />
				</textarea>
	</div>
</div>
<?php
if($address_type == "bill")
	{
?>
<div style="width: 99%;"><!-- Phone, Email -->
	<div style="float: left; width: 29%;" id=<?php echo($address_type); ?>_phone_container<?php echo($address_index); ?>><!--Phone-->
		<input type=text	
			   id=<?php echo($address_type); ?>_phone<?php echo($address_index); ?>
			   name=<?php echo($address_type); ?>_phone<?php echo($address_index); ?>
			   placeholder="<?php echo($lang["phone_placeholder"]); ?>"
			   required
			   maxlength=10 
			   style="width: 100%;" />
	</div>
	<textarea id=<?php echo($address_type); ?>_us_phone<?php echo($address_index); ?> style="display: none;">
	<input type=text	
			   id=<?php echo($address_type); ?>_phone<?php echo($address_index); ?>
			   name=<?php echo($address_type); ?>_phone<?php echo($address_index); ?>
			   placeholder="<?php echo($lang["phone_placeholder"]); ?>"
			   required
			   maxlength=10 
			   style="width: 100%;" />
	</textarea>
	<textarea id=<?php echo($address_type); ?>_other_phone<?php echo($address_index); ?> style="display: none;">
	<input type=text	
	   id=<?php echo($address_type); ?>_phone
	   name=<?php echo($address_type); ?>_phone
	   placeholder="<?php echo($lang["phone_placeholder"]); ?>"
	   required
	   maxlength=20 
	   style="width: 100%;" />
	</textarea>

	<div style="float: left; width: 70%;" id=<?php echo($address_type); ?>_email_container<?php echo($address_index); ?>><!--email-->
				<input type=text	
					   id=<?php echo($address_type); ?>_email<?php echo($address_index); ?>
					   name=<?php echo($address_type); ?>_email<?php echo($address_index); ?>
					   placeholder="<?php echo($lang["email_placeholder"]); ?>"
					   required
					   maxlength=255 
					   style="width: 100%;"/>
	</div>
	<div style="clear: both;"></div>
</div>
<?php
	}
include($path."../_t2/includes/separator.php");
?>