<div>
<?php
if(
	($options["enable_replica"])&&
	(strtolower($_SESSION["g_bkrdesc"][$idx]) != "not offered")&&
	(trim($_SESSION["g_bkrdesc"][$idx]) != "")
  )
	{
	# REPLICA 1
	$image = $_SESSION["cart"][$_SESSION["current_cart_item"]]["replica1_image"];
	$description = $_SESSION["cart"][$_SESSION["current_cart_item"]]["replica1_description"];
	$price = $_SESSION["cart"][$_SESSION["current_cart_item"]]["replica1_price"];
	$qty_label = $lang['cart_quantity'];
	$qty = $_SESSION["cart"][$_SESSION["current_cart_item"]]["replica1_quantity"];
	$minus = "<button onclick='decrement(\"r1_qty\");' class=decrement> - </button>";
	$qty_amt = "<input type=text class=int-quantity maxwidth=2 value='".$qty."' id=r1_qty name=r1_qty readOnly />";
	$plus = "<button onclick='increment(\"r1_qty\");' class=increment> + </button>";
	include($path."../_t2/includes/option_item.php");
	}
if(
	($options["enable_displaycase"])&&
	(strtolower($_SESSION["g_bkdcdesc"][$idx]) != "not offered")&&
	(trim($_SESSION["g_bkdcdesc"][$idx]) != "")
  )
	{
	# DISPLAY CASE 1
	$image = $_SESSION["cart"][$_SESSION["current_cart_item"]]["display_case1_image"];
	$description = $_SESSION["cart"][$_SESSION["current_cart_item"]]["display_case1_description"];
	$price = $_SESSION["cart"][$_SESSION["current_cart_item"]]["display_case1_price"];
	$qty_label = $lang['cart_quantity'];
	$qty = $_SESSION["cart"][$_SESSION["current_cart_item"]]["display_case1_quantity"];
	$minus = "<button onclick='decrement(\"dc1_qty\");' class=decrement> - </button>";
	$qty_amt = "<input type=text class=int-quantity value='".$qty."' id=dc1_qty name=dc1_qty readOnly />";
	$plus = "<button onclick='increment(\"dc1_qty\");' class=increment> + </button>";
	include($path."../_t2/includes/option_item.php");
	}
if(
	($options["enable_replica2"])&&
	(strtolower($_SESSION["g_bkr2desc"][$idx]) != "not offered")&&
	(trim($_SESSION["g_bkr2desc"][$idx]) != "")
  )
	{
	# REPLICA 2
	$image = $_SESSION["cart"][$_SESSION["current_cart_item"]]["replica2_image"];
	$description = $_SESSION["cart"][$_SESSION["current_cart_item"]]["replica2_description"];
	$price = $_SESSION["cart"][$_SESSION["current_cart_item"]]["replica2_price"];
	$qty_label = $lang['cart_quantity'];
	$qty = $_SESSION["cart"][$_SESSION["current_cart_item"]]["replica2_quantity"];
	$minus = "<button onclick='decrement(\"r2_qty\");' class=decrement> - </button>";
	$qty_amt = "<input type=text class=int-quantity value='".$qty."' id=r2_qty name=r2_qty readOnly />";
	$plus = "<button onclick='increment(\"r2_qty\");' class=increment> + </button>";
	include($path."../_t2/includes/option_item.php");
	}
if(
	($options["enable_displaycase2"])&&
	(strtolower($_SESSION["g_bkdc2desc"][$idx]) != "not offered")&&
	(trim($_SESSION["g_bkdc2desc"][$idx]) != "")
  )
	{
	# DISPLAY CASE 2
	$image = $_SESSION["cart"][$_SESSION["current_cart_item"]]["display_case2_image"];
	$description = $_SESSION["cart"][$_SESSION["current_cart_item"]]["display_case2_description"];
	$price = $_SESSION["cart"][$_SESSION["current_cart_item"]]["display_case2_price"];
	$qty_label = $lang['cart_quantity'];
	$qty = $_SESSION["cart"][$_SESSION["current_cart_item"]]["display_case2_quantity"];
	$minus = "<button onclick='decrement(\"dc2_qty\");' class=decrement> - </button>";
	$qty_amt = "<input type=text class=int-quantity value='".$qty."' id=dc2_qty name=dc2_qty readOnly />";
	$plus = "<button onclick='increment(\"dc2_qty\");' class=increment> + </button>";
	include($path."../_t2/includes/option_item.php");
	}
	
?>
</div>