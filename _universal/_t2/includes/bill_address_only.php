	<tr>
		<td style="width: 480px;" align=center><!-- First, Middle, Last Name -->
			<div style="float: left; width: 200px; overflow: hidden;"><!-- First Name -->
				<input type=text	
					   id=<?php echo($address_type); ?>_fn
					   name=<?php echo($address_type); ?>_fn
					   placeholder="<?php echo($lang["fn_placeholder"]); ?>"
					   required
					   maxlength=255
					   class="ib"
					   style="width: 188px; float: left;"
					   value="<?php echo($fn); ?>"
					   readOnly />
			</div>
			<div style="float: left; width: 10px; height: 26px;"><!-- spacer --></div>
			<div style="float: left; width: 60px; overflow: hidden;"><!-- Middle Name -->
				<input type=text	
					   id=<?php echo($address_type); ?>_mn
					   name=<?php echo($address_type); ?>_mn
					   placeholder="<?php echo($lang["mn_placeholder"]); ?>"
					   required
					   maxlength=50 
					   class="ib"
					   style="width: 48px;"
					   value="<?php echo($mn); ?>"
					   readOnly />
			</div>
			<div style="float: left; width: 10px; height: 26px;"><!-- spacer --></div>
			<div style="float: left; width: 200px; overflow: hidden;"><!-- Last Name -->
				<input type=text	
					   id=<?php echo($address_type); ?>_ln
					   name=<?php echo($address_type); ?>_ln
					   placeholder="<?php echo($lang["ln_placeholder"]); ?>"
					   required
					   maxlength=50 
					   class="ib"
					   style="width: 188px; float: right;"
					   value="<?php echo($ln); ?>"
					   readOnly />
			</div>
			<div style="clear: both;"></div>
		</td>
	</tr>
	<tr>
		<td style="width: 480px;"><!-- Country -->
			<input type=text 
				   id=<?php echo($address_type); ?>_country 
				   name=<?php echo($address_type); ?>_country  
				   style="width: 480px;" 
				   class="ib"
				   value="<?php echo($country); ?>"
				   readOnly 
				   value="<?php echo($country); ?>"/>
		</td>
	</tr>
	<tr>
		<td style="width: 480px;"><!-- Company -->
			<input type=text	
				   id=<?php echo($address_type); ?>_company 
				   name=<?php echo($address_type); ?>_company 
				   value="<?php echo($company); ?>"
				   placeholder="<?php echo($lang['cart_shipping_company_placeholder']); ?>"
				   style="width: 480px;"
				   class="ib"
				   readOnly />
		</td>
	</tr>
	<tr>
		<td style="width: 480px;"><!-- Address 1 -->
			<input type=text	
				   id=<?php echo($address_type); ?>_address1 
				   name=<?php echo($address_type); ?>_address1 
				   value="<?php echo($address1); ?>"
				   placeholder="<?php echo($lang["addr1_placeholder"]); ?>"
				   style="width: 480px;"
					   class="ib"
				   readOnly />
		</td>
	</tr>
	<tr>
		<td style="width: 480px;"><!-- Address 2 -->
			<input type=text	
				   id=<?php echo($address_type); ?>_address2 
				   name=<?php echo($address_type); ?>_address2 
				   value="<?php echo($address2); ?>"
				   placeholder="<?php echo($lang["addr2_placeholder"]); ?>"
				   style="width: 480px;"
					   class="ib"
				   readOnly />
		</td>
	</tr>
	<tr>
		<td style="width: 480px;"><!-- City, State, Zip -->
			<div style="width: 190px; float: left; overflow: hidden;"><!-- City -->
				<input type=text	
					   id=<?php echo($address_type); ?>_city
					   name=<?php echo($address_type); ?>_city
					   value="<?php echo($city); ?>"
					   placeholder="<?php echo($lang["city_placeholder"]); ?>"
					   style="width: 178px;"
					   class="ib"
					   readOnly />
			</div>
			<div style="float: left; width: 10px; height: 26px;"><!-- spacer --></div>
			<div style="width: 157px; float: left; overflow: hidden;"><!-- State -->
				<input type=text 
					   id=<?php echo($address_type); ?>_state_prov 
					   name=<?php echo($address_type); ?>_state_prov 
					   style="width: 145px;" 
					   class="ib"
					   readOnly
					   value="<?php echo($state); ?>" />
			</div>
			
			<div style="float: left; width: 10px; height: 26px;"><!-- spacer --></div>
			<div style="width: 113px; float: left; overflow: hidden;"><!-- Zip -->
				<input type=text	
					   id=<?php echo($address_type); ?>_postal_code
					   name=<?php echo($address_type); ?>_postal_code
					   placeholder="<?php echo($lang["zip_placeholder"]); ?>"
					   style="width: 101px;"
					   class="ib"
					   value="<?php echo($zip); ?>" 
					   readOnly />
			</div>
			<div style="clear: both;"></div>
		</td>
	</tr>