<center>
<div class=fs-container>
<div class=fs-title><?php echo($lang["page"]["story"]["featured-head"]); ?></div>
<table class=fs-table>
	<tr>
		<td style=fs-3rd></td>
		<td style=fs-3rd></td>
		<td style=fs-3rd></td>
	</tr>
	
<?php
include($path."configuration/story_settings.php");
$pm = -1;
$ct = 0;
foreach($stories as $key => $story)
	{
	if($ct > 0)
	{
?>
	<tr>
		<td class=fs-pipe colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
<?php
	}
?>
	<tr>
		<td class=fs-submitter colspan=3 align=left><?php echo($story["name"]); ?></td>
	</tr>
	<tr>
		<td class=fs-date colspan=3 align=left><?php echo($story["date"]); ?></td>
	</tr>
<?php
	if($pm < 0)
		{
		$c1 = "<td class=fs-image-container valign=top><img src='".$story["image"]."' class=fs-image /></td>";
		$c2 = "<td class=fs-text colspan=2 valign=top>".$story["text"]."</td>";
		}
	else
		{
		$c1 = "<td class=fs-text colspan=2 valign=top>".$story["text"]."</td>";
		$c2 = "<td class=fs-image-container valign=top><img src='".$story["image"]."' class=fs-image /></td>";
		}

?>
	<tr>
		<?php echo($c1); ?>
		<?php echo($c2); ?>
	</tr>
<?php
	$pm = $pm * -1;
	$ct++;
	}

?>
</table>
<div class=fs-spacer></div>
</div>
</center>