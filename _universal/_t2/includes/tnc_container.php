			<div class="modal fade hide" 
				 id="termsModal" 
				 tabindex="-1" 
				 role="dialog" 
				 aria-labelledby="termsModalLabel" 
				 aria-hidden="true" 
				 data-remote="actions/terms.html">
				<div class="modal-header">
					<h3 id="termsModalLabel">
						<?php echo $lang["page"]["tnc"]["header"]; ?>
					</h3>
				</div>
				<div class="modal-body">
					<p><?php echo $lang["page"]["tnc"]["header"]; ?></p>
				</div>
				<div class="modal-footer">
					<span><?php echo $lang["page"]["tnc"]["close_instructions"]; ?></span>
				</div>
			</div>