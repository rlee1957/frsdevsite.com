<?php

include($path."configuration/image_settings.php");
include($path."configuration/misc_settings.php");
include($path."configuration/color_settings.php");

?>
<div class=faq-top-pad>&nbsp;</div>
<div class="faq-content">
	<center>
		<div class="faq-all">
			<div class="faq-head">
				<span class="faq-title"><?php echo($lang["page"]["faq"]["title"]); ?></span><!--
				<p class="faq-introduction"><?php echo($lang["page"]["faq"]["instruction"]); ?></p>
				<input type=search 
					   onkeyup="search(this);" 
					   placeholder="<?php echo($lang["page"]["faq"]["search"]); ?>" 
					   class=search-button
					   id=faq_search />-->
			</div>
			<div id="faqs">

<?php

include($path."configuration/faq_content.php");

?>
			</div>
		</div>
	</center>
</div>
<div id="end"></div>
<div class=faq-bottom-pad>&nbsp;</div>
