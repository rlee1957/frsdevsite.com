<div id=content2 style="display: <?php echo($display[2]); ?>;" class=landing-container>

<?php
if($options["head_spacer"])
	{
	include("../_t2/elements/head_spacer.php");	
	}
include("../_t2/database/get_pavers.php");
if($show_page_name)
	{
?>
<h3>content2.php</h3>
<?php
	}
?>
<center>
	<br /><br />
	<div class=shop-title>
		<img class=gc-image style="width: 500px;" src='images/<?php echo($image['gift_certificate']); ?>' />
	</div>
	<hr />
	<br />
	<div style="width: 900px; text-align: left;">
	<span class=gc-label><?php echo($lang["gift_certificate"]); ?></span>
	<br />
	<table>
		<tr>
			<td style="font-size: 18pt; padding: 0 30px 0 0;" ><?php echo($lang["select_brick"]); ?></td>
			<td>
				<select id=select_item class=select-paver onchange="select_gc(this.value,  'yes');">
					<option value="" selected disabled><?php echo($lang["select_brick"]); ?></option>
<?php
foreach ($pavers as $key => $row) 
	{
	$value = $row["item_number"];
	$label = $row["description"]." - ".$lang["currency_symbol"].number_format($row["price"], 2);
    echo("
					<option value='".$value."'>".$label."</option>
");
    }

	
?> 						
				</select>
			</td>
		</tr>
	</table>
	</div>
	<div id="gc_separator" style="display: none;"><hr /></div>
	<div id="the_options" style="text-align: left; width: 900px;">
	</div> 
</center>
<?php
if($options["foot_spacer"])
	{
	include("../_t2/elements/foot_spacer.php");	
	}
?>
</div>