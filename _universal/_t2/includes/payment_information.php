<?php
$htm .= "<div class='text-right'>";
if($_SESSION["order"]["info"]["amount"]["order_total"] != $_SESSION["order"]["info"]["amount"]["sub_total"])
	{
        $htm .="<div class='total'>".$lang["page"]["email"]["subtotal-label"]."<span class='wrap'><span class='pull-left'>".$lang["currency_symbol"]."</span>".number_format($_SESSION["order"]["info"]["amount"]["sub_total"], 2, '.', ',')."</span></div>";
        $htm .="<div class='total'>".$lang["page"]["email"]["promo-labe"]."<span class='wrap'><span class='pull-left'>".$lang["currency_symbol"]."</span>".number_format($_SESSION["order"]["info"]["amount"]["promotional_discount"], 2, '.', ',')."</span></div>";
        $htm .="<div class='total'>".$lang["page"]["email"]["fee-label"]."<span class='wrap'><span class='pull-left'>".$lang["currency_symbol"]."</span>".number_format($_SESSION["order"]["info"]["amount"]["convenience_fee"], 2, '.', ',')."</span></div>";
        $htm .="<div class='total'>".$lang["page"]["email"]["tax-label"]."<span class='wrap'><span class='pull-left'>".$lang["currency_symbol"]."</span>".number_format($_SESSION["order"]["info"]["amount"]["sales_tax"], 2, '.', ',')."</span></div>";
        $htm .="<strong class='subtotal'>".$lang["page"]["email"]["total-label"]."<span>".$lang["currency_symbol"]."</span>".number_format($_SESSION["order"]["info"]["amount"]["order_total"], 2, '.', ',')."</strong>";
 	}
$htm .="<div class='total'>".$lang["page"]["email"]["total-label"]."<span class='wrap'><span class='pull-left'>".$lang["currency_symbol"]."</span>".number_format($_SESSION["order"]["info"]["amount"]["order_total"], 2, '.', ',')."</span></div>";
$htm .= "</div>";