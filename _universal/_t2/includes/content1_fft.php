<div id=content1 style="display: <?php echo($display[1]); ?>;" class=landing-container>

<?php
include("../_t2/includes/show_errors.php");
include("../_t2/includes/session_start.php");
include("../_t2/includes/initialize_session.php");
$quantities = "";
$page = "shop";
$del = "";
if($options["head_spacer"])
	{
	include("../_t2/elements/head_spacer.php");	
	}
if($show_page_name)
	{
	echo("<h3>content1.php</h3>");
	}
include("../_t2/database/get_fullfillment_items.php");

?>
<div class="container first">
	<!--div class=shop-title><?php echo $lang['shop-title']; ?></div-->
	<p class="text-reg text-center"><?php echo $lang['shop-title']; ?></p>
	<br />
	<input type="hidden" id=bktype name=bktype />
	<div class="shop">
		<div class="row">
			<div class="col-md-2"></div>
				<?php

				$available_options = "";
				$htm = "";						
				foreach($fullfillment_items as $i => $row)
					{
					$qty = 0;
					$_SESSION["order"]["cart"][$row["item_id"]] = $row;
					if(session_status() == PHP_SESSION_NONE)
						{
						$qty = 0;	
						}
					else
						{
						if(isset($_SESSION["order"]["cart"][$row["item_id"]]["quantity"]))	
							{ 
							$qty = $_SESSION["order"]["cart"][$row["item_id"]]["quantity"];
							}
						}
					$available_options .= $del.$row["item_id"];
					$del = "|";
					$id = $row["item_id"];
					$show_text = false;
					$image = $row["item_image_default"];
					$htm .= "
					<div class='col-md-4'>
						<table cellspacing=0 cellpadding=20>
							<tr>				
				";
					$htm .= "
								<td colspan=3 class=option-image align=center valign=bottom style='width: 300px;'>
									<div style='width: 250px;'>
										<img src='images/".$image."' style='width: 250px;' />
									</div>
								</td>
							</tr><tr>
								<td colspan=3 class=fft-option-label align=center>".$row["description"]."</td>
							</tr><tr>
								<td class=option-price align=right style='width: 110px;' valign=bottom>
									".$lang["currency_symbol"].number_format($row["price"], 2)."&nbsp;
								</td>	
								<td class=option-qty-label align=right style='width: 40px;' valign=bottom>
									".$lang['cart_quantity']."&nbsp;&nbsp;&nbsp;
								</td>
								<td class='option-qty-amt wrap-count' align=left style='width: 100px;' valign=bottom>
				";
					$control_name = $page."_option_".$id;
					$refresh = "do_nothing();";
					$control_pm_height = 25;
					$control_pm_width = 25;
					$control_plus_top = 0;
					$control_minus_top = 0;
					$control_value_left = $control_pm_width + 1;
					$control_value_top= 0;
					$control_value_height = 25;
					$control_value_width = 32;
					$control_height	= 35;
					$control_width = ((($control_pm_width * 2) + $control_value_width) + 7);
					$quantity = $qty;
					$quantities = $del.$qty;
					$del = "|";
					# Return value is stored in $quantity_control variable
					include($path."../_t2/elements/quantity_control_fft.php");
					$htm .= $quantity_control;
					$htm .= "
								</td>	
							</tr>
						</table>
					</div>
				";
				    }
				echo($htm);

				?>  
		</div>
	</div>
</div>
<br /><br />
<center>
<a href="javascript: save_options_fft();" class="add-to-cart">
	<?php echo($lang["add_to_cart"]); ?>
</a>
</center>
<br /><br /><br />
<?php
if($show_session)
	{
	$label = "Session Variables";
	$var_array = $_SESSION;
	include($path."../_t2/includes/show_array.php");
	}
if($show_order)
	{
	$label = "Order Variables";
	$var_array = $_SESSION["order"];
	include($path."../_t2/includes/show_array.php");
	}
if($options["foot_spacer"])
	{
	include("../_t2/elements/foot_spacer.php");	
	}

?>
<input type=hidden 
	   id=available_options 
	   value="<?php echo($available_options); ?>" />
<input type=hidden 
	   id=available_option_quantity 
	   value="<?php echo($quantities); ?>" />
</div>