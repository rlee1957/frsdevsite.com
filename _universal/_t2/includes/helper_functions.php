<?php

function get_inscription($paver)
{
$ins = "";
$del = "";
foreach($paver["inscription"] as $line => $txt)
	{
	$ins .= $del.$txt;
	$del = "<br />";
	}	
return "<center>".$ins."</center>";
}

function get_image($image, $path)
{
$image_file = $image;
include($path."configuration/misc_settings.php");
$link = "https://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
if(strpos($link, "DEVELOPMENT"))
	{
	$image_file = "https://www.brickorder.com/DEVELOPMENT/template2/".$_SESSION["store_folder"]."/images/".$image_file;	
	}
else
	{
	$image_file = "https://www.brickorder.com/".$_SESSION["store_folder"]."/images/".$image_file;		
	}
// echo($image_file."<br />");
return $image_file;
}

function get_options($paver)
{
$options = array();
foreach($paver["options"] as $oid => $option)	
	{
	if($option["quantity"] > 0)
		{
		foreach($option["shipto_addresses"] as $idx => $aid)	
			{
			if(!isset($options[$oid]))	
				{
				$options[$oid] = array();
				$opt = array();
				$opt[$aid]	= array();
				$opt[$aid]["description"] = $option["description"];
				$opt[$aid]["count"] = 1;
				$options[$oid] = $opt;
				}
			else
				{
				if(!isset($options[$oid][$aid]))
					{
					$opt = array();
					$opt["description"] = $option["description"];
					$opt["count"] = 1;
					$options[$oid][$aid] = $opt;
					}
				else
					{
					$options[$oid][$aid]["count"]++;
					}					
				}
			}
		}
	}
return $options;
}

function is_iyb($paver)
{
$iyb = true;
if($paver["gift_certificate"] == "yes" ){ $iyb = false; }
return $iyb;
}

function get_row_count($options)
{
$count = 0;
foreach($options as $oid => $aid)	
	{
	$count += count($aid);
	}
return $count;	
}

function get_address($aid)
{
$address = $_SESSION["order"]["addresses"][$aid]["fn"]."&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["ln"].",&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["address1"].",&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["city"].",&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["state_prov"]."&nbsp;";
$address .= $_SESSION["order"]["addresses"][$aid]["postal_code"];
return $address;
}

?>