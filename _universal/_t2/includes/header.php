<div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="fa fa-times"></span>
        </button>
        <!--	 onclick="toggle_side_cart();"-->
        <a class="cart-box" href="#"


        onclick="show_cart_detail_fft();"

            >
            <span class="cart-icon" id="order_count">0</span>
            <span class="hidden-sm hidden-xs"><?php echo($lang["page"]["side_cart"]["access"]); ?></span>
        </a>
        <a class="navbar-brand" href="<?php echo($image['header_url']); ?>" target="_blank"><img src="<?php echo($path); ?>images/<?php echo($image["head"]); ?>" /></a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
        	<?php

			foreach($menu as $key => $val)
				{
				if($val["show"])	
					{
					$onclick = "";
					if($val["onclick"] != ""){ $onclick = " onclick=\"".$val["onclick"].";\""; }
					$label = $lang[$val["label"]];
					echo("<li><a href='#'".$onclick.">".$label."</a></li>
					");	
				}
			}
			?>
		</ul>
	</td>
    <!--/.nav-collapse -->
</div>
</div>

<div id="cart_blanket" class="popup_bg" style="display: none;" onclick="hide_side_cart();"></div>
<div id="side_cart" class="side-cart" cellpadding=0 cellspacing=0 border=0>
    <!--<div class=side-template>
        Side Cart Template-->
        <div class="side-head" onclick="hide_side_cart();">
            <div class="side-close">></div>
            <center><span><?php echo($lang["page"]["sidecart"]["head"]); ?><span></center>
        </div>
        <div class="side-cart-content" id="side_cart_content"></div>
        <div class="side-foot">
            <center>
                <div style="height: 11px;"></div>
                <a href="javascript: show_cart_detail();" class="add-to-cart">
                    <?php echo($lang["page"]["sidecart"]["details-button"]); ?>
                </a>
            </center>
        </div>
    <!--</div>-->
</div>