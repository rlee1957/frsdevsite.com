<?php 

$refresh = "";
$description = $oarr["description"];
$image = $oarr["item_image_blank"];
if($item["gift_certificate"] == "yes"){ $image = $oarr["item_image_default"]; }
$price = $oarr["price"];
$id = $oarr["item_number"];
$quantity = $oarr["quantity"];
$ship_address = $oarr["ship_address"];	
$ref = $key."_".$oid;
$nme = "option_".$ref."_qty";
$minus = "<button onclick='do_decrement(\"".$nme."\", \"".$refresh."\");' class=decrement> - </button>";
$qty_amt = $quantity;
$plus = "<button onclick='do_increment(\"".$nme."\", \"".$refresh."\");' class=increment> + </button>";
$remove = "<button onclick='zero_qty(\"".$nme."\");' class=increment> Remove </button>";
$subtotal = $quantity * $price;
$class = "cart-table-option-separater";
$refresh = "";
if(!$head){ include($path."../_t2/includes/item_separator.php"); }
?>

	<tr>
		<td align=center>
			<span class="cart-description"><?php echo("&nbsp;&nbsp;<b>".$description."</b>&nbsp;&nbsp;"); ?></span><br />
			<span class="cart-price">
				<?php 
				echo($lang["cart_price_each"]);
				echo($lang["currency_symbol"]); 
				printf("%2.2f", $price); 
				?>
			</span>
		</td>
		<td align=center><?php echo($qty_amt); ?></td>
		<td>&nbsp;</td>
		<td align=right><?php echo($lang["currency_symbol"]); printf("%2.2f", $subtotal); ?></td>
		
	</tr>