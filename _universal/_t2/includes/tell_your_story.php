<?php

$story_div_start = "<center>";
$story_div_end = "</center>";
$story_div_style = "background-color: #007B9A; color: #FFFFFF; padding: 25px;";
$story_fld_style = "background-color: #A6D1DC; color: #000000;";
$story_ttl_style = "font-size: 24pt;";
$story_int_style = "font-size: 9pt;";
$story_submit_style = "border-color: #89BF24; 
background-color: #89BF24; 
color: #FFFFFF;";
$story_submit_hover = "border-color: #007B9A; 
background-color: #007B9A; 
color: #FFFFFF;";
$story_image_style = "border-color: #89BF24; 
background-color: #FFFFFF; 
color: #89BF24;";
$story_image_hover = "border-color: #89BF24; 
background-color: #89BF24; 
color: #FFFFFF;";

$story_fn = "";
$story_ln = "";
$story_phone = "";
$story_email = "";
if(isset($_SESSION["order"]["addresses"][0]["fn"]))
	{ 
	$story_fn = $_SESSION["order"]["addresses"][0]["fn"]; 
	}
if(isset($_SESSION["order"]["addresses"][0]["ln"]))
	{ 
	$story_ln = $_SESSION["order"]["addresses"][0]["ln"]; 
	}
if(isset($_SESSION["order"]["addresses"][0]["phone"]))
	{ 
	$story_phone = $_SESSION["order"]["addresses"][0]["phone"]; 
	}
if(isset($_SESSION["order"]["addresses"][0]["email"]))
	{ 
	$story_email = $_SESSION["order"]["addresses"][0]["email"]; 
	}

?>
<div style="<?php echo($story_div_style); ?>" id="story_share">
<style>
.story_button
{
	
border-style: solid;
border-width: 2px;
padding: 8px;
cursor: pointer;	
	
}

.submit_btn
{
<?php echo($story_submit_style); ?>
}

.submit_btn:hover
{
<?php echo($story_submit_hover); ?>
}

.image_btn
{
<?php echo($story_image_style); ?>
}

.image_btn:hover
{
<?php echo($story_image_hover); ?>	
}
</style>
<input type=hidden id=story_title value="<?php echo($lang["story_header"]); ?>" />
<input type=hidden id=story_file_pre_msg value="<?php echo($lang["story_file_pre_msg"]); ?>" />
<input type=hidden id=story_file_suff_msg value="<?php echo($lang["story_file_suff_msg"]); ?>" />
<input type=hidden id=story_fn_msg value="<?php echo($lang["story_fn_msg"]); ?>" />
<input type=hidden id=story_ln_msg value="<?php echo($lang["story_ln_msg"]); ?>" />
<input type=hidden id=story_phone_msg value="<?php echo($lang["story_phone_msg"]); ?>" />
<input type=hidden id=story_email_msg value="<?php echo($lang["story_email_msg"]); ?>" />

	<?php echo($story_div_start); ?>
	<!--<script language="javascript" type="text/javascript" src="../_t2/js/tell_your_story.js"></script>-->
	<table cellspacing=0 style="table-layout: fixed; width: 900px;" cellpadding=0>
		<tr style="height: 2px;">
			<td style="width: 225px;">&nbsp;</td>
			<td style="width: 675px;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan=2 align=center>
				<span style="<?php echo($story_ttl_style); ?>" ><?php echo($lang['story_header']); ?><span>
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<div><?php echo($lang['story_introduction']); ?></div>
			</td>
		</tr>
		<tr>
			<td style="padding-right: 2px;">
				<input type=text placeholder="First Name" id=story_fn 
					   style="height: 30px; width: 190px; <?php echo($story_fld_style); ?>"
					   onblur="document.getElementById('db_story_fn').value = this.value.trim();"
					   value="<?php echo($story_fn); ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td rowspan=4 valign=top>
				<textarea placeholder="<?php echo($lang['story_header']); ?>" 
						  id=story_text 
						  onblur="document.getElementById('db_story_text').value = this.value.trim();"
						  style="height: 200px; width: 660px; <?php echo($story_fld_style); ?>"></textarea>
			</td>
		</tr>
		<tr>
			<td style="padding-right: 2px;">
				<input type=text placeholder="Last Name" id=story_ln 
					   style="height: 30px; width: 190px; <?php echo($story_fld_style); ?>"
					   onblur="document.getElementById('db_story_ln').value = this.value.trim();"
					   value="<?php echo($story_ln); ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr>
			<td style="padding-right: 2px;">
				<input type=text placeholder="Phone Number" id=story_phone 
					   style="height: 30px; width: 190px; <?php echo($story_fld_style); ?>"
					   onblur="document.getElementById('db_story_phone').value = this.value.trim();"
					   value="<?php echo($story_phone); ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr>
			<td style="padding-right: 2px;">
				<input type=text placeholder="Email Address" id=story_email 
					   style="height: 30px; width: 190px; <?php echo($story_fld_style); ?>"
					   onblur="document.getElementById('db_story_email').value = this.value.trim();"
					   value="<?php echo($story_email); ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<div id=show_image></div>
			</td>
		</tr>
<?php
include($path."configuration/story_settings.php");
include($path."configuration/contact_settings.php");
if($story["categories"])
	{
	include($path."../_common/includes/story_categories.php");
	}
if($story["types"])
	{
	include($path."../_common/includes/story_types.php");
	}

?>
		<tr>
			<td colspan=2  style="<?php echo($story_int_style); ?>">
				<?php 
				echo($lang['story_disclaimer']);
				?>
			</td>
		</tr>
<?php
if($story["photo"])
	{
	include($path."../_t2/includes/story_file.php");
	}

?>		
	</table>
<!--
<h4><?php echo($lang["your_story_title"]); ?></h4>
<textarea id="story_text" 
		  name="story_text" 
		  style="width: 95%; height: 300px;" 
		  required
		  placeholder="<?php echo($lang["your_story_placeholder"]); ?>"
		  onchange="story_change();"></textarea>
<h4><?php echo($lang["story_disclaimer_title"]); ?></h4>-->
<?php 
//echo($lang['story_disclaimer']); 
$oid = time();
if(isset($_SESSION["go_orderid"])){ $oid = $_SESSION["go_orderid"]; }
?> 
	<form id="story_form" 
		  name="story_form" 
		  method="post" 
		  action="action/save_story.php" 
		  target="work">
		<input type=hidden id="db_order_id" name="db_order_id" 
			   value="<?php echo($oid); ?>" />
		<input type=hidden id="db_story_type" name="db_story_type" value="" />
		<input type=hidden id="db_story_category" name="db_story_category" value="" />
		<input type=hidden id="db_is_file" name="db_is_file" value=0 />
		<input type=hidden id="db_story_file" name="db_story_file" value="" />
		<input type=hidden id="db_story_text" name="db_story_text" value="" />
		<input type=hidden id="db_story_fn" name="db_story_fn" value="<?php echo($story_fn); ?>" />
		<input type=hidden id="db_story_ln" name="db_story_ln" value="<?php echo($story_ln); ?>" />
		<input type=hidden id="db_story_phone" name="db_story_phone" value="<?php echo($story_phone); ?>" />
		<input type=hidden id="db_story_email" name="db_story_email" value="<?php echo($story_email); ?>" />
		<input type=hidden id="db_story_next" name="db_story_next" value="<?php echo($story["after_story"]); ?>" />
		<input type=hidden id="originating_page" name="originating_page" value="<?php echo($page); ?>" />
<?php
if(!$story["photo"])
	{
?>
		<input type=button 
			   class="story_button submit_btn" 
			   value="<?php echo($lang["story_submit_story"]); ?>"
			   onclick="submit_story('<?php echo($path); ?>');" />
<?php
	}
?>
	</form>
	<input type=hidden id="cate" name="cate" value="<?php echo($story["categories"]); ?>" />
	<input type=hidden id="catreq" name="catreq" value="<?php echo($story["category_required"]); ?>" />
	<input type=hidden id="catmsg" name="catmsg" value="<?php echo($lang["story_category_required"]); ?>" />
	<input type=hidden id="typ" name="typ" value="<?php echo($story["types"]); ?>" />
	<input type=hidden id="typreq" name="typreq" value="<?php echo($story["type_required"]); ?>" />
	<input type=hidden id="typmsg" name="typmsg" value="<?php echo($lang["story_type_required"]); ?>" />
	<input type=hidden id="fil" name="fil" value="<?php echo($story["photo"]); ?>" />
	<input type=hidden id="filreq" name="filreq" value="<?php echo($story["photo_required"]); ?>" />
	<input type=hidden id="filmsg" name="filmsg" value="<?php echo($lang["story_photo_required"]); ?>" />
	<input type=hidden id="storymsg" name="storymsg" value="<?php echo($lang["story_text_required"]); ?>" />
	<iframe id="work" 
			name="work" 
			style="width: 99%; height: 300px; display: none;"></iframe>
	<?php echo($story_div_end); ?>
</div>
<div style="<?php echo($story_div_style); ?> display: none;" id="story_thanks">
	<center><span style="font-size: 24pt;"><?php echo($lang["story_thanks"]); ?></span></center>
</div>