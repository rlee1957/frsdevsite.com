<div style="width: 99%;"><!-- First, Middle, Last Name -->
	<div class="name" style="float: left; width: 45%;"><!-- First Name -->
		<input type=text	
			   id=<?php echo($address_type); ?>_fn
			   name=<?php echo($address_type); ?>_fn
			   placeholder="<?php echo($lang["fn_placeholder"]); ?>"
			   required
			   maxlength=50
			   style="width: 100%;"
			   value="<?php echo($fn); ?>" />
	</div>
	<div class="mname" style="float: left; width: 9%;"><!-- Middle Name -->
		<input type=text	
			   id=<?php echo($address_type); ?>_mn
			   name=<?php echo($address_type); ?>_mn
			   placeholder="<?php echo($lang["mn_placeholder"]); ?>"
			   required
			   maxlength=50 
			   style="width: 100%;"
			   value="<?php echo($mn); ?>" />
	</div>
	<div class="name" style="float: left; width: 45%;"><!-- Last Name -->
		<input type=text	
			   id=<?php echo($address_type); ?>_ln
			   name=<?php echo($address_type); ?>_ln
			   placeholder="<?php echo($lang["ln_placeholder"]); ?>"
			   required
			   maxlength=50 
			   style="width: 100%;"
			   value="<?php echo($ln); ?>" />
	</div>
	<div style="clear: both;"></div>
</div>
<div style="width: 99%;"><!-- Country -->
	<select id=<?php echo($address_type); ?>_country 
			name=<?php echo($address_type); ?>_country  
			style="width: 100%;" 
			required 
			onchange="country_change('<?php echo($address_type); ?>', '');"
			value="<?php echo($country); ?>">
		<option value="" disabled><?php echo($lang["country_placeholder"]); ?></option>
<?php
$default = $default["country"];
$selected = "";
include($path."../_t2/configuration/country_list.php");	
foreach($country_list as $key => $country)	
	{
	if($key == $default){ $selected = " selected "; }
	else{ $selected = ""; }
	echo("
		<option value='".$key."'".$selected.">".$country."</option>
");
	}		
?>
	</select>
</div>
<div style="width: 99%;"><!-- Company -->
	<input type=text	
		   id=<?php echo($address_type); ?>_company 
		   name=<?php echo($address_type); ?>_company 
		   placeholder="<?php echo($lang['cart_shipping_company_placeholder']); ?>"
		   maxlength=255 
		   style="width: 99%;"
		   value="<?php echo($company); ?>" />
</div>
<div style="width: 99%;"><!-- Address 1 -->
	<input type=text	
		   id=<?php echo($address_type); ?>_address1 
		   name=<?php echo($address_type); ?>_address1 
		   placeholder="<?php echo($lang["addr1_placeholder"]); ?>"
		   required
		   maxlength=255 
		   style="width: 99%;"
		   value="<?php echo($address1); ?>" />
</div>
<div style="width: 99%;"><!-- Address 2 -->
	<input type=text	
		   id=<?php echo($address_type); ?>_address2 
		   name=<?php echo($address_type); ?>_address2 
		   placeholder="<?php echo($lang["addr2_placeholder"]); ?>"
		   maxlength=255 
		   style="width: 99%;"
		   value="<?php echo($address2); ?>" />
</div>
<?php

?>
<div style="width: 99%;"><!-- City, State, Zip -->
	<div style="float: left; width: 40%;"><!-- City -->
		<input type=text	
			   id=<?php echo($address_type); ?>_city
			   name=<?php echo($address_type); ?>_city
			   placeholder="<?php echo($lang["city_placeholder"]); ?>"
			   required
			   maxlength=255 
			   style="width: 100%;"
			   value="<?php echo($city); ?>" />
	</div>
<?php

# US State / Province 
include($path."configuration/us_states.php");
$state_prov1 = "
<select id=".$address_type."_state_prov 
		name=".$address_type."_state_prov 
		style='width: 100%;' 
		required";
if($country == "US")
	{
	$state_prov1 .= "
		value='".$state."'";
	}
$state_prov1 .= ">
	<option value='' disabled>".$lang["state_placeholder"]."</option>
";
$selected = "";
foreach($us_states as $key => $ar)
	{
	if(($ar["abbreviation"] == $state)&&($country == "US")){ $selected = " selected"; }
	$state_prov1 .= "
	<option value='".$ar["abbreviation"]."'".$selected.">".$ar["english"]."</option>
";	
	}
$state_prov1.= "
</select>
";

# Canada State / Province 
include($path."configuration/canada_provinces.php");
$state_prov2 = "
<select id=".$address_type."_state_prov 
		name=".$address_type."_state_prov 
		style='width: 100%;' 
		required";
if($country == "CA")
	{
	$state_prov2 .= "
		value='".$state."'";
	}
$state_prov2 .= ">
	<option value='' disabled selected>".$lang["state_placeholder"]."</option>
";
$selected = "";
foreach($us_states as $key => $ar)
	{
	if(($ar["abbreviation"] == $state)&&($country == "CA")){ $selected = " selected"; }
	$state_prov2 .= "
	<option value='".$ar["abbreviation"]."'".$selected.">".$ar["english"]."</option>
";	
	}
$state_prov2.= "
</select>
";	

# Other State / Province 
$state_prov3 = "
<input type=text 
	   id=".$address_type."_state_prov 
	   name=".$address_type."_state_prov
	   style='width: 100%;' 
	   required
	   placeholder='".$lang["division_placeholder"]."'";
if(($country != "CA")&&($country != "US"))
	{
	$state_prov3 .= "
		value='".$state."'
";
	}
$state_prov3 .= " />
";	
#*****************************************************************	

?>
	<div style="float: left; width: 35%;"><!-- State -->
		<div id=<?php echo($address_type); ?>_state_container>
<?php
if($country == "US"){ echo($state_prov1); }
elseif($country == "CA"){ echo($state_prov2); }
else{ echo($state_prov3); }
?>
		</div>
		<textarea id=<?php echo($address_type); ?>_us_states style="display: none;">
<?php
echo($state_prov1);
?>
		</textarea>
		<textarea id=<?php echo($address_type); ?>_canada_provinces style="display: none;">
<?php
echo($state_prov2);
?>
		</textarea>
		<textarea id=<?php echo($address_type); ?>_other_divisions style="display: none;">
<?php
echo($state_prov3);
?>					
		</textarea>
	</div>
<?php

# US Postal Code 
$zip1 = "
<input type=text	
	   id=".$address_type."_postal_code
	   name=".$address_type."_postal_code
	   placeholder='".$lang["zip_placeholder"]."'
	   required
	   maxlength=5 
	   style='width: 100%;'";
if($country == "US")
	{
	$zip1 .= "
	   value='".$zip."'";
	}
$zip1 .= " />";
# Canada Postal Code 
$zip2 = "
<input type=text	
	   id=".$address_type."_postal_code
	   name=".$address_type."_postal_code
	   placeholder='".$lang["zip_placeholder"]."'
	   required
	   maxlength=7 
	   style='width: 100%;'";
if($country == "CA")
	{
	$zip2 .= "
	   value='".$zip."'";
	}
$zip2 .= " />";	
# Other Postal Code 
$zip3 = "
<input type=text	
	   id=".$address_type."_postal_code
	   name=".$address_type."_postal_code
	   placeholder='".$lang["zip_placeholder"]."'
	   required
	   maxlength=25 
	   style='width: 100%;'";
if(($country != "CA")&&($country != "US"))
	{
	$zip3 .= "
	   value='".$zip."'";
	}
$zip3 .= " />";
#*****************************************************************	
?>
	<div style="float: left; width: 24%;"><!-- Zip -->
		<div id=<?php echo($address_type); ?>_postal_code_container>
<?php
if($country == "US"){ echo($zip1); }
elseif($country == "CA"){ echo($zip2); }
else{ echo($zip3); }
?>					
		</div>
		<textarea id=<?php echo($address_type); ?>_us_zip style="display: none;">
<?php
echo($zip1);
?>
		</textarea>
		<textarea id=<?php echo($address_type); ?>_canada_postal_code style="display: none;">
<?php
echo($zip2);
?>				
		</textarea>
		<textarea id=<?php echo($address_type); ?>_other_postal_code style="display: none;">
<?php
echo($zip3);
?>		
		</textarea>
	</div>
</div>