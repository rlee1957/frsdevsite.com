<?php

$_SESSION["active"] = "yes";
$_SESSION["last_activity"] = time();
$_SESSION["order"] = array();
$_SESSION["order"]["info"] = array();
$_SESSION["order"]["info"]["campaign_idx"] = $campaign_idx;
$_SESSION["order"]["info"]["current_item_id"] = -1;
$_SESSION["order"]["info"]["ship_method"] = "";
$_SESSION["order"]["info"]["ship_address"] = 0;
$_SESSION["order"]["info"]["terms_and_conditions"] = "no";
$_SESSION["order"]["info"]["promotion_code"] = "";

$_SESSION["order"]["info"]["amount"] = array();
$_SESSION["order"]["info"]["amount"]["sub_total"] = 0.00;
$_SESSION["order"]["info"]["amount"]["promotional_discount"] = 0.00;
$_SESSION["order"]["info"]["amount"]["convenience_fee"] = 0.00;
$_SESSION["order"]["info"]["amount"]["sales_tax"] = 0.00;
$_SESSION["order"]["info"]["amount"]["order_total"] = 0.00;

$_SESSION["order"]["info"]["cc"] = array();
$_SESSION["order"]["info"]["cc"]["cc_name"] = "";
$_SESSION["order"]["info"]["cc"]["cc_type"] = "";
$_SESSION["order"]["info"]["cc"]["cc_number"] = "";
$_SESSION["order"]["info"]["cc"]["cc_month"] = "";
$_SESSION["order"]["info"]["cc"]["cc_year"] = "";

$_SESSION["order"]["info"]["marketing"] = array();
$_SESSION["order"]["info"]["marketing"]["how_u_heard"] = "";
$_SESSION["order"]["info"]["marketing"]["buying_as"] = "";
$_SESSION["order"]["info"]["marketing"]["group_code"] = "";
$_SESSION["order"]["info"]["marketing"]["opt_in"] = "";

$_SESSION["order"]["info"]["processing"] = array();
$_SESSION["order"]["info"]["processing"]["data_validation"] = "incomplete";
$_SESSION["order"]["info"]["processing"]["pre_log"] = "incomplete";
$_SESSION["order"]["info"]["processing"]["pre_save"] = "incomplete";
$_SESSION["order"]["info"]["processing"]["merchant_cc"] = "incomplete";
$_SESSION["order"]["info"]["processing"]["post_log"] = "incomplete";
$_SESSION["order"]["info"]["processing"]["update_data"] = "incomplete";
$_SESSION["order"]["info"]["processing"]["email_sent"] = "incomplete";
$_SESSION["order"]["info"]["processing"]["story_submitted"] = "incomplete";
$_SESSION["order"]["info"]["processing"]["receipt"] = "incomplete";
$_SESSION["order"]["info"]["processing"]["final_log"] = "incomplete";
$_SESSION["order"]["info"]["processing"]["merchant_cc_results"] = array();

$_SESSION["order"]["cart"] = array();

$_SESSION["order"]["addresses"] = array();

include($path."../_t2/database/get_fullfillment_items.php");
echo($sql."  --  SQL<br />");
echo("<textarea style='width: 100%; height: 400px;'>");
print_r($results);
echo("</textarea>");
foreach($fullfillment_items as $i => $row)
	{
	$_SESSION["order"]["cart"][$row["item_id"]] = $row;	
	$_SESSION["order"]["cart"][$row["item_id"]]["quantity"] = 0;
	$_SESSION["order"]["cart"][$row["item_id"]]["shipto_addresses"] = array();
	}

?>