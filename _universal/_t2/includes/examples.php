<center>
	<table class=examples-table cellpadding=3 cellspacing=0>
		<tr>
			<td colspan=2 align='left'>
				<span class='faq-title'><?php echo($lang["ins-examples-head"]); ?></span>
				<div style="height: 20px;"></div>
			</td>
		</tr>
<?php

$htm = "";
$example_count = $lang["page"]["inscription-examples"];
$pos = 0;
$start = true;
foreach($lang["page"]["inscription-examples"] as $category_name => $bricks)
	{
	if(($pos == 0)||($pos == 2))
		{
		$pos = 0;
		if(!$start){ $htm .= "</tr><tr style='height: 5px;'><td colspan=2 class=example-separator></td></tr>"; }
		$htm .= "<tr><td style='width: 50%;'>";		
		}
	else
		{
		$htm .=	"<td style='width: 50%;'>";
		}
	$htm .= get_category($category_name, $bricks);
	$htm .= "</td>";
	$pos++;
	$start = false;
	}
echo($htm);

function get_lines($brick_name, $example)
{
$txt = "
	<tr>
		<td class=example-brick>".$brick_name."</td>
	</tr>
	<tr>
		<td class=example-inscription>
";
$del = "";
foreach($example as $idx => $line)
	{
	$txt .= $del.$line;
	$del = "<br />";	
	}
$txt .= "
		</td>
	</tr>
";
return $txt;
}

function get_category($category_name, $bricks)
{
$htm = "
<table>
	<tr>
		<td class=example-category>
			<div style='height: 20px;'></div>
			".$category_name."
		</td>
	</tr>
";
foreach($bricks as $brick_name => $example)
	{
	$htm .= get_lines($brick_name, $example);	
	}
$htm .= "
</table>
";
return $htm;
}

	



?>
	</table>
</center>
<div style="height: 50px;">&nbsp;</div>