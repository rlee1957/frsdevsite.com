<?php

go_clear_response();
$host = $_SESSION["go_url"].$_SESSION["go_page"];

//$mytx is an array containing all POST data from a form
$mytx = array();
$mytx['op']			= 'ProcessCreditCard';
$host 			   .= $mytx['op'];
$mytx['username'] 	= $_SESSION["go_merchant"];
$mytx['password'] 	= $_SESSION["go_password"];
$mytx['amount'] 	= $_SESSION["order"]["info"]["amount"]["order_total"];
$mytx['CardNum']    = $_SESSION["order"]["info"]["cc"]["cc_number"];
$mytx['ExpDate']	= $_SESSION["order"]["info"]["cc"]["cc_month"].$_SESSION["order"]["info"]["cc"]["cc_year"];
$mytx['CVNum']   	= '';//$_POST['CVNum'];
$mytx['NameOnCard'] = $_SESSION["order"]["info"]["cc"]["cc_name"];
$mytx['TransType']  = 'Sale';
$mytx['MagData']  	= '';//$_POST['MagData'];
$mytx['InvNum']  	= $_SESSION["order"]["info"]["purchase_id"];
$mytx['PNRef']  	= '';//$_POST['PNRef'];
$mytx['Zip']  		= $_SESSION["order"]["addresses"][0]["postal_code"];
$mytx['Street'] 	= $_SESSION["order"]["addresses"][0]["address1"];
$mytx['Phone'] 		= $_SESSION["order"]["addresses"][0]["phone"];
$mytx['Email'] 		= $_SESSION["order"]["addresses"][0]["email"];
$mytx['CheckNum']  	= '';//$_POST['CheckNum'];
$mytx['TransitNum'] = '';//$_POST['TransitNum'];
$mytx['AccountNum'] = '';//$_POST['AccountNum'];
$mytx['MICR']  		= '';//$_POST['MICR'];
$mytx['NameOnCheck']= '';//$_POST['NameOnCheck'];
$mytx['DL']  		= '';//$_POST['DL'];
$mytx['SS']  		= '';//$_POST['SS'];
$mytx['DOB']  		= '';//$_POST['DOB'];
$mytx['StateCode'] 	= '';//$_POST['StateCode'];
$mytx['CheckType'] 	= '';//$_POST['CheckType'];
$mytx['Pin'] 		= '';//$_POST['Pin'];
$mytx['RegisterNum'] 	= '';//$_POST['RegisterNum'];
$mytx['SureChargeAmt'] 	= '';//$_POST['SureChargeAmt'];
$mytx['CashBackAmt'] 	= '';//$_POST['CashBackAmt'];
$mytx['ExtData'] 	= '<CustomerID>'.$_SESSION["order"]["info"]["order_id"].'</CustomerID><Force>T</Force>';//$_POST['ExtData']; <Force>T/Force>
//print_r($mytx);
//$paystring = 'https://secure.ftipgw.com/ArgoFire/transact.asmx?op=ProcessCreditCard&username='.$_SESSION["go_merchant"];

//print_r($mytx);


//Generate the string as "key=value" pairs
$mybuilder = array();
foreach($mytx as $key=>$value) 
	{
	$mybuilder[] = $key . '='.$value;
    }
//print_r($mybuilder);

//join the pairs into a single string
$mystring = implode("&",$mybuilder);
//echo $mystringer;

function curl_post($url, array $post = NULL, array $options = array()) 
{ 
$defaults = array( 
					CURLOPT_POST => 1, 
					CURLOPT_HEADER => 0, 
					CURLOPT_URL => $url, 
					CURLOPT_FRESH_CONNECT => 1, 
					CURLOPT_RETURNTRANSFER => 1, 
					CURLOPT_FORBID_REUSE => 1, 
					CURLOPT_TIMEOUT => 60, 
					CURLOPT_POSTFIELDS => http_build_query($post) 
				 ); 
$ch = curl_init(); 
curl_setopt_array($ch, ($options + $defaults)); 
if( ! $result = curl_exec($ch)) 
    { 
    trigger_error(curl_error($ch)); 
    } 
curl_close($ch); 
return $result; 
}

$data = curl_post($host,$mytx);
$xml_data = simplexml_load_string($data);
$_SESSION["order"]["test"] .= "process_payment.php
";	
$res = array();
$res["go_resp_status"] = $xml_data->Result;
$res["go_resp_auth_code"] = $xml_data->AuthCode;
$res["go_resp_auth_response"] = $xml_data->RespMSG;
$res["go_resp_code"] = $xml_data->Result;
$res["go_resp_avs_code"] = $xml_data->GetAVSResultTXT;
$res["go_resp_cvv2_code"] = $xml_data->GetZipMatchTXT;
$res["go_resp_order_id"] = $xml_data->go_orderid;
$res["go_resp_reference_number"] = $xml_data->PNRef;
$res["go_resp_error"] = $xml_data->error;

$force_success = false;
$force_success_password = "No Password Provided";
if((isset($_REQUEST["force_success_pwd"]))&&($_REQUEST["force_success_pwd"] != "admin_password"))
	{
	$force_success_password = "Wrong password provided (".$_REQUEST["force_success_pwd"].")";
	}
if((isset($_REQUEST["force_success_pwd"]))&&($_REQUEST["force_success_pwd"] == "admin_password"))
	{ 
	$force_success = true; 
	$force_success_password = $_REQUEST["force_success_pwd"];
	}
if((trim((string)$res["go_resp_auth_response"]) != "Decline")||($force_success))
	{ 
	$res["go_resp_code"] = "Accepted"; 
	$_SESSION["order"]["info"]["mail_results"] = send_email_confirmation($path);
	echo("
process~successful	
");	
	$status = "Payment Approved ~ ".(string)$res["go_resp_auth_response"];
	if($force_success){ $status .= " ~ forced for testing"; }
	}
else
	{
	$status = "Order Cancelled ~ ".trim((string)$res["go_resp_auth_response"]);
	echo("
process~unsuccessful
results-start|
status~".$status."|
auth_code~".(string)$res["go_resp_auth_code"]."|
auth_response~".(string)$res["go_resp_auth_response"]."|
response_code~".(string)$res["go_resp_code"]."|
avs_code~".(string)$res["go_resp_avs_code"]."|
cvv2_code~".(string)$res["go_resp_cvv2_code"]."|
order_id~".$_SESSION["order"]["info"]["purchase_id"]."|
reference_number~".(string)$res["go_resp_reference_number"]."|
response_error~".(string)$res["go_resp_error"]."|
order_total~".$_SESSION["go_total"]."|
force_success_password~".$force_success_password."	
|results-end
");
	}
include($path."../_t2/includes/update_cc_process_data.php");

?>