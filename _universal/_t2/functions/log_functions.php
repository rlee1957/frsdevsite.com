<?php

function err_log($p1) 
{
$content = "
***** ".date('Y-m-d H:i:s') . "*****
" . clean_log($p1) . "

";
$fd = fopen("/var/log/brick_log/".$_SESSION["store_folder"].".log","a");
if ($fd) 
	{
	fwrite($fd, $content);
	}
fclose($fd);
}

function order_log()
{
	
$max_lines = 5000;
$order_info = "orderid:".$_SESSION["go_orderid"];
$order_info .= "|total:".$_SESSION["go_total"];
$order_info .= "|name:".$_SESSION["go_oname"];
$order_info .= "|street:".$_SESSION["go_ostreet"];
$order_info .= "|city:".$_SESSION["go_ocity"];
$order_info .= "|state:".$_SESSION["go_ostate"];
$order_info .= "|zip:".$_SESSION["go_ozip"];
$order_info .= "|phone:".$_SESSION["g_billing"][9];
$order_info .= "|email:".$_SESSION["g_billing"][10];
$order_info = clean_log($order_info);
$order_info = "
***** ". date('Y-m-d H:i:s') . " *****
" . $order_info."

";
$fd = fopen("/var/log/order_log/".$_SESSION["store_folder"].".log","c+");
if ($fd) 
	{
	$ar = array();
	while (($buffer = fgets($fd, 4096)) !== false) 
		{
        $ar[count($ar)] = $buffer;
		}
    fclose($fd);
	sort($ar);
	$line_count = (count($ar));
	if($line_count>=$max_lines)
		{
		$start_line = $line_count - $max_lines;
		unlink("/var/log/order_log/".$_SESSION["store_folder"].".log");
		$fd = fopen("/var/log/order_log/".$_SESSION["store_folder"].".log","a");
		for($x=$start_line;$x<$line_count;$x++)
			{
			fwrite($fd, $ar[$x]);
			}
		fwrite($fd, $order_info);
		fclose($fd);
		}
	else	
		{
		$fd = fopen("/var/log/order_log/".$_SESSION["store_folder"].".log","a");
		fwrite($fd, $order_info);
		fclose($fd);
		}
	}
else
	{
	fclose($fd);
	}
	
}

function clean_log($str)
{
	
$tmp = $str;
while((strpos($tmp,"\r"))||(strpos($tmp,"\n"))||(strpos($tmp,"\t"))||(strpos($tmp,"  ")))
	{
	$tmp = str_replace("\t", " ", $tmp);
	$tmp = str_replace("\n", " ", $tmp);
	$tmp = str_replace("\r", " ", $tmp);
	$tmp = str_replace("  ", " ", $tmp);
	}
$tmp = trim($tmp);
return $tmp;

}

?>