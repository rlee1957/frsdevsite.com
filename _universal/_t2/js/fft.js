
function toggle_menu_fft(idx)
{
if(idx == 1)
	{
	initialize_session_fft();
	}
var el = "";
for(x=0;x<11;x++)
	{
	el = "content" + x;
	document.getElementById(el).style.display = "none";
	}
el = "content" + idx;
document.getElementById(el).style.display = "block";
    closeCollapsedNavbar();
}

function initialize_session_fft()
{
var campaign_idx = document.getElementById("campaign_idx").value;
var session_initialized = document.getElementById("session_set").value;
if(session_initialized == "yes")
	{ 
	//show_shop_fft("");
	return; 
	}
document.getElementById("session_set").value = "yes";
var href = "actions/initialize_session_fft.php?campaign_idx=" + campaign_idx;
$.ajax
	(
		{
		url: href, 
		success: function(result)
			{ 
			do_nothing(result);
			//show_shop(result);
			}
		}
	);
		
}

function do_increment_fft(o_n, after, page)
{
	
var obj = document.getElementById(o_n);	
var val = new Number(obj.value);
val++;
obj.value = val;
if(page == "cart"){ set_session_fft(o_n, val, after, page); }
else{ set_selected_fft(page); }

}

function do_decrement_fft(o_n, after, page)
{
	
var obj = document.getElementById(o_n);	
var val = new Number(obj.value);
if(val > 0)
	{
	val--;	
	}
obj.value = val;
if(page == "cart"){ set_session_fft(o_n, val, after, page); }
else{ set_selected_fft(page); }	
	
}

function set_selected_fft(page)
{
	
var available = document.getElementById("available_options").value;
var quantity = document.getElementById("available_option_quantity");
var options = available.split("|");
var option_count = options.length;
var del = "";
var options_quantity = "";
for(x=0;x<option_count;x++)
	{
	var nme = page + "_option_" + options[x].trim();
	var num = new Number(document.getElementById(nme).value);
	options_quantity += del + num;
	del = "|";
	}
quantity.value = options_quantity;
	
}

function save_options_fft()
{
var del = "?";
var dl = "";
var specs = "";
var available_options = document.getElementById("available_options").value;
var selections = available_options.split("|");
var options = "";
var items_selected = false;
for(x=0;x<selections.length;x++)
	{
	nme = "shop_option_" + selections[x].trim();	
	var qty = document.getElementById(nme).value;
	if(qty > 0){items_selected = true; }
	var item = selections[x].trim() + "_" + qty;
	options += dl + item;
	dl = "|";
	}
if(items_selected)
	{
	specs += del + "options=" + options;
	var href = "actions/add_option_fft.php" + specs;
	$.ajax
		(
			{
			url: href, 
			success: function(result)
				{ 
				//show_order_info(result);
				show_cart_detail_fft(); 
				}
			}
		);
	}

}

function show_cart_detail_fft()
{
var href = "actions/show_cart_details_fft.php";
hide_side_cart_fft();
clear_content_fft();
$.ajax
	(
		{
		url: href, 
		success: function(result){ show_cart_detail_page(result); }
		}
	);
		
}

function show_checkout_cart_fft()
{

var b = check_marketing();
if(b){ show_checkout_fft();	}
	
}

function show_checkout_fft()
{
var href = "actions/show_checkout_fft.php";
clear_content_fft();
$.ajax
	(
		{
		url: href, 
		success: function(result){ show_checkout_page(result); }
		}
	);
		
}

function set_session_fft(ids, quantity, after)
{

var idz = ids.split("_");
var specs = "?item_id=" + idz[2] + "&quantity=" + quantity;
var href = "actions/session_quantity_fft.php" + specs;
$.ajax
	(
		{
		url: href,
		success: function(result){ show_cart_detail_fft(); }
		}
	);
	
}

function ays_fft(id)
{

var q = document.getElementById("cart_option_" + id).value	
if(q != 0)
	{
	document.getElementById("ays_next_fft").value = id;
	//show_ays_fft();
	remove_item_fft();
	}
	
}

function show_ays_fft()
{
	
document.getElementById("popup").style.display = "block";
document.getElementById("ays_fft").style.display = "block";	

}

function hide_ays_fft()
{
	
document.getElementById("popup").style.display = "none";
document.getElementById("ays_fft").style.display = "none";	
	
}

function remove_item_fft()
{

var id = document.getElementById("ays_next_fft").value;
var specs = "?item_id=" + id + "&quantity=0";
var href = "actions/session_quantity_fft.php" + specs;
$.ajax
	(
		{
		url: href,
		success: function(result){ show_cart_detail_fft(); hide_ays_fft(); }
		}
	);
	 
}

function toggle_side_cart_fft()
{

var obj = document.getElementById("side_cart_fft");
var nv = "0px";
if(obj.style.right == nv)
	{ 
	hide_side_cart_fft();
	}
else
	{
	
	show_side_cart_fft();
	}
	
}

function show_side_cart_fft()
{
var href = "actions/show_side_cart_fft.php";
$.ajax
	(
		{
		url: href, 
		success: function(result)
			{ 
			document.getElementById("cart_blanket").style.display = "block";
			document.getElementById("side_cart_content").innerHTML = result;
			$('.side-cart').animate({ right: 0 }, 500);
			}
		}
	);
}

function hide_side_cart_fft()
{
nv = "-1000px"; 
$('.side-cart').animate({ right: nv }, 500);
document.getElementById("side_cart_content").innerHTML = "";
document.getElementById("cart_blanket").style.display = "none";
}

function clear_content_fft()
{
document.getElementById("gc_separator").style.display = "none";
document.getElementById("the_options").innerHTML = "";
document.getElementById("content3").innerHTML = "";	
document.getElementById("content4").innerHTML = "";
document.getElementById("side_cart_content").innerHTML = "";
nv = "-1000px"; 
$('.side-cart').animate({ right: nv }, 500);
}

function save_shipto_fft(method)
{
	
var shipping_address_values = "";
index = 0;
if(method == "bill")
	{ 
	shipping_address_values = "index=0"; 
	}
else{ shipping_address_values = check_shipto_address(); }
if(shipping_address_values)
	{
		
	shipping_address_values += "&method=" + method;
	var href = "actions/save_shipto_fft.php";
	$.ajax
		(
			{
			url: href, 
			type: "POST",
			data: shipping_address_values,
			success: function(result){ check_fedex_results_fft(1, result, method); }
			}
		);	
	}
else
	{
	var nme = "ship_to_" + method;
	obj = document.getElementById(nme);
	obj.checked = false;	
	}	
	
}

function ship_other_fft(obj)
{


if((save_billing())&&(save_credit_card()))
	{
	var billing_address_values = check_billing_address();
	if(billing_address_values)
		{
		var href = "actions/ship_to_other_fft.php";
		$.ajax
			(
				{
				url: href, 
				type: "POST",
				data: billing_address_values,
				success: function(result){ show_shipto_page(result); }
				}
			);
		}
	else
		{
		obj.checked = false;
		}
	}
else
	{
	obj.checked = false;	
	}
}

function ship_different_fft(obj)
{
if((save_billing())&&(save_credit_card()))
	{
	var billing_address_values = check_billing_address();
	if(billing_address_values)
		{
		var href = "actions/addresses_fft.php";
		$.ajax
			(
				{
				url: href, 
				type: "POST",
				data: billing_address_values,
				success: function(result){ show_shipto_different_page(result); }
				}
			);	
		}
	else
		{
		obj.checked = false;	
		}
	}
else
	{
	obj.checked = false;	
	}
}

function check_fedex_results_fft(idx, res, method)
{

if(method == "bill"){ idx = 0; }
var multi_pass = true;
var typ = "bill";
var msg_label = document.getElementById("validate_billing_head").value;
var msg = document.getElementById("fedex_validating").value;
// show_popup(true, msg_label, false, "", true, msg, false, "", false, "");
if(idx != 0)
	{ 
	typ = "ship"; 
	msg_label = document.getElementById("validate_shipping_head").value;
	}
var results = res.split("|");
idx = 0;
if(method != "bill"){ idx = results[2]; }
var next = false;
if(results[0] == "failed")
	{
	msg = document.getElementById("fedex_avs_invalid").value;
	ele = document.getElementById(typ + "_fn");
	//show_message(msg_label, msg, ele); 
	if(multi_pass)
		{
		accept_suggestion_fft(idx, results[1], method);	
		}
	else
		{
		show_popup(true, msg_label, false, "", true, msg, true, "", false, "");	
		}
	
	}	
else if(results[0] == "changes")
	{ 
	adrs = JSON.parse(results[1]);
	accept_suggestion_fft(idx, results[1], method);
	}
else{ next = true; }
if(next)
	{
	if(idx ==0)	
		{
		hide_popup();
		accept_suggestion_fft(idx, results[1], method);
		}
	else
		{
		accept_suggestion_fft(idx, results[1], method);	
		}
	}

}

function accept_suggestion_fft(idx, results, method)
{
//var pm = results.replace(/~/mg, '"');
var pm = results;
var typ = "bill";
if(idx > 0){ typ = "ship"; }	
try
	{
	adrs = JSON.parse(pm);
	if(document.getElementById(typ + "_address1").value.trim().toUpperCase() != adrs.new.address1)
		{
		document.getElementById(typ + "_address1").value = adrs.new.address1;
		document.getElementById(typ + "_address1").style.color = "#008000";
		document.getElementById(typ + "_address1").style.fontWeight = "bold";
		}
	if(document.getElementById(typ + "_city").value.trim().toUpperCase() != adrs.new.city)
		{
		document.getElementById(typ + "_city").value = adrs.new.city;
		document.getElementById(typ + "_city").style.color = "#008000";
		document.getElementById(typ + "_city").style.fontWeight = "bold";
		}
	if(document.getElementById(typ + "_postal_code").value.trim().toUpperCase() != adrs.new.postal_code)
		{
		document.getElementById(typ + "_postal_code").value = adrs.new.postal_code;
		document.getElementById(typ + "_postal_code").style.color = "#008000";
		document.getElementById(typ + "_postal_code").style.fontWeight = "bold";
		}
	document.getElementById(typ + "_state_prov").value = adrs.new.state_prov;
	document.getElementById(typ + "_country").value = adrs.new.country;
	var address_data = "idx=" + idx;
	address_data += "&fn=" + adrs.new.fn;
	address_data += "&mn=" + adrs.new.mn;
	address_data += "&ln=" + adrs.new.ln;
	address_data += "&company=" + adrs.new.company;
	address_data += "&address1=" + adrs.new.address1;
	address_data += "&address2=" + adrs.new.address2;
	address_data += "&city=" + adrs.new.city;
	address_data += "&state_prov=" + adrs.new.state_prov;
	address_data += "&postal_code=" + adrs.new.postal_code;
	address_data += "&country=" + adrs.new.country;
	address_data += "&validated_javascript=true";
	address_data += "&validated_fedex=true";
	address_data += "&validated_overall=true";
	}
catch(e)
	{
	var address_data = "idx=" + idx;
	address_data += "&fn=" + document.getElementById(typ + "_fn").value;
	address_data += "&mn=" + document.getElementById(typ + "_mn").value;
	address_data += "&ln=" + document.getElementById(typ + "_ln").value;
	address_data += "&company=" + document.getElementById(typ + "_company").value;
	address_data += "&address1=" + document.getElementById(typ + "_address1").value;
	address_data += "&address2=" + document.getElementById(typ + "_address2").value;
	address_data += "&city=" + document.getElementById(typ + "_city").value;
	address_data += "&state_prov=" + document.getElementById(typ + "_state_prov").value;
	address_data += "&postal_code=" + document.getElementById(typ + "_postal_code").value;
	address_data += "&country=" + document.getElementById(typ + "_country").value;
	address_data += "&validated_javascript=true";
	address_data += "&validated_fedex=true";
	address_data += "&validated_overall=true";	
	}
document.getElementById(typ + "_validated_javascript").value = "true";
document.getElementById(typ + "_validated_fedex").value = "true";
document.getElementById(typ + "_validated_overall").value = "true";
var href = "actions/update_address.php";
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: address_data,
		success: function(result){ do_nothing(result); }
		}
	);
if(method == "bill")
	{
	hide_popup();
	document.getElementById("ship_to_bill").checked = true;	
	}
else
	{
	if(method == "other")
		{
		set_other_shipto_fft(idx);
		hide_popup();
		hide_shipto();
		document.getElementById("ship_to_other").checked = true;		
		}
	else
		{
		hide_popup();
		hide_shipto();
		document.getElementById("ship_to_different").click();
		}
	}
}

function set_other_shipto_fft(idx)
{
var href = "actions/set_other_shipto_fft.php?idx=" + idx;
$.ajax
	(
		{
		url: href, 
		type: "GET",
		success: function(result){ do_nothing(result); }
		}
	);	
}

function add_new_address_fft()
{
var href = "actions/add_an_address_fft.php";
document.getElementById("shipdifferent_container").innerHTML = "";	
document.getElementById("shipdifferent_container").style.display = "none";	
$.ajax
	(
		{
		url: href, 
		type: "POST",
		success: function(result){ show_shipto_page(result); }
		}
	);
}

function option_save_shipto_fft()
{
var option_count = document.getElementById("option_count").value;
var n = new Number(option_count), address_code = "", address_data;
var obj, nme, val, href;
del = "";
for(x=0;x<n;x++)
	{
	obj = document.getElementById("address_" + x);
	val = obj.value;
	address_code += del + obj.name + "_" + val;
	del = "|";
	}
address_data = "address_data=" + address_code;
href = "actions/record_addresses_fft.php";
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: address_data,
		success: function(result){ do_nothing(result); }
		}
	);
}

function process_order_fft()
{
var form_data = process_verify();
if(!form_data){ return; }
var blabel = true;
var tlabel = document.getElementById("process_order_label").value;
var bimage = false;
var timage = document.getElementById("process_order_image").value;
var binstruction = true;
var tinstruction = document.getElementById("process_order_standby").value;
var bclose = false;
var cimage = "";
var bnextstep = false;
var tnextstep = "";
show_popup(blabel, tlabel, bimage, timage, binstruction, tinstruction, bclose, cimage, bnextstep, tnextstep);
var href = "actions/process_order_fft.php";
var force_success_pwd = document.getElementById("force_success_pwd").value;
if(force_success_pwd.trim() != "")
	{
	form_data += "&force_success_pwd=" + force_success_pwd;	
	}
 
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ check_process_fft(result); }
		}
	);
		
}

function check_process_fft(result)
{
	// document.getElementById("content3").innerHTML = result; toggle_menu(3); hide_popup()
if(result.indexOf("process~successful") == -1)
	{
	show_process_error(result);	
	}
else
	{
	var href = "actions/show_confirmation_fft.php";
	location.href = href;	
	}
}

