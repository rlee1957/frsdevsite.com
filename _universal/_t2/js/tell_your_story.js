//var files;
//$('form').on('submit', uploadFiles);

function uploadFiles(event, path)
{
event.stopPropagation(); // Stop stuff happening
event.preventDefault(); // Totally stop stuff happening
// START A LOADING SPINNER HERE

// Create a formdata object and add the files
var data = new FormData();
var href = path + '../stories/actions/upload_file.php?files';
$.each(files, function(key, value)
    {
    data.append(key, value);
    });
$.ajax(
	{
    url: href,
	type: 'POST',
	data: data,
	cache: false,
	dataType: 'json',
	processData: false, // Don't process the files
	contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	success: function(data, textStatus, jqXHR)
		{
        if(typeof data.error === 'undefined')
            {
			echo("Undefined error");
                // Success so call function to process the form
                //submitForm(event, data);
            }
            else
            {
			echo('ERRORS: ' + data.error);
                // Handle errors here
                //console.log('ERRORS: ' + data.error);
            }
        },
    error: function(jqXHR, textStatus, errorThrown)
        {
		echo('ERRORS: ' + textStatus);
            // Handle errors here
            //console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
        }
    });
}

function inspect_file(obj)
{
var filename = obj.value;
var pattern = new RegExp("\.(gif|jpg|jpeg|png)$", 'i');
if(pattern.test(filename))
	{
	var dot = filename.lastIndexOf(".");
	var len = filename.length;
	var sufstart = len - dot;
	var startat = len - sufstart;
	var suffix = filename.substr(startat, sufstart);
	document.getElementById("suffix").value = suffix;	
	files = event.target.files;
	var sub = document.getElementById("image_submit_button");
	sub.click();
	}
else
	{
	var blabel = true;
	var tlabel = document.getElementById("story_title").value;
	var bimage = true;
	var timage = "images/baddata.png";
	var binstruction = true;
	var tinstruction = document.getElementById("story_file_pre_msg").value + filename + document.getElementById("story_file_suff_msg").value;
	var bclose = true;
	var cimage = "images/close.jpg";
	var bnextstep = true;
	var tnextstep = "document.getElementById('" + obj.id + "').value = ''; document.getElementById('" + obj.id + "').click();";
	show_popup(blabel, tlabel, bimage, timage, binstruction, tinstruction, bclose, cimage, bnextstep, tnextstep);
	}		
}

function type_change()
{
var chks = document.getElementsByTagName("INPUT");
var retVal = "";
var del = "";
var x = 0;
var len = chks.length;
for(x; x<len; x++)
	{
	var name = document.getElementsByTagName("INPUT")[x].name;
	var value = document.getElementsByTagName("INPUT")[x].value;
	var typ = document.getElementsByTagName("INPUT")[x].type;
	var checked = document.getElementsByTagName("INPUT")[x].checked;
	if(typ = "checkbox")	
		{
		if(checked)
			{
			retVal += del + value;	
			del = "|";
			}
		}
	}
document.getElementById("db_story_type").value = retVal;
}

function category_change()
{
document.getElementById("db_story_category").value = document.getElementById("dd_category").value;	
}

function submit_story(path)
{
var form_data = validate_story();
if(!form_data){ return; }
//show_processing_splash();
var href = path + "actions/save_story.php";
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ submit_story_results(result); }
		}
	);
		
}

function submit_story_results(results)
{
	
/*alert(results);
obj = JSON.parse(results);	
show_popup(obj.blabel, obj.tlabel, obj.bimage,	obj.timage, obj.binstruction, obj.tinstruction, obj.bclose, obj.cimage, obj.bnextstep, obj.tnextstep);*/
document.getElementById("story_share").style.display = "none";
document.getElementById("story_thanks").style.display = "block";
}

function validate_story()
{
var form_data = "";
var blabel = true;
var tlabel = document.getElementById("story_title").value;
var bimage = true;
var timage = "images/baddata.png";
var binstruction = true;
var tinstruction = "";
var bclose = true;
var cimage = "images/close.jpg";
var bnextstep = true;
var tnextstep = "";
var cate = document.getElementById("cate").value;
var catreq = document.getElementById("catreq").value;
var catmsg = document.getElementById("catmsg").value;
var typ = document.getElementById("typ").value;
var typreq = document.getElementById("typreq").value;
var typmsg = document.getElementById("typmsg").value;
var fil = document.getElementById("fil").value;
var filreq = document.getElementById("filreq").value;
var filmsg = document.getElementById("filmsg").value;
var storymsg = document.getElementById("storymsg").value;
var type_required = false;
var category_required = false;
var file_required = false;
var story_required = true;
var del = "";
if((cate == 1)&&(catreq == 1)){ category_required = true; }
if((typ == 1)&&(typreq == 1)){ type_required = true; }
if((fil == 1)&&(filreq == 1)){ file_required = true; }	
form_data += del + "originating_page=" + document.getElementById("originating_page").value.trim(); 
del = "&";
if(category_required)
	{
	if(document.getElementById("db_story_category").value == "")
		{
		alert(catmsg);
		document.getElementById("dd_category").focus();
		return false;
		}
	}
form_data += del + "db_story_category=" + document.getElementById("db_story_category").value.trim(); del = "&";
if(type_required)
	{
	if(document.getElementById("db_story_type").value == "")
		{
		alert(typmsg);
		document.getElementById("rdo_0").focus();
		return false;
		}
	}
form_data += del + "db_story_type=" + document.getElementById("db_story_type").value.trim(); 
if(file_required)
	{
	if(document.getElementById("db_story_file").value == "")
		{
		alert(filmsg);
		document.getElementById("btn_select_image").focus();
		return false;
		}
	}
form_data += del + "db_story_file=" + document.getElementById("db_story_file").value.trim();
if(document.getElementById("db_story_text").value.trim() == "")
	{
	tinstruction = storymsg;
	tnextstep = "document.getElementById('story_text').focus();";
	show_popup(blabel, tlabel, bimage, timage, binstruction, tinstruction, bclose, cimage, bnextstep, tnextstep);
	return false;
	}
form_data += del + "db_story_text=" + document.getElementById("db_story_text").value.trim();
if(document.getElementById("db_story_fn").value.trim() == "")
	{
	tinstruction = document.getElementById('story_fn_msg').value;
	tnextstep = "document.getElementById('story_fn').focus();";
	show_popup(blabel, tlabel, bimage, timage, binstruction, tinstruction, bclose, cimage, bnextstep, tnextstep);
	return false;
	}
form_data += del + "db_story_fn=" + document.getElementById("db_story_fn").value.trim();
if(document.getElementById("db_story_ln").value.trim() == "")
	{
	tinstruction = document.getElementById('story_ln_msg').value;
	tnextstep = "document.getElementById('story_ln').focus();";
	show_popup(blabel, tlabel, bimage, timage, binstruction, tinstruction, bclose, cimage, bnextstep, tnextstep);
	return false;
	}
form_data += del + "db_story_ln=" + document.getElementById("db_story_ln").value.trim();
if(document.getElementById("db_story_phone").value.trim() == "")
	{
	tinstruction = document.getElementById('story_phone_msg').value;
	tnextstep = "document.getElementById('story_phone').focus();";
	show_popup(blabel, tlabel, bimage, timage, binstruction, tinstruction, bclose, cimage, bnextstep, tnextstep);
	return false;
	}
stype = "phone";	
ele = document.getElementById("db_story_phone");
msg = document.getElementById("story_phone_msg").value;
other = "";
if(!test_element(stype, other, tlabel, msg, ele)){ return false; }	
form_data += del + "db_story_phone=" + document.getElementById("db_story_phone").value.trim();
if(document.getElementById("db_story_email").value.trim() == "")
	{
	tinstruction = document.getElementById('story_email_msg').value;
	tnextstep = "document.getElementById('story_email').focus();";
	show_popup(blabel, tlabel, bimage, timage, binstruction, tinstruction, bclose, cimage, bnextstep, tnextstep);
	return false;
	}
stype = "email";	
ele = document.getElementById("db_story_email");
msg = document.getElementById("story_email_msg").value;
other = "";
if(!test_element(stype, other, tlabel, msg, ele)){ return false; }	
form_data += del + "db_story_email=" + document.getElementById("db_story_email").value.trim();
//alert(form_data);
return form_data;

}
