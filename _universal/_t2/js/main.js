
function closeCollapsedNavbar()
{
    var navbar_toggle = $('.navbar-toggle');
    if (!navbar_toggle.hasClass('collapsed')) {
        navbar_toggle.trigger('click');
    }
}

function toggle_menu(idx)
{
if(idx == 1)
	{
	initialize_session();	
	return;
	}
var el = "";
for(x=0;x<11;x++)
	{
	el = "content" + x;
	document.getElementById(el).style.display = "none";
	}
el = "content" + idx;
document.getElementById(el).style.display = "block";
    closeCollapsedNavbar();
}

function initialize_session()
{
var campaign_idx = document.getElementById("campaign_idx").value;
var session_initialized = document.getElementById("session_set").value;
if(session_initialized == "yes")
	{ 
	show_shop("");
	return; 
	}
document.getElementById("session_set").value = "yes";
var href = "initialize_session.php?campaign_idx=" + campaign_idx;
$.ajax
	(
		{
		url: href, 
		success: function(result)
			{ 
			show_shop(result);
			}
		}
	);
		
}

function show_shop(result)
{
//alert(result);
var el = "";
for(x=0;x<11;x++)
	{
	el = "content" + x;
	document.getElementById(el).style.display = "none";
	}
el = "content1";
window.setInterval("check_session();", 3000);
document.getElementById(el).style.display = "block";	
	
}
   
function select_item(idx, gc)
{ 
var gc = "no";	
clear_content();
var href = "actions/add_item.php?idx=" + idx + "&gift_certificate=" + gc;
$.ajax
	(
		{
		url: href, 
		success: function(result){ show_options(result); }
		}
	);
		
}

function select_gc(idx)
{
if(idx != "")
	{
	var gc = "yes";
	var href = "actions/add_item.php?idx=" + idx + "&gift_certificate=" + gc;
	$.ajax
		(
			{
			url: href, 
			success: function(result){ show_gc(result); }
			}
		);
	}	
}

function show_gc(result)
{
document.getElementById("gc_separator").style.display = "block";
document.getElementById("the_options").innerHTML = result;
}

function show_options(result)
{
document.getElementById("content3").innerHTML = result;
toggle_menu(3);
}

function iterate_object(obj, level)
{
	
var msg = "";
var lev = "";
for(x=1;x<level+1;x++){ lev += "-"; }
for (var key in obj) 
	{
	if (obj.hasOwnProperty(key)) 
		{
		if(typeof(obj[key]) == "object")
			{
			msg += lev + key + " -> " + "\n" + iterate_object(obj[key], level + 1);	
			}
		else
			{
			msg += lev + key + " -> " + obj[key] + "\n";
			}
		}
	}	
return msg;	
}

function increment(o_n)
{
var obj = document.getElementById(o_n);	
var val = new Number(obj.innerHTML);
if(val < 10)
	{
	val++;	
	}
obj.innerHTML = val;
set_selected();
}

function decrement(o_n)
{
var obj = document.getElementById(o_n);	
var val = new Number(obj.innerHTML);
if(val > 0)
	{
	val--;	
	}
obj.innerHTML = val;
set_selected();	
}

function do_increment(o_n, after, page)
{
var obj = document.getElementById(o_n);	
var val = new Number(obj.innerHTML);
if(val < 10)
	{
	val++;	
	}
obj.innerHTML = val;
if(page != "options"){ set_session(o_n, val, after, page); }
else{ set_selected(); }	

}

function do_decrement(o_n, after, page)
{
var obj = document.getElementById(o_n);	
var val = new Number(obj.innerHTML);
if(val > 0)
	{
	val--;	
	}
obj.innerHTML = val;
if(page != "options"){ set_session(o_n, val, after, page); }
else{ set_selected(); }	
}

function zero_qty(o_n)
{
var obj = document.getElementById(o_n);	
obj.value = 0;
set_session(o_n, 0);	
}
 
function set_session(ids, quantity, after)
{

var idz = ids.split("_");
var specs = "?cart_id=" + idz[2] + "&option_id=" + idz[3] + "&quantity=" + quantity;
var href = "actions/session_quantity.php" + specs;
$.ajax
	(
		{
		url: href,
		success: function(result){ show_cart_detail(); }
		}
	);
	
}

function ays(id)
{
	
document.getElementById("ays_next").value = id;
show_ays();
	
}

function show_ays()
{
	
document.getElementById("popup").style.display = "block";
document.getElementById("ays").style.display = "block";	

}

function hide_ays()
{
	
document.getElementById("popup").style.display = "none";
document.getElementById("ays").style.display = "none";	
	
}

function remove_item()
{

var id = document.getElementById("ays_next").value;
var specs = "?cart_id=" + id;
var href = "actions/remove_item.php" + specs;
$.ajax
	(
		{
		url: href,
		success: function(result){ hide_ays(); show_cart_detail(); }
		}
	);
	
}

function do_nothing(txt)
{
//alert(txt);
}

function set_selected()
{
	
var available = document.getElementById("available_options").value;
var options = available.split("|");
var option_count = options.length;
var del = "";
var options_selected = "";
for(x=0;x<option_count;x++)
	{
	var nme = "options_option_" + options[x].trim();
	var num = new Number(document.getElementById(nme).value);
	options_selected += del + options[x].trim();
	del = "|";
	}
// document.getElementById("options_selected").value = options_selected;
	
}

function save_options(max_lines, max_characters)
{
var del = "?";
var dl = "";
var specs = "";
var lines = max_lines;
var available_options = document.getElementById("available_options").value;
var selections = available_options.split("|");
var options = "";
for(x=0;x<selections.length;x++)
	{
	nme = "options_option_" + selections[x].trim();	
	var qty = document.getElementById(nme).innerHTML;
	var item = selections[x].trim() + "_" + qty;
	options += dl + item;
	dl = "|";
	}
specs += del + "options=" + options;
del = "&"
for(x=1;x<=lines;x++)
	{
	var line = "";	
	if(document.getElementById("line" + x))
		{
		line = document.getElementById("line" + x).value;
		}	
	specs += del + "line" + x + "=" + line;
	}

var href = "actions/add_option.php" + specs;
$.ajax
	(
		{
		url: href, 
		success: function(result)
			{ 
			//show_order_info(result);
			show_cart_detail(); 
			}
		}
	);
		
}

function show_order_info(result)
{
document.getElementById("content6").innerHTML = result;
toggle_menu(6);
}

function select_gift_certificate(gc_yes, gc_no)
{
var pagc = document.getElementById("pagc");	
var is_gc = document.getElementById("is_gc");
if(pagc.value == "true")	
	{
	pagc.value = "false";
	is_gc.src = "images/" + gc_no;
	}
else
	{
	pagc.value = "true";
	is_gc.src = "images/" + gc_yes;	
	}
}

function start_over()
{
window.location.assign("start_over.php");
}

function show_checkout()
{
var href = "actions/show_checkout.php";
clear_content();
$.ajax
	(
		{
		url: href, 
		success: function(result){ show_checkout_page(result); }
		}
	);
		
}

function show_story_share()
{
var href = "actions/show_story_share.php?page=home";
clear_content();
$.ajax
	(
		{
		url: href, 
		success: function(result){ show_story_share_page(result); }
		}
	);
		
}

function show_story_share_page(result)
{
document.getElementById("content8").innerHTML = result;
toggle_menu(8);
}

function show_cart_detail()
{
var href = "actions/show_cart_details.php";
hide_side_cart();
clear_content();
$.ajax
	(
		{
		url: href, 
		success: function(result){ show_cart_detail_page(result); }
		}
	);
		
}

function show_cart_detail_page(result)
{
document.getElementById("content3").innerHTML = result;
toggle_menu(3);
document.getElementById("order_count").innerHTML = document.getElementById("order_count_value").value;
}

function show_cart_summary()
{
var href = "actions/show_cart_summary.php";
clear_content();
$.ajax
	(
		{
		url: href, 
		success: function(result){ show_cart_summary_page(result); }
		}
	);
		
}

function show_cart_summary_page(result)
{
document.getElementById("content3").innerHTML = result;
toggle_menu(3);
}

function country_change(add_typ, add_idx)
{
var obj_name = add_typ + "_country";
var obj = document.getElementById(obj_name);
var state, zip, phone;
if(obj.value != "")
	{
	if(obj.value == "US")
		{
		state = document.getElementById(add_typ + "_us_states").value;
		zip = document.getElementById(add_typ + "_us_zip").value;	
		}
	else
		{
		if(obj.value == "CA")
			{
			state = document.getElementById(add_typ + "_canada_provinces").value;
			zip = document.getElementById(add_typ + "_canada_postal_code").value;
			}
		else
			{
			state = document.getElementById(add_typ + "_other_divisions").value;
			zip = document.getElementById(add_typ + "_other_postal_code").value;			
			}	
		}
	document.getElementById(add_typ + "_state_container").innerHTML = state;
	document.getElementById(add_typ + "_postal_code_container").innerHTML = zip;
	}
	
	
}

function process_order()
{
var form_data = process_verify();
if(!form_data){ return; }
var blabel = true;
var tlabel = document.getElementById("process_order_label").value;
var bimage = false;
var timage = document.getElementById("process_order_image").value;
var binstruction = true;
var tinstruction = document.getElementById("process_order_standby").value;
var bclose = false;
var cimage = "";
var bnextstep = false;
var tnextstep = "";
show_popup(blabel, tlabel, bimage, timage, binstruction, tinstruction, bclose, cimage, bnextstep, tnextstep);
var href = "actions/process_order.php";
var force_success_pwd = document.getElementById("force_success_pwd").value;
if(force_success_pwd.trim() != "")
	{
	form_data += "&force_success_pwd=" + force_success_pwd;	
	}
 
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ check_process(result); }
		}
	);
		
}

function check_process(result)
{
	// document.getElementById("content3").innerHTML = result; toggle_menu(3); hide_popup()
if(result.indexOf("process~successful") == -1)
	{
	show_process_error(result);	
	}
else
	{
	var href = "actions/show_confirmation.php";
	location.href = href;	
	}
}

function show_process_error(result)
{
//alert(result);
var	st = result.indexOf("results-start|") + 14;
var	ed = result.indexOf("|results-end");
var error_data = result.substring(st, ed - st);
var ed = error_data.split("|");
var prop;
var x = 0;
var head = document.getElementById("cc_process_head").value;
var err_msg = "<div style='text-align: left;'>" + document.getElementById("cc_process_message").value + "<br /><br />";
var err_detail = "<b><u>" + document.getElementById("cc_process_detail").value + "</u></b><br />";
var del = "<br />";
var cim = "images/delete.png";
for(x=0;x<ed.length;x++)
	{
	if(ed[x].indexOf("~") > -1)
		{
		prop = ed[x].split("~");
		err_detail += prop[0].trim() + ": " + prop[1].trim() + "<br />";
		}
	}
err_msg += err_detail + "</div>";
hide_popup();
show_popup(true, head, false, '', true, err_msg, true, cim, false, '');
// window.open("email_confirmation.php", "_blank");
}

function show_confirmation(result)
{
window.open("confirmation.php");
}

function show_checkout_page(result)
{
document.getElementById("content4").innerHTML = result;
toggle_menu(4);
}

function process_verify()
{
	
var form_data = true;
form_data = "";
var address_data = check_billing_address();
if(address_data == ""){ return false; }
form_data += address_data;
var credit_card_data = check_credit_card();
if(credit_card_data == ""){ return false; }
form_data += credit_card_data;
var shipping_data = check_shipping();
if(shipping_data === false){ return false; }
form_data += shipping_data;
var additional = check_marketing();
if(additional === false)
	{ 
	document.getElementById("div_marketing").style.display = "block";
	return false; 
	}
form_data += additional;
var promotion = check_promotion();
if(promotion === false){ return false; }
form_data += promotion;
var terms = check_terms();
if(terms === false){ return false; }
form_data += terms;
return form_data;
	
}

function check_billing_address()
{
var ele, msg_label, msg, foc, form_data, del;
var required = ["_fn", "_ln", "_country", "_address1", "_city", "_state_prov", "_postal_code", "_phone", "_email"];
var special = [{element: "_postal_code", dependant: "_country", type: "postal_code"}, {element: "_email", dependant: "_country", type: "email"}, {element: "_phone", dependant: "_country", type: "phone"}];
var all_data = ["_fn", "_mn", "_ln", "_country", "_company", "_address1", "_address2", "_city", "_state_prov", "_postal_code", "_phone", "_email"];
var pre = "bill";

form_data = "";
del = "";
msg_label = document.getElementById("validate_billing_head").value;

// Billing Address Required
for(r=0;r<required.length;r++)
	{
	foc = required[r];	
	ele = document.getElementById(pre+foc);
	msg = document.getElementById(foc + "_validate").value;
	val = ele.value.trim();
	if(val == "")
		{ 
		show_message(msg_label, msg, ele); 
		return false; 
		}
	}

// Billing Address Validated
for(s=0;s<special.length;s++)
	{
	foc = special[s].element;	
	ele = document.getElementById(pre+foc);
	msg = document.getElementById(foc + "_validate2").value;
	dep = pre + special[s].dependant;
	other = document.getElementById(dep).value;
	if(!test_element(special[s].type, other, msg_label, msg, ele)){ return false; }
	}

// Billing Address Values
del = "";
for(s=0;s<all_data.length;s++)
	{
	foc = all_data[s];	
	ele = document.getElementById(pre+foc);
	form_data += del + pre + foc + "=" + ele.value;
	del = "&";
	}
	
return form_data;	
}

function check_credit_card()
{
var ele, msg_label, msg, foc, form_data, del;
var required = ["_cc_name", "_cc_type", "_cc_number", "_cc_month", "_cc_year"];
var special = [{element: "_cc_number", dependant: "_cc_type", type: "credit_card"}];
var all_data = ["_cc_name", "_cc_type", "_cc_number", "_cc_month", "_cc_year"];
var pre = "bill";

form_data = "";
del = "";
msg_label = document.getElementById("validate_credit_card_head").value;

// Billing Address Required
for(r=0;r<required.length;r++)
	{
	foc = required[r];	
	ele = document.getElementById(pre+foc);
	msg = document.getElementById(foc + "_validate").value;
	val = ele.value.trim();
	if(val == "")
		{ 
		show_message(msg_label, msg, ele); 
		return false; 
		}
	}

// Billing Address Validated
for(s=0;s<special.length;s++)
	{
	foc = special[s].element;	
	ele = document.getElementById(pre+foc);
	msg = document.getElementById(foc + "_validate2").value;
	dep = pre + special[s].dependant;
	other = document.getElementById(dep).value;
	if(!test_element(special[s].type, other, msg_label, msg, ele)){ return false; }
	}

// Billing Address Values
del = "";
for(s=0;s<all_data.length;s++)
	{
	foc = all_data[s];	
	ele = document.getElementById(pre+foc);
	form_data += del + pre + foc + "=" + ele.value;
	del = "&";
	}
	
return "&" + form_data;	
}

function check_shipping()
{
	
var msg_label = document.getElementById("validate_shipping_method_head").value;
var el = document.getElementsByName("ship_method");
if(el.length > 0)
	{
	for(i=0;i<el.length;i++)
		{
		if(el[i].checked){ return "&ship_method=" + el[i].value; }	
		}
	foc = "ship_to_bill";	
	ele = document.getElementById(foc);
	msg = document.getElementById("validate_shipping_method_msg").value;
	show_message(msg_label, msg, ele); 
	return false;
	}
return "";
	
}

function check_marketing()
{

var form_data = "", name = "", marketing_value = "", ele, msg_label = "", msg = "", del = "";	
ar = ["how_u_heard", "buying_as", "opt_in", "group_code"];
for(x=0;x<ar.length;x++)
	{
	name = ar[x];
	if(document.getElementById(name + "_value"))
		{
		marketing_value = document.getElementById(name + "_value").value;
		if(document.getElementById(name + "_required").value == "true")
			{
			if(marketing_value == "")	
				{
				ele = document.getElementById(name);
				msg_label = document.getElementById(name + "_warning_label").value;
				msg = document.getElementById(name + "_warning").value;
				show_message(msg_label, msg, ele); 
				return false;
				}
			}
		form_data += del + name + "=" + marketing_value;
		del = "&";
		}
	}
return form_data;

}

function check_promotion()
{
return "";	
}

function check_terms()
{
var opt_name = "tnc";
var form_data = "";
var opt = document.getElementById(opt_name);	
if(!opt.checked)
	{
	ele = opt;
	msg_label = document.getElementById("validate_tnc_head").value;
	msg = document.getElementById("validate_tnc_message").value;
	show_message(msg_label, msg, ele); 
	return false;	
	}
form_data = "&terms_and_conditions=yes";
return form_data;	
}

function show_message(label, msg, elem)
{
	
var im = "images/baddata.png";
var cim = "images/delete.png";
var next_step = "";
var bnext = false;
if((elem) && (elem.id))
	{
	var name = elem.id;
	next_step = "document.getElementById('" + name + "').focus();";
	bnext = true
	}
show_popup(true, label, false, im, true, msg, true, cim, bnext, next_step);

}

function ship_billing(obj)
{
if((save_billing())&&(save_credit_card()))
	{
	var billing_address_values = check_billing_address();
	if(billing_address_values)
		{
		var href = "actions/ship_to_bill.php";	
		$.ajax
			(
				{
				url: href, 
				type: "POST",
				data: billing_address_values,
				success: function(result){ show_shipto_page(result); }
				}
			);	
		}
	else
		{
		obj.checked = false;	
		}
	}
else 
	{
	obj.checked = false;	
	}
}

function save_billing()
{

var process_save = false;
var bill_validated_overall = document.getElementById("bill_validated_overall").value;
var bill_validated_javascript = document.getElementById("bill_validated_javascript").value;
var bill_validated_fedex = document.getElementById("bill_validated_fedex").value;
if(bill_validated_overall == "true"){ process_save = true; }
else
	{
	if(bill_validated_javascript == "true")
		{
		if(bill_validated_fedex == "true")
			{
			process_save = true;
			document.getElementById("bill_validated_overall").value = "true";
			record_bill_address_static();
			}
		else
			{
			check_fedex_address(0, "billing");
			}	
		}
	else
		{
		var billing_address_values = check_billing_address();
		if(billing_address_values)
			{	
			document.getElementById("bill_validated_javascript").value = "true";
			record_bill_address_static();
			save_billing();
			}
		}
	}

if(process_save)
	{
	return true;
	}
return false;
	
}

function check_fedex_address(idx, method)
{
var href = "actions/check_fedex_address.php";
var address_values = "idx=" + idx;	
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: address_values,
		success: function(result){ check_fedex_results(idx, result, method); }
		}
	);
return true;	
}

function check_fedex_results(idx, res, method)
{
var multi_pass = true;
var typ = "bill";
var msg_label = document.getElementById("validate_billing_head").value;
var msg = document.getElementById("fedex_validating").value;
// show_popup(true, msg_label, false, "", true, msg, false, "", false, "");
if(idx != 0)
	{ 
	typ = "ship"; 
	msg_label = document.getElementById("validate_shipping_head").value;
	}
var results = res.split("|");
idx = 0;
if(method != "billing"){ idx = results[2]; }
var next = false;
if(results[0] == "failed")
	{
	msg = document.getElementById("fedex_avs_invalid").value;
	ele = document.getElementById(typ + "_fn");
	//show_message(msg_label, msg, ele); 
	if(multi_pass)
		{
		accept_suggestion(idx, results[1], method);	
		}
	else
		{
		show_popup(true, msg_label, false, "", true, msg, true, "", false, "");	
		}
	}	
else if(results[0] == "changes")
	{ 
	adrs = JSON.parse(results[1]);
	accept_suggestion(idx, results[1], method);
	}
else
	{ 
	next = true; 
	}
if(next)
	{
	if(idx ==0)	
		{
		hide_popup();
		accept_suggestion(idx, results[1], method);
		}
	else
		{
		accept_suggestion(idx, results[1], method);	
		}
	}

}

function get_changed_message(addresses, results, idx, method)
{
var pm = results.replace(/"/mg, "~");	
var retval = "<div style='text-align: left;'><p>" + document.getElementById("fedex_avs_changes_message").value + "</p></div>";
retval += "<div style='text-align: left;'><p>" + document.getElementById("fedex_avs_changes_accept").value + "</p></div>";
retval += "<div style='text-align: left;'><p>" + document.getElementById("fedex_avs_changes_deny").value + "</p></div>";
retval += "<div style='text-align: left;'><p>" + document.getElementById("for_assistance").value + "</p></div>";
retval += "<div><center><table cellspacing=25><tr>";
retval += "<td align=center><b>" + document.getElementById("current_address").value + "</b></td>";
retval += "<td align=center><b>" + document.getElementById("suggested_address").value + "</b></td>";
retval += "</tr><tr>";
retval += "<td style='border-style: solid; border-color: #F0F0F0; border-width: 1px; padding: 0 25px 25px 25px;'>";
retval += format_address_html(addresses.current) + "</td>";
retval += "<td style='border-style: solid; border-color: #F0F0F0; border-width: 1px; padding: 0 25px 25px 25px;'>";
retval += format_address_html(addresses.new) + "</td>";
retval += "</tr><tr><td></td><td align=center>";
retval += "<a class=add-to-cart href='javascript: accept_suggestion(" + idx + ", \"" + pm + "\", \"" + method + "\");' style='width: 100%;'>";
retval += document.getElementById("fedex_accept").value + "</a><br /><br />";
retval += "<a class=add-to-cart href='javascript: refuse_suggestion(" + idx + ", \"" + method + "\");' style='width: 100%;'>";
retval += document.getElementById("fedex_deny").value + "</a>";
retval += "<input type=hidden id=suggested_address value='" + pm + "' /></td>";
retval += "</tr></table></center></div>";
//alert(retval);
//retval = "<textarea style='width:400px; height: 200px;'>" + retval + "</textarea>";
return retval;
}

function accept_suggestion(idx, results, method)
{
//var pm = results.replace(/~/mg, '"');
var pm = results;
var typ = "bill";
if(idx > 0){ typ = "ship"; }	
try
	{
	adrs = JSON.parse(pm);
	if(document.getElementById(typ + "_address1").value.trim().toUpperCase() != adrs.new.address1)
		{
		document.getElementById(typ + "_address1").value = adrs.new.address1;
		document.getElementById(typ + "_address1").style.color = "#008000";
		document.getElementById(typ + "_address1").style.fontWeight = "bold";
		}
	if(document.getElementById(typ + "_city").value.trim().toUpperCase() != adrs.new.city)
		{
		document.getElementById(typ + "_city").value = adrs.new.city;
		document.getElementById(typ + "_city").style.color = "#008000";
		document.getElementById(typ + "_city").style.fontWeight = "bold";
		}
	if(document.getElementById(typ + "_postal_code").value.trim().toUpperCase() != adrs.new.postal_code)
		{
		document.getElementById(typ + "_postal_code").value = adrs.new.postal_code;
		document.getElementById(typ + "_postal_code").style.color = "#008000";
		document.getElementById(typ + "_postal_code").style.fontWeight = "bold";
		}
	document.getElementById(typ + "_state_prov").value = adrs.new.state_prov;
	document.getElementById(typ + "_country").value = adrs.new.country;
	var address_data = "idx=" + idx;
	address_data += "&fn=" + adrs.new.fn;
	address_data += "&mn=" + adrs.new.mn;
	address_data += "&ln=" + adrs.new.ln;
	address_data += "&company=" + adrs.new.company;
	address_data += "&address1=" + adrs.new.address1;
	address_data += "&address2=" + adrs.new.address2;
	address_data += "&city=" + adrs.new.city;
	address_data += "&state_prov=" + adrs.new.state_prov;
	address_data += "&postal_code=" + adrs.new.postal_code;
	address_data += "&country=" + adrs.new.country;
	address_data += "&validated_javascript=true";
	address_data += "&validated_fedex=true";
	address_data += "&validated_overall=true";
	}
catch(e)
	{
	var address_data = "idx=" + idx;
	address_data += "&fn=" + document.getElementById(typ + "_fn").value;
	address_data += "&mn=" + document.getElementById(typ + "_mn").value;
	address_data += "&ln=" + document.getElementById(typ + "_ln").value;
	address_data += "&company=" + document.getElementById(typ + "_company").value;
	address_data += "&address1=" + document.getElementById(typ + "_address1").value;
	address_data += "&address2=" + document.getElementById(typ + "_address2").value;
	address_data += "&city=" + document.getElementById(typ + "_city").value;
	address_data += "&state_prov=" + document.getElementById(typ + "_state_prov").value;
	address_data += "&postal_code=" + document.getElementById(typ + "_postal_code").value;
	address_data += "&country=" + document.getElementById(typ + "_country").value;
	address_data += "&validated_javascript=true";
	address_data += "&validated_fedex=true";
	address_data += "&validated_overall=true";	
	}
document.getElementById(typ + "_validated_javascript").value = "true";
document.getElementById(typ + "_validated_fedex").value = "true";
document.getElementById(typ + "_validated_overall").value = "true";
var href = "actions/update_address.php";
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: address_data,
		success: function(result){ do_nothing(result); }
		}
	);
if(method == "billing")
	{
	hide_popup();
	document.getElementById("ship_to_bill").checked = true;	
	}
else
	{
	if(method == "other")
		{
		set_other_shipto(idx);
		hide_popup();
		hide_shipto();
		document.getElementById("ship_to_other").checked = true;		
		}
	else
		{
		hide_popup();
		hide_shipto();
		document.getElementById("ship_to_different").click();
		}
	}
}

function set_other_shipto(idx)
{
var href = "actions/set_other_shipto.php?idx=" + idx;
$.ajax
	(
		{
		url: href, 
		type: "GET",
		success: function(result){ do_nothing(result); }
		}
	);	
}

function refuse_suggestion(idx, method)
{
var typ = "bill";
if(idx > 0){ typ = "ship"; }
hide_popup();
document.getElementById("bill_validated_fedex").value = "false";
document.getElementById(typ + "_address1").focus();	
}

function format_address_html(address)
{
	
var retval = address.fn;
if(address.mn != ""){ retval += " " + address.mn; }
retval += " " + address.ln;
retval += "<br />" + address.address1;
if(address.address2 != ""){ retval += "<br />" + address.address2; }
retval += "<br />" + address.city + ", " + address.state_prov + " " + address.postal_code;	
return retval;
}

 
function save_credit_card()
{
var credit_card_values = check_credit_card();
if(credit_card_values)
	{
	var href = "actions/save_credit_card.php";	
	$.ajax
		(
			{
			url: href, 
			type: "POST",
			data: credit_card_values,
			success: function(result){ do_nothing(result); }
			}
		);	
	return true;
	}	
return false;
}
  
function save_shipping()
{
var shipping_values = check_shipping();
if(shipping_values)
	{
	var href = "actions/save_shipping.php";	
	$.ajax
		(
			{
			url: href, 
			type: "POST",
			data: shipping_values,
			success: function(result){ do_nothing(result); }
			}
		);	
	return true;
	}
return false;
	
}

function ship_other(obj)
{
if((save_billing())&&(save_credit_card()))
	{
	var billing_address_values = check_billing_address();
	if(billing_address_values)
		{
		var href = "actions/ship_to_other.php";
		$.ajax
			(
				{
				url: href, 
				type: "POST",
				data: billing_address_values,
				success: function(result){ show_shipto_page(result); }
				}
			);
		}
	else
		{
		obj.checked = false;
		}
	}
else
	{
	obj.checked = false;	
	}
}

function add_new_address()
{
var href = "actions/add_an_address.php";
document.getElementById("shipdifferent_container").innerHTML = "";	
document.getElementById("shipdifferent_container").style.display = "none";	
$.ajax
	(
		{
		url: href, 
		type: "POST",
		success: function(result){ show_shipto_page(result); }
		}
	);
}

function ship_different(obj)
{
if((save_billing())&&(save_credit_card()))
	{
	var billing_address_values = check_billing_address();
	if(billing_address_values)
		{
		var href = "actions/addresses.php";
		$.ajax
			(
				{
				url: href, 
				type: "POST",
				data: billing_address_values,
				success: function(result){ show_shipto_different_page(result); }
				}
			);	
		}
	else
		{
		obj.checked = false;	
		}
	}
else
	{
	obj.checked = false;	
	}
}

function show_shipto_page(result)
{
	
document.getElementById("shipto_container").innerHTML = result;
document.getElementById("shipto").style.display = "block";	
document.getElementById("shipto_container").style.display = "block";

}

function show_shipto_different_page(result)
{
	
document.getElementById("shipdifferent_container").innerHTML = result;
document.getElementById("shipto").style.display = "block";	
document.getElementById("shipdifferent_container").style.display = "block";

}

function hide_shipto_page()
{
document.getElementById("shipto").style.display = "none";	
document.getElementById("shipto_container").innerHTML = "";	
document.getElementById("shipto_container").style.display = "none";	
}

function hide_shipto_different_page()
{
document.getElementById("shipto").style.display = "none";	
document.getElementById("shipdifferent_container").innerHTML = "";	
document.getElementById("shipdifferent_container").style.display = "none";	
}

function test_element(test_type, other, msg_label, msg, obj)
{
	
rx = get_test(test_type, other);
if(!rx.test(obj.value))
	{ 
	show_message(msg_label, msg, obj); 
	return false;
	}
return true;

}

function get_test(test_type, other)
{
	
if(test_type == "email")
	{
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;	
	}
if(test_type == "postal_code")
	{
	return get_postal_code_test(other);	
	}
if(test_type == "phone")
	{
	return get_phone_test(other);	
	}
if(test_type == "credit_card")
	{
	return get_credit_card_test(other);	
	}
return;

}

function get_postal_code_test(country)
{
if(country == "US")	
	{
	return /\d{5}([ \-]\d{4})?/;	
	}
else
	{
	if(country == "CA")	
		{
		return /[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ ]?\d[ABCEGHJ-NPRSTV-Z]\d/;	
		}	
	else
		{
		return /.*/;	
		}
	}
}

function get_phone_test(country)
{
return /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;	
}

function get_credit_card_test(type)
{
if(type.toLowerCase() == "visa"){ return /4[0-9]{12}(?:[0-9]{3})/; }
if(type.toLowerCase() == "mastercard"){ return /5[1-5][0-9]{14}/; }
if(type.toLowerCase() == "discover"){ return /6(?:011|5[0-9]{2})[0-9]{12}/; }
if(type.toLowerCase() == "amex"){ return /3[47][0-9]{13}/; }
return;
}

function hide_shipto_other()
{
hide_shipto();
document.getElementById("ship_to_other").checked = false;		
}

function save_shipto_other()
{
	
}

function hide_shipto_different()
{
hide_shipto_different_page();
document.getElementById("ship_to_different").checked = false;		
}

function hide_shipto()
{
document.getElementById("shipto").style.display = "none";	
document.getElementById("shipto_container").innerHTML = "";	
document.getElementById("shipto_container").style.display = "none";		
}

function hide_shipto_bill()
{
hide_shipto();		
document.getElementById("ship_to_bill").checked = false;		
}

function save_shipto(method)
{
	
var shipping_address_values = "";
if(method == "bill"){ shipping_address_values = "index=0"; }
else{ shipping_address_values = check_shipto_address(); }
if(shipping_address_values)
	{
		
	shipping_address_values += "&method=" + method;
	var href = "actions/save_shipto.php";
	$.ajax
		(
			{
			url: href, 
			type: "POST",
			data: shipping_address_values,
			success: function(result){ check_fedex_results(1, result, method); }
			}
		);	
	}
else
	{
	var nme = "ship_to_" + method;
	obj = document.getElementById(nme);
	obj.checked = false;	
	}	
	
}

function save_shipto_results(result)
{
	
var nme = "ship_to_" + result;
obj = document.getElementById(nme);
obj.checked = true;
hide_shipto();	
	
}

function check_shipto_address()
{
var ele, msg_label, msg, foc, form_data, del;
var required = ["_fn", "_ln", "_country", "_address1", "_city", "_state_prov", "_postal_code"];
var special = [{element: "_postal_code", dependant: "_country", type: "postal_code"}];
var all_data = ["_fn", "_mn", "_ln", "_country", "_company", "_address1", "_address2", "_city", "_state_prov", "_postal_code"];
var pre = "ship";

form_data = "";
del = "";
msg_label = document.getElementById("validate_shipping_head").value;

// shipto Address Required
for(r=0;r<required.length;r++)
	{
	foc = required[r];	
	ele = document.getElementById(pre+foc);
	msg = document.getElementById(foc + "_validate").value;
	val = ele.value.trim();
	if(val == "")
		{ 
		show_message(msg_label, msg, ele); 
		return false; 
		}
	}

// Billing Address Validated
for(s=0;s<special.length;s++)
	{
	foc = special[s].element;	
	ele = document.getElementById(pre+foc);
	msg = document.getElementById(foc + "_validate2").value;
	dep = pre + special[s].dependant;
	other = document.getElementById(dep).value;
	if(!test_element(special[s].type, other, msg_label, msg, ele)){ return false; }
	}

// Billing Address Values
del = "";
for(s=0;s<all_data.length;s++)
	{
	foc = all_data[s];	
	ele = document.getElementById(pre+foc);
	form_data += del + pre + foc + "=" + ele.value;
	del = "&";
	}
	
return form_data;	
}

// show_popup(...) instructions
//show_popup(
//		   blabel,     	(true or false) should label be displayed
//		   tlabel,		(text) Label of the Pop Up
//		   bimage,		(true or false) should image be displayed
//		   timage,		(text) image file url
//		   binstruction,(true or false) should instruction be displayed
//		   tinstruction,(text) instructions
//		   bclose,		(true or false) close button will be displayed
//		   cimage,		(text) button image file url
//		   bnextstep,	(true or false) a follow up procedure will be processed
//		   tnextstep	(text) code to be processed
//		  )

function show_popup(blabel, tlabel, bimage, timage, binstruction, tinstruction, bclose, cimage, bnextstep, tnextstep)
{
if (bclose)
	{
	document.getElementById("popup_button_container").style.display = "block";
	if (cimage.trim().length > 0) 
		{
		document.getElementById("popup_close").src = cimage;
		} 
	else 
		{
			document.getElementById("popup_close").src = "images/delete.png";
		}
	document.getElementById("popup_close").style.display = "block";
	document.getElementById("popup_head_spacer").style.display = "none";
	document.getElementById("popup_button_container").style.display = "block";
	//alert(document.getElementById("popup_button_container").style.display);
	} 
else
	{
	document.getElementById("popup_button_container").style.display = "none";
	document.getElementById("popup_head_spacer").style.display = "block";
	document.getElementById("popup_close").style.display = "none";
	}
if (blabel)
	{
	document.getElementById("popup_label").style.display = "block";
	document.getElementById("popup_label").innerHTML = tlabel;
	} 
else 
	{
	document.getElementById("popup_label").style.display = "none";
	}
if (bimage)
	{
	document.getElementById("popup_image_row").style.display = "block";
	document.getElementById("popup_image").src = timage;
	} 
else 
	{
	document.getElementById("popup_image_row").style.display = "none";
	}
if (binstruction)
	{
	document.getElementById("popup_table_instruction").style.display = "block";
	document.getElementById("popup_instruction").innerHTML = tinstruction;
	} 
else 
	{
	document.getElementById("popup_table_instruction").style.display = "none";
	}
if (bnextstep)
	{
	document.getElementById("popup_next").value = tnextstep;
	} 
else 
	{
	document.getElementById("popup_next").value = "";
	}
document.getElementById("popup").style.display = "block";
document.getElementById("popup_content").style.display = "block";
// scroll to top
fixPopupPosition();
//alert(document.getElementById("popup_button_container").style.display);
}

function hide_popup()
{
	
document.getElementById("popup").style.display = "none";
document.getElementById("popup_content").style.display = "none";
document.getElementById("popup_label").innerHTML = "";
document.getElementById("popup_label").style.display = "none";
document.getElementById("popup_image").src = "../_t2/images/blank.png";
document.getElementById("popup_image_row").style.display = "none";
document.getElementById("popup_instruction").innerHTML = "";
document.getElementById("popup_table_instruction").style.display = "none";
document.getElementById("popup_close").src = "images/delete.png";
if(document.getElementById("popup_next").value.trim() != "")
	{
	eval(document.getElementById("popup_next").value);	
	}
	
}

function clear_content()
{
document.getElementById("gc_separator").style.display = "none";
document.getElementById("the_options").innerHTML = "";
document.getElementById("content3").innerHTML = "";	
document.getElementById("content4").innerHTML = "";
document.getElementById("side_cart_content").innerHTML = "";
nv = "-1000px"; 
$('.side-cart').animate({ right: nv }, 500);
}

function update_options(idx, refresh)
{
var specs = "?idx=" + idx;
var lines = document.getElementById("max_lines").value;
del = "&";
for(x=1;x<=lines;x++)
	{
	var line = "";	
	if(document.getElementById("line" + x))
		{
		line = document.getElementById("line" + x).value;
		}	
	specs += del + "line" + x + "=" + line;
	}
var href = "actions/update_option.php" + specs;
$.ajax
	(
		{
		url: href, 
		success: function(result){ show_cart_detail(); }
		}
	);	
}

function edit_inscription(idx, refresh)
{
	
var href = "actions/edit_inscription.php";
var val = "idx=" + idx + "&refresh=" + refresh;
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: val,
		success: function(result){ show_order_info(result); }
		}
	);
	
}

function gift_certificate()
{
document.getElementById("the_options").innerHTML = "";	
document.getElementById("select_item").selectedIndex = 0;
toggle_menu(2);
}

function check_session()
{
	
var href = "actions/session_timeout.php";
var obj = document.getElementById("session_counter");
var newValue = new Number(obj.value);
obj.value = newValue + 1;
$.ajax
	(
		{
		url: href,
		success: function(result){ check_session_results(result); }
		}
	);	
	
}

function check_session_results(result)
{
	
obj = JSON.parse(result);	
if(obj.reset)
	{
	show_popup(obj.blabel, obj.tlabel, false, "", obj.binstruction, obj.tinstruction, true, "images/delete.png", obj.bnextstep, obj.tnextstep);
	}
	
}

function marketing_dd_change(obj, name, exceptions)
{
	
var coid = name + "_container";
var ar = exceptions.split("|");
var arc = ar.length;
var show_exception = false;

for(x=0;x<arc;x++)
	{
	var comp = ar[x].split("~");
	var label = comp[0];
	var id = comp[1];
	var idx = name + "_" + id + "_container";
	var el = document.getElementById(idx);
	if(obj.value == label)
		{ 
		show_exception = true; 
		el.style.display = "block";
		} 
	else
		{
		el.style.display = "none";	
		el.value = "";	
		}
	}
var disp = "none";
var idx = name + "_exception_elements";	
if(show_exception){ disp = "block"; }
document.getElementById(idx).style.display = disp;
document.getElementById(name + "_value").value = obj.value + "~";
record_marketing(name);

}

function marketing_dd_exception(name, dd, ex)
{
	
var dd_obj = document.getElementById(dd);
var ex_obj = document.getElementById(ex);
var value = dd_obj.value + "~" + ex_obj.value;
document.getElementById(name + "_value").value = value;	
record_marketing(name);
	
}

function marketing_checkbox_group_change(name, obj, options)
{
	
var opt_obj = document.getElementById(options);
var opt_val = opt_obj.value;
var find = /\^/g;
opt_val = opt_val.replace(find, '"');
var opt = JSON.parse(opt_val);
var ar = new Array();
var final_value = "";
var del = "";
for(pt in opt)
	{
	var value = pt;
	var obj = eval("opt." + pt);
	var label = obj.label;
	var id = obj.id;
	var exception  = obj.exception;
	var explabel = obj.explabel;
	var placeholder = obj.placeholder;
	var chname = "chk_" + name + "_" + id;
	var excontainer = name + "_" + id + "_container";
	var exvalue = name + "_" + id + "_value";
	if(document.getElementById(chname).checked)
		{ 
		final_value += del + document.getElementById(chname).value; 
		if(exception)
			{
			document.getElementById(excontainer).style.display = "inline";
			final_value += "~" + document.getElementById(exvalue).value;	
			}
		}
	else
		{
		if(exception)
			{
			document.getElementById(excontainer).style.display = "none";	
			}	
		}
	del = "|";
	}
document.getElementById(name + "_value").value = final_value;
record_marketing(name);
	
}

function text_input_change(name, obj)
{
	
document.getElementById(name + "_value").value = obj.value;
record_marketing(name);	
	
}

function record_marketing(name)
{
	
var href = "actions/record_marketing.php";
var value = document.getElementById(name + "_value").value;
var form_data = "name=" + name + "&value=" + value;
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ do_nothing(); }
		}
	);	
	
}

function record_bill_address()
{

document.getElementById("bill_validated_javascript").value = "false";
document.getElementById("bill_validated_fedex").value = "false";
document.getElementById("bill_validated_overall").value = "false";
var href = "actions/record_bill_address.php";
var fields = ["bill_fn", "bill_mn", "bill_ln", "bill_country", "bill_company", "bill_address1", "bill_address2", "bill_city","bill_state_prov", "bill_postal_code", "bill_phone", "bill_email", "bill_validated_javascript", "bill_validated_fedex", "bill_validated_overall"];
var form_data = "";
var del = "";
for(x=0;x<fields.length;x++)
	{
	field_name = strip_type("bill", fields[x]);
	field_value = document.getElementById(fields[x]).value;
	form_data += del + field_name + "=" + field_value;
	del = "&";	
	}

$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ do_nothing(); }
		}
	);

}

function record_bill_address_static()
{

var href = "actions/record_bill_address.php";
var fields = ["bill_fn", "bill_mn", "bill_ln", "bill_country", "bill_company", "bill_address1", "bill_address2", "bill_city","bill_state_prov", "bill_postal_code", "bill_phone", "bill_email", "bill_validated_javascript", "bill_validated_fedex", "bill_validated_overall"];
var form_data = "";
var del = "";
for(x=0;x<fields.length;x++)
	{
	field_name = strip_type("bill", fields[x]);
	field_value = document.getElementById(fields[x]).value;
	form_data += del + field_name + "=" + field_value;
	del = "&";	
	}

$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ do_nothing(); }
		}
	);

}

function strip_type(typ, val)
{

var len = typ.length + 1;
var l = val.length - len;
var retval = "";
for(z=len;z<val.length;z++)
	{
	retval += val.charAt(z);	
	}
return retval;
	
}

function record_credit_card()
{

var href = "actions/record_credit_card.php";
var fields = ["bill_cc_name", "bill_cc_type", "bill_cc_number", "bill_cc_month", "bill_cc_year"];
var form_data = "";
var del = "";
for(x=0;x<fields.length;x++)
	{
	field_name = strip_type("bill", fields[x]);
	field_value = document.getElementById(fields[x]).value;
	form_data += del + field_name + "=" + field_value;
	del = "&";	
	}
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ do_nothing(); }
		}
	);

}

function tnc_change(obj)
{
	
var href = "actions/record_tnc.php";
var accept = "no";
var form_data = "";
if(obj.checked){ accept = "yes"; }	
form_data = "terms_and_conditions=" + accept;
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: form_data,
		success: function(result){ do_nothing(); }
		}
	);
}

function show_tnc()
{
	document.getElementById('tnc_bg').style.display = 'block'; 
	document.getElementById('tnc_container').style.display = 'block';
	
	// scroll to top
	fixPopupPosition();
}

function hide_tnc()
{

document.getElementById('tnc_bg').style.display = 'none'; 
document.getElementById('tnc_container').style.display = 'none';	
	
}

function toggle_side_cart()
{

var obj = document.getElementById("side_cart");
var nv = "0px";
if(obj.style.right == nv)
	{ 
	hide_side_cart();
	}
else
	{
	
	show_side_cart();
	}
	
}

function show_side_cart()
{
var href = "actions/show_side_cart.php";
$.ajax
	(
		{
		url: href, 
		success: function(result)
			{ 
			document.getElementById("cart_blanket").style.display = "block";
			document.getElementById("side_cart_content").innerHTML = result; 
			$('.side-cart').animate({ right: 0 }, 500);
			}
		}
	);
}

function hide_side_cart()
{
nv = "-1000px"; 
$('.side-cart').animate({ right: nv }, 500);
document.getElementById("side_cart_content").innerHTML = "";
document.getElementById("cart_blanket").style.display = "none";
}

function show_checkout_cart()
{

var b = check_marketing();
if(b){ show_checkout();	}
	
}

function address_click(idx)
{
	
var ct = document.getElementById("address_count").value;
for(x=0;x<ct;x++)	
	{
	name = "address_" + x;
	document.getElementById(name).className = "item-not-selected items";	
	}
name = "address_" + idx;
document.getElementById(name).className = "item-selected items";
document.getElementById("shipto_value").value = idx;
check_save();

}

function option_click(idx)
{
	
var ct = document.getElementById("options_count").value;
for(x=0;x<ct;x++)	
	{
	name = "option_" + x;
	document.getElementById(name).className = "item-not-selected items";	
	}
name = "option_" + idx;
document.getElementById(name).className = "item-selected items";
document.getElementById("item_selected").value = document.getElementById("cart_" + idx).value;	
document.getElementById("option_selected").value = document.getElementById("option_item_" + idx).value;
document.getElementById("option_index").value = idx;
document.getElementById("shipto_index").value = document.getElementById("shipto_index_" + idx).value;
check_save();

}

function check_save()
{
	
var item = document.getElementById("item_selected").value;	
var option = document.getElementById("option_selected").value;
var shipto_index = document.getElementById("shipto_index").value;
var shipto_value = document.getElementById("shipto_value").value;
var option_index = document.getElementById("option_index").value;
if((option_index != "")&&(shipto_value != "")){ document.getElementById("shipto_value_" + option_index).value = shipto_value; }
if((item != "")&&(option != "")&&(shipto_value != ""))
	{
	document.getElementById("option_shipto_" + option_index).innerHTML = document.getElementById("address_" + shipto_value).innerHTML;
	var href = "actions/record_option_address.php";
	var form_data = "item=" + item; 
	form_data += "&option=" + option;  
	form_data += "&shipto_index=" + shipto_index;  
	form_data += "&shipto_value=" + shipto_value;  
	//alert(form_data);
	$.ajax
		(
			{
			url: href, 
			type: "POST",
			data: form_data,
			success: function(result){ do_nothing(); }
			}
		);	
	}	
}

function hide_add_address()
{
document.getElementById("shipto_container").innerHTML = "";	
document.getElementById("shipto_container").style.display = "none";	
document.getElementById("ship_to_different").click();
}

function save_add_address()
{
var shipping_address_values = "";
shipping_address_values = check_shipto_address();
if(shipping_address_values)
	{
	shipping_address_values += "&method=different";
	var href = "actions/save_shipto.php";
	$.ajax
		(
			{
			url: href, 
			type: "POST",
			data: shipping_address_values,
			success: function(result)
				{ 
				var href = "actions/addresses.php";
				$.ajax
					(
						{
						url: href, 
						type: "POST",
						success: function(result)
							{ 
							hide_shipto();
							document.getElementById("ship_to_different").click();
							}
						}
					);	
				
				}
			}
		);	
	}
}

function validate_shipto()
{

var ct = document.getElementById("options_count").value;
for(x=0;x<ct;x++)	
	{
	name = "option_shipto_" + x;
	if(document.getElementById(name).innerHTML == "")
		{
		msg_label = document.getElementById("different_addresses_head").value;
		msg = document.getElementById(foc + "_validate").value;
		val = ele.value.trim();
		show_message(msg_label, msg, name); 
		var act = document.getElementById("address_count").value;
		for(y=0;y<act;y++)	
			{
			add = "address_" + y;
			document.getElementById(add).className = "item-not-selected items";	
			}
		option_click(x);
		document.getElementById("shipto_value").value = "";
		return false; 	
		}	
	}	
return true;
	
}


function save_shipto_different()
{

hide_shipto_different();
document.getElementById("ship_to_different").checked = true;
	
}

function promo_code_test(page)
{
var ele = document.getElementById("promotional_code_id");
var pcode = ele.value;
var msg_label = document.getElementById("validate_promo_head").value;
if(pcode != "")	
	{
	promo_code_check(pcode, page);		
	}
else
	{
	msg = document.getElementById("promo_empty_message").value;
	show_message(msg_label, msg, ele); 	
	}
}

function promo_code_check(pcode, page)
{
var promo_code = "promo_code=" + pcode;
href = "actions/apply_promo.php";
document.getElementById("temp0").value = page;
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: promo_code,
		success: function(result, page){ promo_code_apply(result); }
		}
	);
}

function promo_code_apply(result)
{
var page = document.getElementById("temp0").value;
if(result == "Success")	
	{
	msg_label = document.getElementById("validate_promo_head").value;
	ele = document.getElementById("promotional_code_id");
	msg = document.getElementById("promo_success_message").value;
	show_message(msg_label, msg, ele);
	if(page == "checkout"){ show_checkout(); }
	}
else
	{
	msg_label = document.getElementById("validate_promo_head").value;
	ele = document.getElementById("promotional_code_id");
	msg = document.getElementById("promo_invalid_message").value;
	show_message(msg_label, msg, ele);	
	}
}

function enter_text(idx, max_char, spc_used, spc_remaining, chr_finish, iyb, max_line, page)
{
	
var source = document.getElementById("line" + idx);
var target = document.getElementById(idx + "line");
var instct = document.getElementById(idx + "_num");
var msg = "";
if(iyb)
	{
	target.innerHTML = source.value.toUpperCase();
	show_text(max_line);
	}
msg = spc_used + source.value.length + spc_remaining + (max_char - source.value.length) + chr_finish;
instct.innerHTML = msg;

}

function show_text(lines, page)
{
	
var msg = "";
var del = "";
for(x=1;x<=lines;x++)	
	{
	msg += del + document.getElementById("line" + x).value.toUpperCase();
	del = "<br />";
	}	
var objtxt = document.getElementById("objects_text").value;
var ar = objtxt.split("|");
for(x=0;x<ar.length;x++)
	{
	document.getElementById(ar[x]).innerHTML = msg;	
	}
	
}

function option_save_shipto()
{
var option_count = document.getElementById("option_count").value;
var n = new Number(option_count), address_code = "", address_data;
var obj, nme, val, href;
del = "";
for(x=0;x<n;x++)
	{
	obj = document.getElementById("address_" + x);
	val = obj.value;
	address_code += del + obj.name + "_" + val;
	del = "|";
	}
address_data = "address_data=" + address_code;
href = "actions/record_addresses.php";
$.ajax
	(
		{
		url: href, 
		type: "POST",
		data: address_data,
		success: function(result){ do_nothing(result); }
		}
	);
}

function print_receipt()
{
if(document.getElementById("story_share"))
	{
	document.getElementById("story_share").style.display = "none";
	}
javascript: window.print();	
	
}

function address_changed()
{
document.getElementById("ship_validated_javascript").value = "false";
document.getElementById("ship_validated_fedex").value = "false";
document.getElementById("ship_validated_overall").value = "false";	
}

function perform_next(method)
{
if(method == "other"){ hide_shipto(); }
else{  }
	
}

function restore_look(obj)
{
obj.style.color = "#606060";
obj.style.fontWeight = "normal";
}



/**
 * Scroll to top
 * @returns {undefined}
 */
function fixPopupPosition(){
	if (typeof jQuery == 'undefined') {
		console.log('Error: jQuery is not loaded!')
	} else {
		$().ready(function(){
			$('body,html').animate({
				scrollTop: 0
			}, 400);
		})
	}
}