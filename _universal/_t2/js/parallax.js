
// Create cross browser requestAnimationFrame method:
window.requestAnimationFrame = window.requestAnimationFrame
								|| window.mozRequestAnimationFrame
								|| window.webkitRequestAnimationFrame
								|| window.msRequestAnimationFrame
								|| function(f){setTimeout(f, 20)}

var main_image = document.getElementById('main_image');
var main_content = document.getElementById('main_content');
// alert($('#main_content').height());
var scrollheight = getDocHeight(); 						// height of entire document
scrollheight = 10; 						// height of entire document
var windowheight = window.innerHeight; 
 
function parallaxmain()
{
var scrolltop = window.pageYOffset; 						// get number of pixels document has scrolled vertically 
var scrollamount = (scrolltop / (scrollheight-windowheight)) * 100; 

main_image.style.top = -scrolltop * .25 + 'px'; 							// move bubble1 at 20% of scroll rate
main_content.style.top = -scrolltop * 1 + 'px'; 							// move bubble2 at 50% of scroll rate
}
 
window.addEventListener('scroll', function(){ // on page scroll
											requestAnimationFrame(parallaxmain);
											// call parallaxbubbles() on next available screen paint
											}
					   , false
					   );
 
window.addEventListener('resize', function(){ 
											// on window resize
											var scrolltop = window.pageYOffset; 
											// get number of pixels document has scrolled vertically
											var scrollamount = (scrolltop / (scrollheight-windowheight)) * 100; 
											}
					   , false
					   )
					   
function getDocHeight() 
{
var D = document;
var dbsh = 0;
var ddesh = 0;
var dbosh = 0;
var ddeosh = 0;
var dbck = 0;
var ddech = 0;
try{ if(D.body.scrollHeight){ dbsh = D.body.scrollHeight; } }catch(e){  }
try{ if(D.documentElement.scrollHeight){ ddesh = D.documentElement.scrollHeight; } }catch(e){  }
try{ if(D.body.offsetHeight){ dbosh = D.body.offsetHeight; } }catch(e){  }
try{ if(D.documentElement.offsetHeight){ ddeosh = D.documentElement.offsetHeight; } }catch(e){  }
try{ if(D.body.clientHeight){ dbch = D.body.clientHeight; } }catch(e){  }
try{ if(D.documentElement.clientHeight){ ddech = D.documentElement.clientHeight; } }catch(e){  }
return Math.max(dbsh, ddesh, dbosh, ddeosh, dbck, ddech);
}
