		<select id=bill_cc_month 
				name=bill_cc_month  
				onfocus="save_billing();" 
				autocomplete="on"
				class="form-control col-6"
				onchange="record_credit_card();"
				required >
<?php
echo("
			<option value=''".$selected." disabled>".$lang["cc_month_placeholder"]."</option>
");
for($x=1;$x<13;$x++)
    {
	$selected = "";
	if(intval($cc_month) == $x){ $selected = " selected"; }
	$value = substr("0".$x, -2, 2);
	$text = get_month($x);
	echo("
			<option value='".$value."'".$selected.">".$text."</option>
");
	}
?>							
		</select>