<?php
/* What's expected
-----------------------------------------------------
$iyb_width 			- width of the control
$iyb_height			- height of the control
$iyb_image			- image file
$iyb_page			- current page (to keep elements unique throughout the website)
$iyb_paver_id		- id of paver
$iyb_option_id		- id of option
$iyb_line_spacing	- space between lines of inscription
$iyb_start_top		- top margin to start inscription
$iyb_font_size		- font size
$iyb_font_color		- text color
$iyb_default_inscription - inscription
*/
$iyb_thumbnail_control = "
<div style='width: ".$iyb_width."px; 
			height: ".($iyb_height + 20)."px; 
			line-height: ".$iyb_line_spacing."px; 
			margin-top: ".$iyb_start_top."px;
			font-size: ".$iyb_font_size."pt;
			color: ".$iyb_font_color.";
			background-color: ".$iyb_back_color.";
			overflow: auto;
			font-family: Calibri, Arial, Optima;'>
	".strtoupper($iyb_default_inscription)."
</div>
";
?>