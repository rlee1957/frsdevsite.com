	<select id=<?php echo($address_type); ?>_country 
			name=<?php echo($address_type); ?>_country
            class="form-control"
			required 
			autocomplete="on"
			onchange="country_change('<?php echo($address_type); ?>', '');"
			value="<?php echo($country); ?>"
			"<?php echo($onchange); ?>">
		<option value="" disabled><?php echo($lang["country_placeholder"]); ?></option>
<?php
$default = $country;
$selected = "";
include($path."../_t2/configuration/country_list.php");	
foreach($country_list as $key => $ctry)	
	{
	if($key == $default){ $selected = " selected "; }
	else{ $selected = ""; }
	echo("
		<option value='".$key."'".$selected.">".$ctry."</option>
");
	}		
?>
	</select>