<?php
/* quantity control settings
$control_name 			- name of the quantity control value div
$refresh				- function to call after (+/-) is clicked
$control_pm_height		- height of the (+/-) divs
$control_pm_width		- width of the (+/-) divs
$control_plus_top		- top position of the plus div
$control_minus_top		- top position of the minus div
$control_value_left		- left position of the value div
$control_value_top		- top position of the value div
$control_value_height	- height of the value div
$control_value_width	- width of the value div
$control_height			- height of the quantity control
$control_width			- width of the quantity control
$quantity				- default quantity
*/
$quantity_control = "



<div class='quantity-control'>
	<div onclick='do_decrement_fft(\"".$control_name."\", \"".$refresh."\", \"".$page."\");' 
		 class='dec-img'>
		<!--<span>-</span>-->
	</div>
	<div class='qty-value'
		 id='".$control_name."' 
		 name='".$control_name."'>
		".$quantity."
	</div>
	<div onclick='do_increment_fft(\"".$control_name."\", \"".$refresh."\", \"".$page."\");' 
		 class='inc-img' 
		 style='position: absolute; 
				top: 0; 
				right: 0; 
				width: 25px; 
				height: 25px;
				overflow: hidden;'> 
		<!--<span>+</span> -->
	</div>
</div> ";

$quantity_control = "
<a class='input-control' href='javascript:void(0);' onclick='do_decrement_fft(\"".$control_name."\", \"".$refresh."\", \"".$page."\");'>-</a>
    <input id='".$control_name."'
		 name='".$control_name."' type='text' class='form-control' value='".$quantity."'>
<a class='input-control' href='javascript:void(0);' onclick='do_increment_fft(\"".$control_name."\", \"".$refresh."\", \"".$page."\");'>+</a>



";
?>