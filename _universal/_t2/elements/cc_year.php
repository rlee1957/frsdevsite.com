		<select id=bill_cc_year 
				name=bill_cc_year 
				onfocus="save_billing();"
				autocomplete="on"
				class="form-control col-6"
				onchange="record_credit_card();"
				required>
<?php
$selected = "";
if($cc_year == ""){ $selected = " selected"; }
echo("
			<option value=''".$selected." disabled>".$lang["cc_year_placeholder"]."</option>
");

$yr = date('Y');
for($x=0;$x<15;$x++)
	{
	$selected = "";
	$yy = (string)($yr + $x);
	$yy = substr($yy, 2);
	if($yy == $cc_year){ $selected = " selected"; }
	echo('
			<option value="'.$yy.'"'.$selected.'>'.(string)($yr + $x).'</option>');
	}
?>			
		</select>