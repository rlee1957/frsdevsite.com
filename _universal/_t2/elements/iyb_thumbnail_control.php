<?php
/* What's expected
-----------------------------------------------------
$iyb_width 			- width of the control
$iyb_height			- height of the control
$iyb_image			- image file
$iyb_page			- current page (to keep elements unique throughout the website)
$iyb_paver_id		- id of paver
$iyb_option_id		- id of option
$iyb_line_spacing	- space between lines of inscription
$iyb_start_top		- top margin to start inscription
$iyb_font_size		- font size
$iyb_font_color		- text color
$iyb_default_inscription - inscription
*/
$iyb_thumbnail_control = "
<div style='width: ".$iyb_width."px; height: ".$iyb_height."px; position: relative; padding: 0; margin: 0;'>
	<img src='".$iyb_image."' style='width: ".$iyb_width."px; position: absolute; bottom: 0; left: 0;' />
	<div class='imageText' 
		 id='".$iyb_page."_p_".$iyb_paver_id."_o_".$iyb_option_id."'
		 style='width: ".$iyb_width."px; 
				height: ".$iyb_height."px; 
				line-height: ".$iyb_line_spacing."px; 
				margin-top: ".$iyb_start_top."px;
				font-size: ".$iyb_font_size."pt;
				color: ".$iyb_font_color.";
				position: absolute; 
				left: 0; 
				top: 0;'>
		".strtoupper($iyb_default_inscription)."
	</div>
</div>
";
?>