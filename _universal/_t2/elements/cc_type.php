		<select name="bill_cc_type" 
				id="bill_cc_type" 
				required 
				onfocus="save_billing();"
				autocomplete="on"
				class="form-control dd"
				onchange="record_credit_card();" >
<?php
if($cc_type == ""){ $selected = " selected"; }
else{ $selected = ""; }
?>
			<option value=""<?php echo($selected); ?> disabled><?php echo($lang["cc_type_placeholder"]); ?></option>
<?php

include($path."configuration/credit_card_settings.php");
$item_selected = false;
foreach($creditcard as $type => $arr)
	{
	$disabled = $arr["disabled"];
	$selected = "";
	if(($disabled != "disabled") && ($cc_type == $type))
		{	
		$selected = " selected";
		$item_selected = true;
		}
	if($arr["display"])
		{
		echo('
			<option value="'.$type.'" '.$disabled.''.$selected.' >'.$arr["label"].'</option>
');
		}
	}
?>
		</select>