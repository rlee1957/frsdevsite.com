<?php

# US State / Province 
include($path."configuration/us_states.php");




$state_prov1 = "
<select id=".$address_type."_state_prov 
		name=".$address_type."_state_prov 
		required
		class='form-control col-3'
		".$onchange."
		autocomplete='on'";
if($country == "US")
	{
	$state_prov1 .= "
		value='".$state."'";
	}
$selected = "";
if($state == ""){ $selected = " selected"; }
$state_prov1 .= ">
	<option value='' disabled".$selected.">".$lang["state_placeholder"]."</option>
";
$selected = "";
foreach($us_states as $key => $ar)
	{
	if(($ar["abbreviation"] == $state)&&($country == "US")){ $selected = " selected"; }
	else{ $selected = ""; }
	$state_prov1 .= "
	<option value='".$ar["abbreviation"]."'".$selected.">".$ar["english"]."</option>
";	
	}
$state_prov1.= "
</select>
";

# Canada State / Province 
include($path."configuration/canada_provinces.php");
$state_prov2 = "
<select id=".$address_type."_state_prov 
		name=".$address_type."_state_prov 
		required
		class='form-control col-3 '
		".$onchange."
		autocomplete='on'";
if($country == "CA")
	{
	$state_prov2 .= "
		value='".$state."'";
	}
$selected = "";
if($state == ""){ $selected = " selected"; }
$state_prov2 .= ">
	<option value='' disabled".$selected.">".$lang["province_placeholder"]."</option>
";
$selected = "";
foreach($canada_provinces as $key => $ar)
	{
	if(($ar["abbreviation"] == $state)&&($country == "CA")){ $selected = " selected"; }
	else{ $selected = ""; }
	$state_prov2 .= "
	<option value='".$ar["abbreviation"]."'".$selected.">".$ar["english"]."</option>
";	
	}
$state_prov2.= "
</select>
";	

# Other State / Province 
$state_prov3 = "
<input type=text 
	   id=".$address_type."_state_prov 
	   name=".$address_type."_state_prov
	   required
	   autocomplete='on'
	   class='ib form-control col-3' 
	   ".$onchange."
	   placeholder='".$lang["division_placeholder"]."'";
if(($country != "CA")&&($country != "US"))
	{
	$state_prov3 .= "
		value='".$state."'
";
	}
$state_prov3 .= " />
";	
#*****************************************************************
$lft = 195;
if($address_type == "bill")
	{
	$lft = 185;	
	}
?>


	<!-- State -->
		<span id=<?php echo($address_type); ?>_state_container>
<?php
if($country == "US"){ echo($state_prov1); }
elseif($country == "CA"){ echo($state_prov2); }
else{ echo($state_prov3); }
?>
		</span>
		<textarea id=<?php echo($address_type); ?>_us_states style="display: none;">
<?php
echo($state_prov1);
?>
		</textarea>
		<textarea id=<?php echo($address_type); ?>_canada_provinces style="display: none;">
<?php
echo($state_prov2);
?>
		</textarea>
		<textarea id=<?php echo($address_type); ?>_other_divisions style="display: none;">
<?php
echo($state_prov3);
?>					
		</textarea>

