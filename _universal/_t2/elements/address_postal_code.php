<?php

# US Postal Code 
$zip1 = "
<input type=text	
	   id=".$address_type."_postal_code
	   name=".$address_type."_postal_code
	   placeholder='".$lang["zip_placeholder"]."'
	   required
	   autocomplete='on'
	   maxlength=5 
	   class='form-control col-3 ib upp'
	   ".$onchange."
	   onkeyup='restore_look(this);'
	   onpaste='restore_look(this);'";
if($country == "US")
	{
	$zip1 .= "
	   value='".$zip."'";
	}
$zip1 .= " />";
# Canada Postal Code 
$zip2 = "
<input type=text	
	   id=".$address_type."_postal_code
	   name=".$address_type."_postal_code
	   placeholder='".$lang["zip_placeholder"]."'
	   required
	   autocomplete='on'
	   maxlength=7 
	   class='form-control col-3 ib upp'
	   ".$onchange."
	   onkeyup='restore_look(this);'
	   onpaste='restore_look(this);'";
if($country == "CA")
	{
	$zip2 .= "
	   value='".$zip."'";
	}
$zip2 .= " />";	
# Other Postal Code 
$zip3 = "
<input type=text	
	   id=".$address_type."_postal_code
	   name=".$address_type."_postal_code
	   placeholder='".$lang["zip_placeholder"]."'
	   required
	   autocomplete='on'
	   maxlength=25 
	   class='form-control col-3 ib upp'
	   ".$onchange."
	   onkeyup='restore_look(this);'
	   onpaste='restore_look(this);'";
if(($country != "CA")&&($country != "US"))
	{
	$zip3 .= "
	   value='".$zip."'";
	}
$zip3 .= " />";
#*****************************************************************	
?>
	<!--<div style="position: absolute; width: 160px; top: 200px; right: 0;"> Zip -->
		<span id=<?php echo($address_type); ?>_postal_code_container
			>
<?php
if($country == "US"){ echo($zip1); }
elseif($country == "CA"){ echo($zip2); }
else{ echo($zip3); }
?>					
		</span>
		<textarea id=<?php echo($address_type); ?>_us_zip style="display: none;">
<?php
echo($zip1);
?>
		</textarea>
		<textarea id=<?php echo($address_type); ?>_canada_postal_code style="display: none;">
<?php
echo($zip2);
?>				
		</textarea>
		<textarea id=<?php echo($address_type); ?>_other_postal_code style="display: none;">
<?php
echo($zip3);
?>		
		</textarea>
	<!--</div>-->