<?php

# _gateway.php
include("configuration/environment_settings.php");
# includes with variables
# **********************************************************
include("../".$environment_path."/includes/_gateway_config_common.php");
include("../".$environment_path."/includes/language_check.php"); //

# includes with functions
# **********************************************************
include("../".$environment_path."/includes/_gateway_functions.php");
include("../".$environment_path."/includes/string_functions.php");
include("../".$environment_path."/includes/log_functions.php");
#include("../".$environment_path."/includes/test_var.php");

?>
