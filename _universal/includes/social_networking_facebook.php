<?php

include("configuration/option_settings.php");
if($options["facebook_integration"])
	{
	include("configuration/environment_settings.php");
	include("../".$environment_path."/includes/language_check.php");
	$url = $facebook["app_url"];
	$fb_app_id = "?app_id=".urlencode($facebook["app_id"]);
	$fb_app_caption = "&caption=".urlencode($lang["social_networking_caption"]);
	$fb_app_description = "&description=".urlencode($lang["social_networking_description"]);
	$fb_app_display = "&display=".urlencode($facebook["app_display"]);
	$fb_app_e2e = "&e2e=".urlencode($facebook["app_e2e"]);
	$fb_app_link = "&link=".urlencode($facebook["link"]);
	$fb_app_locale = "&locale=".urlencode($facebook["locale"]);
	$fb_app_name = "&name=".urlencode($social_networking["name"]);
	$fb_app_next = "&next=".urlencode($facebook["app_next"]);
	$fb_app_picture = "&picture=".urlencode($facebook["app_picture"]);
	$fb_app_sdk = "&sdk=".urlencode($facebook["sdk"]);
	$fb_button_image = $facebook["button_image"];
	$fb_button_image_alt = $social_networking["name"];
	$href = $url.$fb_app_id.$fb_app_caption.$fb_app_description.$fb_app_display.$fb_app_e2e.$fb_app_link;
	$href .= $fb_app_locale.$fb_app_name.$fb_app_next.$fb_app_picture.$fb_app_sdk;
	$ht = '
			<DIV style="WIDTH: 130px">
				<A href="'.$href.'" 
				   target=_blank>
					<IMG alt="'.$fb_button_image_alt.'" src="'.$fb_button_image.'" />
				</A><br />
				<A href="'.$href.'" target=_blank>'.$fb_button_image_alt.'</A>
			</DIV>
';
	}
else
	{
	$ht = "";	
	}

?>