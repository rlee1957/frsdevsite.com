<?php

function sanitize_sql($str)
{
$tmp = $str;
$tmp = str_replace("\\", "", $tmp);
$tmp = str_replace("'", "`", $tmp);
return $tmp;
}


function clean_html($html)
{
	
$retval = preg_replace("/\s+-\s+/", " ~ ", $html);
$retval = preg_replace("/\s+–\s+/", " ~ ", $retval);
$retval = preg_replace("/\s+</", "<", $retval);
$retval = preg_replace("/>\s+/", ">", $retval);
$retval = utf8_encode($retval);
return $retval;

}

?>