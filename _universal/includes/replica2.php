			<div class="<?php echo($r2_span); ?>" 
				 style="float: left; width: 250px;"> 
				<?php echo($f2_head); ?>
				<small>
					<?php echo($r2_desc); ?>
				</small>
				<div>
					<img src="images/<?php echo($r2_image); ?>" 
						 class="img-rounded" />
				</div>
				<div>
					<small><b>
						<?php echo($lang['cart_replica'] . ' ' . $lang['cart_quantity']); ?></b>
					</small>
				</div>
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">#</strong>
					</span>
					<?php echo($r2_qty); ?></b>
				</div>
				<div>
					<small><b>
						<?php echo($lang['cart_replica2'].' '.$lang['cart_price_each']); ?></b>
					</small>
				</div>
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">
							<?php echo($lang['published_currency_symbol']); ?>
						</strong>
					</span>
					<input class="input-small" 
						   type="Text" 
						   id="x_r2cost" 
						   name="x_r2cost" 
						   value="<?php printf("%2.2f", $r2_price); ?>"
						   readonly />
				</div>
				<div>
					<small><b>
						<?php echo($lang['cart_replica2'].' '.$lang['cart_total']); ?></b>
					</small>
				</div>
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">
							<?php echo($lang['published_currency_symbol']); ?>
						</strong>
					</span>
					<input class="input-small subtotal" 
						   type="Text" 
						   id="x_r2total" 
						   name="x_r2total" 
						   value="<?php echo($r2_subtotal); ?>" 
						   readonly />
				</div>		
			</div>