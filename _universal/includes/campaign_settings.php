<?php

include("configuration/environment_settings.php");
include("configuration/payment_processing_settings.php");
include("configuration/postgres_database_settings.php");
include("configuration/option_settings.php");
include("configuration/default_values.php");
include("configuration/color_settings.php");
include("configuration/image_settings.php");
include("configuration/contact_settings.php");
include("configuration/gift_certificate_settings.php");
include("configuration/tax_settings.php");
include("configuration/google_analytics_settings.php");
include("configuration/social_networking_settings.php");
include("configuration/fee_settings.php");
include("configuration/misc_settings.php");
include("configuration/story_settings.php"); # added 08/13/2015

?>