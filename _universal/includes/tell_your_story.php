<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo($lang["tell_your_story"]); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo($lang['published_campaign_name']); ?>">
<meta name="author" content="Fund Raisers, Ltd.">
<link href="../_common/css/bootstrap.min.css" rel="stylesheet">
<link href="../_common/css/bootstrap-responsive.min.css" rel="stylesheet">
<meta property="og:type" content="product" />
<meta property="og:title" content="<?php echo($lang['published_campaign_name']); ?>" />
<meta property="og:description" content="Confirmation" />
<meta property="og:url" content="https://www.brickorder.com/<?php echo($lang['published_campaign_name_short']); ?>" />
<meta property="og:image" content="https://www.brickorder.com/<?php echo($lang['published_campaign_name_short']); ?>/images/<?php echo($lang['published_logo']); ?>'" />
<meta property="og:site_name" content="<?php echo($lang['published_campaign_name']); ?>" />
<meta property="og:determiner" content="auto" />
<?php
Style_Sheets();
?>
<link href="css/custom.css" rel="stylesheet">
<script language="javascript" type="text/javascript" src="../_common/js/tell_your_story.js"></script>
</head>
<body>
<div class="container">
<?php
Customer_Header_Body();
?>
	<div class="row">
		<div class="span12">
			<div class="well">
				<?php echo($lang["story_thank_you_message"]); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<div class="well">
				<?php echo(send_email()); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<div class="well">
				<h4><?php echo($lang['story_header']); ?></h4>
				<div><?php echo($lang['story_introduction']); ?></div>
<?php
if($story["categories"])
	{
	include("../_common/includes/story_categories.php");
	}
if($story["types"])
	{
	include("../_common/includes/story_types.php");
	}
if($story["photo"])
	{
	include("../_common/includes/story_file.php");
	}
?>
				<h4><?php echo($lang["your_story_title"]); ?></h4>
				<textarea id="story_text" 
						  name="story_text" 
						  style="width: 95%; height: 300px;" 
						  required
						  placeholder="<?php echo($lang["your_story_placeholder"]); ?>"
						  onchange="story_change();"></textarea>
				<h4><?php echo($lang["story_disclaimer_title"]); ?></h4>
				<?php echo($lang['story_disclaimer']); ?>
				<div>
					<form id="story_form" 
						  name="story_form" 
						  method="post" 
						  action="save_story.html" 
						  target="work">
						<input type=hidden id="db_order_id" name="db_order_id" 
							   value="<?php echo($_SESSION["go_orderid"]); ?>" />
						<input type=hidden id="db_story_type" name="db_story_type" value="" />
						<input type=hidden id="db_story_category" name="db_story_category" value="" />
						<input type=hidden id="db_is_file" name="db_is_file" value=0 />
						<input type=hidden id="db_story_file" name="db_story_file" value="" />
						<input type=hidden id="db_story_text" name="db_story_text" value="" />
						<input type=hidden 
							   id="db_story_next" 
							   name="db_story_next" 
							   value="<?php echo($story["after_story"]); ?>" />
						<input type=button 
							   class="btn btn-custom" 
							   value="<?php echo($lang["story_submit_story"]); ?>"
							   onclick="submit_story();" />
						<input type=button 
							   class="btn btn-custom" 
							   value="<?php echo($lang["story_decline"]); ?>" 
							   onclick="window.location.assign('<?php echo($story["after_story"]); ?>');" />
					</form>
				</div>
			</div>
		</div>
	</div>
<?php
// echo($social_media);
Customer_Footer();
?>
	
</div>
<br /><br /><br />
<input type=hidden id="cate" name="cate" value="<?php echo($story["categories"]); ?>" />
<input type=hidden id="catreq" name="catreq" value="<?php echo($story["category_required"]); ?>" />
<input type=hidden id="catmsg" name="catmsg" value="<?php echo($lang["story_category_required"]); ?>" />
<input type=hidden id="typ" name="typ" value="<?php echo($story["types"]); ?>" />
<input type=hidden id="typreq" name="typreq" value="<?php echo($story["type_required"]); ?>" />
<input type=hidden id="typmsg" name="typmsg" value="<?php echo($lang["story_type_required"]); ?>" />
<input type=hidden id="fil" name="fil" value="<?php echo($story["photo"]); ?>" />
<input type=hidden id="filreq" name="filreq" value="<?php echo($story["photo_required"]); ?>" />
<input type=hidden id="filmsg" name="filmsg" value="<?php echo($lang["story_photo_required"]); ?>" />
<input type=hidden id="storymsg" name="storymsg" value="<?php echo($lang["story_text_required"]); ?>" />
<iframe id="work" 
		name="work" 
		style="position: absolute; width: 99%; height: 300px; display: none;"></iframe>
</BODY>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
var pageTracker = _gat._getTracker("<?php echo($lang['set_analytics']); ?>");
pageTracker._trackPageview();
</script>

<script language="javascript" text="text/javascript">



</script>

</HTML>
