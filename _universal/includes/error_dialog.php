<?php

?>
<!-- Error Handling Tool -->
<input type=hidden id="validate_shipto_title" value="<?php echo($lang["validate_shipto_title"]); ?>" />
<input type=hidden id="error_occurred" value="false" />
<a href="#ErrorDialog" style="display: none;" id="show_modal" >Open Modal</a>
<div id="ErrorDialog" class="modalDialog">
    <div>	
		<a href="#error" title="Error" class="err_class">X</a>
		<center><h2><span id="error_title"></span></h2></center>
		<center><span id="error_content"></span></center>
    </div>
</div>
