			<!-- Brick (Type and Image) -->			
			<table style="float: left; width: 200px; height: 150px; overflow: hidden;">
				<tr>
					<td>
						<br /><br />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<strong><?php echo($lang['selected_customize_options']); ?></strong>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<small><?php echo($brick_name); ?></small>
					</td>
				</tr>
				<tr>
					<td>
						<img src="images/<?php echo($brick_image); ?>" 
							 class="img-rounded" />
					</td>
				</tr>
			</table>
			<!-- Brick Quantity -->
			<div style="float: left; width: 150px; height: 200px;">
				<br /><br />
				<small>
					<b>
						<?php echo($lang['cart_paver'].' '.$lang['cart_quantity']); ?>
					</b>
				</small>
				<div class="control-group">
					<div class="input-prepend">
						<span class="add-on"><strong style="color:black;">#</strong></span>
						<input type="text" 
							   class="input-small" 
							   id="x_pqty" 
							   name="x_pqty" 
							   readOnly 
							   value="<?php echo($brick_quantity); ?>" />
					</div>
				</div>
			</div>
			<!-- Brick Logo Options -->
<?php echo($logo_options); ?>
			<!-- Brick Price -->
			<div style="float: left; clear: right; width: 150px; height: 200px;">
				<br /><br />
				<small>
					<b>
						<?php echo($lang['cart_paver'] . ' ' . $lang['cart_price_each']); ?>
					</b>
				</small>
				<div class="control-group">
					<div class="input-prepend">
						<span class="add-on">
							<strong style="color:black;">
								<?php echo($lang['published_currency_symbol']); ?>
							</strong>
						</span>
						<input class="input-small" 
							   type="Text" 
							   id="x_pcost" 
							   name="x_pcost" 
							   value="<?php printf("%2.2f", $brick_price); ?>" readonly />
					</div>
				</div>
			</div>
			<!-- Brick Subtotal -->
			<div style="float: left; clear: right; width: 150px; height: 200px;">
				<br /><br />
				<small>
					<b>
						<?php echo($lang['cart_paver'] . ' ' . $lang['cart_subtotal']); ?>
					</b>
				</small>
				<div class="control-group">
					<div class="input-prepend">
						<span class="add-on">
							<strong style="color:black;">
								<?php echo($lang['published_currency_symbol']); ?>
							</strong>
						</span>
						<input class="input-small subtotal" type="Text" id="x_ptotal" name="x_ptotal" value="<?php printf("%2.2f", $brick_subtotal); ?>"
						readonly />
					</div>
				</div>
			</div>
