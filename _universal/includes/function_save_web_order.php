<?php
/*
---------------------------------------------------------------------------------------------------------
    SAVE_WEB_ORDER()
---------------------------------------------------------------------------------------------------------
*/
function save_web_order() 
{
include("system/application/language/language_check.php");
$a_ins[] = "";
$rs1 = 0;
$p_msg = "";
if($_POST['b_buying_as'] != null || $_POST['b_buying_as'] |= "")
	{
	$_SESSION["g_extra3"]  = implode("|", $_POST['b_buying_as']);
	}
if ($_POST['b_optin'])
	{
	$optin = $_POST['b_optin'];
	}
else
	{
	$optin = "no";
	}
$dbcon = db_connect();
if (!$dbcon) 
	{
	err_log("db_connect() " . $_SESSION["g_billing"][1] . $_SESSION["g_billing"][9]);
	// taken out Feb 14 2007
	// echo "Your Order was processed, but you will need to contact us at " . $_SESSION["g_head4"];
	//exit;
    }
//rosebowl
$p_mm = substr($_SESSION["go_cexp"],0,2);
$p_yy = substr($_SESSION["go_cexp"],2,2);
$p_cardname = $_SESSION["go_cname"];
		
// Touchnet Changes
//$p_cardname = "N/A";
//$_SESSION["go_resp_reference_number"] = "PENDING";
//$_SESSION["go_resp_code"] = "PENDING";
//$_SESSION["go_resp_auth_code"] = "PENDING";
//$_SESSION["go_resp_auth_response"] = "PENDING";
//

// Prepare promo fields
		
if  (strlen($_SESSION["g_promo_box1"]) > 0) 
	{
	$p_promo1 = $_SESSION["g_promo_text"]; //"YES"; // Added 2/17/2009 - Dustin
	$p_promo2 = $_SESSION["g_promo_exttext"]; // Added 1/26/2010 - Dustin for rosebowl
	} 
else 
	{
	$p_promo1 = "NO";
	}
//
if  (strlen($_SESSION["g_promo_box2"]) > 0) 
	{
	$p_promo2 = $p_promo2 = $_SESSION["g_promo_exttext"];
	} 
else 
	{
	$p_promo2 = $p_promo2 = $_SESSION["g_promo_exttext"];
	}
//		
if  (strlen($_SESSION["g_promo_box3"]) > 0) 
	{
	$p_promo3 = "YES";
	} 
else 
	{
	$p_promo3 = "NO";
	}
//		
if  (strlen($_SESSION["g_promo_box4"]) > 0) 
	{
	$p_promo4 = "YES";
	} 
else 
	{
	$p_promo4 = "NO";
	}
//
if  ($_SESSION["g_gc_redeem_amount"] > 0) 
	{
//if  (strlen($_SESSION["g_promo_box5"]) > 0) {
	$p_promo5 = $_SESSION["g_gc_redeem_obj"];
	} 
else 
	{
	$p_promo5 = "NO";
	}
if  ($_SESSION["b_staff"] == "" || $_SESSION["b_staff"] == NULL) 
	{
	$_SESSION["b_staff"] = "WEB";
	}
// added to accomadate member number
$member_number = "";
if (isset($_POST['member_number']))
	{
	$member_number = $_POST['member_number'];
	if(strlen($member_number) > 0)
		{
		$_SESSION["staff_notes"] = "Member Number: [".$member_number."]  ".$_SESSION["staff_notes"];
		}
	}
if  ($_SESSION["b_staff"] == "" || $_SESSION["b_staff"] == NULL) 
	{
	$_SESSION["b_staff"] = "WEB";
	}

// Payment Plan Disabled
$_SESSION["g_payplan"] = 0;
$_SESSION["g_totalcost_pay1"] = 0;
$_SESSION["g_totalcost_pay2"] = 0;
$_SESSION["g_totalcost_pay3"] = 0;	

/*--------------------------------------------------------------------------
Insert Order Information
--------------------------------------------------------------------------*/
$camp_short = $lang['published_campaign_name_short'];
if(!isset($_SESSION["g_taxrate"]))
	{
	$_SESSION["g_taxrate"] = 0;
	}
else
	{
	if(!is_numeric($_SESSION["g_taxrate"]))
		{
		$_SESSION["g_taxrate"] = 0;
		}
	else
		{
		if($_SESSION["g_taxrate"] <= 0)	
			{
			$_SESSION["g_taxrate"] = 0;
			}
		}
	}
if(!isset($_SESSION["g_taxtotal"]))
	{
	$_SESSION["g_taxtotal"] = 0;
	}
else
	{
	if(!is_numeric($_SESSION["g_taxtotal"]))
		{
		$_SESSION["g_taxtotal"] = 0;
		}
	else
		{
		if($_SESSION["g_taxtotal"] <= 0)	
			{
			$_SESSION["g_taxtotal"] = 0;
			}
		}
	}
include("../_common/includes/insert_into_w_order.php");
//$_SESSION["b_staff"] = $_POST["b_staff"];
//$_SESSION["staff_notes"] = $_POST["staff_notes"];

// echo "payplay" . $_SESSION["g_payplan"];


/*		  $_SESSION["g_payplan"] ,
$_SESSION["g_totalcost_pay1"] ,
$_SESSION["g_totalcost_pay2"] ,
$_SESSION["g_totalcost_pay3"]  )";
*/

pg_send_query($dbcon,$sqry);
$rs1 = pg_get_result($dbcon);

if (pg_result_error($rs1)) 
	{
	$p_msg = "save_web_order:w_order" . $_SESSION["g_billing"][1] . $_SESSION["g_billing"][9] ;
	err_log(pg_result_error($rs1) . ": " . $p_msg . " :SQRY: " . $sqry);					
	// taken out Feb 14 2007
	//echo "Your Order was processed, but you will need to contact us at " . $_SESSION["g_head4"];
	//exit;
	}

/*--------------------------------------------------------------------------
Insert Line Item Information
This routine loops thru each line item orded and
inserts the record into w_lineitem table
--------------------------------------------------------------------------*/
for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
	{
	if (isset($_SESSION["g_libktype"][$ii])) 
		{
		/*
		bo_cart_line_heading();
		bo_cart_paver($ii);
		bo_cart_replica($ii);
		bo_cart_blue_line();
		bo_cart_inscr($ii, $_SESSION["g_libktype"][$ii]);      */
		$tmp_bktype = $_SESSION["g_libktype"][$ii];
		$tmp_bkdesc = $_SESSION["g_bkdesc"][ $_SESSION["g_libktype"][$ii]];
		$tmp_bklines = $_SESSION["g_bklines"][ $_SESSION["g_libktype"][$ii]];
		$tmp_bkchar = $_SESSION["g_bkchar"][ $_SESSION["g_libktype"][$ii]];
		$tmp_pcost =  $_SESSION["g_bkpcost"][ $_SESSION["g_libktype"][$ii]];
		$tmp_rcost =  $_SESSION["g_bkrcost"][ $_SESSION["g_libktype"][$ii]];
		$tmp_dccost =  $_SESSION["g_bkdccost"][ $_SESSION["g_libktype"][$ii]];
		$tmp_r2cost =  $_SESSION["g_bkr2cost"][ $_SESSION["g_libktype"][$ii]];
		$tmp_dc2cost =  $_SESSION["g_bkdc2cost"][ $_SESSION["g_libktype"][$ii]];
		$tmp_location =  $_SESSION["g_libklocation"][$ii];
		//$tmp_bktype = $_SESSION["g_libktype"][$ii];
		//$tmp_bkno = $_SESSION["g_bkno"][$ii][$tmp_bktype];
		$tmp_bkno = $_SESSION["g_bkno"][$tmp_bktype];
		//echo "bktype:".$tmp_bktype;
		//echo "<br />bkno:".$tmp_bkno;
		if($tmp_dc2cost == "" ||  $tmp_dc2cost == NULL)
			{
			$tmp_dc2cost = 0;
			}

		$tmp_rship =  $_SESSION["g_bkrship"][ $_SESSION["g_libktype"][$ii]];
		 // bktype
		$tmp_pqty =  $_SESSION["g_lipqty"][$ii];
		$tmp_rqty =  $_SESSION["g_lirqty"][$ii];
		$tmp_dcqty =  $_SESSION["g_lidcqty"][$ii];
		if($tmp_dcqty == "" ||  $tmp_dcqty == NULL)
			{
			$tmp_dcqty = 0;
			}	 
		$tmp_dc2qty =  $_SESSION["g_lidc2qty"][$ii];
		if($tmp_dc2qty == "" ||  $tmp_dc2qty == NULL)
			{
			$tmp_dc2qty = 0;
			}	
		if($tmp_rqty == "" ||  $tmp_rqty == NULL)
			{
			$tmp_rqty = 0;
			}
		$tmp_r2qty =  $_SESSION["g_lir2qty"][$ii];
		if($tmp_r2qty == "" ||  $tmp_r2qty == NULL)
			{
			$tmp_r2qty = 0;
			}
		$tmp_logoname = trim($_SESSION["g_lilogoname"][$ii]);
		$tmp_logono = $_SESSION["g_lilogono"][$ii];

		$tmp_bkdc_bkno = $_SESSION["g_bkdc_bkno"][$ii];
		if($tmp_bkdc_bkno == "" ||  $tmp_bkdc_bkno == NULL)
			{
			$tmp_bkdc_bkno = 0;
			}
		$tmp_bkdc2_bkno = $_SESSION["g_bkdc2_bkno"][$ii];
		if($tmp_bkdc2_bkno == "" ||  $tmp_bkdc2_bkno == NULL)
			{
			$tmp_bkdc2_bkno = 0;
			}

		//echo "<br />Session CB: ".$_SESSION["g_licb_gc"][$ii];
		if (isset($_SESSION["g_licb_gc"][$ii]))
			{
			$tmp_gc = 1;
			}
		else
			{
			$tmp_gc = 0;
			}


		// Populate array $a_ins[]  with inscription lines
		$a_ins[0] = return_line_ins($ii, $tmp_bktype, 1);
		$a_ins[1] = return_line_ins($ii, $tmp_bktype, 2);
		$a_ins[2] = return_line_ins($ii, $tmp_bktype, 3);
		$a_ins[3] = return_line_ins($ii, $tmp_bktype, 4);
		$a_ins[4] = return_line_ins($ii, $tmp_bktype, 5);
		$a_ins[5] = return_line_ins($ii, $tmp_bktype, 6);
		$a_ins[6] = return_line_ins($ii, $tmp_bktype, 7);
		$a_ins[7] = return_line_ins($ii, $tmp_bktype, 8);
		$a_ins[8] = return_line_ins($ii, $tmp_bktype, 9);
		$a_ins[9] = return_line_ins($ii, $tmp_bktype, 10);
		$a_ins[10] = return_line_ins($ii, $tmp_bktype, 11);
		$a_ins[11] = return_line_ins($ii, $tmp_bktype, 12);
		$a_ins[12] = return_line_ins($ii, $tmp_bktype, 13);
		$a_ins[13] = return_line_ins($ii, $tmp_bktype, 14);
		$a_ins[14] = return_line_ins($ii, $tmp_bktype, 15);


		//   echo $tmp_bktype . "<br>" . $tmp_bkdesc . "<br>" ;

include("../_common/includes/insert_into_w_lineitem.php");
		pg_send_query($dbcon,$sqry);
		$rs1 = pg_get_result($dbcon);
		if (pg_result_error($rs1)) 
			{ 
			$p_msg = "save_web_order:w_lineitem" . $_SESSION["g_billing"][1] . $_SESSION["g_billing"][9] ;
			err_log(pg_result_error($rs1) . ": " . $p_msg . " :SQRY: " . $sqry);
			// taken out Feb 14 2007
			//echo "Your Order was processed, but you will need to contact us at " . $_SESSION["g_head4"];
			//exit;
			}    // end if
		} // end if isset
	} // end for



/*--------------------------------------------------------------------------
Insert Shipto Information
This routine loops thru each line item orded and $_SESSION["g_liship"][$li][$li_ship]["s_prefix"]
inserts the record into w_shipto table
--------------------------------------------------------------------------*/
$space = " ";
for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
{
if (isset($_SESSION["g_libktype"][$ii])) {
$li_ship = $_SESSION["g_lishiptoqty"][$ii] ;
for ($ss = 1; $ss <=  $li_ship; $ss++)  {

// Name Building
$data = array();
$data = array($_SESSION["g_liship"][$ii][$ss]["s_prefix"], $_SESSION["g_liship"][$ii][$ss]["s_name"], $_SESSION["g_liship"][$ii][$ss]["s_mname"], $_SESSION["g_liship"][$ii][$ss]["s_lname"]);
$full_name = implode(" ", array_filter($data));


//added so email can have name for free brick
$_SESSION["shipping_name"] = $_SESSION["g_liship"][$ii][$ss]["s_name"].$space.$_SESSION["g_liship"][$ii][$ss]["s_lname"];

include("../_common/includes/insert_into_w_shipto.php");
pg_send_query($dbcon,$sqry);
$rs1 = pg_get_result($dbcon);

if (pg_result_error($rs1)) {                     
$p_msg = "save_web_order:w_shipto" . $_SESSION["g_billing"][1] . $_SESSION["g_billing"][9];
err_log(pg_result_error($rs1) . ": " . $p_msg . " :SQRY: " . $sqry);
// taken out Feb 14 2007
//echo "Your Order was processed, but you will need to contact us at " . $_SESSION["g_head4"];
//exit;
}    // end if

}  // end of for

} // end if isset
} // end for


/*--------------------------------------------------------------------------
UPDATE GC Information - 12/20/2013
updates the record into w_giftcodes_data
--------------------------------------------------------------------------*/ 
include("../_common/includes/update_w_giftcodes_data.php");
pg_send_query($dbcon,$sqry);
$rs1 = pg_get_result($dbcon);
if (pg_result_error($rs1)) {                     
$p_msg = "save_web_order:w_giftcodes_data" . $_SESSION["go_orderid"];
err_log(pg_result_error($rs1) . ": " . $p_msg . " :SQRY: " . $sqry);
}

/*--------------------------------------------------------------------------
Insert Options Information - Added May 11, 2008
This routine loops thru each line item orded and
inserts the record into w_options table
--------------------------------------------------------------------------*/

if ($_SESSION["g_op_enable"] || $_SESSION["g_gc_enable"]) {
for ($ii = 1; $ii <= $_SESSION["g_op_no_li"]; $ii++)
{
if (isset($_SESSION["g_op_li"][$ii]["op_type"])) {	

$iii =  $_SESSION["g_op_li"][$ii]["op_type"];
$xx_qty = $_SESSION["g_op_li"][$ii]["op_qty"];
$xx_cost =  $_SESSION["g_op_cost"][$iii];
$xx_ship =  $_SESSION["g_op_ship"][$iii];
$xx_ship_total =  $xx_ship * $xx_qty;
$xx_cost_total = $_SESSION["g_op_cost"][$iii] * $xx_qty;
$xx_total = $xx_cost_total + $xx_ship_total;

include("../_common/includes/insert_into_w_options.php");
			pg_send_query($dbcon,$sqry);
			$rs1 = pg_get_result($dbcon);
			if (pg_result_error($rs1)) 
				{                     
				$p_msg = "save_web_order:w_options" . $_SESSION["go_orderid"];
				err_log(pg_result_error($rs1) . ": " . $p_msg . " :SQRY: " . $sqry);
				}    // end if
			} // end if isset
		} // end for
	} // END OF IF  ($_SESSION["g_op_enable"])
db_close($dbcon);
}

?>