<?php
    if($lang["story_type_options"] != "")
		{
		$required = "";
		if($story["types_required"]){ $required = "required"; }
?>
				<br />
				<div class="control-group">
					<label class="control-label">
						<span style="font-weight: bold; font-size: 12pt;">
							<?php echo($lang["story_type_title"]); ?>
						</span>
					</label>
					<div class="controls">
<?php
		$values = explode("|",$lang["story_type_options"]);
		if($story["types_sorted"]){ sort($values); } //sort the values
		$x = 0;
		foreach ($values as $val)
			{
?>
						<div>
							<table cellspacing=0 cellpadding=0>
								<tr style="height: 30px;">
									<td valign=top>
										<input type="checkbox" <?php echo($required); ?>
											   name="rdo_<?php echo($x); ?>" 
											   id="rdo_<?php echo($x); ?>" 
											   value="<?php echo($val); ?>"
											   style="height: 15px;"
											   onclick="type_change();" /> 
									</td>
									<td valign=middle>
										<label for="rdo_<?php echo($x); ?>"><?php echo($val); ?></label>
									</td>
								</tr>
							</table>
						</div>
<?php
		$x++;
		}
?>
						<p class="help-block"></p>
					</div>
				</div>
<?php	
	}
?>