<?php

include("configuration/environment_settings.php");
include("../_common/functions/_gateway_valkey.php");
include("../_common/functions/_gateway_process_cc.php");
include("../_common/functions/_gateway_do_post_request.php");
include("../_common/functions/_gateway_go_log.php");
include("../_common/functions/_gateway_efs_mmddyy.php");
include("../_common/functions/_gateway_db_connect.php");
include("../_common/functions/_gateway_db_pconnect.php");
include("../_common/functions/_gateway_db_freeresult.php");
include("../_common/functions/_gateway_db_close.php");
include("../_common/functions/_gateway_db_control_next_unique_key.php");
include("../_common/functions/_gateway_return_line_ins.php");
include("../_common/functions/_gateway_go_clear_response.php");
include("../_common/functions/_gateway_process_argofire.php");
include("../_common/functions/_gateway_process_receipt.php");
include("../_common/functions/_gateway_send_email.php");
include("../_common/functions/social_networking.php");

?>
