<?php

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
$url = $config['service_objects_url'];
$sk = $config['service_objects_key'];
if(
    (
	isset($country_locations[$_SESSION["country"]][$_SESSION["tax_state"]])
	) 
	&& 
	(
	$country_locations[$_SESSION["country"]][$_SESSION["tax_state"]]["tax"]
	)
  )
    {
	if($_SESSION["country"] == "US")
		{
		include("../_common/includes/tax_us.php");
		}
	elseif($_SESSION["country"] == "CA")
		{
		include("../_common/includes/tax_canada.php");	
		}
	else
		{
		include("../_common/includes/tax_other.php");	
		}
	}
else
	{
	$_SESSION["service_objects_totaltaxrate"] = 0.00;	
	}
if($tax["calculate_nexus_tax"])
	{
	include("../_common/includes/tax_nexus.php");		
	}
$_SESSION["g_taxrate"] = $_SESSION["service_objects_totaltaxrate"];

?>