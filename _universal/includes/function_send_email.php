<?php
function send_email()
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
/*//////////////////
/ Testing
*//////////////////
  //echo 'response: '.$_SESSION["go_resp_status"].'<br>';
  //echo 'email: '.$_SESSION["g_liship"][1][1]["s_email"].'<br>';
  //echo 'name: '.$_SESSION["g_liship"][1][1]["s_name"].'<br>';
//////////////////

// Message Body that will be received when approved
$approvedBody = '<body style="margin-left:150px; margin-right:150px;margin-top:10px;">
<div style="background-color:#ffffff;width:600px">
<center><img src="images/'.$lang["published_logo"].'" width="600" alt="'.$lang["published_campaign_name"].'"></center>

</div>
<div align="center" style="width:600px">
<p align="center"><strong>'.$lang["confirmation_payment"].' '.$lang["confirmation_payment_successful"].'</strong></p>
<p style="color:#F00; font-style:italic; font-size:11px;" align="center">('.strtoupper($lang["confirmation_payment_please_print"]).')</p>
<div align="left" style="width:600px">
<p>'.$lang["confirmation_thank_you_text1_approved"].'</p>
<p>'.$lang["confirmation_processed_success"].'</p>
</div>
</div>
<DIV align="center" style="width:600px">
  <TABLE border="1" cellPadding="0" width="587">
    <TBODY>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_date"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.date("F j, Y, g:i a").'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_number"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_orderid"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_code"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_auth_code"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_response"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.strtoupper($lang['confirmation_approved']).'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_payment_amount"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_total"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_customer_name"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_oname"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_message"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_auth_response"].'</p></TD>
      </TR>
    </TBODY>
  </TABLE>
</DIV>
<div style="width:600px">'.$lang["confirmation_auth_body_success"].'</div>
</body>';
// INSERT THIS JUST BEFORE </body> -->  '.get_facebook_share_button().'
// Free Brick Body - Used For All campaigns offering free brick (Offering Free brick with code)
$freeBody = 
'<body style="margin-left:150px; margin-right:150px;margin-top:10px;">
<div style="background-color: #ffffff">
<center><img src="images/'.$lang["published_logo"].'" width="600" height="65" alt="'.$lang["published_campaign_name"].'"></center>
<div style="color:#FFF" align="center"><strong>'.$lang["published_telephone_acronym"].' '.$lang["published_telephone_short"].' :   '.$lang["published_hours_of_operation"].'</strong></div>

</div>
<p align="center"><strong>'.$lang["confirmation_order_confirmation"].'  '.$lang["confirmation_order_received"].'</strong></p>
<p style="color:#F00; font-style:italic; font-size:11px;" align="center">('.strtoupper($lang["confirmation_payment_please_print"]).')</p>
<p>'.$lang["confirmation_thank_you_text1"].'</p>
<p>'.$lang['confirmation_processed_received'].'</p>
<DIV align="center">
  <TABLE border="1" cellPadding="0" width="587">
    <TBODY>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_date"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.date("F j, Y, g:i a").'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_number"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_orderid"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_code"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_auth_code"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_response"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.strtoupper($lang['confirmation_approved']).'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_payment_amount"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_total"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_customer_name"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["shipping_name"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_message"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_status"].'</p></TD>
      </TR>
    </TBODY>
  </TABLE>
</DIV>
<div style="width:600px">'.$lang["confirmation_auth_body_accepted"].'</div>
</body>';
// INSERT THIS JUST BEFORE </body> -->  '.get_facebook_share_button().'
// Received without processing
$ReceivedBody = 
'<body style="margin-left:150px; margin-right:150px;margin-top:10px;">
<div style="background-color: #ffffff">
<center><img src="images/'.$lang["published_logo"].'" width="600" height="65" alt="'.$lang["published_campaign_name"].'"></center>
<div style="color:#FFF" align="center"><strong>'.$lang["published_telephone_acronym"].' '.$lang["published_telephone_short"].' :   '.$lang["published_hours_of_operation"].'</strong></div>

</div>
<p align="center"><strong>'.$lang["confirmation_order_confirmation"].'  '.$lang["confirmation_order_received"].'</strong></p>
<p style="color:#F00; font-style:italic; font-size:11px;" align="center">('.strtoupper($lang["confirmation_payment_please_print"]).')</p>
<p>'.$lang["confirmation_thank_you_text1"].'</p>
<p>'.$lang["confirmation_processed_success"].'</p>
<DIV align="center">
  <TABLE border="1" cellPadding="0" width="587">
    <TBODY>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_date"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.date("F j, Y, g:i a").'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_number"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_orderid"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_code"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_auth_code"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_response"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.strtoupper($lang['confirmation_pending']).'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_payment_amount"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_total"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_customer_name"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_oname"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_message"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_status"].'</p></TD>
      </TR>
    </TBODY>
  </TABLE>
</DIV>
<div style="width:600px">'.$lang["confirmation_web_receipt"].'</div>
</body>';
// INSERT THIS JUST BEFORE </body> -->  '.get_facebook_share_button().'

//Declined Body
$declinedBody = '<body style="margin-left:150px; margin-right:150px;margin-top:10px;">
<div style="background-color: #ffffff; width:600px">
<center><img src="images/'.$lang["published_logo"].'" width="600" alt="'.$lang["published_campaign_name"].'"></center>
</div>
<div align="center" style="width:600px">
<p align="center"><strong>'.strtoupper($lang["confirmation_payment"]).' '.strtoupper($lang["confirmation_payment_failed"]).'</strong></p>
<p style="color:#F00; font-style:italic; font-size:11px;" align="center">('.strtoupper($lang["confirmation_payment_please_print"]).')</p>
</div>
<p>'.$lang["confirmation_processed_failed"].'</p>
<DIV align="center" style="width:600px">
  <TABLE border="1" cellPadding="0" width="587">
    <TBODY>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_date"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.date("F j, Y, g:i a").'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_number"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_orderid"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_code"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_auth_code"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_response"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.strtoupper($lang["confirmation_declined"]).'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_payment_amount"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_total"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_customer_name"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_oname"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_message"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_status"].'</p></TD>
      </TR>
    </TBODY>
  </TABLE>
</DIV>
<div style="width:600px">'.$lang["confirmation_auth_body_declined"].'</div>
</body>';

require_once('../".$environment_path."/includes/class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mail = new PHPMailer(false); // the true param means it will throw exceptions on errors, which we need to catch

$mail->IsSMTP(); // telling the class to use SMTP
$mail->IsHTML(true);

try {
//  $mail->Host       = "127.0.0.1"; // SMTP server
  $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing) 2? debug? 0 no.
//  $mail->SMTPAuth   = false;                  // enable SMTP authentication
//  $mail->Username   = "orders@brickorder.com"; // SMTP account username
//  $mail->Password   = "Jes0n123";        // SMTP account password
//  $mail->SetFrom('orders@brickorder.com', 'Brick Orders');
// Mandrill Changes
  $mail->Host       = "smtp.mandrillapp.com"; // SMTP server
  $mail->SMTPAuth   = true;                  // enable SMTP authentication
  $mail->Username   = "dustin@fundraisersltd.com"; // SMTP account username
  $mail->Password   = "wePsKEQcRJf6D33hL4wTTg";        // SMTP account password
  $mail->SMTPSecure = 'tls';
  $mail->Port       = 587;                    // set the SMTP port for the GMAIL server
  $mail->addCustomHeader("X-MC-Tags: ".$lang['published_campaign_name']);
  $mail->addCustomHeader("X-MC-GoogleAnalytics: https://www.brickorder.com/".$lang['published_campaign_name_short']);
  //$mail->SetFrom($lang['published_fr_mailto'], $lang['published_campaign_name']);
  $mail->SetFrom($lang['published_fr_mailto'], $lang['published_campaign_name']);
  $mail->AddReplyTo($lang['published_fr_mailto'], $lang['published_campaign_name']);
  //$mail->AddReplyTo($lang['published_fr_mailto'], 'Levi\'s Stadium Fanwalk');
  // End Mandrill
  
//  $mail->AddReplyTo($lang['published_mailto'], $lang['published_campaign_name']);

//  $mail->Port       = 25;                    // set the SMTP port for the GMAIL server

  //$mail->AddReplyTo($lang['published_mailto'], 'Brick Orders');
  
  $mail->AddBCC("ccorders@fundraisersltd.com", "CC Orders");
//  $mail->AddBCC("dustin@fundraisersltd.com", "Dustin Blad");

 // $mail->AddReplyTo('orders@brickorder.com', 'Brick Orders');
  $mail->Subject = $lang["published_campaign_name"].': #'.$_SESSION["go_orderid"];
  //$mail->Subject = 'Levi\'s Stadium Fanwalk: #'.$_SESSION["go_orderid"];
  $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
  switch ($_SESSION["go_resp_code"]){ 
  	case 0: // Change to 0 For Argofire
	case "0":
		$mail->AddAddress($_SESSION["g_billing"][10], $_SESSION["go_oname"]);
		$mail->MsgHTML($approvedBody);
		//echo "Order receipt sent to ".$_SESSION["g_billing"][10].".  Please check your email inbox.</p>\n";
		break;
	case "Accepted":
		$mail->AddAddress($_SESSION["g_liship"][1][1]["s_email"], $_SESSION["g_liship"][1][1]["s_name"]);
		$mail->MsgHTML($freeBody);
		//echo "Order receipt sent to ".$_SESSION["g_liship"][1][1]["s_email"].
		//".  Please check your email inbox.</p>\n";
		break;
  	default:
	$mail->AddAddress($_SESSION["g_billing"][10], $_SESSION["go_oname"]);
	$mail->MsgHTML($declinedBody);
	//$mail->MsgHTML($ReceivedBody);
	$mailerror = false;
  }
  //$mail->AddAttachment('images/logo.gif');      // attachment
  $mail->AddEmbeddedImage('images/'.$lang['published_logo'], '01acf62315aa67c7ff9e9bb359fd98de',$lang['published_logo'],'base64',$lang['published_logo']);
  //$mail->AddAttachment('images/bar.gif'); // attachment
  $mail->Send();
  
} catch (phpmailerException $e) {
  $mailerror = true;
echo '<div class="alert alert-error">';
  $e->getMessage(); //Boring error messages from anything else!
echo '</div>';
  
} catch (Exception $e) {
	$mailerror = true;
echo '<div class="alert alert-error">';
  $e->getMessage(); //Boring error messages from anything else!
echo '</div>';
}

if ($mailerror == false){
	echo '
	<div class="alert alert-success">
	Order receipt sent to '.$_SESSION["g_billing"][10].'.  Please check your email inbox.
	</div>';
}
}
?>