			<div class="<?php echo($r1_span); ?>">
				<?php echo($r1_head); ?>
				<small><?php echo($r1_desc); ?></small>
				<div>
					<img src="images/<?php echo($r1_image); ?>" 
						 class="img-rounded" />
				</div>				
				<div>
					<small>
						<b>
							<?php echo($lang['cart_replica'] . ' ' . $lang['cart_quantity']); ?>
						</b>
					</small>
				</div>				
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">#</strong>
					</span>
					<?php echo($r1_qty); ?>
				</div>				
				<div>
					<small>
						<b>
							<?php echo($lang['cart_replica'].' '.$lang['cart_price_each']); ?>
						</b>
					</small>
				</div>			
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">
							<?php echo($lang['published_currency_symbol']); ?>
						</strong>
					</span>
					<input class="input-small" 
						   type="Text" 
						   id="x_rcost" 
						   name="x_rcost" 
						   value="<?php printf("%2.2f", $r1_price); ?>" 
						   readonly />
				</div>		
				<div>
					<small>
						<b>
							<?php echo($lang['cart_replica'].' '.$lang['cart_total']); ?>
						</b>
					</small>
				</div>				
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">
							<?php echo($lang['published_currency_symbol']); ?>
						</strong>
					</span>
					<input class="input-small subtotal" 
						   type="Text" 
						   id="x_rtotal" 
						   name="x_rtotal" 
						   value="<?php printf("%2.2f", $r1_subtotal); ?>" 
						   readonly />
				</div>
			</div>