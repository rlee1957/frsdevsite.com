<?php
$sqry = "
INSERT INTO w_lineitem 
	(
	li_order_no,
	li_line,
	li_bktype,
	li_bkdesc,
	li_bklines,
	li_bkchar,
	li_pqty,
	li_rqty,
	li_bkpcost,
	li_bkrcost,
	li_bkrship,
	li_ins1,
	li_ins2,
	li_ins3,
	li_ins4,
	li_ins5,
	li_ins6,
	li_ins7,
	li_ins8,
	li_ins9,
	li_ins10,
	li_ins11,
	li_ins12,
	li_ins13,
	li_ins14,
	li_ins15,
	li_logonam,
	li_logono,
	li_bkno,
	li_dcqty,
	li_dccost,
	li_dcbkno,
	li_dc2qty,
	li_dc2cost,
	li_dc2bkno,
	li_r2qty,
	li_bkr2cost,
	li_gc,
	li_location 
	)
VALUES 
	(
	'". sanitize_sql($_SESSION["go_orderid"]) ."',
	$ii,
	$tmp_bktype,
	'" . sanitize_sql($tmp_bkdesc) . "',
	$tmp_bklines,
	$tmp_bkchar,
	$tmp_pqty,
	$tmp_rqty,
	$tmp_pcost,
	$tmp_rcost,
	$tmp_rship,
	'" . sanitize_sql($a_ins[0]) . "',
	'" . sanitize_sql($a_ins[1]) . "',
	'" . sanitize_sql($a_ins[2]) . "',
	'" . sanitize_sql($a_ins[3]) . "',
	'" . sanitize_sql($a_ins[4]) . "',
	'" . sanitize_sql($a_ins[5]) . "',
	'" . sanitize_sql($a_ins[6]) . "',
	'" . sanitize_sql($a_ins[7]) . "',
	'" . sanitize_sql($a_ins[8]) . "',
	'" . sanitize_sql($a_ins[9]) . "',
	'" . sanitize_sql($a_ins[10]) . "',
	'" . sanitize_sql($a_ins[11]) . "',
	'" . sanitize_sql($a_ins[12]) . "',
	'" . sanitize_sql($a_ins[13]) . "',
	'" . sanitize_sql($a_ins[14]) . "',
	'" . sanitize_sql($tmp_logoname) . "',
	'" . sanitize_sql($tmp_logono) . "',
	$tmp_bkno,
	$tmp_dcqty,
	$tmp_dccost,
	$tmp_bkdc_bkno,
	$tmp_dc2qty,
	$tmp_dc2cost,
	$tmp_bkdc2_bkno,
	$tmp_r2qty,
	$tmp_r2cost,
	$tmp_gc,
	'" . sanitize_sql($tmp_location). "'  
	)
";

?>