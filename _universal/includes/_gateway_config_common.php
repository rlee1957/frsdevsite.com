<?php
# common Credit Card Processing variables
$_SESSION["go_url"] = "https://secure.ftipgw.com";
$_SESSION["go_page"] = "/smartpayments/transact.asmx/";
$_SESSION["go_email"] = 'lindsy@fundraisersltd.com';

# common Processing variables
$_SESSION["go_orderid"]	= '';
$_SESSION["go_total"] = '';
$_SESSION["go_cname"] = '';
$_SESSION["go_cno"]	 = '';
$_SESSION["go_cexp"] =  '';
$_SESSION["go_cvv2"] = '';
$_SESSION["go_oname"] = '';
$_SESSION["go_ostreet"] = '';
$_SESSION["go_ocity"] = '';
$_SESSION["go_ostate"] = '';
$_SESSION["go_ozip"] = '';
$_SESSION["go_resp_status"] = '';
$_SESSION["go_resp_auth_code"] = '';
$_SESSION["go_resp_auth_response"] = '';
$_SESSION["go_resp_code"] = '';
$_SESSION["go_resp_avs_code"] = '';
$_SESSION["go_resp_cvv2_code"] = '';
$_SESSION["go_resp_order_id"] = '';
$_SESSION["go_resp_reference_number"] = '';
$_SESSION["go_resp_error"] = '';
?>