<?php

$request = array();
$request['WebAuthenticationDetail'] = array();
$request['WebAuthenticationDetail']['ParentCredential'] = array();
$request['WebAuthenticationDetail']['ParentCredential']['Key'] = $fedex_key; 
$request['WebAuthenticationDetail']['ParentCredential']['Password'] = $fedex_password;
$request['WebAuthenticationDetail']['UserCredential'] = array();
$request['WebAuthenticationDetail']['UserCredential']['Key'] = $fedex_key; 
$request['WebAuthenticationDetail']['UserCredential']['Password'] = $fedex_password;
$request['ClientDetail'] = array();
$request['ClientDetail']['AccountNumber'] = $fedex_account_number; 
$request['ClientDetail']['MeterNumber'] =  $fedex_meter_number;
$request['TransactionDetail'] = array();
$request['TransactionDetail']['CustomerTransactionId'] = ' *** Address Validation Request using PHP ***';
$request['Version'] = array();
$request['Version']['ServiceId'] = 'aval'; 
$request['Version']['Major'] = '4'; 
$request['Version']['Intermediate'] = '0'; 
$request['Version']['Minor'] = '0';
$request['InEffectAsOfTimestamp'] = date('c');
$request['AddressesToValidate'] = array();
$request['AddressesToValidate'][0] = array();
$request['AddressesToValidate'][0]['ClientReferenceId'] = 'ClientReferenceId1';
$request['AddressesToValidate'][0]['Address'] = array();
$request['AddressesToValidate'][0]['Address']['StreetLines'] = array($test["address1"]);
$request['AddressesToValidate'][0]['Address']['PostalCode'] = $test["postal_code"];
$request['AddressesToValidate'][0]['Address']['City'] = $test["city"];
$request['AddressesToValidate'][0]['Address']['StateOrProvinceCode'] = $test["state_prov"];
$request['AddressesToValidate'][0]['Address']['CountryCode'] = $test["country"];
$path_to_wsdl = $path."../_fedex/AddressValidationService_v4.wsdl"; 

?>