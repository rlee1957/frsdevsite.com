<?php

if(isset($results["AddressResults"]["Attributes"]))
	{
	$ar = array();
	$ar["process_status"] = "success";
	foreach($results["AddressResults"]["Attributes"] as	$key => $pair)
		{ 
		$ar[strtolower($pair["Name"])] = $pair["Value"]; 
		}
	$results["plain"] = $ar;
	}
else
	{
	$ar = array();
	$ar["process_status"] = "failed";
	$results["plain"] = $ar;
	}
if($results["plain"]["process_status"] == "success")
	{
	if((isset($results["plain"]["resolved"]))&&($results["plain"]["resolved"] == "true"))
		{
		if(isset($results["AddressResults"]["EffectiveAddress"]))
			{
			$ea = $results["AddressResults"]["EffectiveAddress"];
			$pc = substr(trim($ea["PostalCode"]), 0, 5);			
			if((strtoupper(trim($test["address1"])) != strtoupper(trim($ea["StreetLines"])))||	
			   (strtoupper(trim($test["city"])) != strtoupper(trim($ea["City"])))||	
			   (strtoupper(trim($test["state_prov"])) != strtoupper(trim($ea["StateOrProvinceCode"])))||
			   (strtoupper(trim($test["postal_code"])) != strtoupper($pc))||
			   (strtoupper(trim($test["country"])) != strtoupper(trim($ea["CountryCode"]))))
			    {
				$results["plain"]["address_changed"] = "true";
				$results["plain"]["resolved"] = "true";
				}
			else
				{
				$results["plain"]["address_changed"] = "false";	
				$results["plain"]["resolved"] = "true";
				}
			}
		else
			{
			$results["plain"]["address_changed"] = "false";
			$results["plain"]["resolved"] = "true";			
			}
		}
	else
		{
		$results["plain"]["resolved"] = "false";	
		$results["plain"]["address_changed"] = "false";
		}
	}
$report = "success|";
if($results["plain"]["resolved"] == "false"){ $report = "failed|"; }
else
	{
	if($results["plain"]["address_changed"] == "true")
		{
		$current = $test;
		$new = array();
		$new["fn"] = $test["fn"];
		$new["mn"] = $test["mn"];
		$new["ln"] = $test["ln"];
		$new["company"] = $test["company"];
		$new["address1"] = $results["AddressResults"]["EffectiveAddress"]["StreetLines"];
		$new["address2"] = $test["address2"];
		$new["city"] = $results["AddressResults"]["EffectiveAddress"]["City"];
		$new["state_prov"] = $results["AddressResults"]["EffectiveAddress"]["StateOrProvinceCode"];
		$new["postal_code"] = $results["AddressResults"]["EffectiveAddress"]["PostalCode"];
		$new["country"] = $results["AddressResults"]["EffectiveAddress"]["CountryCode"];
		$ar = array();
		$ar["current"] = $current;
		$ar["new"] = $new;
		$report = "changes|".json_encode($ar);
		}
	}
/*echo("<textarea style='width: 100%; height: 300px;'>");
print_r($results);
echo("</textarea>");*/
echo($report);

?>