			<div class="<?php echo($dc2_span); ?>"  
				 style="float: left; width: 250px;"> 
				<?php echo($dc2_head); ?>
				<small>
					<?php echo($dc2_desc); ?>
				</small>
				<div>
					<img src="images/<?php echo($dc2_image); ?>" 
						 class="img-rounded" />
				</div>
				<div>
					<small><b>
						<?php echo($lang['cart_display_case2'].' '.$lang['cart_quantity']); ?></b>
					</small>
				</div>
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">#</strong>
					</span>
					<?php echo($dc2_qty); ?>
				</div>
				<div>
					<small><b>
						<?php echo($lang['cart_display_case2'].' '.$lang['cart_price_each']); ?></b>
					</small>
				</div>
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">
							<?php echo($lang['published_currency_symbol']); ?>
						</strong>
					</span>
					<input class="input-small" 
						   type="Text" 
						   id="x_dc2cost" 
						   name="x_dc2cost" 
						   value="<?php printf("%2.2f", $dc2_price); ?>"
						   readonly />
				</div>
				<div>
					<small><b>
						<?php echo($lang['cart_display_case2'] . ' ' . $lang['cart_total']); ?></b>
					</small>
				</div>
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">
							<?php echo($lang['published_currency_symbol']); ?>
						</strong>
					</span>
					<input class="input-small subtotal" 
						   type="Text" 
						   id="x_dc2total" 
						   name="x_dc2total" 
						   value="<?php echo($dc2_subtotal); ?>"
						   readonly />
				</div>
			</div>