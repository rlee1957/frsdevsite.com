<?php
$campaign_name = $lang['published_campaign_name_short'];
$order_id = $_SESSION["go_orderid"];
?>
				<br />
				<div class="control-group">
					<label class="control-label">
						<span style="font-weight: bold; font-size: 12pt;">
							<?php echo($lang["story_photo_title"]); ?>
						</span>
					</label>
					<div class="controls">
						<form action="../stories/actions/upload_file.php" 
							  method="post" 
							  enctype="multipart/form-data" 
							  target="work">
							<input type="file" 
								   name="upload_file" 
								   id="upload_file" 
								   accept=".gif, .jpg, .jpeg, .png"
								   onchange="inspect_file(this)"
								   style="position: absolute; left: -1000; top: -1000; height: 0px; width: 0px;" />
							<div style="float: left;">
								<?php echo($lang["story_photo_placeholder"]); ?>&nbsp;&nbsp;
							</div>
								
							<div style="float: left;">
								<input type="button" class="btn btn-custom" 
									   value="<?php echo($lang["story_photo_select"]); ?>"
									   onclick="document.getElementById('upload_file').click();"
									   id="btn_select_image" />
								&nbsp;&nbsp;
							</div>
							<div style="float: left;">
								<span id="show_file_name"></span>
								&nbsp;&nbsp;
							</div>
							<div style="float: left;">
								<input type="submit" 
									   value="<?php echo($lang["story_photo_upload"]); ?>" 
									   name="submit" 
									   id="submit_image" 
									   style="display: none;"
									   class="btn btn-custom"/>
								&nbsp;&nbsp;
							</div>
							<div style="float: left;" id="show_image"></div>
							<div style="clear: both;"></div>
							<input type=hidden id="suffix" name="suffix" value="" />
							<input type=hidden id="campaign_name" name="campaign_name" value="<?php echo($campaign_name); ?>" />
							<input type=hidden id="order_id" name="order_id" value="<?php echo($order_id); ?>" />
						</form>
					</div>
				</div>
