<?php

# Service Objects Parameters
	$proc = $config['service_objects_us_address'];
	$fs = "Address=".urlencode($_SESSION["tax_address"])."&";
	$fs .= "City=".urlencode($_SESSION["tax_city"])."&";
	$fs .= "State=".urlencode($_SESSION["tax_state"])."&";
	$fs .= "ZipCode=".urlencode($_SESSION["tax_zip"])."&";
	$fs .= "TaxType=".urlencode("Sales")."&";
	$fs .= "LicenseKey=".urlencode($sk);
	$url = $url.$proc."?".$fs;

	include("configuration/environment_settings.php");
	include("../".$environment_path."/includes/curl_execute.php");

	# Check for error
	if((string)$xml_data->Error->Desc != "")
		{
		# on error processing
		$_SESSION["service_objects_errordescription"] = (string)$xml_data->Error->Desc;
		$_SESSION["service_objects_totaltaxrate"] = 0.00;
		if(!isset($_SESSION["shipto_error"]))
			{
			$_SESSION["shipto_error"] = 1;	
			}
		else	
			{
			$_SESSION["shipto_error"]++;	
			}
		include("custom/ins_shipto.html");
		exit();
		}
	else
		{
		# on success processing
		$_SESSION["shipto_error"] = 0;
		$_SESSION["tax_zip"] = (string)$xml_data->Zip;
		$_REQUEST["s_zip"] = $_SESSION["tax_zip"];
		$_SESSION["service_objects_city"] = (string)$xml_data->City;
		$_SESSION["service_objects_county"] = (string)$xml_data->County;
		$_SESSION["service_objects_countyfips"] = (string)$xml_data->CountyFIPS;
		$_SESSION["service_objects_statename"] = (string)$xml_data->StateName;
		$_SESSION["service_objects_stateabbreviation"] = (string)$xml_data->StateAbbreviation;
		$_SESSION["service_objects_totaltaxrate"] = (string)$xml_data->TotalTaxRate;
		$_SESSION["service_objects_totaltaxexempt"] = (string)$xml_data->TotalTaxExempt;
		$_SESSION["service_objects_staterate"] = (string)$xml_data->StateRate;
		$_SESSION["service_objects_cityrate"] = (string)$xml_data->CityRate;
		$_SESSION["service_objects_countyrate"] = (string)$xml_data->CountyRate;
		$_SESSION["service_objects_countydistrictrate"] = (string)$xml_data->CountyDistrictRate;
		$_SESSION["service_objects_citydistrictrate"] = (string)$xml_data->CityDistrictRate;
		# RAW tax information
		include("configuration/environment_settings.php");
		include("../".$environment_path."/includes/raw_tax_information.php");
		}

?>