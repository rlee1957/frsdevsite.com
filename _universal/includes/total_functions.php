<?php

# ../_common/includes/total_functions.php

function get_current_paver_total()
{
	
$object_type = $_SESSION["g_currentbktype"];
$paver_quantity = $_SESSION["g_currentpqty"];
$paver_amount = $_SESSION["g_bkpcost"][$object_type];
$total = 0.00;
$total = $paver_quantity * $paver_amount;
return $total;	

}

function get_paver_total()
{

$total = 0.00;
if(isset($_SESSION["g_litotals"]))
	{
	if($_SESSION["g_litotals"] == 1)
		{
		$total = get_current_paver_total();
		}
	else
		{
		for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
			{
			$object_type = $_SESSION["g_libktype"][$ii];
			$paver_quantity = $_SESSION["g_lipqty"][$ii];
			$paver_amount = $_SESSION["g_bkpcost"][$object_type];
			$total += ($paver_quantity * $paver_amount);
			}	
		}	
	}
return $total;

}

function get_current_replica_total()
{

include("configuration/option_settings.php");
$total = 0.00;
if($options["enable_replica"])
	{
	$object_type = $_SESSION["g_currentbktype"];
	$replica_quantity = $_SESSION["g_currentrqty"];
	$replica_amount = $_SESSION["g_bkrcost"][$object_type];
	$total = ($replica_quantity * $replica_amount);
	}
return $total;	
	
}

function get_replica_total()
{

include("configuration/option_settings.php");
$total = 0.00;
if($options["enable_replica"])
	{
	if(isset($_SESSION["g_litotals"]))
		{
		if($_SESSION["g_litotals"] == 1)
			{
			$total = get_current_replica_total();
			}
		else
			{
			for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
				{
				if(isset($_SESSION["g_libktype"][$ii]))
					{
					$object_type = $_SESSION["g_libktype"][$ii];
					$replica_quantity = $_SESSION["g_lirqty"][$ii];
					$replica_amount = $_SESSION["g_bkrcost"][$object_type];
					$total += ($replica_quantity * $replica_amount);
					}
				}	
			}	
		}
	}
return $total;	
	
}

function get_current_replica2_total()
{

include("configuration/option_settings.php");
$total = 0.00;
if($options["enable_replica2"])
	{
	$object_type = $_SESSION["g_currentbktype"];
	$replica2_quantity = $_SESSION["g_currentr2qty"];
	$replica2_amount = $_SESSION["g_bkr2cost"][$object_type];
	$total = ($replica2_quantity * $replica2_amount);
	}
return $total;	
	
}

function get_replica2_total()
{
	
include("configuration/option_settings.php");
$total = 0.00;
if($options["enable_replica2"])
	{
	if(isset($_SESSION["g_litotals"]))
		{
		if($_SESSION["g_litotals"] == 1)
			{
			$total = get_current_replica2_total();
			}
		else
			{
			for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
				{
				if(isset($_SESSION["g_libktype"][$ii]))
					{
					$object_type = $_SESSION["g_libktype"][$ii];
					$replica2_quantity = $_SESSION["g_lir2qty"][$ii];
					$replica2_amount = $_SESSION["g_bkr2cost"][$object_type];
					$total += ($replica2_quantity * $replica2_amount);
					}
				}	
			}	
		}
	}
return $total;
	
}

function get_current_display_case_total()
{

include("configuration/option_settings.php");
$total = 0.00;
if($options["enable_displaycase"])
	{
	$object_type = $_SESSION["g_currentbktype"];
	$displaycase_quantity = $_SESSION["g_currentdcqty"];
	$displaycase_amount = $_SESSION["g_bkdccost"][$object_type];
	$total = ($displaycase_quantity * $displaycase_amount);
	}
return $total;
	
}

function get_display_case_total()
{

include("configuration/option_settings.php");
$total = 0.00;
if($options["enable_displaycase"])
	{
	if(isset($_SESSION["g_litotals"]))
		{
		if($_SESSION["g_litotals"] == 1)
			{
			$total = get_current_display_case_total();
			}
		else
			{
			for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
				{
				if(isset($_SESSION["g_libktype"][$ii]))
					{
					$object_type = $_SESSION["g_libktype"][$ii];
					$displaycase_quantity = $_SESSION["g_lidcqty"][$ii];
					$displaycase_amount = $_SESSION["g_bkdccost"][$object_type];
					$total += ($displaycase_quantity * $displaycase_amount);
					}
				}	
			}	
		}
	}
return $total;
	
}

function get_current_display_case2_total()
{

include("configuration/option_settings.php");
$total = 0.00;
if($options["enable_displaycase2"])
	{
	$object_type = $_SESSION["g_currentbktype"];
	$displaycase2_quantity = $_SESSION["g_currentdc2qty"];
	$displaycase2_amount = $_SESSION["g_bkdc2cost"][$object_type];
	$total = ($displaycase2_quantity * $displaycase2_amount);
	}
return $total;
	
}

function get_display_case2_total()
{

include("configuration/option_settings.php");
$total = 0.00;
if($options["enable_displaycase2"])
	{
	if(isset($_SESSION["g_litotals"]))
		{
		if($_SESSION["g_litotals"] == 1)
			{
			$total = get_current_display_case2_total();
			}
		else
			{
			for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
				{
				if(isset($_SESSION["g_libktype"][$ii]))
					{
					$object_type = $_SESSION["g_libktype"][$ii];
					$displaycase2_quantity = $_SESSION["g_lidc2qty"][$ii];
					$displaycase2_amount = $_SESSION["g_bkdc2cost"][$object_type];
					$total += ($displaycase2_quantity * $displaycase2_amount);
					}
				}	
			}	
		}
	}
return $total;
	
}

function get_gift_certificate_total()
{

include("configuration/option_settings.php");
$total = 0.00;
if($options["gift_certificates"])
	{
	if(isset($_SESSION["g_op_no_li"]))
		{
		for ($gc = 1; $gc <= $_SESSION["g_op_no_li"]; $gc++)
			{
			if (isset($_SESSION["g_op_li"][$gc]["op_type"])) 
				{		
				$object_type =  $_SESSION["g_op_li"][$gc]["op_type"];
				$gift_certificate_quantity = $_SESSION["g_op_li"][$gc]["op_qty"];
				$gift_certificate_amount =  $_SESSION["g_op_cost"][$object_type];
				$gc_ship =  $_SESSION["g_op_ship"][$object_type] * $gift_certificate_quantity;
				$total += ($gift_certificate_amount * $gift_certificate_quantity);
				}
			}
		}
	}
return $total;

}

function get_convenience_fee()
{

include("configuration/option_settings.php");
$total = 0.00;
if($options["web_convenience_fee"])
	{
	if(isset($_SESSION["convenience_fee"]))
		{
		if((!isset($_SESSION["user"]))|| 
		   ((isset($_SESSION["user"]))&&($_SESSION["user"] != "STAFF")))
		    {
			$total = $_SESSION["convenience_fee"];
		    }
		}
	}
return $total;

}

function get_current_paver_tax_total()
{

include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
$taxrate = 0.00;
if($tax["paver_method"] == "nexus")
	{
	$taxrate = $_SESSION["nexus_totaltaxrate"];
	}
elseif($tax["paver_method"] == "shipto")
	{
	$taxrate = 	$_SESSION["service_objects_totaltaxrate"];
	}
if($options["apply_sales_tax"])
	{
	if($tax["paver"])
		{
		if($taxrate > 0)
			{
			$apply_tax = true;
			if(($_SESSION["g_currentcb_gc"] == 1)&&(!$tax["gift_certificate"]))
				{ $apply_tax = false; }
			if($apply_tax)
				{
				$object_type = $_SESSION["g_currentbktype"];
				$paver_quantity = $_SESSION["g_currentpqty"];
				$paver_taxable_amount = $_SESSION["g_bktaxable_amt"][$object_type];
				$total = ($paver_quantity * $paver_taxable_amount) * $taxrate;
				}
			}
		}
	}
return $total;

}

function get_paver_tax_total()
{

include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
$taxrate = 0.00;
if($tax["paver_method"] == "nexus")
	{
	$taxrate = $_SESSION["nexus_totaltaxrate"];
	}
elseif($tax["paver_method"] == "shipto")
	{
	$taxrate = 	$_SESSION["service_objects_totaltaxrate"];
	}
if($options["apply_sales_tax"])
	{
	if($tax["paver"])
		{
		if($taxrate > 0)
			{
			if(isset($_SESSION["g_litotals"]))
				{
				if($_SESSION["g_litotals"] == 1)
					{
					$total = get_current_paver_tax_total();
					}
				else
					{
					for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
						{
						$apply_tax = true;
						if(($_SESSION["g_licb_gc"][$ii] == 1)&&(!$tax["gift_certificate"]))
							{ $apply_tax = false; }
						if($apply_tax)
							{
							$object_type = $_SESSION["g_libktype"][$ii];
							$paver_quantity = $_SESSION["g_lipqty"][$ii];
							$paver_taxable_amount = $_SESSION["g_bktaxable_amt"][$object_type];
							$total += ($paver_quantity * $paver_taxable_amount) * $taxrate;
							}	
						}
					}	
				}
			}
		}
	}
return $total;

}

function get_current_replica_tax_total()
{

include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
$taxrate = 0.00;
if($tax["replica_method"] == "nexus")
	{
	$taxrate = $_SESSION["nexus_totaltaxrate"];
	}
elseif($tax["replica_method"] == "shipto")
	{
	$taxrate = 	$_SESSION["service_objects_totaltaxrate"];
	}
if($options["apply_sales_tax"])
	{
	if($tax["replica"])
		{
		if($taxrate > 0)
			{
			$apply_tax = true;
			if(($_SESSION["g_currentcb_gc"] == 1)&&(!$tax["gift_certificate"]))
				{ $apply_tax = false; }
			if($apply_tax)
				{
				$replica_total = get_current_replica_total();
				$total = ($replica_total * $taxrate);
				}
			}
		}
	}
return $total;
	
}

function get_replica_tax_total()
{

include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
$taxrate = 0.00;
if($tax["replica_method"] == "nexus")
	{
	$taxrate = $_SESSION["nexus_totaltaxrate"];
	}
elseif($tax["replica_method"] == "shipto")
	{
	$taxrate = 	$_SESSION["service_objects_totaltaxrate"];
	}
if($options["apply_sales_tax"])
	{
	if($tax["replica"])
		{
		if($taxrate > 0)
			{
			if(isset($_SESSION["g_litotals"]))
				{
				if($_SESSION["g_litotals"] == 1)
					{
					$total = get_current_replica_tax_total();
					}
				else
					{
					for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
						{
						if(isset($_SESSION["g_libktype"][$ii]))
							{
							$apply_tax = true;
							if(($_SESSION["g_licb_gc"][$ii] == 1)&&(!$tax["gift_certificate"]))
								{ $apply_tax = false; }
							if($apply_tax)
								{
								$object_type = $_SESSION["g_libktype"][$ii];
								$replica_quantity = $_SESSION["g_lirqty"][$ii];
								$replica_amount = $_SESSION["g_bkrcost"][$object_type];
								$total += ($replica_quantity * $replica_amount) * $taxrate;
								}
							}	
						}	
					}
				}
			}
		}
	}
return $total;
	
}

function get_current_replica2_tax_total()
{
	
include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
$taxrate = 0.00;
if($tax["replica2_method"] == "nexus")
	{
	$taxrate = $_SESSION["nexus_totaltaxrate"];
	}
elseif($tax["replica2_method"] == "shipto")
	{
	$taxrate = 	$_SESSION["service_objects_totaltaxrate"];
	}
if($options["apply_sales_tax"])
	{
	if($tax["replica2"])
		{
		if($taxrate > 0)
			{
			$apply_tax = true;
			if(($_SESSION["g_currentcb_gc"] == 1)&&(!$tax["gift_certificate"]))
				{ $apply_tax = false; }
			if($apply_tax)
				{
				$replica2_total = get_current_replica2_total();
				$total = ($replica2_total * $taxrate);
				}
			}
		}
	}
return $total;
	
}

function get_replica2_tax_total()
{

include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
$taxrate = 0.00;
if($tax["replica2_method"] == "nexus")
	{
	$taxrate = $_SESSION["nexus_totaltaxrate"];
	}
elseif($tax["replica2_method"] == "shipto")
	{
	$taxrate = 	$_SESSION["service_objects_totaltaxrate"];
	}
if($options["apply_sales_tax"])
	{
	if($tax["replica2"])
		{
		if($taxrate > 0)
			{
			if(isset($_SESSION["g_litotals"]))
				{
				if($_SESSION["g_litotals"] == 1)
					{
					$total = get_current_replica2_tax_total();
					}
				else
					{
					for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
						{
						if(isset($_SESSION["g_libktype"][$ii]))
							{
							$apply_tax = true;
							if(($_SESSION["g_licb_gc"][$ii] == 1)&&(!$tax["gift_certificate"]))
								{ $apply_tax = false; }
							if($apply_tax)
								{
								$object_type = $_SESSION["g_libktype"][$ii];
								$replica2_quantity = $_SESSION["g_lir2qty"][$ii];
								$replica2_amount = $_SESSION["g_bkr2cost"][$object_type];
								$total += ($replica2_quantity * $replica2_amount) * $taxrate;
								}
							}
						}
					}	
				}
			}
		}
	}
return $total;
	
}

function get_current_display_case_tax_total()
{

include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
$taxrate = 0.00;
if($tax["display_case_method"] == "nexus")
	{
	$taxrate = $_SESSION["nexus_totaltaxrate"];
	}
elseif($tax["display_case_method"] == "shipto")
	{
	$taxrate = 	$_SESSION["service_objects_totaltaxrate"];
	}
if($options["apply_sales_tax"])
	{
	if($tax["display_case"])
		{
		if($taxrate > 0)
			{
			$apply_tax = true;
			if(($_SESSION["g_currentcb_gc"] == 1)&&(!$tax["gift_certificate"]))
				{ $apply_tax = false; }
			if($apply_tax)
				{
				$displaycase_total = get_current_display_case_total();
				$total = ($displaycase_total * $taxrate);
				}
			}
		}
	}
return $total;
	
}

function get_display_case_tax_total()
{
	
include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
$taxrate = 0.00;
if($tax["display_case_method"] == "nexus")
	{
	$taxrate = $_SESSION["nexus_totaltaxrate"];
	}
elseif($tax["display_case_method"] == "shipto")
	{
	$taxrate = 	$_SESSION["service_objects_totaltaxrate"];
	}
if($options["apply_sales_tax"])
	{
	if($tax["display_case"])
		{
		if($taxrate > 0)
			{
			if(isset($_SESSION["g_litotals"]))
				{
				if($_SESSION["g_litotals"] == 1)
					{
					$total = get_current_display_case_tax_total();
					}
				else
					{
					for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
						{
						if(isset($_SESSION["g_libktype"][$ii]))
							{
							$apply_tax = true;
							if(($_SESSION["g_licb_gc"][$ii] == 1)&&(!$tax["gift_certificate"]))
								{ $apply_tax = false; }
							if($apply_tax)
								{
								$object_type = $_SESSION["g_libktype"][$ii];
								$displaycase_quantity = $_SESSION["g_lidcqty"][$ii];
								$displaycase_amount = $_SESSION["g_bkdccost"][$object_type];
								$total += ($displaycase_quantity * $displaycase_amount) * $taxrate;
								}
							}
						}	
					}	
				}
			}
		}
	}
return $total;
	
}

function get_current_display_case2_tax_total()
{

include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
$taxrate = 0.00;
if($tax["display_case2_method"] == "nexus")
	{
	$taxrate = $_SESSION["nexus_totaltaxrate"];
	}
elseif($tax["display_case2_method"] == "shipto")
	{
	$taxrate = 	$_SESSION["service_objects_totaltaxrate"];
	}
if($options["apply_sales_tax"])
	{
	if($tax["display_case2"])
		{
		if($taxrate > 0)
			{
			$apply_tax = true;
			if(($_SESSION["g_currentcb_gc"] == 1)&&(!$tax["gift_certificate"]))
				{ $apply_tax = false; }
			if($apply_tax)
				{
				$displaycase2_total = get_current_display_case2_total();
				$total = ($displaycase2_total * $taxrate);
				}
			}
		}
	}
return $total;
	
}

function get_display_case2_tax_total()
{

include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
$taxrate = 0.00;
if($tax["display_case2_method"] == "nexus")
	{
	$taxrate = $_SESSION["nexus_totaltaxrate"];
	}
elseif($tax["display_case2_method"] == "shipto")
	{
	$taxrate = 	$_SESSION["service_objects_totaltaxrate"];
	}
if($options["apply_sales_tax"])
	{
	if($tax["display_case2"])
		{
		if($taxrate > 0)
			{
			if(isset($_SESSION["g_litotals"]))
				{
				if($_SESSION["g_litotals"] == 1)
					{
					$total = get_current_display_case2_tax_total();
					}
				else
					{
					for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
						{
						if(isset($_SESSION["g_libktype"][$ii]))
							{
							$apply_tax = true;
							if(($_SESSION["g_licb_gc"][$ii] == 1)&&(!$tax["gift_certificate"]))
								{ $apply_tax = false; }
							if($apply_tax)
								{
								$object_type = $_SESSION["g_libktype"][$ii];
								$displaycase2_quantity = $_SESSION["g_lidc2qty"][$ii];
								$displaycase2_amount = $_SESSION["g_bkdc2cost"][$object_type];
								$total += ($displaycase2_quantity * $displaycase2_amount) * $taxrate;
								}
							}	
						}							
					}	
				}
			}
		}
	}
return $total;
	
}

function get_gift_certificate_tax_total()
{

include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
if($options["apply_sales_tax"])
	{
	if($tax["gift_certificate"])
		{
		if($_SESSION["service_objects_totaltaxrate"] > 0)
			{
			$taxrate = $_SESSION["service_objects_totaltaxrate"];
			$gift_certificate_total = get_gift_certificate_total();
			$total = ($gift_certificate_total * $taxrate);
			}
		}
	}
return $total;
	
}

function get_convenience_fee_tax()
{

include("configuration/option_settings.php");
include("configuration/tax_settings.php");
$total = 0.00;
if($options["web_convenience_fee"])
	{
	if($options["apply_sales_tax"])
		{
		if($tax["convenience_fee"])
			{
			$taxrate = $_SESSION["service_objects_totaltaxrate"];
			$convenience_fee = get_convenience_fee();
			$total = ($convenience_fee * $taxrate);
			}
		}
	}
return $total;
	
}

function get_shipping_total()
{

$shipping_information = ship_to_information();
return $shipping_information["total_shipping_cost"];

}

function get_shipping_tax_total()
{

include("configuration/tax_settings.php");
$shipping_cost =  get_shipping_total();
$ship_tax = 0.00;
if($tax["shipping"])
	{
	$ship_tax = $shipping_cost * $_SESSION["service_objects_totaltaxrate"];	
	}
return $ship_tax;
}

function ship_to_information()
{

include("configuration/shipping_settings.php");
include("../_common/includes/language_check.php");
if($_SESSION["country"] == "US")
	{
	include("configuration/us_states.php");
	$divisions = $us_states;
	}
else
	{
	if($_SESSION["country"] == "CA")
		{
		include("configuration/canada_provinces.php");
		$divisions = $canada_provinces;
		}	
	}
$shipto = array();	
$items_to_ship = 0;
$ship_valid = true;
$ship_place = "";
$total_shipping_cost = 0.00;
$del = "";
$sp = array();
if(isset($_SESSION["g_litotals"]))
	{
	for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
		{
		$quantity_to_ship = $_SESSION["g_lirqty"][$ii];
		$quantity_to_ship += $_SESSION["g_lir2qty"][$ii];
		if($quantity_to_ship > 0)
			{
			$item_ship_place = $_SESSION["g_liship"][$ii][1]["s_state"];
			$shipping_method = $divisions[$item_ship_place]["shipping"];
			if(isset($sp[$item_ship_place])){ $sp[$item_ship_place]++; }
			else{ $sp[$item_ship_place] = 1; }
			$items_to_ship++;
			if($shipping_method == -1){ $ship_valid = false; }
			if($shipping_method > 0){ $total_shipping_cost += ($shipping_method * $quantity_to_ship); }
			}
		}	
	}

foreach($sp as $key => $val)
	{
	if($val == 1){ $itm = $lang["ship item"]; }
	else{ $itm = $lang["ship_items"]; }
	if(	$divisions[$key]["shipping"] == -1)
		{
		$ship_place .= $del."<span style='color: #ff0000;'>".$val." (".$val.$itm.")</span>";
		}
	else
		{
		$ship_place .= $del.$val." (".$val.$itm.")";
		}
	}
$shipto["ship_valid"] = $ship_valid;
$shipto["items_to_ship"] = $items_to_ship;
$shipto["ship_places"] = $ship_place;
$shipto["total_shipping_cost"] = $total_shipping_cost;
return $shipto;

}

function valid_shipto()
{
	
$shipping_information = ship_to_information();
return $shipping_information["ship_valid"];	

}

function shipping_places()
{
	
$shipping_information = ship_to_information();
return $shipping_information["ship_places"];	
	
}

function total_shipto_lines()
{
	
$shipping_information = ship_to_information();
return $shipping_information["items_to_ship"];
	
}

function get_shipto_label()
{

include("../_common/includes/language_check.php");	
$shipping_text = "";	
$shipping_information = ship_to_information();
if($shipping_information["total_shipping_cost"] == 0.00)
	{ 
	$shipping_text = $lang["ship_free_label"]; 
	if(!$shipping_information["ship_valid"])
		{ 
		$shipping_text = $lang["ship_invalid_label"]; 
		}
	}
else
	{ 
	$tsc = floatval($shipping_information["total_shipping_cost"]);
	$shipping_text = number_format($tsc , 2, '.', '');
	}
return $shipping_text;	

}


function get_greek_character_fee()
{

include("configuration/option_settings.php");
$total = 0.00;
if((isset($options["greek_lettering"]))&&($options["greek_lettering"]))
	{
	if((isset($options["fee_for_greek"]))&&($options["fee_for_greek"]))
		{
		if((isset($options["greek_character_fee_amt"]))&&($options["greek_character_fee_amt"] > 0))
			{
			if(isset($_SESSION["g_liins"]))
				{
				$ct = count($_SESSION["g_liins"]);
				for($x=1;$x<=$ct-1;$x++)
					{
					foreach($_SESSION["g_liins"][$x] as $id => $ins)
						{
						if(strlen($ins) > 0)
							{
							$pattern = "/.*[ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ].*/";
							$success = preg_match($pattern, $ins, $match);
							if($success)
								{
								$total = $options["greek_character_fee_amt"];
								break;
								}
							}
						}
					}
				}
			}
		}
	}
return $total;	
	
}


?>