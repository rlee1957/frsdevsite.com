<script language="javascript" type="text/javascript">

function validate_shipto()
{


fname_message = "<?php echo($lang['cart_shipping_first_name'].' '.$lang["validate_generic"]); ?>";	
fname = document.getElementById("s_name");
lname_message = "<?php echo($lang['cart_shipping_last_name'].' '.$lang["validate_generic"]); ?>";	
lname = document.getElementById("s_lname")
addr1_message = "<?php echo($lang['cart_shipping_address'].' '.$lang["validate_generic"]); ?>";	
addr1 = document.getElementById("s_addr1");
city_message = "<?php echo($lang["cart_shipping_city"].' '.$lang["validate_generic"]); ?>";	
city = document.getElementById("s_city");
state_message = "<?php echo($lang["validate_state_".strtolower($_SESSION["country"])]); ?>";	
state = document.getElementById("s_state");
zip_message = "<?php echo($lang["validate_zip_".strtolower($_SESSION["country"])]); ?>";	
zip = document.getElementById("s_zip");
phone_message = "<?php echo($lang["validate_us_phone"]); ?>";	
phone = document.getElementById("s_phone");
email_message = "<?php echo($lang["validate_email"]); ?>";	
email = document.getElementById("s_email");
if(!validate_required(fname, fname_message)){ return; }
if(!validate_required(lname, lname_message)){ return; }
if(!validate_required(addr1, addr1_message)){ return; }
if(!validate_required(city, city_message)){ return; }
if(!validate_state(state, state_message)){ return; }
if(!validate_postal(zip, zip_message, "<?php echo($_SESSION["country"]); ?>")){ return; }
if(!validate_us_phone(phone, phone_message)){ return; }
if(!validate_email(email, email_message)){ return; }
document.getElementById("frmShipto").submit();

}

</script>