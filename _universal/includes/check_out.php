<?php
if (session_status() == PHP_SESSION_NONE) 
	{
    session_start();
	}
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/inscription.php");
include("configuration/customer.php");
include("../".$environment_path."/includes/language_check.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Brick Order Cart </title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="sfacoc Stadium Legacy Brick Program">
<meta name="author" content="Fund Raisers, Ltd.">
<link href="../<?php echo($environment_path); ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="../<?php echo($environment_path); ?>/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="../<?php echo($environment_path); ?>/css/bootstrap-select.min.css" rel="stylesheet">
<script src="../<?php echo($environment_path); ?>/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../<?php echo($environment_path); ?>/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="../<?php echo($environment_path); ?>/js/jquery.validate.min.js"></script>
<?php if ($_SESSION["language"] != 0)
{ ?>
<script type="text/javascript" src="../<?php echo($environment_path); ?>/js/localization/messages_es.js"></script>
<?php }
?>
<script type="text/javascript" src="../<?php echo($environment_path); ?>/js/jquery.validate.creditcard2.pack-1.0.0.js"></script>
<!--       <script src="../<?php echo($environment_path); ?>/js/cmxforms.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" media="screen" href="../<?php echo($environment_path); ?>/css/screen.css" />
<link href="/sfacoc/facebox/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="../<?php echo($environment_path); ?>/css/payment_plan.css" media="screen" rel="stylesheet" type="text/css" />
<script src="/sfacoc/facebox/facebox.js" type="text/javascript"></script>
-->
<script type="text/javascript">
            $( document ).ready(function() {
        	$("#frmcheckout").validate({
            rules: {
                cc_number: {
                    creditcard2: function(){ return $('#cc_type').val(); }
                }
            },
			submitHandler: function(form) {
            $.blockUI({ message: '<h2><img src="../<?php echo($environment_path); ?>/img/ajax-loader.gif"/><?php echo $lang["cart_credit_card_process_credit_ajax"]; ?></h2><br/>' });
               form.submit();
              }
        });

		$("#b_referral").change(function (){
			if($("#b_referral").val() == 'Other'){ 

				$("#b_volname_display").show();
			}
			else{
				$("#b_volname_display").hide();
				}
			}).change();
		
			
		$('#show_Alumni').change(function() {
			$('#container_Alumni').toggle('fast', function() {
					$('#select_Alumni').val('none');
			 //Animation complete.
			});
		});
		$('#show_Other').change(function() {
			$('#container_Other').toggle('fast', function() {
					$('#select_Other').val('none');
			 //Animation complete.
			});
		});
		
		$('#same_as_shipping').click(function() {
			$('[name="b_fname"]').val('<?php echo $_SESSION["g_liship"][1][1]["s_name"];?>');
			$('[name="b_lname"]').val('<?php echo $_SESSION["g_liship"][1][1]["s_lname"];?>');
			$('[name="b_company"]').val('<?php echo $_SESSION["g_liship"][1][1]["s_company"];?>');
			$('[name="b_addr1"]').val('<?php echo $_SESSION["g_liship"][1][1]["s_addr1"];?>');
			$('[name="b_addr2"]').val('<?php echo $_SESSION["g_liship"][1][1]["s_addr2"];?>');
			$('[name="b_city"]').val('<?php echo $_SESSION["g_liship"][1][1]["s_city"];?>');
			$('[name="b_state"]').val('<?php echo $_SESSION["g_liship"][1][1]["s_state"];?>');
			$('[name="b_zip"]').val('<?php echo $_SESSION["g_liship"][1][1]["s_zip"];?>');
			$('[name="b_phone"]').val('<?php echo $_SESSION["g_liship"][1][1]["s_phone"];?>');
			$('[name="b_email"]').val('<?php echo $_SESSION["g_liship"][1][1]["s_email"];?>');
		});
		$('#same_as_shipping').click(citytax);
		$("#same_as_shipping_clear").bind("click", function() {
			$('[name="b_fname"], [name="b_lname"], [name="b_company"], [name="b_addr1"],[name="b_addr2"], [name="b_city"],[name="b_state"],[name="b_zip"],[name="b_phone"],[name="b_email"]').val("");
		});
		
		$('#b_state').change(citytax);
        // helper function to fire validation on field2 when cardType changes
        //
        $('#cc_type').change(function(){
            $("#frmcheckout").validate().element('#cc_number');
        });
    });
</script>
<style type="text/css">
<!--
.paidinfull {color: #FF0000; font-weight:bold;}
-->
</style>
<style type="text/css">
.cmxform fieldset p.error label { color: red; }

form.cmxform { width: 30em; }
form.cmxform label.error {
	display: block;
	margin-left: 1em;
	width: auto;
}
</style>
<?php Style_Sheets();  ?>
<link href="css/custom.css" rel="stylesheet">
</head>
<body>
<div class="container">
<?php  Customer_Header_Body(); ?>
<form class="form-horizontal" name="frmcheckout" id="frmcheckout" action="proc_ins.html" method="POST"  > <!-- CheckCardNumber(this.form)  id="entry" return checkfields(this)-->
<?php
if ($_SESSION["g_ordertotal"] > 5)
	{
	echo '
<div class="row">
	<div class="span12">
    	<div class="well">';
	bo_extra();
	if($_SESSION["user"] == "STAFF")
		{
		bo_staff();
		}
    bo_getbillto();
    bo_creditcard();
    echo '
		</div>
	</div>
</div>
    ';
	bo_combined_cart();
    }
?>
<div class="row">
	<div class="span12">
		<div class="well pagination-centered">
          <div style="margin-bottom:10px;">
              <input type="checkbox" required name="accept_tc">
        <?php echo $lang['cart_credit_card_terms_accept']; ?>
        <p class="help-block"></p>
          </div>
            <a data-target="#termsModal" role="button" class="btn btn-custom" data-toggle="modal"><?php echo $lang['cart_credit_card_terms_view']; ?></a>
      <button style="margin-left:10px;" type="submit" name="process_cc" class="btn btn-custom"><?php echo $lang['cart_credit_card_process_credit']; ?></button> 
		</div>
    </div>
</div>
<div class="modal fade hide" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="termsModalLabel" aria-hidden="true" data-remote="../<?php echo($environment_path); ?>/html/terms.html">
  <div class="modal-header">
    <h3 id="termsModalLabel"><?php echo $lang['terms_terms_conditions_header']; ?></h3>
  </div>
  <div class="modal-body">
    <p><?php echo $lang['terms_terms_conditions_header']; ?></p>
  </div>
  <div class="modal-footer">
  	<span>Click outside the box/window to close</span>
  </div>
</div>
</form>
<?php  Customer_Footer();  ?>
</div>
<?php include_once("../".$environment_path."/includes/analyticstracking.php") ?>
</body>
</html>