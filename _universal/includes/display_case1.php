			<div class="<?php echo($dc1_span); ?>" 
				 style="float: left; width: 250px;">	
				<?php echo($dc1_head); ?>				
				<small>
					<?php echo($dc1_desc); ?>
				</small>
				<div>
					<img src="images/<?php echo($dc1_image); ?>" 
						 class="img-rounded" />
				</div>
				<div>
					<small><b>
						<?php echo($lang['cart_display_case'].' '.$lang['cart_quantity']); ?></b>
					</small>
				</div>
								
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">#</strong>
					</span>
					<?php echo($dc1_qty); ?>
				</div>				
				<div>
					<small><b>
						<?php echo($lang['cart_display_case'].' '.$lang['cart_price_each']); ?></b>
					</small>
				</div>
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">
							<?php echo($lang['published_currency_symbol']); ?>
						</strong>
					</span>
					<input class="input-small" 
						   type="Text" 
						   id="x_dccost" 
						   name="x_dccost" 
						   value="<?php printf("%2.2f", $dc1_price); ?>"
							readonly />
				</div>
				<div>
					<small><b>
						<?php echo($lang['cart_display_case'] . ' ' . $lang['cart_total']); ?></b>
					</small>
				</div>			
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">
							<?php echo($lang['published_currency_symbol']); ?>
						</strong>
					</span>
					<input class="input-small subtotal" 
						   type="Text" 
						   id="x_dctotal" 
						   name="x_dctotal" 
						   value="<?php echo($dc1_subtotal); ?>" 
						   readonly />
				</div>
			</div>	