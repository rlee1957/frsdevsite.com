<?php

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/configuration.php");
if(!isset($_SESSION["language"]))
	{
	if($config["offer_language"])
		{
		if(!isset($_REQUEST["language_choice"]))
			{
			include("custom/declare_location.html");
			exit();
			}
		else	
			{
			$_SESSION["language"] = $_REQUEST["language_choice"];
			$_SESSION["country"] = $_REQUEST["country_choice"];
			}
		}
	else	
		{
		$_SESSION["language"] = $config["default_language"];
		$_SESSION["country"] = $config["default_country"];
		}
	}

if(!isset($_SESSION["g_promo_good"]))
	{
	if($config["promo"])	
		{
		if(!isset($_POST["x_chkbox"]))
			{
			include("custom/promo.html");
			exit();
			}
		else
			{
			$_SESSION["g_promo_good"] = $_POST["x_chkbox"];
			}
		}
	else	
		{
		$_SESSION["g_promo_good"] = 0;
		}
	}
elseif (isset($_POST["submit1"]))  
	{					
	if (isset($_POST["remote"]))  
		{
		$_SESSION["remote"] = true; //set remote to true;
        if (isset($_POST["logo_name"]))  
			{
			$logo_name = $_POST["logo_name"];
			$name1 = explode('/',$logo_name);
			$name2 = explode('.',$name1[7]);
			$name3 = $name2[0];
			$_SESSION["logo_name"] = $name3;
			}
        $_SESSION["g_liins"] = null;
    	for ($line = 1; $line <= 10; $line++)
			{
        	$line_text = $_POST["line".$line];
        	for ($c = 0; $c <= strlen($line_text); $c++)
				{
				$_SESSION["g_liins"][1][$line][$c+1] = strtoupper(substr($line_text, $c, 1));
				}
			}
		include("configuration/_customer.php");
		}
    else
		{
		$_SESSION["remote"] = false;
		}
    $_SESSION["g_currentbktype"] = $_POST["bktype"];
	if(isset($_SESSION["g_bk_op_map"]))
		{
		$_SESSION["g_op_selected_type"] = $_SESSION["g_bk_op_map"][$_POST["bktype"]]; 
		}
    include("custom/ins_get.html");
    exit;
    } 
elseif   (isset($_POST["ins_cancel"])) 
	{      
	$_SESSION["g_rqtyremaining"]  = 0;
	$_SESSION["show_buttons"] = 1;
	include("custom/cart.html");
	exit;
    } 
elseif   (isset($_POST["ins_conf_cancel"])) 
	{      
	$_SESSION["g_rqtyremaining"]  = 0;
	$_SESSION["show_buttons"] = 1;
	ses_del_line_item_cancel();
	include("custom/cart.html");
	exit;
    } 
elseif  (isset($_POST["ins_get_cont"])) 
	{         
	ses_save_line_item();		 	 
	// If replica is greater than 0 goto shipping
	if (1 > 0) 
		{
		ses_shipto_rqtyremaining();
		if(isset($_POST["x_op_qty"]))
			{
			$_SESSION["g_op_selected_qty"] =  $_POST["x_op_qty"];
			}
		else
			{
			$_SESSION["g_op_selected_qty"] = 0;
			}
		include("custom/ins_shipto.html");
		} 
	else 
		{
		include("custom/ins_confirm.html");
		}
	exit;
	} 
elseif  (isset($_POST["ins_conf_cont"])) 
	{
	$_SESSION["show_buttons"] = 1;
	if ($_SESSION["g_op_selected_qty"] > 0) 
		{
		op_save_lineitem($_SESSION["g_op_selected_qty"]);
		}	
	include("custom/cart.html");
	exit;
	} 
elseif  (isset($_POST["ins_conf_edit"])) 
	{
	include("custom/ins_edit.html");
	exit;
    } 
elseif  (isset($_POST["ins_ship"])) 
	{
	ses_shipto_rqtyremaining();
	include("custom/ins_shipto.html");
	exit;
	} 
elseif  (isset($_POST["shipto_add"])) 
	{
	ses_shipto_add();
	ses_shipto_rqtyremaining();
	include("custom/ins_shipto.html");
	exit;
	} 
elseif  (isset($_POST["shipto_cancel"])) 
	{
	ses_shipto_cancel();
	ses_shipto_rqtyremaining();
	include("custom/ins_confirm.html");
	exit;
	} 
elseif  (isset($_POST["shipto_cont"])) 								// after shipping confirmation page
	{
	$_SESSION["country"] = $_REQUEST["s_country"];
	$_SESSION["s_prefix"] = $_REQUEST["s_prefix"];
	$_SESSION["s_name"] = $_REQUEST["s_name"];
	$_SESSION["s_mname"] = $_REQUEST["s_mname"];
	$_SESSION["s_lname"] = $_REQUEST["s_lname"];
	$_SESSION["s_company"] = $_REQUEST["s_company"];
	$_SESSION["s_addr1"] = $_REQUEST["s_addr1"];
	$_SESSION["s_addr2"] = $_REQUEST["s_addr2"];
	$_SESSION["s_city"] = $_REQUEST["s_city"];
	$_SESSION["s_state"] = $_REQUEST["s_state"];
	$_SESSION["s_zip"] = $_REQUEST["s_zip"];
	$_SESSION["s_phone"] = $_REQUEST["s_phone"];
	$_SESSION["s_email"] = $_REQUEST["s_email"];
	if($options["verify_address"] || $options["get_tax_information"]) // tax and address validation
		{
		$_SESSION["tax_address"] = $_SESSION["s_addr1"];
		$_SESSION["tax_city"] = $_SESSION["s_city"];
		$_SESSION["tax_state"] = $_SESSION["s_state"];
		$_SESSION["tax_zip"] = $_SESSION["s_zip"];
		if(($_SESSION["country"] == "US")||($_SESSION["country"] == "CA"))
			{
			$_SESSION["tax_info"] = "";
			if(!isset($_SESSION["tax_rate_identified"]))
				{
				$_SESSION["tax_rate_identified"] = false;
				}
			if(!$_SESSION["tax_rate_identified"])
				{
				if(!isset($_SESSION["service_objects_totaltaxrate"]))
					{
					$_SESSION["service_objects_totaltaxrate"] = 0.00;
					$_SESSION["nexus_totaltaxrate"] = 0.00;
					}
				include("../_common/location/country_locations.php");
				if(((isset($country_locations[$_SESSION["country"]][$_SESSION["tax_state"]])) && 
				   ($country_locations[$_SESSION["country"]][$_SESSION["tax_state"]]["tax"])) || ($tax["calculate_nexus_tax"]))
					{
					include("../_common/includes/set_tax_rate.php");
					$_SESSION["tax_rate_identified"] = true;
					}
				else	
					{
					$_SESSION["service_objects_totaltaxrate"] = 0.00;
					$_SESSION["nexus_totaltaxrate"] = 0.00;
					$_SESSION["tax_rate_identified"] = true;
					}
				}
			}
		}
	else
		{
		$_SESSION["tax_state"] = $_SESSION["s_state"];
		include("../_common/location/country_locations.php");
		if(((isset($country_locations[$_SESSION["country"]][$_SESSION["tax_state"]])) && 
		($country_locations[$_SESSION["country"]][$_SESSION["tax_state"]]["tax"]))||($lang["taxable_states"] == "ALL"))
			{
			$_SESSION["service_objects_totaltaxrate"] = $tax["rate"];
			$_SESSION["nexus_totaltaxrate"] = $tax["nexus_rate"];
			$_SESSION["tax_rate_identified"] = true;
			}
		else	
			{
			$_SESSION["service_objects_totaltaxrate"] = 0.00;
			$_SESSION["nexus_totaltaxrate"] = 0.00;
			$_SESSION["tax_rate_identified"] = true;
			}
	
		}
		
	ses_shipto_add();
	ses_shipto_rqtyremaining();
	$_SESSION["show_buttons"] = 1;
	include("custom/ins_confirm.html");
	exit;
	}  
elseif  (isset($_POST["cart_checkout"])) 
	{
	$_SESSION["show_buttons"] = 0;
	include("custom/check_out.html");   // add more here
	exit;
	} 
elseif  (isset($_POST["process_cc"])) 
	{
	include("../".$environment_path."/includes/language_check.php");
	//Add taxes if certain state is selected
	if($options["apply_sales_tax"])
		{ // set the states initial for charge only tax for that state
		$_SESSION["g_taxtotal"] = round($_SESSION["service_objects_totaltaxrate"] * ($_SESSION["g_ordertotal"]-5),2);
		$_SESSION["g_ordertotal"] = round($_SESSION["g_taxtotal"] + $_SESSION["g_ordertotal"],2) ;
		$_SESSION["g_tax_state"] = $_POST["b_state"];
		}
	else
		{
		$_SESSION["g_tax_state"] = "";
		}
	if (isset($_POST["pay_plan"])) 
		{		
		$_SESSION["g_payplan"] = 1;
		} 
	else 
		{
		$_SESSION["g_payplan"] = 0;
		}
	ses_save_bill();
	include("custom/process_cc.html");
	exit;
	} 
elseif  (isset($_POST["but_cc_denied"])) 
	{
	include("custom/check_out.html");
	exit;
	} 
elseif  (isset($_POST["but_cc_approved"])) 
	{
	include("index.html");
	exit;
	} 
elseif  (isset($_POST["but_cart_edit"])) 
	{
	include("custom/ins_edit.html");
	exit;
	} 
elseif  (isset($_POST["ins_edit_cancel"])) 
	{
	if ($_SESSION["called_from_cart"] == 1) 
		{
		include("custom/cart.html");
		} 
	else 
		{
		ses_del_line_item_cancel();
		include("custom/cart.html");
		}
	exit;
	} 
elseif  (isset($_POST["ins_edit_save"])) 
	{
	save_edit_qty();
	save_edit_ins();
	save_location();
	ses_shipto_save_one();
	include("custom/cart.html");
	exit;
	} 
elseif  (isset($_GET["selected_line_item_del"])) 
	{
	ses_del_line_item();
	include("custom/cart.html");
	exit;
	} 
elseif  (isset($_GET["selected_line_item"]))
	{
	include("custom/ins_edit.html");
	exit;
	}
elseif  (isset($_POST["but_promo_cont"])) 
	{
	$_SESSION["language"] = $_POST["x_lang"];
	$_SESSION["g_promo_text"] = strtoupper($_POST["g_promo_text"]);
	if(!isset($_SESSION["g_promo_exttext"]))
		{
		$_SESSION["g_promo_exttext"] = strtoupper($_POST["g_promo_exttext"]);
		}
	$_SESSION["redeem_email"] = $_POST["x_email"]; 
	$_SESSION["redeem_quantity_remain"] = $_POST["x_remain"]; 
	if($_POST["x_chkbox"] == "1")
		{
		$_SESSION["g_promo_good"] = 1;
		include("index.html");
		}
	elseif($_POST["x_chkbox"] == "2")
		{
		$_SESSION["g_promo_good"] = 2;
		include("index.html");
		}
	elseif($_POST["x_chkbox"] == "4")
		{
		$_SESSION["g_promo_good"] = 4;
		include("index.html");
		}
	else 
		{
		$_SESSION["g_promo_good"] = 0;
		include("index.html");
		}
	exit;
	} 
elseif  (isset($_POST["but_promocode_cont"])) 
	{
	if ($_POST["g_promo_code"] == "CRVA908") 
		{ 		 	
		$_SESSION["g_promo_good"] = 1;
		include("index.html"); 			
		} 
	else 
		{
		$_SESSION["g_promo_code_msg"] = "Invalid Code.  Please try again.";
		$_SESSION["g_promo_good"] = 0;
		include("custom/promo.html");
		}
	exit;
	} 
elseif  (isset($_POST["but_order_options"])) 
	{
	$_SESSION["g_op_selected_type"] = $_POST["optype"];
	include("custom/get_options.html");
	exit;
	} 
elseif  (isset($_POST["but_order_options2"])) 
	{
	$_SESSION["g_op_selected_type"] = $_POST["optype2"];
	include("custom/get_options.html");
	exit;
	} 
elseif  (isset($_POST["but_op_get_cont"])) 
	{
	$x_HoldOptionName = strtoupper($_SESSION["g_op_desc"][$_SESSION["g_op_selected_type"]]);
	if (strstr($x_HoldOptionName,"DONATE"))  
		{
		$donate = true;
		} 
	else
		{
		$donate = false;
		}
	if($donate)
		{
		$_SESSION["g_op_cost"][3] = $_POST["x_op_cost"];
		}
	//------------------------------------------
	// Continue button - Save options stuff
	//------------------------------------------
	op_save_lineitem($_POST["x_op_qty"],$_POST["x_op_rqty"]);
	if (isset($_SESSION["g_lishiptoqty"][1])) 
		{
		include("custom/cart.html");		  
		} 
	else 
		{
		include("custom/ins_op_shipto.html");
		}
	exit;
	} 
elseif  (isset($_POST["but_op_shipto_cont"])) 
	{
	ses_shipto_add();
	ses_shipto_rqtyremaining();	   
	include("custom/cart.html");		  		 
	exit;
	} 
elseif  (isset($_POST["but_op_get_cancel"])) 
	{
	include("custom/cart.html");		  		 
	exit;
	} 
elseif  (isset($_GET["selected_op_line_item_del"])) 
	{
	op_del_line_item();
	include("custom/cart.html");         
	exit;
	}
	
?>
