<?php

	$required = "";
	if($story["category_required"]){ $required = "required"; }
?>
				<br />
				<div class="control-group">
					<label class="control-label">
						<span style="font-weight: bold; font-size: 12pt;">
							<?php echo($lang['story_category_title']); ?>
						</span>
					</label>
					<div class="controls">
						<select class="selectpicker span6 show-tick show-menu-arrow" <?php echo($required); ?>
							id="dd_category" 
							name="dd_category"
							onchange="category_change();">
							<option value="" selected disabled><?php echo($lang['story_category_options_prompt']); ?></option>
<?php
	$options = explode("|",$lang['story_category_options']);
	if($story["category_sorted"]){ sort($options); }
	foreach ($options as $value)
		{
		echo('
							<option value="'.$value.'"> '.$value.'</option>
');
		}
?>
						</select>
					</div>
				</div>
