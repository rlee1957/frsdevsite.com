<?php

include("configuration/environment_settings.php");
include("../".$environment_path."/functions/proc_ins_ses_del_line_item.php");
include("../".$environment_path."/functions/proc_ins_op_del_line_item.php");
include("../".$environment_path."/functions/proc_ins_ses_del_line_item_cancel.php");
include("../".$environment_path."/functions/proc_ins_ses_save_line_item.php");
include("../".$environment_path."/functions/proc_ins_save_edit_ins.php");
include("../".$environment_path."/functions/proc_ins_save_edit_qty.php");
include("../".$environment_path."/functions/proc_ins_save_location.php");
include("../".$environment_path."/functions/proc_ins_ses_shipto_rqtyremaining.php");
include("../".$environment_path."/functions/proc_ins_ses_shipto_add.php");
include("../".$environment_path."/functions/proc_ins_ses_shipto_save_one.php");
include("../".$environment_path."/functions/proc_ins_ses_shipto_cancel.php");
include("../".$environment_path."/functions/proc_ins_ses_save_bill.php");
include("../".$environment_path."/functions/proc_ins_op_save_lineitem.php");
include("../".$environment_path."/functions/proc_ins_checkhandlingfee.php");

?>