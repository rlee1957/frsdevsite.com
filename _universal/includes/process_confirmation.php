<?php
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo($lang["order_confirmation"]); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo($lang['published_campaign_name']); ?>">
<meta name="author" content="Fund Raisers, Ltd.">
<link href="../_common/css/bootstrap.min.css" rel="stylesheet">
<link href="../_common/css/bootstrap-responsive.min.css" rel="stylesheet">
<meta property="og:type" content="product" />
<meta property="og:title" content="<?php echo($lang['published_campaign_name']); ?>" />
<meta property="og:description" content="Confirmation" />
<meta property="og:url" content="https://www.brickorder.com/<?php echo($lang['published_campaign_name_short']); ?>" />
<meta property="og:image" content="https://www.brickorder.com/<?php echo($lang['published_campaign_name_short']); ?>/images/<?php echo($lang['published_logo']); ?>" />
<meta property="og:site_name" content="<?php echo($lang['published_campaign_name']); ?>" />
<meta property="og:determiner" content="auto" />
<?php
Style_Sheets();
?>
<link href="css/custom.css" rel="stylesheet">
</head>
<body>
<div class="container">
<?php
	Customer_Header_Body();
?>
<div class="row">
	<div class="span12">
		<div class="well">
		<?php echo($lang['confirmation_thank_you_text2']); ?>
		</div>
	</div>
</div>
<?php
bo_combined_cart();
?>
  <div class="row">
	  <div class="span12">
		<div class="well">
			<?php echo($lang['cart_order_confirmation_header']); ?>
		</div>
	  </div>
  </div>

  <div class="row">
	  <div class="span12">
	  	<div class="well">
			<?php echo($lang['confirmation_web_receipt']); ?>
		</div>
	  </div>
  </div>

  <div class="row">
	  <div class="span12">
	  	<div class="well">
			<?php echo($lang['confirmation_payment_please_print']); ?>
		 </div>
	  </div>
  </div>
  <div class="row">
	  <div class="span12">
	  	<div class="well">
<?php
	send_email();
?>
		</div>
	  </div>
  </div>
<?php
	echo($social_media);
	Customer_Footer();
?>
</div>
</BODY>
  <script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
var pageTracker = _gat._getTracker("';echo $lang['set_analytics']; echo '");
pageTracker._trackPageview();
</script>
</HTML>
