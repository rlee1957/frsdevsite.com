<?php

	# Service Objects Parameters
	$proc = $config['service_objects_us_address'];
	$fs = "Address=".urlencode($tax["nexus_address"])."&";
	$fs .= "City=".urlencode($tax["nexus_city"])."&";
	$fs .= "State=".urlencode($tax["nexus_state"])."&";
	$fs .= "ZipCode=".urlencode($tax["nexus_zip"])."&";
	$fs .= "TaxType=".urlencode("Sales")."&";
	$fs .= "LicenseKey=".urlencode($sk);
	$url = $config['service_objects_url'].$proc."?".$fs;
	include("../_common/includes/curl_execute.php");

	# Check for error
	if((string)$xml_data->Error->Desc != "")
		{
		# on error processing
		$_SESSION["nexus_errordescription"] = (string)$xml_data->Error->Desc;
		$_SESSION["nexus_totaltaxrate"] = 0.00;
		}
	else
		{
		# on success processing
		$_SESSION["nexus_totaltaxrate"] = (string)$xml_data->TotalTaxRate;
		}

?>