<?php

	# Service Objects Parameters
	$proc = $config['service_objects_ca_address'];
	$fs = "province=".urlencode($_SESSION["tax_state"])."&";
	$fs .= "LicenseKey=".urlencode($sk);
	$url = $url.$proc."?".$fs;

	include("configuration/environment_settings.php");
	include("../".$environment_path."/includes/curl_execute.php");

	# Check for error
	if((string)$xml_data->Error->Desc != "")
		{
		# on error processing
		$_SESSION["service_objects_totaltaxrate"] = 0.00;
		if(!isset($_SESSION["shipto_error"]))
			{
			$_SESSION["shipto_error"] = 1;	
			}
		else	
			{
			$_SESSION["shipto_error"]++;	
			}
		include("custom/ins_shipto.html");
		exit();
		}
	else
		{
		# on success processing
		$_SESSION["shipto_error"] = 0;
		$_REQUEST["s_zip"] = $_SESSION["tax_zip"];
		$_SESSION["service_objects_ProvinceName"] = (string)$xml_data->ProvinceName;
		$_SESSION["service_objects_ProvinceAbbreviation"] = (string)$xml_data->ProvinceAbbreviation;
		$_SESSION["service_objects_GoodsSalesTax"] = (string)$xml_data->GoodsSalesTax;
		$_SESSION["service_objects_ProvinceSalesTax"] = (string)$xml_data->ProvinceSalesTax;
		$_SESSION["service_objects_HarmonizedSalesTax"] = (string)$xml_data->HarmonizedSalesTax;
		$_SESSION["service_objects_ApplyGSTFirst"] = (string)$xml_data->ApplyGSTFirst;
		$_SESSION["service_objects_totaltaxrate"] = $_SESSION["service_objects_HarmonizedSalesTax"] + $_SESSION["service_objects_ProvinceSalesTax"] + $_SESSION["service_objects_GoodsSalesTax"];
		# RAW tax information
		$_SESSION["tax_info"] = "Address~".$_SESSION["tax_address"]."|";
		$_SESSION["tax_info"] .= "City~".$_SESSION["tax_city"]."|";
		$_SESSION["tax_info"] .= "Province Abbreviation~".$_SESSION["tax_state"]."|";
		$_SESSION["tax_info"] .= "Province~".$_SESSION["service_objects_ProvinceName"]."|";
		$_SESSION["tax_info"] .= "Postal Code~".$_SESSION["tax_zip"]."|";
		$_SESSION["tax_info"] .= "Province Sales Tax~".$_SESSION["service_objects_ProvinceSalesTax"]."|";
		$_SESSION["tax_info"] .= "Harmonized Sales Tax~".$_SESSION["service_objects_HarmonizedSalesTax"]."|";
		$_SESSION["tax_info"] .= "Apply GST First~".$_SESSION["service_objects_ApplyGSTFirst"]."|";
		$_SESSION["tax_info"] .= "Total Tax Rate~".$_SESSION["service_objects_totaltaxrate"];
		}

?>