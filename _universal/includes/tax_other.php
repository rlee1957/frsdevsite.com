<?php

# Service Objects Parameters
	$_SESSION["shipto_error"] = 0;
	$_REQUEST["s_zip"] = $_SESSION["tax_zip"];
	$_SESSION["service_objects_city"] = $_SESSION["tax_city"];
	$_SESSION["service_objects_county"] = "";
	$_SESSION["service_objects_countyfips"] = "";
	$_SESSION["service_objects_statename"] = $_SESSION["tax_state"];
	$_SESSION["service_objects_stateabbreviation"] = $_SESSION["tax_state"];
	$_SESSION["service_objects_totaltaxrate"] = 0.00;
	$_SESSION["service_objects_totaltaxexempt"] = 0.00;
	$_SESSION["service_objects_staterate"] = 0.00;
	$_SESSION["service_objects_cityrate"] = 0.00;
	$_SESSION["service_objects_countyrate"] = 0.00;
	$_SESSION["service_objects_countydistrictrate"] = 0.00;
	$_SESSION["service_objects_citydistrictrate"] = 0.00;
	# RAW tax information
	$_SESSION["tax_info"] = "Tax Information(";
	$_SESSION["tax_info"] .= "Address~".$_SESSION["tax_address"]."|";
	$_SESSION["tax_info"] .= "City~".$_SESSION["tax_city"]."|";
	$_SESSION["tax_info"] .= "County~".$_SESSION["service_objects_county"]."|";
	$_SESSION["tax_info"] .= "State~".$_SESSION["tax_state"]."|";
	$_SESSION["tax_info"] .= "State Name~".$_SESSION["service_objects_statename"]."|";
	$_SESSION["tax_info"] .= "zip~".$_SESSION["tax_zip"]."|";
	$_SESSION["tax_info"] .= "County FIPS~".$_SESSION["service_objects_countyfips"]."|";
	$_SESSION["tax_info"] .= "Total Tax Rate~".$_SESSION["service_objects_totaltaxrate"]."|";
	$_SESSION["tax_info"] .= "Total Tax Exempt~".$_SESSION["service_objects_totaltaxexempt"]."|";
	$_SESSION["tax_info"] .= "State Tax Rate~".$_SESSION["service_objects_staterate"]."|";
	$_SESSION["tax_info"] .= "City Tax Rate~".$_SESSION["service_objects_cityrate"]."|";
	$_SESSION["tax_info"] .= "County Tax Rate~".$_SESSION["service_objects_countyrate"]."|";
	$_SESSION["tax_info"] .= "County District Tax Rate~".$_SESSION["service_objects_countydistrictrate"]."|";
	$_SESSION["tax_info"] .= "City District Tax Rate~".$_SESSION["service_objects_citydistrictrate"];
	$_SESSION["tax_info"] = ") ";

?>