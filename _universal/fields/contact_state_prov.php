<?php

# State / Prov input field
echo('
			<!-- '.$comment.' TEXT INPUT Field -->
			<div class="control-group">
				<label class="control-label">
					'.$lang['cart_shipping_state'].'
				</label>
				<div class="controls">');
echo(get_prov_states_options($name, $placeholder, $value, $required, $onchange));
echo('
					<div><small>'.$lang['cart_all_orders_taxed_text'].'</small></div>
					<p class="help-block"></p>
				</div>
			</div>
');
?>