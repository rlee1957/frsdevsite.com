<?php
echo('
			<!-- '.$comment.' TEXT INPUT Field -->
			<div class="control-group">
				<label class="control-label">
					'.$label.'
				</label>
				<div class="controls">
					<input type="email"'.$name.$required.$placeholder.$value.$onchange.$onblur.'
						   maxlength="255" />
					<div><small>'.$lang['cart_shipping_email_note'].'</small></div>
					<p class="help-block"></p>
				</div>
			</div>
');

?>