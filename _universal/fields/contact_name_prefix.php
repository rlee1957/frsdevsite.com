<?php

echo('
			<!-- '.$comment.' TEXT INPUT Field -->
			<div class="control-group">
				<label class="control-label">
					'.$label.'
				</label>
				<div class="controls">
					<select class="selectpicker show-tick show-menu-arrow"'.$name.'>
');
$prefix = explode("|",$lang['cart_shipping_prefix_data']);
sort($prefix);
foreach ($prefix as $val)
	{
	if ($value == $val)
		{
		echo '
						<option selected="selected" value="'.$val.'"> '.$val.'</option>';
		}
	else
		{
		echo '
						<option value="'.$val.'"> '.$val.'</option>';
		}
	}
echo '
					</select>
					<p class="help-block"></p>
				</div>
			</div>
';

?>