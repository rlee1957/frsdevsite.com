<!DOCTYPE html>
<html>
<head>
</head>
<body>
<div id="config_language">
<table>
	<tr>
		<td colspan=3 align=center><h2>Promo Variables</h2></td>
	</tr>
	<tr>
		<td align=center><h3>Name</h3></td>
		<td align=center><h3>Value</h3></td>
		<td align=center><h3>Example</h3></td>
	</tr>
    <tr>
        <td>published_language</td>
        <td>
            <input type=text style="width: 100%;"
                    id="published_language"
                   name="published_language"
                   value="en-us" />
        </td>
        <td>en-us</td>
    </tr>
</table>
</div>
<div id="config_promo">
<table>
	<tr>
		<td colspan=3 align=center><h2>Promo Variables</h2></td>
	</tr>
	<tr>
		<td style="width: 33%;" align=center><h3>Name</h3></td>
		<td style="width: 33%;" align=center><h3>Value</h3></td>
		<td style="width: 33%;" align=center><h3>Example</h3></td>
	</tr>
	<tr>
		<td>promo_prompt</td>
		<td>	
			<input type=text style="width: 100%;"
                    id="promo_prompt" 
			       name="promo_prompt" 
				   value="I have a Promotion Code:" />
		</td>
		<td>I have a Promotion Code:</td>
	</tr>
    <tr>
        <td>promo_yes</td>
        <td>
            <input type=text style="width: 100%;"
                    id="promo_yes"
                   name="promo_yes"
                   value="Yes" />
        </td>        <td>Yes</td>
    </tr>
    <tr>
        <td>promo_no</td>
        <td>
            <input type=text style="width: 100%;"
                    id="promo_no"
                   name="promo_no"
                   value="No" />
        </td>        <td>No</td>
    </tr>
    <tr>
        <td>promo_submit</td>
        <td>
            <input type=text style="width: 100%;"
                    id="promo_submit"
                   name="promo_submit"
                   value="Submit Code" />
        </td>
        <td>Submit Code</td>
    </tr>
    <tr>
        <td>promo_instruction</td>
        <td>
            <input type=text style="width: 100%;"
                    id="promo_instruction"
                   name="promo_instruction"
                   value="&nbsp;&nbsp;Please enter your promotion code." />
        </td>
        <td>&nbsp;&nbsp;Please enter your promotion code.</td>
    </tr>
    <tr>
        <td>promo_fail</td>
        <td>
            <input type=text style="width: 100%;"
                    id="promo_fail"
                   name="promo_fail"
                   value="&nbsp;&nbsp;Invalid code, please try again or continue without code.&nbsp;&nbsp;" />
        </td>
        <td>&nbsp;&nbsp;Invalid code, please try again or continue without code.&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td>promo_invalid</td>
        <td>
            <input type=text style="width: 100%;"
                    id="promo_invalid"
                   name="promo_invalid"
                   value="&nbsp;&nbsp;INVALID&nbsp;CODE,&nbsp;PLEASE&nbsp;TRY&nbsp;AGAIN&nbsp;OR&nbsp;CONTINUE&nbsp;WITHOUT&nbsp;CODE&nbsp;&nbsp;" />
        </td>
        <td>&nbsp;&nbsp;INVALID&nbsp;CODE,&nbsp;PLEASE&nbsp;TRY&nbsp;AGAIN&nbsp;OR&nbsp;CONTINUE&nbsp;WITHOUT&nbsp;CODE&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td>promo_valid</td>
        <td>
            <input type=text style="width: 100%;"
                    id="promo_valid"
                   name="promo_valid"
                   value="&nbsp;&nbsp;PROMOTIONAL&nbsp;CODE&nbsp;VALIDATED,&nbsp;PLEASE&nbsp;CONTINUE&nbsp;&nbsp;" />
        </td>
        <td>&nbsp;&nbsp;PROMOTIONAL&nbsp;CODE&nbsp;VALIDATED,&nbsp;PLEASE&nbsp;CONTINUE&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td>promo_web_title</td>
        <td>
            <input type=text style="width: 100%;"
                    id="promo_web_title"
                   name="promo_web_title"
                   value="Air Canada Center" />
        </td>
        <td>Air Canada Center</td>
    </tr>
    <tr>
        <td>published_campaign_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="published_campaign_name"
                   name="published_campaign_name"
                   value="Air Canada Centre Legacy Plaza" />
        </td>
        <td>Air Canada Centre Legacy Plaza</td>
    </tr>
    <tr>
        <td>published_campaign_name_ab</td>
        <td>
            <input type=text style="width: 100%;"
                    id="published_campaign_name_ab"
                   name="published_campaign_name_ab"
                   value="ACCBRICKS" />
        </td>
        <td>ACCBRICKS</td>
    </tr>
    <tr>
        <td>published_hours_of_operation</td>
        <td>
            <input type=text style="width: 100%;"
                    id="published_hours_of_operation"
                   name="published_hours_of_operation"
                   value="Monday - Friday, 7:00am - 7:00pm EST" />
        </td>
        <td>Monday - Friday, 7:00am - 7:00pm EST</td>
    </tr>
    <tr>
        <td>published_currency_symbol</td>
        <td>
            <input type=text style="width: 100%;"
                    id="published_currency_symbol"
                   name="published_currency_symbol"
                   value="USD" />
        </td>
        <td>USD</td>
    </tr> 
    <tr>
        <td>published_web_fee</td>
		<td><input id="published_web_fee" name="published_web_fee" value="5.00" />
        <td>5.00</td>
    </tr>
    <tr>
        <td>set_fb_message</td>
        <td>
            <input type=text style="width: 100%;"
                    id="set_fb_message"
                   name="set_fb_message"
                   value="Create your Legacy!" />
        </td>
        <td>Create your Legacy!</td>
    </tr>
    <tr>
        <td>set_fb_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="set_fb_name"
                   name="set_fb_name"
                   value="Air Canada Center" />
        </td>
        <td>Air Canada Center</td>
    </tr>
    <tr>
        <td>set_fb_caption</td>
        <td>
            <input type=text style="width: 100%;"
                    id="set_fb_caption"
                   name="set_fb_caption"
                   value="keeping it between the quotes fb_caption" />
        </td>
        <td>keeping it between the quotes fb_caption</td>
    </tr>
    <tr>
        <td>set_fb_settings_key</td>
		<td>
			<input type=text style="width: 100%;"
                    id="set_fb_settings_key"
                   name="set_fb_settings_key"
                   value="" />
		</td>
		<td></td>
	</tr>
	<tr>
        <td>set_fb_message</td>
		<td>
			<input type=text style="width: 100%;"
                    id="set_fb_message"
                   name="set_fb_message"
                   value="" />
		</td>
		<td></td>
	</tr>
	<tr>
        <td>set_fb_link</td>
		<td>
			<input type=text style="width: 100%;"
                    id="set_fb_link"
                   name="set_fb_link"
                   value="" />
		</td>
		<td></td>
	</tr>
	<tr>
        <td>set_fb_name</td>
		<td>
			<input type=text style="width: 100%;"
                    id="set_fb_name"
                   name="set_fb_name"
                   value="" />
		</td>
		<td></td>
	</tr>
	<tr>
        <td>set_fb_caption</td>
		<td>
			<input type=text style="width: 100%;"
                    id="set_fb_caption"
                   name="set_fb_caption"
                   value="" />
		</td>
		<td></td>
	</tr>
	<tr>
        <td>set_fb_settings_path</td>
		<td>
			<input type=text style="width: 100%;"
                    id="set_fb_settings_path"
                   name="set_fb_settings_path"
                   value="" />
		</td>
		<td></td>
	</tr>
	<tr>
        <td>set_fb_settings_image3</td>
		<td>
			<input type=text style="width: 100%;"
                    id="set_fb_settings_image3"
                   name="set_fb_settings_image3"
                   value="" />
		</td>
		<td></td>
	</tr>
    <tr>
        <td>facebook_description</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="facebook_description"
                   name="facebook_description">
I just sponsored a brick and enshrined my name. Check it out!
			</textarea>
        </td>
        <td>I just sponsored a brick and enshrined my name. Check it out!</td>
    </tr>
    <tr>
        <td>facebook_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="facebook_name"
                   name="facebook_name"
                   value="Air Canada Center" />
        </td>
        <td>Air Canada Center</td>
    </tr>
    <tr>
        <td>facebook_caption</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="facebook_caption"
                   name="facebook_caption">
Create your own lasting legacy! Click the link above or call to learn more.			
			</textarea>
        </td>
        <td>Create your own lasting legacy! Click the link above or call to learn more.</td>
    </tr>
    <tr>
        <td>home</td>
        <td>
            <input type=text style="width: 100%;"
                    id="home"
                   name="home"
                   value="Home Page" />
        </td>
        <td>Home Page</td>
    </tr>
    <tr>
        <td>brick_image_path_alt</td>
        <td>
            <input type=text style="width: 100%;"
                    id="brick_image_path_alt"
                   name="brick_image_path_alt"
                   value="View Bricks" />
        </td>
        <td>View Bricks</td>
    </tr>
    <tr>
        <td>order_form</td>
        <td>
            <input type=text style="width: 100%;"
                    id="order_form"
                   name="order_form"
                   value="OPTIONS" />
        </td>
        <td>OPTIONS</td>
    </tr>
    <tr>
        <td>make_your_selection</td>
        <td>
            <input type=text style="width: 100%;"
                    id="make_your_selection"
                   name="make_your_selection"
                   value="" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td>add_your_brick</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="add_your_brick"
                   name="add_your_brick">
<span style='font-style:italic; font-weight:normal'>Select an option below by highlighting it, then click continue</span>
			</textarea>
        </td>
        <td><span style='font-style:italic; font-weight:normal'>Select an option below by highlighting it, then click continue</span></td>
    </tr>
    <tr>
        <td>add_your_brick_additional</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="add_your_brick_additional"
                   name="add_your_brick_additional">
PERSONALIZE ANOTHER BRICK; OTHERWISE PROCEED TO "CHECK-OUT"		   
			</textarea>
        </td>
        <td>PERSONALIZE ANOTHER BRICK; OTHERWISE PROCEED TO "CHECK-OUT"</td>
    </tr>
    <tr>
        <td>customize_options</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="customize_options"
                   name="customize_options">
<div style=\"text-align:left; font-style:italic; font-weight:normal\">Enter your personal message below in the \"Personal Message\" section or check the \"Gift Certificate\" box if you would like to have this selection processed as a gift certificate.</div>	   
			</textarea>
        </td>
        <td><div style=\"text-align:left; font-style:italic; font-weight:normal\">Enter your personal message below in the \"Personal Message\" section or check the \"Gift Certificate\" box if you would like to have this selection processed as a gift certificate.</div></td>
    </tr>
    <tr>
        <td>selected_customize_options</td>
        <td>
            <input type=text style="width: 100%;"
                    id="selected_customize_options"
                   name="selected_customize_options"
                   value="SELECTED BRICK OPTION:" />
        </td>
        <td>SELECTED BRICK OPTION:</td>
    </tr>
    <tr>
        <td>add_your_brick_placement</td>
        <td>
            <input type=text style="width: 100%;"
                    id="add_your_brick_placement"
                   name="add_your_brick_placement"
                   value="" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td>add_your_replica</td>
        <td>
            <input type=text style="width: 100%;"
                    id="add_your_replica"
                   name="add_your_replica"
                   value="ADD ADDITIONAL REPLICA(S):" />
        </td>
        <td>ADD ADDITIONAL REPLICA(S):</td>
    </tr>
    <tr>
        <td>selected_replica</td>
        <td>
            <input type=text style="width: 100%;"
                    id="selected_replica"
                   name="selected_replica"
                   value="SELECTED REPLICA(S):" />
        </td>
        <td>SELECTED REPLICA(S):</td>
    </tr>
    <tr>
        <td>add_your_dc</td>
        <td>
            <input type=text style="width: 100%;"
                    id="add_your_dc"
                   name="add_your_dc"
                   value="ADD DISPLAY CASE(S):" />
        </td>
        <td>ADD DISPLAY CASE(S):</td>
    </tr>
    <tr>
        <td>selected_dc</td>
        <td>
            <input type=text style="width: 100%;"
                    id="selected_dc"
                   name="selected_dc"
                   value="SELECTED DISPLAY CASE(S):" />
        </td>
        <td>SELECTED DISPLAY CASE(S):</td>
    </tr>
    <tr>
        <td>add_your_gc</td>
        <td>
            <input type=text style="width: 100%;"
                    id="add_your_gc"
                   name="add_your_gc"
                   value="SELECT A GIFT CERTIFICATE" />
        </td>
        <td>SELECT A GIFT CERTIFICATE</td>
    </tr>
    <tr>
        <td>selected_location</td>
        <td>
            <input type=text style="width: 100%;"
                    id="selected_location"
                   name="selected_location"
                   value="" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td>add_to_cart</td>
        <td>
            <input type=text style="width: 100%;"
                    id="add_to_cart"
                   name="add_to_cart"
                   value="Personalize another brick" />
        </td>
        <td>Personalize another brick</td>
    </tr>
    <tr>
        <td>splash_announcement_1</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="splash_announcement_1"
                   name="splash_announcement_1">
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>splash_announcement_2</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="splash_announcement_2"
                   name="splash_announcement_2">
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>splash_announcement_3</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="splash_announcement_3"
                   name="splash_announcement_3">
<p>
All pavers and keepsake items can be converted into a gift certificate on the next page for gift giving or to secure your place in history now while allowing additional time to personalize your paver later.
</p>
<p>
If this is an international order or if you have already personalized a paver and wish to add another keepsake replica and/or replica display case to your order please call.
</p>
<p>
Not sure what to put on your paver? 
 
<a href='http://www.imagineyourbrick.com/paver/design/accbricks/' target='_blank'>
<span style='font-weight:bold; font-style:italic; font-size: 10pt; color:#fec04c;'>DESIGN YOUR BRICK HERE</a>

</span>&nbsp; first on a virtual paver and view other inscription examples to nudge your creativity!
</p>
			</textarea>
        </td>
        <td>
<p>
All pavers and keepsake items can be converted into a gift certificate on the next page for gift giving or to secure your place in history now while allowing additional time to personalize your paver later.
</p>
<p>
If this is an international order or if you have already personalized a paver and wish to add another keepsake replica and/or replica display case to your order please call.
</p>
<p>
Not sure what to put on your paver? 
 
<a href='http://www.imagineyourbrick.com/paver/design/accbricks/' target='_blank'>
<span style='font-weight:bold; font-style:italic; font-size: 10pt; color:#fec04c;'>DESIGN YOUR BRICK HERE</a>

</span>&nbsp; first on a virtual paver and view other inscription examples to nudge your creativity!
</p></td>
    </tr>
    <tr>
        <td>your_shopping_cart</td>
        <td>
            <input type=text style="width: 100%;"
                    id="your_shopping_cart"
                   name="your_shopping_cart"
                   value="Your Shopping Cart" />
        </td>
        <td>Your Shopping Cart</td>
    </tr>
    <tr>
        <td>empty_cart</td>
        <td>
            <input type=text style="width: 100%;"
                    id="empty_cart"
                   name="empty_cart"
                   value="* * * EMPTY &mdash; No Bricks and/or Tiles ordered * * *" />
        </td>
        <td>* * * EMPTY &mdash; No Bricks and/or Tiles ordered * * *</td>
    </tr>
    <tr>
        <td>cart_quantity_header</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_quantity_header"
                   name="cart_quantity_header"
                   value="Cart Quantity" />
        </td>
        <td>Cart Quantity</td>
    </tr>
    <tr>
        <td>cart_quantity</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_quantity"
                   name="cart_quantity"
                   value="Quantity" />
        </td>
        <td>Quantity</td>
    </tr>
    <tr>
        <td>cart_quantity_</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_quantity_"
                   name="cart_quantity_"
                   value="Quantity" />
        </td>
        <td>Quantity</td>
    </tr>
    <tr>
        <td>cart_description</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_description"
                   name="cart_description"
                   value="Description" />
        </td>
        <td>Description</td>
    </tr>
    <tr>
        <td>cart_price_each</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_price_each"
                   name="cart_price_each"
                   value="Price ea." />
        </td>
        <td>Price ea.</td>
    </tr>
    <tr>
        <td>cart_total</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_total"
                   name="cart_total"
                   value="Total" />
        </td>
        <td>Total</td>
    </tr>
    <tr>
        <td>cart_subtotal</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_subtotal"
                   name="cart_subtotal"
                   value="Subtotal" />
        </td>
        <td>Subtotal</td>
    </tr>
    <tr>
        <td>footer_note</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="footer_note"
                   name="footer_note">
Note: If you are experiencing difficulties with this page, please check your Browser settings and verify that your Cookies setting is enabled or contact us at	   
			</textarea>
        </td>
        <td>Note: If you are experiencing difficulties with this page, please check your Browser settings and verify that your Cookies setting is enabled or contact us at</td>
    </tr>
    <tr>
        <td>cart_bo_header</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_bo_header"
                   name="cart_bo_header">
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_bo_header_confirm</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_bo_header_confirm"
                   name="cart_bo_header_confirm"
                   value="ORDER CONFIRMATION" />
        </td>
        <td>ORDER CONFIRMATION</td>
    </tr>
    <tr>
        <td>cart_bo_</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_bo_"
                   name="cart_bo_"
                   value="Bricks" />
        </td>
        <td>Bricks</td>
    </tr>
    <tr>
        <td>cart_bo__type</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_bo__type"
                   name="cart_bo__type"
                   value="Type" />
        </td>
        <td>Type</td>
    </tr>
    <tr>
        <td>cart_bo_logo_options</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_bo_logo_options"
                   name="cart_bo_logo_options"
                   value="" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_bo_logo_options_selected</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_bo_logo_options_selected"
                   name="cart_bo_logo_options_selected"
                   value="" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_paver</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_paver"
                   name="cart_paver"
                   value="Brick" />
        </td>
        <td>Brick</td>
    </tr>
    <tr>
        <td>cart_replica</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_replica"
                   name="cart_replica"
                   value="Replica" />
        </td>
        <td>Replica</td>
    </tr>
    <tr>
        <td>cart_display_case</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_display_case"
                   name="cart_display_case"
                   value="Display Case" />
        </td>
        <td>Display Case</td>
    </tr>
    <tr>
        <td>cart_display_case2</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_display_case2"
                   name="cart_display_case2"
                   value="Display Case" />
        </td>
        <td>Display Case</td>
    </tr>
    <tr>
        <td>cart_gift_certificate_note</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_gift_certificate_note"
                   name="cart_gift_certificate_note">
If you would like to customize your gift certificate with other options not listed, please call us.	   
			</textarea>
        </td>
        <td>If you would like to customize your gift certificate with other options not listed, please call us.</td>
	</tr>
    <tr>
        <td>cart_note1</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_note1"
                   name="cart_note1">
Note: <span style='font-style:italic; font-weight:normal'>If you are ordering multiple pavers and/or gift certificates, you will have the opportunity to add them on a later screen prior to final check-out.  Replicas are made of the same material and inscribed with the same personal message as the installed pavers. Colors may vary slightly.				   
			</textarea>
        </td>
        <td>Note: <span style='font-style:italic; font-weight:normal'>If you are ordering multiple pavers and/or gift certificates, you will have the opportunity to add them on a later screen prior to final check-out.  Replicas are made of the same material and inscribed with the same personal message as the installed pavers. Colors may vary slightly.</td>
    </tr>
    <tr>
        <td>cart_note2</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_note2"
                   name="cart_note2">	   
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_note3</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_note3"
                   name="cart_note3">
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_replica_note</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_replica_note"
                   name="cart_replica_note">
If you already purchased a paver and would like to purchase additional replicas and/or replica display cases, please call.
			</textarea>
        </td>
        <td>If you already purchased a paver and would like to purchase additional replicas and/or replica display cases, please call.</td>
    </tr>
    <tr>
        <td>cart_gift_certificate_option_header</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_gift_certificate_option_header"
                   name="cart_gift_certificate_option_header"
                   value="GIFT CERTIFICATE:" />
        </td>
        <td>GIFT CERTIFICATE:</td>
    </tr>
    <tr>
        <td>cart_gift_certificate_option_text</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_gift_certificate_option_text"
                   name="cart_gift_certificate_option_text">
Click here if you would like your selections above processed as a gift certificate. Gift certificates secure placement and do not require an inscription to be entered at this time.
			</textarea>
        </td>
        <td>Click here if you would like your selections above processed as a gift certificate. Gift certificates secure placement and do not require an inscription to be entered at this time.</td>
    </tr>
    <tr>
        <td>cart_gift_certificate_option_text2</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_gift_certificate_option_text2"
                   name="cart_gift_certificate_option_text2">
A gift certificate will be processed and sent for the item(s) selected above for the recipient to redeem at a later date.
			</textarea>
        </td>
        <td>A gift certificate will be processed and sent for the item(s) selected above for the recipient to redeem at a later date.</td>
    </tr>
    <tr>
        <td>cart_inscription_header</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_inscription_header"
                   name="cart_inscription_header"
                   value="PERSONAL BRICK MESSAGE:" />
        </td>
        <td>PERSONAL BRICK MESSAGE:</td>
    </tr>
    <tr>
        <td>cart_inscription_guide_title</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_inscription_guide_title"
                   name="cart_inscription_guide_title">
<span style='text-decoration: underline; font-size: 12pt;'>Inscription Guidelines:</span> &nbsp; Not sure what to put on your paver? &nbsp;<a href=\"http://www.imagineyourbrick.com/paver/design/accbricks\" target=\"_blank\"><span style='font-weight:bold; font-style:italic; font-size: 10pt; color:#fec04c;'>PERSONALIZE YOUR MESSAGE HERE</a></span>&nbsp; first on a virtual paver and see other examples provided.				   
			</textarea>
        </td>
        <td><span style='text-decoration: underline; font-size: 12pt;'>Inscription Guidelines:</span> &nbsp; Not sure what to put on your paver? &nbsp;<a href=\"http://www.imagineyourbrick.com/paver/design/accbricks\" target=\"_blank\"><span style='font-weight:bold; font-style:italic; font-size: 10pt; color:#fec04c;'>PERSONALIZE YOUR MESSAGE HERE</a></span>&nbsp; first on a virtual paver and see other examples provided.</td>
    </tr>
    <tr>
        <td>cart_inscription_guide_body</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_inscription_guide_body"
                   name="cart_inscription_guide_body">
characters per line of text can be engraved on a paver. A character consists of letters, numbers, punctuation marks and blank spaces entered using a standard keyboard. Inscriptions will be automatically centered and engraved in UPPER CASE letters. Inscriptions that contain websites, phone numbers, advertisements and/or manifestos are not acceptable. Discriminatory, political, offensive or inappropriate messages as determined will be declined.
			</textarea>
        </td>
        <td>characters per line of text can be engraved on a paver. A character consists of letters, numbers, punctuation marks and blank spaces entered using a standard keyboard. Inscriptions will be automatically centered and engraved in UPPER CASE letters. Inscriptions that contain websites, phone numbers, advertisements and/or manifestos are not acceptable. Discriminatory, political, offensive or inappropriate messages as determined will be declined.</td>
    </tr>
    <tr>
        <td>cart_inscription_as_follows</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_inscription_as_follows"
                   name="cart_inscription_as_follows">
Enter message to be engraved on your paver here:				   
			</textarea>
        </td>
        <td>Enter message to be engraved on your paver here:</td>
    </tr>
    <tr>
        <td>cart_inscription_line</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_inscription_line"
                   name="cart_inscription_line"
                   value="Line" />
        </td>
        <td>Line</td>
    </tr>
    <tr>
        <td>cart_inscription_footer</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_inscription_footer"
                   name="cart_inscription_footer">
We reserve the right to approve all inscriptions. Inscriptions that do not conform to the "Inscription Guidelines" or that are deemed unsuitable will be rejected and will require a new inscription to be submitted. Additionally, we cannot guarantee that multiple orders will be installed together.				   
			</textarea>
        </td>
        <td>We reserve the right to approve all inscriptions. Inscriptions that do not conform to the "Inscription Guidelines" or that are deemed unsuitable will be rejected and will require a new inscription to be submitted. Additionally, we cannot guarantee that multiple orders will be installed together.</td>
    </tr>
    <tr>
        <td>cart_location_footer</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_location_footer"
                   name="cart_location_footer">
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_continue</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_continue"
                   name="cart_continue"
                   value="Continue" />
        </td>
        <td>Continue</td>
    </tr>
    <tr>
        <td>cart_cancel</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_cancel"
                   name="cart_cancel"
                   value="Cancel" />
        </td>
        <td>Cancel</td>
    </tr>
    <tr>
        <td>cart_edit</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_edit"
                   name="cart_edit"
                   value="Edit" />
        </td>
        <td>Edit</td>
    </tr>
    <tr>
        <td>cart_save</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_save"
                   name="cart_save"
                   value="Save" />
        </td>
        <td>Save</td>
    </tr>
    <tr>
        <td>cart_delete</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_delete"
                   name="cart_delete"
                   value="Delete" />
        </td>
        <td>Delete</td>
    </tr>
    <tr>
        <td>cart_shipping_</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_"
                   name="cart_shipping_"
                   value="Shipping Address" />
        </td>
        <td>Shipping Address</td>
    </tr>
    <tr>
        <td>cart_shipping_addr_btn</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_addr_btn"
                   name="cart_shipping_addr_btn"
                   value="Use First Shipping Address" />
        </td>
        <td>Use First Shipping Address</td>
    </tr>
    <tr>
        <td>cart_shipping_cancel_btn</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_cancel_btn"
                   name="cart_shipping_cancel_btn"
                   value="Clear Form" />
        </td>
        <td>Clear Form</td>
    </tr>
    <tr>
        <td>cart_shipping_text1</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_text1"
                   name="cart_shipping_text1">
A street address is required to send all keepsake items (replicas, display cases and certificates).				   
			</textarea>
        </td>
        <td>A street address is required to send all keepsake items (replicas, display cases and certificates).</td>
    </tr>
    <tr>
        <td>cart_shipping_text2</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_text2"
                   name="cart_shipping_text2">
<span style='font-style:italic; color:#fec04c;'>P.O. and A.P.O. boxes cannot be accepted for shipment tracking purposes.</span>				   
			</textarea>
        </td>
        <td><span style='font-style:italic; color:#fec04c;'>P.O. and A.P.O. boxes cannot be accepted for shipment tracking purposes.</span></td>
    </tr>
    <tr>
        <td>cart_shipping_text3</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_text3"
                   name="cart_shipping_text3">
Shipping is included within the contiguous U.S. (excludes HI and AK).				   
			</textarea>
        </td>
        <td>Shipping is included within the contiguous U.S. (excludes HI and AK).</td>
    </tr>
    <tr>
        <td>cart_shipping_text4</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_text4"
                   name="cart_shipping_text4">
For international orders please call.			
			</textarea>
        </td>
        <td>For international orders please call.</td>
    </tr>
    <tr>
        <td>cart_shipping_announcment</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_announcment"
                   name="cart_shipping_announcment">
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_shipping_method</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_method"
                   name="cart_shipping_method"
                   value="Shipping Method" />
        </td>
        <td>Shipping Method</td>
    </tr>
    <tr>
        <td>cart_shipping_method_option1</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_method_option1"
                   name="cart_shipping_method_option1">
Yes, I am interested in receiving my replica in time for Christmas!  Please contact me regarding the applicable rush fees.				   
			</textarea>
        </td>
        <td>Yes, I am interested in receiving my replica in time for Christmas!  Please contact me regarding the applicable rush fees.</td>
    </tr>
    <tr>
        <td>cart_shipping_method_option2</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_method_option2"
                   name="cart_shipping_method_option2">
No, I am not interested in paying rush fees to receive my replica in time for Christmas.				   
			</textarea>
        </td>
        <td>No, I am not interested in paying rush fees to receive my replica in time for Christmas.</td>
    </tr>
    <tr>
        <td>cart_shipping_text1</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_text1"
                   name="cart_shipping_text1">
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_shipping_text2</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_text2"
                   name="cart_shipping_text2">
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_shipping_text3</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_text3"
                   name="cart_shipping_text3">
A street address is required to send all keepsake items (replicas, display cases and certificates). <span style='font-style:italic; color:#fec04c;'>P.O. and A.P.O. boxes cannot be accepted for shipment tracking purposes.</span>
			</textarea>
        </td>
        <td>A street address is required to send all keepsake items (replicas, display cases and certificates). <span style='font-style:italic; color:#fec04c;'>P.O. and A.P.O. boxes cannot be accepted for shipment tracking purposes.</span></td>
    </tr>
    <tr>
        <td>cart_shipping_text4</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_text4"
                   name="cart_shipping_text4">
<span style='font-style:italic; text-align:left; font-weight:normal'>Shipping is included within the contiguous U.S. (excludes HI and AK). For international orders please call.</span>
			</textarea>
        </td>
        <td><span style='font-style:italic; text-align:left; font-weight:normal'>Shipping is included within the contiguous U.S. (excludes HI and AK). For international orders please call.</span></td>
    </tr>
    <tr>
        <td>cart_shipping_prefix_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_prefix_name"
                   name="cart_shipping_prefix_name"
                   value="Prefix" />
        </td>
        <td>Prefix</td>
    </tr>
    <tr>
        <td>cart_shipping_prefix_data</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_prefix_data"
                   name="cart_shipping_prefix_data"
                   value=" |Mr|Mrs|Ms" />
        </td>
        <td> |Mr|Mrs|Ms</td>
    </tr>
    <tr>
        <td>cart_shipping_first_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_first_name"
                   name="cart_shipping_first_name"
                   value="First Name" />
        </td>
        <td>First Name</td>
    </tr>
    <tr>
        <td>cart_shipping_first_name_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_first_name_placeholder"
                   name="cart_shipping_first_name_placeholder"
                   value="Enter Your First Name" />
        </td>
        <td>Enter Your First Name</td>
    </tr>
    <tr>
        <td>cart_shipping_middle_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_middle_name"
                   name="cart_shipping_middle_name"
                   value="Middle Name" />
        </td>
        <td>Middle Name</td>
    </tr>
    <tr>
        <td>cart_shipping_middle_name_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_middle_name_placeholder"
                   name="cart_shipping_middle_name_placeholder"
                   value="Enter Your Middle Name" />
        </td>
        <td>Enter Your Middle Name</td>
    </tr>
    <tr>
        <td>cart_shipping_last_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_last_name"
                   name="cart_shipping_last_name"
                   value="Last Name" />
        </td>
        <td>Last Name</td>
    </tr>
    <tr>
        <td>cart_shipping_last_name_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_last_name_placeholder"
                   name="cart_shipping_last_name_placeholder"
                   value="Enter Your Last Name" />
        </td>
        <td>Enter Your Last Name</td>
    </tr>
    <tr>
        <td>cart_shipping_company</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_company"
                   name="cart_shipping_company"
                   value="Company" />
        </td>
        <td>Company</td>
    </tr>
    <tr>
        <td>cart_shipping_company_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_company_placeholder"
                   name="cart_shipping_company_placeholder"
                   value="Enter Your Organization Info" />
        </td>
        <td>Enter Your Organization Info</td>
    </tr>
    <tr>
        <td>cart_shipping_address</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_address"
                   name="cart_shipping_address"
                   value="Address" />
        </td>
        <td>Address</td>
    </tr>
    <tr>
        <td>cart_shipping_address_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_address_placeholder"
                   name="cart_shipping_address_placeholder"
                   value="Enter Your Address" />
        </td>
        <td>Enter Your Address</td>
    </tr>
    <tr>
        <td>cart_shipping_address2_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_address2_placeholder"
                   name="cart_shipping_address2_placeholder"
                   value="Additional Address Info" />
        </td>
        <td>Additional Address Info</td>
    </tr>
    <tr>
        <td>cart_shipping_city</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_city"
                   name="cart_shipping_city"
                   value="City" />
        </td>
        <td>City</td>
    </tr>
    <tr>
        <td>cart_shipping_city_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_city_placeholder"
                   name="cart_shipping_city_placeholder"
                   value="Enter Your City" />
        </td>
        <td>Enter Your City</td>
    </tr>
    <tr>
        <td>cart_shipping_state</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_state"
                   name="cart_shipping_state"
                   value="Province/State" />
        </td>
        <td>Province/State</td>
    </tr>
    <tr>
        <td>cart_shipping_state_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_state_placeholder"
                   name="cart_shipping_state_placeholder"
                   value="Enter Your Province/State" />
        </td>
        <td>Enter Your Province/State</td>
    </tr>
    <tr>
        <td>cart_shipping_zip</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_zip"
                   name="cart_shipping_zip"
                   value="Postal Code" />
        </td>
        <td>Postal Code</td>
    </tr>
    <tr>
        <td>cart_shipping_zip_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_zip_placeholder"
                   name="cart_shipping_zip_placeholder"
                   value="Enter Your Postal Code" />
        </td>
        <td>Enter Your Postal Code</td>
    </tr>
    <tr>
        <td>cart_shipping_country</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_country"
                   name="cart_shipping_country"
                   value="Country" />
        </td>
        <td>Country</td>
    </tr>
    <tr>
        <td>cart_shipping_country_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_country_placeholder"
                   name="cart_shipping_country_placeholder"
                   value="Enter Your Country" />
        </td>
        <td>Enter Your Country</td>
    </tr>
    <tr>
        <td>cart_shipping_daytime_phone</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_daytime_phone"
                   name="cart_shipping_daytime_phone"
                   value="Phone" />
        </td>
        <td>Phone</td>
    </tr>
    <tr>
        <td>cart_shipping_daytime_phone_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_daytime_phone_placeholder"
                   name="cart_shipping_daytime_phone_placeholder"
                   value="Enter Your Phone" />
        </td>
        <td>Enter Your Phone</td>
    </tr>
    <tr>
        <td>cart_shipping_email</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_email"
                   name="cart_shipping_email"
                   value="Email" />
        </td>
        <td>Email</td>
    </tr>
    <tr>
        <td>cart_shipping_email_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_email_placeholder"
                   name="cart_shipping_email_placeholder"
                   value="Enter Your Email" />
        </td>
        <td>Enter Your Email</td>
    </tr>
    <tr>
        <td>cart_shipping_email_note</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_email_note"
                   name="cart_shipping_email_note">
All correspondence regarding your order will be sent to this email address				   
			</textarea>
        </td>
        <td>All correspondence regarding your order will be sent to this email address</td>
    </tr>

    <tr>
        <td>cart_order_confirmation_</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_order_confirmation_"
                   name="cart_order_confirmation_"
                   value="Order Confirmation" />
        </td>
        <td>Order Confirmation</td>
    </tr>
    <tr>
        <td>cart_order_confirmation_note</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_order_confirmation_note"
                   name="cart_order_confirmation_note">
**** Inscriptions will be automatically centered and engraved in UPPERCASE ****   
			</textarea>
        </td>
        <td>**** Inscriptions will be automatically centered and engraved in UPPERCASE ****</td>
    </tr>
    <tr>
        <td>cart_order_edit_paver_quantity</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_order_edit_paver_quantity"
                   name="cart_order_edit_paver_quantity"
                   value="Order Confirmation" />
        </td>
        <td>Brick Quantity</td>
    <tr>
        <td>cart_order_edit_replica_quantity</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_order_edit_replica_quantity"
                   name="cart_order_edit_replica_quantity"
                   value="Replica Quantity" />
        </td>
        <td>Replica Quantity</td>
    <tr>
        <td>cart_order_edit_display_case_quantity</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_order_edit_display_case_quantity"
                   name="cart_order_edit_display_case_quantity"
                   value="Display Case Quantity" />
        </td>
        <td>Display Case Quantity</td>
    <tr>
        <td>cart_order_edit_display_case2_quantity</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_order_edit_display_case2_quantity"
                   name="cart_order_edit_display_case2_quantity"
                   value="Display Case Quantity" />
        </td>
        <td>Display Case Quantity</td>
    <tr>
        <td>cart_shipping_replica_shipping</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_replica_shipping"
                   name="cart_shipping_replica_shipping"
                   value="Replica Shipping" />
        </td>
        <td>Replica Shipping</td>
    <tr>
        <td>cart_shipping_replica_total</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_shipping_replica_total"
                   name="cart_shipping_replica_total"
                   value="Total number of replicas ordered" />
        </td>
        <td>Total number of replicas ordered</td>
    </tr>
    <tr>
        <td>cart_add_additional</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_add_additional"
                   name="cart_add_additional"
                   value="Add Additional Items to my Shopping Cart" />
        </td>
        <td>Add Additional Items to my Shopping Cart</td>
    </tr>
    <tr>
        <td>cart_shipping_header</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_header"
                   name="cart_shipping_header">
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_shipping_cert_note</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_shipping_cert_note"
                   name="cart_shipping_cert_note">
			</textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_order_paver</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_order_paver"
                   name="cart_order_paver"
                   value="Order Brick" />
        </td>
        <td>Order Brick</td>
    <tr>
        <td>cart_check_out</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_check_out"
                   name="cart_check_out"
                   value="Check Out" />
        </td>
        <td>Check Out</td>
    </tr>
    <tr>
        <td>cart_tax</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_tax"
                   name="cart_tax"
                   value="Tax" />
        </td>
        <td>Tax</td>
    </tr>
    <tr>
        <td>cart_replica_shipping</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_replica_shipping"
                   name="cart_replica_shipping"
                   value="Replica Shipping" />
        </td>
        <td>Replica Shipping</td>
    </tr>
    <tr>
        <td>cart_replica_display_case_shipping</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_replica_display_case_shipping"
                   name="cart_replica_display_case_shipping"
                   value="Replica Display Case Shipping" />
        </td>
        <td>Replica Display Case Shipping</td>
    </tr>
    <tr>
        <td>cart_handling_fee</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_handling_fee"
                   name="cart_handling_fee"
                   value="Web Convenience Fee" />
        </td>
        <td>Web Convenience Fee</td>
    </tr>
    <tr>
        <td>cart_all_orders_taxed_text</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_all_orders_taxed_text"
                   name="cart_all_orders_taxed_text"
                   value="cart_all_orders_taxed_text" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td>cart_all_orders_taxed</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_all_orders_taxed"
                   name="cart_all_orders_taxed">
A $5.00 non-refundable web convenience fee applies to online purchases.			
			</textarea>
        </td>
        <td>A $5.00 non-refundable web convenience fee applies to online purchases.</td>
    </tr>
    <tr>
        <td>cart_extra_information</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_extra_information"
                   name="cart_extra_information"
                   value="Additional Information" />
        </td>
        <td>Additional Information</td>
    </tr>
    <tr>
        <td>cart_collect_marketing</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_collect_marketing"
                   name="cart_collect_marketing"
                   value="Opt-In for marketing" />
        </td>
        <td>Opt-In for marketing</td>
    </tr>
    <tr>
        <td>cart_collect_marketing_label</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_collect_marketing_label"
                   name="cart_collect_marketing_label">
I would like to be informed of events and updates.				   
			</textarea>
        </td>
        <td>I would like to be informed of events and updates.</td>
    </tr>
    <tr>
        <td>cart_collect_marketing_data</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_collect_marketing_data"
                   name="cart_collect_marketing_data"
                   value="checked" />
        </td>
        <td>checked</td>
    </tr>
    <tr>
        <td>cart_collect_marketing_enable</td>
		<td><input type=checkbox 
				   id="cart_collect_marketing_enable"
				   name="cart_collect_marketing_enable"
				   checked />
		</td>
		<td></td>
	</tr>
    <tr>
        <td>cart_extra_referral</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_extra_referral"
                   name="cart_extra_referral"
                   value="How did you hear about the program?" />
        </td>
        <td>How did you hear about the program?</td>
    </tr>
    <tr>
        <td>cart_extra_referral_data</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_extra_referral_data"
                   name="cart_extra_referral_data"
                   value=" |Facebook|Twitter|Email|Web Ad|Event|Direct Mail|Other" />
        </td>
        <td> |Facebook|Twitter|Email|Web Ad|Event|Direct Mail|Other</td>
    </tr>
    <tr>
        <td>cart_extra_buyer_as</td>
        <td>
            <input type=text style="width: 100%;"
                    id=""
                   name=""
                   value="" />
        </td>
        <td>I am buying this as a:</td>
    </tr>
    <tr>
        <td>cart_extra_buyer_as_foot</td>
        <td>
            <input type=text style="width: 100%;"
                    id=""
                   name=""
                   value="" />
        </td>
        <td>(Check all that apply)</td>
    </tr>
    <tr>
        <td>cart_extra_buyer_as_data</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_extra_buyer_as_data"
                   name="cart_extra_buyer_as_data"
                   value="Staff|Gift|Athlete|Charter Member|Other" />
        </td>
        <td>Staff|Gift|Athlete|Charter Member|Other</td>
    </tr>
    <tr>
        <td>cart_extra_university</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_extra_university"
                   name="cart_extra_university"
                   value="Enter the University" />
        </td>
        <td>Enter the University</td>
    </tr>
    <tr>
        <td>cart_extra_university_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_extra_university_placeholder"
                   name="cart_extra_university_placeholder"
                   value="Enter the University" />
        </td>
        <td>Enter the University</td>
    </tr>
    <tr>
        <td>cart_extra_other1</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_extra_other1"
                   name="cart_extra_other1"
                   value="Describe how you heard" />
        </td>
        <td>Describe how you heard</td>
    </tr>
    <tr>
        <td>cart_extra_other2</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_extra_other2"
                   name="cart_extra_other2"
                   value=" ~ Describe Your Affiliation" />
        </td>
        <td> ~ Describe Your Affiliation</td>
    </tr>
    <tr>
        <td>cart_extra_other1_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_extra_other1_placeholder"
                   name="cart_extra_other1_placeholder"
                   value="How Did You Hear" />
        </td>
        <td>How Did You Hear</td>
    </tr>
    <tr>
        <td>cart_extra_other2_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_extra_other2_placeholder"
                   name="cart_extra_other2_placeholder"
                   value="Describe Your Affiliation" />
        </td>
        <td>Describe Your Affiliation</td>
    </tr>
    <tr>
        <td>cart_billing_information</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_information"
                   name="cart_billing_information"
                   value="Billing Information" />
        </td>
        <td>Billing Information</td>
    </tr>
    <tr>
        <td>cart_billing_notes</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_notes"
                   name="cart_billing_notes"
                   value="Notes" />
        </td>
        <td>Notes</td>
    </tr>
    <tr>
        <td>cart_billing_method</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_method"
                   name="cart_billing_method"
                   value="Billing Method" />
        </td>
        <td>Billing Method</td>
    </tr>
    <tr>
        <td>cart_billing_first_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_first_name"
                   name="cart_billing_first_name"
                   value="First Name" />
        </td>
        <td>First Name</td>
    </tr>
    <tr>
        <td>cart_billing_last_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_last_name"
                   name="cart_billing_last_name"
                   value="Last Name" />
        </td>
        <td>Last Name</td>
    </tr>
    <tr>
        <td>cart_billing_company</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_company"
                   name="cart_billing_company"
                   value="Company" />
        </td>
        <td>Company</td>
    </tr>
    <tr>
        <td>cart_billing_address</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_address"
                   name="cart_billing_address"
                   value="Address" />
        </td>
        <td>Address</td>
    </tr>
    <tr>
        <td>cart_billing_city</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_city"
                   name="cart_billing_city"
                   value="City" />
        </td>
        <td>City</td>
    </tr>
    <tr>
        <td>cart_billing_state</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_state"
                   name="cart_billing_state"
                   value="State" />
        </td>
        <td>State</td>
    </tr>
    <tr>
        <td>cart_billing_zip</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_zip"
                   name="cart_billing_zip"
                   value="Zip" />
        </td>
        <td>Zip</td>
    </tr>
    <tr>
        <td>cart_billing_phone</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_phone"
                   name="cart_billing_phone"
                   value="Phone" />
        </td>
        <td>Phone</td>
    </tr>
    <tr>
        <td>cart_billing_email</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_billing_email"
                   name="cart_billing_email"
                   value="Email" />
        </td>
        <td>Email</td>
    </tr>
    <tr>
        <td>cart_credit_card_information</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_information"
                   name="cart_credit_card_information"
                   value="Credit Card Information" />
        </td>
        <td>Credit Card Information</td>
    </tr>
    <tr>
        <td>cart_gift_card_discount_information</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_gift_card_discount_information"
                   name="cart_gift_card_discount_information"
                   value="Enter Gift Code Information" />
        </td>
        <td>Enter Gift Code Information</td>
    </tr>
    <tr>
        <td>cart_credit_card_type</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_type"
                   name="cart_credit_card_type"
                   value="Type" />
        </td>
        <td>Type</td>
    </tr>
    <tr>
        <td>cart_credit_card_number</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_number"
                   name="cart_credit_card_number"
                   value="Card No." />
        </td>
        <td>Card No.</td>
    </tr>
    <tr>
        <td>cart_credit_card_number_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_number_placeholder"
                   name="cart_credit_card_number_placeholder"
                   value="Enter Your Credit Card Number" />
        </td>
        <td>Enter Your Credit Card Number</td>
    </tr>
    <tr>
        <td>cart_credit_card_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_name"
                   name="cart_credit_card_name"
                   value="Name on Card" />
        </td>
        <td>Name on Card</td>
    </tr>
    <tr>
        <td>cart_credit_card_name_placeholder</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_name_placeholder"
                   name="cart_credit_card_name_placeholder"
                   value="Name on Card" />
        </td>
        <td>Name on Card</td>
    </tr>
    <tr>
        <td>cart_credit_card_month</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_month"
                   name="cart_credit_card_month"
                   value="Expiration Month" />
        </td>
        <td>Expiration Month</td>
    </tr>
    <tr>
        <td>cart_credit_card_year</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_year"
                   name="cart_credit_card_year"
                   value="Expiration Year" />
        </td>
        <td>Expiration Year</td>
    </tr>
    <tr>
        <td>cart_credit_card_terms_accept</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_terms_accept"
                   name="cart_credit_card_terms_accept"
                   value="Accept Terms & Conditions" />
        </td>
        <td>Accept Terms & Conditions</td>
    </tr>
    <tr>
        <td>cart_credit_card_terms_view</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_terms_view"
                   name="cart_credit_card_terms_view"
                   value="View Terms" />
        </td>
        <td>View Terms</td>
    </tr>
    <tr>
        <td>cart_credit_card_process_credit_ajax</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_process_credit_ajax"
                   name="cart_credit_card_process_credit_ajax"
                   value="Processing Order...Please Wait" />
        </td>
        <td>Processing Order...Please Wait</td>
    </tr>
    <tr>
        <td>cart_credit_card_process_credit</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cart_credit_card_process_credit"
                   name="cart_credit_card_process_credit"
                   value="Process Order" />
        </td>
        <td>Process Order</td>
    </tr>
    <tr>
        <td>confirmation_payment</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_payment"
                   name="confirmation_payment"
                   value="CONFIRMATION" />
        </td>
        <td>CONFIRMATION</td>
    </tr>
    <tr>
        <td>confirmation_payment_successful</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_payment_successful"
                   name="confirmation_payment_successful"
                   value="TRANSACTION SUCCESSFUL" />
        </td>
        <td>TRANSACTION SUCCESSFUL</td>
    </tr>
    <tr>
        <td>confirmation_payment_failed</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_payment_failed"
                   name="confirmation_payment_failed"
                   value="TRANSACTION UNSUCCESSFUL" />
        </td>
        <td>TRANSACTION UNSUCCESSFUL</td>
    </tr>
    <tr>
        <td>confirmation_order_confirmation</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_order_confirmation"
                   name="confirmation_order_confirmation"
                   value="Order Confirmation" />
        </td>
        <td>Order Confirmation</td>
    </tr>
    <tr>
        <td>confirmation_order_received</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_order_received"
                   name="confirmation_order_received"
                   value="Order Received" />
        </td>
        <td>Order Received</td>
    </tr>
    <tr>
        <td>confirmation_order_received_text</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_order_received_text"
                   name="confirmation_order_received_text">Your Order is nearly complete! To complete your transaction, click below to secure your placement at the Minnesota Vikings Fan Recognition Program.</textarea>
        </td>
        <td>Your Order is nearly complete! To complete your transaction, click below to secure your placement at the Minnesota Vikings Fan Recognition Program.</td>
    </tr>
    <tr>
        <td>confirmation_order_received_make_payment</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_order_received_make_payment"
                   name="confirmation_order_received_make_payment"
                   value="Make Payment" />
        </td>
        <td>Make Payment</td>
    </tr>
    <tr>
        <td>confirmation_order_received_thank_you</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_order_received_thank_you"
                   name="confirmation_order_received_thank_you">Thank you for ordering a personalized brick. With your legacy brick order, you have etched your name into our "Inner Circle". Your contribution will benefit future generations.</textarea>
        </td>
        <td>Thank you for ordering a personalized brick. With your legacy brick order, you have etched your name into our "Inner Circle". Your contribution will benefit future generations.</td>
    </tr>
    <tr>
        <td>confirmation_pending</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_pending"
                   name="confirmation_pending"
                   value="Pending" />
        </td>
        <td>Pending</td>
    </tr>
    <tr>
        <td>confirmation_approved</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_approved"
                   name="confirmation_approved"
                   value="Approved" />
        </td>
        <td>Approved</td>
    </tr>
    <tr>
        <td>confirmation_declined</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_declined"
                   name="confirmation_declined"
                   value="Declined" />
        </td>
        <td>Declined</td>
    </tr>
    <tr>
        <td>confirmation_payment_please_print</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_payment_please_print"
                   name="confirmation_payment_please_print">Please print for your records.</textarea>
        </td>
        <td>Please print for your records.</td>
    </tr>
    <tr>
        <td>confirmation_thank_you_text1</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_thank_you_text1"
                   name="confirmation_thank_you_text1">Thank you and congratulations on joining the "Inner Circle" and etching your name into history with your legacy brick!</textarea>
        </td>
        <td>Thank you and congratulations on joining the \"Inner Circle\" and etching your name into history with your legacy brick!</td>
    </tr>
    <tr>
        <td>confirmation_thank_you_text1_approved</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_thank_you_text1_approved"
                   name="confirmation_thank_you_text1_approved">Thank you and congratulations on joining the \"Inner Circle\" and etching your name into history with your legacy brick!<br /><br />Your transaction has been successfully processed:</textarea>
        </td>
        <td>Thank you and congratulations on joining the \"Inner Circle\" and etching your name into history with your legacy brick!<br /><br />Your transaction has been successfully processed:</td>
    </tr>
    <tr>
        <td>confirmation_thank_you_text2</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_thank_you_text2"
                   name="confirmation_thank_you_text2">Thank you and congratulations on joining the \"Inner Circle\" and etching your name into history with your legacy brick! Our records indicate you have purchased the following:</textarea>
        </td>
        <td>Thank you and congratulations on joining the \"Inner Circle\" and etching your name into history with your legacy brick! Our records indicate you have purchased the following:</td>
    </tr>
    <tr>
        <td>confirmation_processed_success</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_processed_success"
                   name="confirmation_processed_success"></textarea>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>confirmation_processed_received</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_processed_received"
                   name="confirmation_processed_received">Your order was received successfully.</textarea>
        </td>
        <td>Your order was received successfully.</td>
    </tr>
    <tr>
        <td>confirmation_processed_failed</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_processed_failed"
                   name="confirmation_processed_failed">We're Sorry, an error occured while processing your order.</textarea>
        </td>
        <td>We're Sorry, an error occured while processing your order.</td>
    </tr>
    <tr>
        <td>confirmation_cancel_failed</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_cancel_failed"
                   name="confirmation_cancel_failed">It looks like you have cancelled your order.</textarea>
        </td>
        <td>It looks like you have cancelled your order.</td>
    </tr>
    <tr>
        <td>confirmation_order_date</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_order_date"
                   name="confirmation_order_date"
                   value="Order Date" />
        </td>
        <td>Order Date</td>
    </tr>
    <tr>
        <td>confirmation_order_number</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_order_number"
                   name="confirmation_order_number"
                   value="Order Number" />
        </td>
        <td>Order Number</td>
    </tr>
    <tr>
        <td>confirmation_auth_code</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_auth_code"
                   name="confirmation_auth_code"
                   value="Auth Code" />
        </td>
        <td>Auth Code</td>
    </tr>
    <tr>
        <td>confirmation_auth_response</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_auth_response"
                   name="confirmation_auth_response"
                   value="Auth Response" />
        </td>
        <td>Auth Response</td>
    </tr>
    <tr>
        <td>confirmation_payment_amount</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_payment_amount"
                   name="confirmation_payment_amount"
                   value="Amount" />
        </td>
        <td>Amount</td>
    </tr>
    <tr>
        <td>confirmation_customer_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_customer_name"
                   name="confirmation_customer_name"
                   value="Customer Name" />
        </td>
        <td>Customer Name</td>
    </tr>
    <tr>
        <td>confirmation_auth_message</td>
        <td>
            <input type=text style="width: 100%;"
                    id="confirmation_auth_message"
                   name="confirmation_auth_message"
                   value="Message" />
        </td>
        <td>Message</td>
    </tr>
    <tr>
        <td>confirmation_web_receipt</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_web_receipt"
                   name="confirmation_web_receipt"><p>An email order confirmation will be sent within 72 hours of receiving and processing your order. It is recommended to always check your spam or junk folder if this is your first time ordering.</p>
<p>Gift Certificates will be emailed within 5-7 business days of receipt.</p>
<p>A final Inscription Verification letter will be sent within 3-4 weeks of your approved brick order as one last chance to verify that your inscription is accurate prior to engraving. Any corrections must be made within the proof time indicated on your letter.</p>
<p>Engraved legacy bricks are installed randomly according to sales volume. Generally one to two times per year. Please check for the next installation if it was not listed at the beginning when you placed this order. Once bricks are installed, a locator map will be provided to assist you in finding your Legacy brick at the Minnesota Vikings.</p>
<p>If you purchased keepsake replica items, they will be engraved and shipped within 10-12 weeks of your approved order unless other arrangements were made. Shipping is included within the contiguous U.S. (excluding HI and AK).</p>
<p>Replicas are made of the same material and inscribed with the same personal message as your installed Legacy brick. Colors may vary slightly.</p>
<p>If you have any questions regarding your order, please call ".    <tr>
        <td>published_telephone"]." or email ".    <tr>
        <td>published_mailto"].".</p>
<p>The Minnesota Vikings Fan Recognition Program reserves the right to approve all inscription prior to production.</p></textarea>
        </td>
        <td>
<p>An email order confirmation will be sent within 72 hours of receiving and processing your order. It is recommended to always check your spam or junk folder if this is your first time ordering.</p>
<p>Gift Certificates will be emailed within 5-7 business days of receipt.</p>
<p>A final Inscription Verification letter will be sent within 3-4 weeks of your approved brick order as one last chance to verify that your inscription is accurate prior to engraving. Any corrections must be made within the proof time indicated on your letter.</p>
<p>Engraved legacy bricks are installed randomly according to sales volume. Generally one to two times per year. Please check for the next installation if it was not listed at the beginning when you placed this order. Once bricks are installed, a locator map will be provided to assist you in finding your Legacy brick at the Minnesota Vikings.</p>
<p>If you purchased keepsake replica items, they will be engraved and shipped within 10-12 weeks of your approved order unless other arrangements were made. Shipping is included within the contiguous U.S. (excluding HI and AK).</p>
<p>Replicas are made of the same material and inscribed with the same personal message as your installed Legacy brick. Colors may vary slightly.</p>
<p>If you have any questions regarding your order, please call ".    <tr>
        <td>published_telephone"]." or email ".    <tr>
        <td>published_mailto"].".</p>
<p>The Minnesota Vikings Fan Recognition Program reserves the right to approve all inscription prior to production.</p>
</td>
    </tr>
    <tr>
        <td>confirmation_auth_body_success</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_auth_body_success"
                   name="confirmation_auth_body_success"><p>An email order confirmation will be sent within 72 hours of receiving and processing your order. It is recommended to always check your spam or junk folder if this is your first time ordering.</p>
<p>Gift Certificates will be emailed within 5-7 business days of receipt.</p>
<p>A final Inscription Verification letter will be sent within 3-4 weeks of your approved brick order as one last chance to verify that your inscription is accurate prior to engraving. Any corrections must be made within the proof time indicated on your letter.</p>
<p>Engraved legacy bricks are installed randomly according to sales volume. Generally one to two times per year. Please check for the next installation if it was not listed at the beginning when you placed this order. Once bricks are installed, a locator map will be provided to assist you in finding your Legacy brick at the Minnesota Vikings.</p>
<p>If you purchased keepsake replica items, they will be engraved and shipped within 10-12 weeks of your approved order unless other arrangements were made. Shipping is included within the contiguous U.S. (excluding HI and AK).</p>
<p>Replicas are made of the same material and inscribed with the same personal message as your installed Legacy brick. Colors may vary slightly.</p>
<p>If you have any questions regarding your order, please call ".    <tr>
        <td>published_telephone_short"]." or email <A href=".    <tr>
        <td>published_mailto"].">".    <tr>
        <td>published_mailto"]."</A>.</p>
<p>We appreciate your participation in the Minnesota Vikings Fan Recognition Program!</p>
<p>Sincerely,</p>
<p style='font-style:italic;'><strong>"."The Minnesota Vikings Fan Recognition Program"."</strong></p></textarea>
        </td>
        <td>
<p>An email order confirmation will be sent within 72 hours of receiving and processing your order. It is recommended to always check your spam or junk folder if this is your first time ordering.</p>
<p>Gift Certificates will be emailed within 5-7 business days of receipt.</p>
<p>A final Inscription Verification letter will be sent within 3-4 weeks of your approved brick order as one last chance to verify that your inscription is accurate prior to engraving. Any corrections must be made within the proof time indicated on your letter.</p>
<p>Engraved legacy bricks are installed randomly according to sales volume. Generally one to two times per year. Please check for the next installation if it was not listed at the beginning when you placed this order. Once bricks are installed, a locator map will be provided to assist you in finding your Legacy brick at the Minnesota Vikings.</p>
<p>If you purchased keepsake replica items, they will be engraved and shipped within 10-12 weeks of your approved order unless other arrangements were made. Shipping is included within the contiguous U.S. (excluding HI and AK).</p>
<p>Replicas are made of the same material and inscribed with the same personal message as your installed Legacy brick. Colors may vary slightly.</p>
<p>If you have any questions regarding your order, please call ".    <tr>
        <td>published_telephone_short"]." or email <A href=".    <tr>
        <td>published_mailto"].">".    <tr>
        <td>published_mailto"]."</A>.</p>
<p>We appreciate your participation in the Minnesota Vikings Fan Recognition Program!</p>
<p>Sincerely,</p>
<p style='font-style:italic;'><strong>"."The Minnesota Vikings Fan Recognition Program"."</strong></p></td>
    </tr> // use html

    <tr>
        <td>confirmation_auth_body_accepted</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_auth_body_accepted"
                   name="confirmation_auth_body_accepted">
<p>An email order confirmation will be sent within 72 hours of receiving and processing your order. It is recommended to always check your spam or junk folder if this is your first time ordering.</p>
<p>Gift Certificates will be emailed within 5-7 business days of receipt.</p>
<p>A final Inscription Verification letter will be sent within 3-4 weeks of your approved brick order as one last chance to verify that your inscription is accurate prior to engraving. Any corrections must be made within the proof time indicated on your letter.</p>
<p>Engraved legacy bricks are installed randomly according to sales volume. Generally one to two times per year. Please check for the next installation if it was not listed at the beginning when you placed this order. Once bricks are installed, a locator map will be provided to assist you in finding your Legacy brick at the Minnesota Vikings.</p>
<p>If you purchased keepsake replica items, they will be engraved and shipped within 10-12 weeks of your approved order unless other arrangements were made. Shipping is included within the contiguous U.S. (excluding HI and AK).</p>
<p>Replicas are made of the same material and inscribed with the same personal message as your installed Legacy brick. Colors may vary slightly.</p>
<p>If you have any questions regarding your order, please call ".    <tr>
        <td>published_telephone_short"]." or email <A href=".    <tr>
        <td>published_mailto"].">".    <tr>
        <td>published_mailto"]."</A>.</p>
<p>We appreciate your participation in the Minnesota Vikings Fan Recognition Program!</p>
<p>Sincerely,</p>
<p style='font-style:italic;'><strong>".    <tr>
        <td>published_campaign_name_ab"]."</strong></p>
<br />
<p><em>We reserve the right to approve all inscriptions. Inscriptions that do not conform to the \"Inscription Guidelines\" or that are deemed unsuitable will be rejected and will require a new inscription to be submitted. Additionally, we cannot guarantee that multiple orders will be installed together.</em></p>				   
			</textarea>
        </td>
        <td>
<p>An email order confirmation will be sent within 72 hours of receiving and processing your order. It is recommended to always check your spam or junk folder if this is your first time ordering.</p>
<p>Gift Certificates will be emailed within 5-7 business days of receipt.</p>
<p>A final Inscription Verification letter will be sent within 3-4 weeks of your approved brick order as one last chance to verify that your inscription is accurate prior to engraving. Any corrections must be made within the proof time indicated on your letter.</p>
<p>Engraved legacy bricks are installed randomly according to sales volume. Generally one to two times per year. Please check for the next installation if it was not listed at the beginning when you placed this order. Once bricks are installed, a locator map will be provided to assist you in finding your Legacy brick at the Minnesota Vikings.</p>
<p>If you purchased keepsake replica items, they will be engraved and shipped within 10-12 weeks of your approved order unless other arrangements were made. Shipping is included within the contiguous U.S. (excluding HI and AK).</p>
<p>Replicas are made of the same material and inscribed with the same personal message as your installed Legacy brick. Colors may vary slightly.</p>
<p>If you have any questions regarding your order, please call ".    <tr>
        <td>published_telephone_short"]." or email <A href=".    <tr>
        <td>published_mailto"].">".    <tr>
        <td>published_mailto"]."</A>.</p>
<p>We appreciate your participation in the Minnesota Vikings Fan Recognition Program!</p>
<p>Sincerely,</p>
<p style='font-style:italic;'><strong>".    <tr>
        <td>published_campaign_name_ab"]."</strong></p>
<br />
<p><em>We reserve the right to approve all inscriptions. Inscriptions that do not conform to the \"Inscription Guidelines\" or that are deemed unsuitable will be rejected and will require a new inscription to be submitted. Additionally, we cannot guarantee that multiple orders will be installed together.</em></p>
</td>
    </tr>

    <tr>
        <td>confirmation_auth_body_declined</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="confirmation_auth_body_declined"
                   name="confirmation_auth_body_declined">
<p>Please contact us at ".    <tr>
        <td>published_telephone_short"]." within 48 hours to correct and complete your pending transaction.</p>
<p>We must hear back from you within 48 hours to avoid your order being cancelled.</p>
<p>Thank you, we appreciate your support!</p>
<p style='font-style:italic;'><strong>"."The Minnesota Vikings Fan Recognition Program"."</strong></p>
			</textarea>
        </td>
        <td><p>Please contact us at ".    <tr>
        <td>published_telephone_short"]." within 48 hours to correct and complete your pending transaction.</p>
<p>We must hear back from you within 48 hours to avoid your order being cancelled.</p>
<p>Thank you, we appreciate your support!</p>
<p style='font-style:italic;'><strong>"."The Minnesota Vikings Fan Recognition Program"."</strong></p></td>
    </tr>
    <tr>
        <td>terms_terms_conditions_header</td>
        <td>
            <input type=text style="width: 100%;"
                    id="terms_terms_conditions_header"
                   name="terms_terms_conditions_header"
                   value="Terms and Conditions" />
        </td>
        <td>Terms and Conditions</td>
    </tr>
    <tr>
        <td>terms_returns_cancellations_header</td>
        <td>
            <input type=text style="width: 100%;"
                    id="terms_returns_cancellations_header"
                   name="terms_returns_cancellations_header"
                   value="Returns/Cancellations " />
        </td>
        <td>Returns/Cancellations </td>
    </tr>
    <tr>
        <td>terms_returns_cancellations_text</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="terms_returns_cancellations_text"
                   name="terms_returns_cancellations_text">
			Because material purchased is custom produced to your specifications, returns and cancellations are not allowed.	   
			</textarea>
        </td>
        <td>Because material purchased is custom produced to your specifications, returns and cancellations are not allowed.</td>
    </tr>
    <tr>
        <td>terms_inscriptions_disclaimer_header</td>
        <td>
            <input type=text style="width: 100%;"
                    id="terms_inscriptions_disclaimer_header"
                   name="terms_inscriptions_disclaimer_header"
                   value="Inscriptions Disclaimer" />
        </td>
        <td>Inscriptions Disclaimer</td>
    </tr>
    <tr>
        <td>terms_inscriptions_disclaimer_text</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="terms_inscriptions_disclaimer_text"
                   name="terms_inscriptions_disclaimer_text">
<p style='font-style:italic;'>Reading and acknowledging this document is an important step in
finalizing your order.</p>
<p>Please be aware that we will produce your order exactly as
you enter it. If you submitted your order showing words with no spaces, this is exactly how it will
be inscribed. (i.e. JACK&amp;JILLSMITH)</p>
<p>Please allow 10-12 weeks for delivery of your replica(s) after your order has successfully been processed, approved and paid in full.</p>
<p>Any inscription changes to the original order that results in a new inscription is required to be re-submitted for approval and therefore subject to a $10 reprocessing fee payable only by credit card over the phone.</p>
<p style='font-style:italic;'>We reserve the right to approve all text prior to production.</p>	   
			</textarea>
        </td>
        <td>
<p style='font-style:italic;'>Reading and acknowledging this document is an important step in
finalizing your order.</p>
<p>Please be aware that we will produce your order exactly as
you enter it. If you submitted your order showing words with no spaces, this is exactly how it will
be inscribed. (i.e. JACK&amp;JILLSMITH)</p>
<p>Please allow 10-12 weeks for delivery of your replica(s) after your order has successfully been processed, approved and paid in full.</p>
<p>Any inscription changes to the original order that results in a new inscription is required to be re-submitted for approval and therefore subject to a $10 reprocessing fee payable only by credit card over the phone.</p>
<p style='font-style:italic;'>We reserve the right to approve all text prior to production.</p></td>
    </tr>
    <tr>
        <td>terms_payment_disclaimer_header</td>
        <td>
            <input type=text style="width: 100%;"
                    id="terms_payment_disclaimer_header"
                   name="terms_payment_disclaimer_header"
                   value="Transaction Disclaimer" />
        </td>
        <td>Transaction Disclaimer</td>
    </tr>
    <tr>
        <td>terms_payment_disclaimer_text</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="terms_payment_disclaimer_text"
                   name="terms_payment_disclaimer_text">
This web site uses 256 bit encryption and secure socket layers for your credit card information protection. SSL is the standard form of encyrption used on the Internet today. If you buy anything using your credit card number on the Web, you should be aware of the security risks involved. Theoretically, your card number could be intercepted en route and used by unauthorized people. If you send your credit card number via Internet, you must understand that this risk is yours and only you can decide whether the convenience is worth the risk. If you nevertheless choose to use this electronic transaction option, we disclaim any liability for direct, consequential, special or punitive damages or losses which you may incur as a result of your use of this transaction option.	   
			</textarea>
        </td>
        <td>This web site uses 256 bit encryption and secure socket layers for your credit card information protection. SSL is the standard form of encyrption used on the Internet today. If you buy anything using your credit card number on the Web, you should be aware of the security risks involved. Theoretically, your card number could be intercepted en route and used by unauthorized people. If you send your credit card number via Internet, you must understand that this risk is yours and only you can decide whether the convenience is worth the risk. If you nevertheless choose to use this electronic transaction option, we disclaim any liability for direct, consequential, special or punitive damages or losses which you may incur as a result of your use of this transaction option.</td>
    </tr>
    <tr>
        <td>terms_privacy_disclaimer_header</td>
        <td>
            <input type=text style="width: 100%;"
                    id="terms_privacy_disclaimer_header"
                   name="terms_privacy_disclaimer_header"
                   value="Privacy Policy Disclaimer" />
        </td>
        <td>Privacy Policy Disclaimer</td>
    </tr>
    <tr>
        <td>terms_privacy_disclaimer_text</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="terms_privacy_disclaimer_text"
                   name="terms_privacy_disclaimer_text">
Our business is highly sensitive
to the privacy interests of consumers and believes that the protection of those interests 
is one of its most significant responsibilities.  In acknowledgment of its obligations,  We
have adopted the following Privacy Policy applicable to information about consumers that it
requires in the course of its business:<br />
1. Acquisition of Information.  We do not acquire any more information about consumers than is 
required by law or is otherwise necessary to provide a high level of service efficiently and securely.<br />
2. Our Employees and Privacy.  We train all of our employees about the importance of privacy.  We give access to 
information about customers only to those employees who require it to perform their jobs.<br />
3. Security Measures.  We make access to privacy-sensitive information subject to rigorous procedural and
technological controls, consistent with legal requirements and the demands of customer service.<br />
4. Disclosure to Third Parties.  We will provide individually-identifiable information about consumers to third
parties only if we are compelled to do so by order of a duly-empowered governmental authority, we have
the express permission of the consumer, or it is necessary to process transactions and provide our services.<br />
5. Privacy and Our Business Partners.  When we make our technology or services available to business partners, we will 
not share with them any more consumer information than is necessary and we will make every reasonable effort to assure,
by contract or otherwise, that they use our technology and services in a manner that is consistent with this Privacy Policy.<br />
<br /><br />
If you have any questions, please feel free to contact us.  Thank you.				   
			</textarea>
        </td>
        <td>
Our business is highly sensitive
to the privacy interests of consumers and believes that the protection of those interests 
is one of its most significant responsibilities.  In acknowledgment of its obligations,  We
have adopted the following Privacy Policy applicable to information about consumers that it
requires in the course of its business:<br />
1. Acquisition of Information.  We do not acquire any more information about consumers than is 
required by law or is otherwise necessary to provide a high level of service efficiently and securely.<br />
2. Our Employees and Privacy.  We train all of our employees about the importance of privacy.  We give access to 
information about customers only to those employees who require it to perform their jobs.<br />
3. Security Measures.  We make access to privacy-sensitive information subject to rigorous procedural and
technological controls, consistent with legal requirements and the demands of customer service.<br />
4. Disclosure to Third Parties.  We will provide individually-identifiable information about consumers to third
parties only if we are compelled to do so by order of a duly-empowered governmental authority, we have
the express permission of the consumer, or it is necessary to process transactions and provide our services.<br />
5. Privacy and Our Business Partners.  When we make our technology or services available to business partners, we will 
not share with them any more consumer information than is necessary and we will make every reasonable effort to assure,
by contract or otherwise, that they use our technology and services in a manner that is consistent with this Privacy Policy.<br />
<br /><br />
If you have any questions, please feel free to contact us.  Thank you.</td>
    </tr>
    <tr>
        <td>js_invalid_quantity</td>
        <td>
            <input type=text style="width: 100%;"
                    id="js_invalid_quantity"
                   name="js_invalid_quantity"
                   value="Invalid Quantity.  Try again." />
        </td>
        <td>Invalid Quantity.  Try again.</td>
    </tr>
    <tr>
        <td>js_invalid_name</td>
        <td>
            <input type=text style="width: 100%;"
                    id="js_invalid_name"
                   name="js_invalid_name"
                   value="Please enter your Name." />
        </td>
        <td>Please enter your Name.</td>
    </tr>
    <tr>
        <td>js_invalid_address</td>
        <td>
            <input type=text style="width: 100%;"
                    id="js_invalid_address"
                   name="js_invalid_address"
                   value="Please enter your address." />
        </td>
        <td>Please enter your address.</td>
    </tr>
    <tr>
        <td>js_invalid_city</td>
        <td>
            <input type=text style="width: 100%;"
                    id="js_invalid_city"
                   name="js_invalid_city"
                   value="Please enter your city." />
        </td>
        <td>Please enter your city.</td>
    </tr>
    <tr>
        <td>js_invalid_state</td>
        <td>
            <input type=text style="width: 100%;"
                    id="js_invalid_state"
                   name="js_invalid_state"
                   value="Please enter your State / Province." />
        </td>
        <td>Please enter your state / Province.</td>
    </tr>
    <tr>
        <td>js_invalid_zip</td>
        <td>
            <input type=text style="width: 100%;"
                    id="js_invalid_zip"
                   name="js_invalid_zip"
                   value="Please enter your postal code." />
        </td>
        <td>Please enter your postal code.</td>
    </tr>
    <tr>
        <td>js_invalid_phone</td>
        <td>
            <input type=text style="width: 100%;"
                    id="js_invalid_phone"
                   name="js_invalid_phone"
                   value="Please enter your phone." />
        </td>
        <td>Please enter your phone.</td>
    </tr>
    <tr>
        <td>js_invalid_email</td>
        <td>
            <input type=text style="width: 100%;"
                    id="js_invalid_email"
                   name="js_invalid_email"
                   value="Please enter your email." />
        </td>
        <td>Please enter your email.</td>
    </tr>
    <tr>
        <td>js_invalid_cc</td>
        <td>
            <input type=text style="width: 100%;"
                    id="js_invalid_cc"
                   name="js_invalid_cc"
                   value="Please enter a correct credit card." />
        </td>
        <td>Please enter a correct credit card.</td>
    </tr>
    <tr>
        <td>cert_brickno_text</td>
        <td>
            <input type=text style="width: 100%;"
                    id="cert_brickno_text"
                   name="cert_brickno_text"
                   value="Brick #:" />
        </td>
        <td>Brick #:</td>
    </tr>
    <tr>
        <td>selected_product_mobile</td>
        <td>
            <input type=text style="width: 100%;"
                    id="selected_product_mobile"
                   name="selected_product_mobile"
                   value="OPTIONS:" />
        </td>
        <td><br/>OPTIONS:</td>
    </tr>
    <tr>
        <td>cart_replica_qty_mobile</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_replica_qty_mobile"
                   name="cart_replica_qty_mobile">*ADD REPLICA QTY <br /> for Brick Option Selected:/<textarea>
        </td>
        <td>
*ADD REPLICA QTY <br /> for Brick Option Selected:
		</td>
    </tr>
    <tr>
        <td>cart_display_case_qty_mobile</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="cart_display_case_qty_mobile"
                   name="cart_display_case_qty_mobile">
ADD REPLICA DISPLAY CASE QTY <br /> for number of Replica(s):				   
			</textarea>
        </td>
        <td>
ADD REPLICA DISPLAY CASE QTY <br /> for number of Replica(s):
		</td>
    </tr>
    <tr>
        <td>brick_options_mobile</td>
        <td>
            <textarea style="width: 100%; height: 150px"
                   id="brick_options_mobile"
                   name="brick_options_mobile">
8x8 Replica Bricks - $85 and 8x8 Replica Display Cases - $70<br/>
4x8 Replica Bricks - $50 and 4x8 Replica Display Cases - $55<br/>
*A brick option must be selected in order to add replicas and 
display cases to the order. (1) keepsake replica brick is included 
with three of the options.				   
			</textarea>
        </td>
        <td>
8x8 Replica Bricks - $85 and 8x8 Replica Display Cases - $70<br/>
4x8 Replica Bricks - $50 and 4x8 Replica Display Cases - $55<br/>
*A brick option must be selected in order to add replicas and 
display cases to the order. (1) keepsake replica brick is included 
with three of the options.
		</td>
    </tr>
    <tr>
        <td>edshp_address_btn</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_address_btn"
                   name="edshp_address_btn"
                   value="Use First Shipping Address" />
        </td>
        <td>Use First Shipping Address</td>
    </tr>
    <tr>
        <td>edshp_clear_btn</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_clear_btn"
                   name="edshp_clear_btn"
                   value="Clear Form" />
        </td>
        <td>Clear Form</td>
    </tr>
    <tr>
        <td>edshp_fname_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_fname_plchldr"
                   name="edshp_fname_plchldr"
                   value="Enter Your First Name" />
        </td>
        <td>Enter Your First Name</td>
    </tr>
    <tr>
        <td>edshp_mname_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_mname_plchldr"
                   name="edshp_mname_plchldr"
                   value="Enter Your Middle Name" />
        </td>
        <td>Enter Your Middle Name</td>
    </tr>
    <tr>
        <td>edshp_lname_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_lname_plchldr"
                   name="edshp_lname_plchldr"
                   value="Enter Your Last Name" />
        </td>
        <td>Enter Your Last Name</td>
    </tr>
    <tr>
        <td>edshp_org_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_org_plchldr"
                   name="edshp_org_plchldr"
                   value="Enter Your Organization Name" />
        </td>
        <td>Enter Your Organization Name</td>
    </tr>
    <tr>
        <td>edshp_add_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_add_plchldr"
                   name="edshp_add_plchldr"
                   value="Enter Your Street Address" />
        </td>
        <td>Enter Your Street Address</td>
    </tr>
    <tr>
        <td>edshp_add2_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_add2_plchldr"
                   name="edshp_add2_plchldr"
                   value="Enter Additional Address" />
        </td>
        <td>Enter Additional Address</td>
    </tr>
    <tr>
        <td>edshp_city_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_city_plchldr"
                   name="edshp_city_plchldr"
                   value="Enter Your City" />
        </td>
        <td>Enter Your City</td>
    </tr>
    <tr>
        <td>edshp_prov_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_prov_plchldr"
                   name="edshp_prov_plchldr"
                   value="Enter Your State / Province" />
        </td>
        <td>Enter Your State / Province</td>
    </tr>
    <tr>
        <td>edshp_pc_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_pc_plchldr"
                   name="edshp_pc_plchldr"
                   value="Enter Your Postal Code" />
        </td>
        <td>Enter Your Postal Code</td>
    </tr>
    <tr>
        <td>edshp_country_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_country_plchldr"
                   name="edshp_country_plchldr"
                   value="Enter Your Country" />
        </td>
        <td>Enter Your Country</td>
    </tr>
    <tr>
        <td>edshp_phone_plchldr</td>
        <td>
            <input type=text style="width: 100%;"
                    id="edshp_phone_plchldr"
                   name="edshp_phone_plchldr"
                   value="Enter Your Phone Number" />
        </td>
        <td>Enter Your Phone Number</td>
    </tr>
    <tr>
        <td>bill_addr_btn</td>
        <td>
            <input type=text style="width: 100%;"
                    id="bill_addr_btn"
                   name="bill_addr_btn"
                   value="Same As Shipping" />
        </td>
        <td>Same As Shipping</td>
    </tr>
    <tr>
        <td>bill_clear_btn</td>
        <td>
            <input type=text style="width: 100%;"
                    id="bill_clear_btn"
                   name="bill_clear_btn"
                   value="Clear Form" />
        </td>
        <td>Clear Form</td>
    </tr>
</body>
</html>
<script type="text/javascript">

</script>