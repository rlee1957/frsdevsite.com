<?php

function bo_creditcard() 
{

include("configuration/environment_settings.php");
include("../_common/includes/language_check.php");
echo('
<div id="creditcarddiv">
	<h4>'.$lang['cart_credit_card_information'].'</h4>
	<div class="control-group">
		<label class="control-label">
			'.$lang['cart_credit_card_type'].'
		</label>
		<div class="controls">
			<select name="cc_type" 
					id="cc_type" 
					required
					class="selectpicker show-tick show-menu-arrow">
');
include("configuration/credit_card_settings.php");
$item_selected = false;
foreach($creditcard as $type => $arr)
	{
	$disabled = $arr["disabled"];
	$selected = "";
	if(($disabled != "disabled") && (!$item_selected))
		{	
		$selected = "selected";
		$item_selected = true;
		}
	if($arr["display"])
		{
		echo('
				<option value="'.$type.'" '.$disabled.' '.$selected.'>
					'.$arr["label"].'
				</option>
');
		}
	}
echo('
			</select>
			<p class="help-block"></p>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">
			'.$lang['cart_credit_card_number'].'
		</label>
		<div class="controls">
			<input type="text" 
				   name="cc_number" 
				   id="cc_number" 
				   placeholder="'.$lang['cart_credit_card_number_placeholder'].'" 
				   class="creditcard"
				   onchange="sanitize_sql(this);"
				   onblur="sanitize_sql(this)" />
			<p class="help-block"></p>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">
			'.$lang['cart_credit_card_name'].'
		</label>
		<div class="controls">
			<input type="text" 
				   name="cc_owner" 
				   id="cc_owner" 
				   required 
				   placeholder="'.$lang['cart_credit_card_name_placeholder'].'"
				   onchange="sanitize_sql(this);"
				   onblur="sanitize_sql(this)" />
			<p class="help-block"></p>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">
			'.$lang['cart_credit_card_month'].'
		</label>
		<div class="controls">
			<select name="cc_mm" 
					id="cc_mm" 
					class="selectpicker show-tick show-menu-arrow">');
for($mo=1;$mo<13;$mo++)
	{
	$val = "0".$mo;
	if(strlen($val) > 2)
		{
		$val = substr($val, 1);	
		}
	echo('
				<option value="'.$val.'">'.$val.'</option>');
	}
echo('
			</select>
			<p class="help-block"></p>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">
			'.$lang['cart_credit_card_year'].'
		</label>
		<div class="controls">
			<select name="cc_yy" 
					id="cc_yy" 
					class="selectpicker show-tick show-menu-arrow">');
$yr = date('yy');
for($x=0;$x<15;$x++)
	{
	$yy = (string)($yr + $x);
	$yy = substr($yy, 2);
	echo('
				<option value="'.$yy.'">'.$yy.'</option>');
	}
echo('
			</select>
			<p class="help-block"></p>
		</div>
	</div>
</div>
');
}

?>