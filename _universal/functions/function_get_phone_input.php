<?php

function get_phone_input($name, $placeholder, $value, $required, $onchange)
{
include("configuration/environment_settings.php");
include("configuration/countries_offered.php");
if(!isset($countries_offered[strtolower($_SESSION["country"])]["phone"]))
	{
	$phone = $countries_offered["ot"]["phone"];
	}
else
	{
	$phone = $countries_offered[strtolower($_SESSION["country"])]["phone"];
	}

$ar = explode("|", $phone);
$maxlength = trim($ar[0]);
$minlength = trim($ar[1]);
$reTVal = '
						<input type="text" 
							   name="'.$name.'" 
							   id="'.$name.'"
							   class="required digits" 
							   '.$required.' 
							   placeholder="'.$placeholder.'" 
							   maxlength="'.$maxlength.'" 
							   minlength="'.$minlength.'"
							   onchange="'.$onchange.'"
							   value="'.$value.'" />';	
return $reTVal;
}

?>