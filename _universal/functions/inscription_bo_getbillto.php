<?php

function bo_getbillto() 
{

include("configuration/environment_settings.php");	
include("configuration/misc_settings.php");
include("../".$environment_path."/includes/language_check.php");

$ar = array();
if(isset($_SESSION["g_liship"][1][1]["s_name"]))
	{
	$ar = $_SESSION["g_liship"][1][1];	
	}
else
	{
	$ar["s_prefix"] = "";
	$ar["s_name"] = "";
	$ar["s_mname"] = "";
	$ar["s_lname"] = "";
	$ar["s_company"] = "";
	$ar["s_addr1"] = "";
	$ar["s_addr2"] = "";
	$ar["s_city"] = "";
	$ar["s_state"] = "";
	$ar["s_zip"] = "";
	$ar["s_phone"] = "";
	$ar["s_email"] = "";	
	}
$tb = "                               ";
$fn = "
";
$validate1 = 'sanitize_sql(this);';
$validate2 = 'validate_required(this, \'';
$validate3 = '';
$required0 = '';
$required_ = "required";

echo('
		<div>
');

# AUTOFILL and CLEAR BUTTONS

echo('
			<h4>'.$lang['cart_billing_information'].'</h4>
			<div class="span12" style="text-align:center; padding:15px;">
');
if((isset($button["use_address"]))&&(!$button["use_address"])){  }
else
	{
	echo('
				<input type="button" 
					   class="btn btn-custom" 
					   id="same_as_shipping" 
					   name="same_as_shipping" 
					   value="'.$lang['bill_addr_btn'].'"
					   onclick="pre_fill_address();" />
');
	}
if((isset($button["clear_address"]))&&(!$button["clear_address"])){  }
else
	{
	echo('
				<input type="button" 
					   style="margin-left:10px;" 
					   class="btn btn-custom" 
					   id="same_as_shipping_clear" 
					   name="same_as_shipping_clear" 
					   value="'.$lang['bill_clear_btn'].'"
					   onclick="clear_address();" />
');
	}
echo('
			</div>
');

# FIRST NAME
$comment = "First Name";
$label = $lang['cart_billing_first_name'];
$name = $tb.'name="b_fname"'.$fn.$tb.'id="b_fname"'.$fn;
$placeholder = $tb.'placeholder="'.$lang["cart_shipping_first_name_placeholder"].'"'.$fn;
$value = $tb.'value="'.$ar["s_name"].'"'.$fn;
$required = $tb.$required_.$fn;
$msg = $lang['cart_billing_first_name'].' '.$lang["validate_generic"].'\');"';
$onchange = $tb.'onchange="'.$validate1.' '.$validate2.$msg.$fn;
$onblur = $tb.'onblur="'.$validate1.' '.$validate2.$msg.$fn;
$maxlength = $tb.'maxlength="50"'.$fn;
include("../".$environment_path."/fields/contact_text_input.php");	

# LAST NAME
$comment = "Last Name";
$label = $lang['cart_billing_last_name'];
$name = $tb.'name="b_lname"'.$fn.$tb.'id="b_lname"'.$fn;
$placeholder = $tb.'placeholder="'.$lang["cart_shipping_last_name_placeholder"].'"'.$fn;
$value = $tb.'value="'.$ar["s_lname"].'"'.$fn;
$required = $required_;
$msg = $lang['cart_billing_last_name'].' '.$lang["validate_generic"].'\');"';
$onchange = $tb.'onchange="'.$validate1.' '.$validate2.$msg.$fn;
$onblur = $tb.'onblur="'.$validate1.' '.$validate2.$msg.$fn;
$maxlength = $tb.'maxlength="50"'.$fn;
include("../".$environment_path."/fields/contact_text_input.php");

# COMPANY
$comment = "Company";
$label = $lang['cart_billing_company'];
$name = $tb.'name="b_company"'.$fn.$tb.'id="b_company"'.$fn;
$placeholder = $tb.'placeholder="'.$lang["cart_shipping_company_placeholder"].'"'.$fn;
$value = $tb.'value="'.$ar["s_company"].'"'.$fn;
$required = $required0;
$onchange = $tb.'onchange="'.$validate1.'"'.$fn;
$onblur = $tb.'onblur="'.$validate1.'"'.$fn;
$maxlength = $tb.'maxlength="255"'.$fn;
include("../".$environment_path."/fields/contact_text_input.php");

# BILLING ADDRESS
$comment = "Billing Address";
$label = $lang['cart_billing_address'];
$name = $tb.'name="b_addr1"'.$fn.$tb.'id="b_addr1"'.$fn;
$placeholder = $tb.'placeholder="'.$lang["cart_shipping_address_placeholder"].'"'.$fn;
$value = $tb.'value="'.$ar["s_addr1"].'"'.$fn;
$required = $required_;
$msg = $lang['cart_billing_address'].' '.$lang["validate_generic"].'\');"';
$onchange = $tb.'onchange="'.$validate1.' '.$validate2.$msg.$fn;
$onblur = $tb.'onblur="'.$validate1.' '.$validate2.$msg.$fn;
$maxlength = $tb.'maxlength="255"'.$fn;
include("../".$environment_path."/fields/contact_text_input.php");

# BILLING ADDRESS 2
$comment = "Billing Address 2";
$label = $lang['cart_shipping_address2'];
$name = $tb.'name="b_addr2"'.$fn.$tb.'id="b_addr2"'.$fn;
$placeholder = $tb.'placeholder="'.$lang["cart_shipping_address2_placeholder"].'"'.$fn;
$value = $tb.'value="'.$ar["s_addr2"].'"'.$fn;
$required = $required0;
$onchange = $tb.'onchange="'.$validate1.'"'.$fn;
$onblur = $tb.'onblur="'.$validate1.'"'.$fn;
$maxlength = $tb.'maxlength="255"'.$fn;
include("../".$environment_path."/fields/contact_text_input.php");

# CITY
$comment = "City";
$label = $lang['cart_billing_city'];
$name = $tb.'name="b_city"'.$fn.$tb.'id="b_city"'.$fn;
$placeholder = $tb.'placeholder="'.$lang["cart_shipping_city_placeholder"].'"'.$fn;
$value = $tb.'value="'.$ar["s_city"].'"'.$fn;
$required = $tb.$required_.$fn;
$msg = $lang['cart_billing_city'].' '.$lang["validate_generic"].'\');"';
$onchange = $tb.'onchange="'.$validate1.' '.$validate2.$msg.$fn;
$onblur = $tb.'onblur="'.$validate1.' '.$validate2.$msg.$fn;
$maxlength = $tb.'maxlength="255"'.$fn;
include("../".$environment_path."/fields/contact_text_input.php");
							
# STATE/PROVINCE
$comment = "State / Province";
$label = $lang['cart_billing_state'];
$name = 'b_state';
$placeholder = $lang["cart_shipping_state_placeholder"];
$value = $ar["s_state"];
$required = $required_;
$msg = $lang['cart_billing_state'].' '.$lang["validate_generic"].'\');"';
$onchange = 'onchange="'.$validate1.' '.$validate2.$msg;
$onblur = 'onblur="'.$validate1.' '.$validate2.$msg;
$maxlength = '255';
include("../".$environment_path."/fields/contact_state_prov.php");
							
								
# POSTAL CODE
$comment = "Postal Code";
$label = $lang['cart_billing_zip'];
$name = $tb.'name="b_zip"'.$fn.$tb.'id="b_zip"'.$fn;
$placeholder = $tb.'placeholder="'.$lang["cart_shipping_country_placeholder"].'"'.$fn;
$value = $tb.'value="'.$ar["s_zip"].'"'.$fn;
switch($_SESSION["country"])
	{
	case "US":
		{
		$description = "'".$lang["validate_zip_us"]."'";
		$msg = 'validate_zip(this, '.$description.');"';
		$required = $tb.$required_.$fn;
		$onchange = $tb.'onchange="'.$validate1.' '.$msg.$fn;
		$onblur = $tb.'onclur="'.$validate1.' '.$msg.$fn;
		$maxlength = $tb.'maxlength="5"'.$fn;
		break;	
		}
	case "CA":
		{
		$description = "'".$lang["validate_zip_ca"]."'";
		$msg = 'validate_ca_postal_code(this, '.$description.');"';
		$required = $tb.$required_.$fn;
		$onchange = $tb.'onchange="'.$validate1.' '.$msg.$fn;
		$onblur = $tb.'onclur="'.$validate1.' '.$msg.$fn;
		$maxlength = $tb.'maxlength="7"'.$fn;
		break;	
		}
	default:
		{
		$required = $required0;
		$onchange = $tb.'onchange="'.$validate1.'"'.$fn;
		$onblur = $tb.'onblur="'.$validate1.'"'.$fn;
		$maxlength = $tb.'maxlength="20"'.$fn;
		break;	
		}
	}
include("../".$environment_path."/fields/contact_text_input.php");
							
# COUNTRY
$comment = "Country";
$label = $lang['cart_shipping_country'];
$name = 'b_country';
$placeholder = $lang["cart_shipping_country_placeholder"];
$value = $_SESSION["country"];
$required = $required_;
$msg = $lang['cart_shipping_country'].' '.$lang["validate_generic"].'\');"';
$onchange = $validate1.' '.$validate2.$msg;
$onblur = $onchange;
include("../".$environment_path."/fields/contact_country.php");

# PHONE NUMBER
$comment = "Phone Number";
$label = $lang['cart_billing_phone'];
$name = 'b_phone';
$placeholder = $lang["cart_shipping_daytime_phone_placeholder"];
$value = $ar["s_phone"];
$required = $required_;
$msg = 'validate_us_phone(this, \''.$lang["validate_us_phone"].'\');"';
$onchange = $validate1.' '.$msg;
$onblur = $onchange;
include("../".$environment_path."/fields/contact_phone.php");

# EMAIL ADDRESS
$comment = "Email Address";
$label = $lang['cart_billing_email'];
$name = $tb.'name="b_email"'.$fn.$tb.'id="b_email"'.$fn;
$placeholder = $tb.'placeholder="'.$lang["cart_shipping_email_placeholder"].'"'.$fn;
$value = $tb.'value="'.$ar["s_email"].'"'.$fn;
$required = $tb.$required_.$fn;
$msg = 'validate_email(this, \''.$lang["validate_email"].'\');"';
$onchange = $tb.'onchange="'.$validate1.' '.$msg.$tb.'onblur="'.$validate1.' '.$msg;
$onblur = $tb.'onblur="'.$validate1.' '.$msg;
include("../".$environment_path."/fields/contact_email.php");

echo('
	</div>
<input type="hidden" name="b_nprofit" value="'.$_SESSION["g_billing"][11].'" />
<hr />
');

include("../".$environment_path."/fields/contact_bill_fill_clear.php");

}

?>