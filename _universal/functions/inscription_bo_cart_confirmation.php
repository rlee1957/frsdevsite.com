<?php

function bo_cart_confirmation() 
{
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
$_SESSION["g_ordertotal"] = 0;
echo '
    <TABLE CELLPADDING=0 CELLSPACING=8 BORDER=0 WIDTH="100%">
    <TR CLASS="tbl_grey" ALIGN="center">
    <TD class="tbl_head_txt"><FONT SIZE=3><b>Your Shopping Cart</b></FONT><BR>
    <TABLE CELLPADDING=0 CELLSPACING=2 BORDER=0 WIDTH="100%">
    <TR BGCOLOR="#FFFFFF">
    <TD>
    <TABLE CELLPADDING=0 CELLSPACING=3 BORDER=0 WIDTH="100%">
         ';
for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
	{
    if (isset($_SESSION["g_libktype"][$ii])) 
		{
		bo_cart_line_heading();
		bo_cart_paver($ii);
		if ($options["enable_replica"])
			{
            bo_cart_replica($ii);
			}
		bo_cart_blue_line();
		bo_cart_inscr($ii, $_SESSION["g_libktype"][$ii]);
        }
    }
echo '
    </TABLE>
    </TD>
    </TR>
    </TABLE> ';
echo '
     </TD>
</TR>
</TABLE>
 ';
}

?>