<?php


function bo_staff()
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
echo '
<div id="content">
    <TABLE CELLPADDING=0 CELLSPACING=8 BORDER=0 WIDTH="100%">
    <TR CLASS="tbl_grey" ALIGN="center">
    <TD><FONT SIZE=3><b>'.$lang['cart_staff_information'].'</b></FONT><BR>
        <TABLE CELLPADDING=0 CELLSPACING=2 BORDER=0 WIDTH="100%">
        <TR BGCOLOR="#FFFFFF">
        <TD>

            <table width="755" align="center" border="1"  cellspacing="2">
            <table width="750" align="center" border="0" cellspacing="5">
			<div class="cmxform">
            <tr>
			<td width="40%" align="right">*'.$lang['staff_name'].'
			</td>
			<div class="cmxform">
            <td align="left" width="60%">
			<select class="required" id="b_staff" name="b_staff" size="1">';
			$staff = explode("|",$lang['staff_users']);
			sort($staff);
			foreach ($staff as $value){
				echo '<option value="'.$value.'"> '.$value.'</option>';
			}
			echo '
			</select>
            </tr>
            </table>
            <table width="750" align="center" border="0" cellspacing="5">
			<div class="cmxform">
            <tr>
			<td width="40%" valign="top" align="right">
			'.$lang['staff_notes'].'
			</td>
			<div class="cmxform">
            <td align="left" width="60%">
				<textarea rows="4" cols="40" name="staff_notes" id="staff_notes"></textarea>
            </tr>
            </table>
            </table>
        </TD>
        </TR>
        </TABLE>	
     </TD>
    </TR>
    </TABLE>
</div>
<hr>
';
}


?>