<?php
function process_argofire() 
{
	
	go_clear_response();

	//
	// Argofire
	//
$host = $_SESSION["go_url"].$_SESSION["go_page"];
//$mytx is an array containing all POST data from a form
$mytx = array();
$mytx['op']			= 'ProcessCreditCard';
$host .= $mytx['op'];
$mytx['username'] 	= $_SESSION["go_merchant"];
$mytx['password'] 	= $_SESSION["go_password"];
$mytx['amount'] 	= $_SESSION["go_total"];
$mytx['CardNum']    = $_SESSION["go_cno"];
$mytx['ExpDate']	= $_SESSION["go_cexp"];
$mytx['CVNum']   	= '';//$_POST['CVNum'];
$mytx['NameOnCard'] = $_SESSION["go_oname"];
$mytx['TransType']  = 'Sale';
$mytx['MagData']  	= '';//$_POST['MagData'];
$mytx['InvNum']  	= $_SESSION["go_orderid"];
$mytx['PNRef']  	= '';//$_POST['PNRef'];
$mytx['Zip']  		= $_SESSION["go_ozip"];
$mytx['Street'] 	= $_SESSION["go_ostreet"];
$mytx['Phone'] 	= $_SESSION["g_billing"][9];
$mytx['Email'] 	= $_SESSION["g_billing"][10];
$mytx['CheckNum']  	= '';//$_POST['CheckNum'];
$mytx['TransitNum'] = '';//$_POST['TransitNum'];
$mytx['AccountNum'] = '';//$_POST['AccountNum'];
$mytx['MICR']  		= '';//$_POST['MICR'];
$mytx['NameOnCheck']= '';//$_POST['NameOnCheck'];
$mytx['DL']  		= '';//$_POST['DL'];
$mytx['SS']  		= '';//$_POST['SS'];
$mytx['DOB']  		= '';//$_POST['DOB'];
$mytx['StateCode'] 	= '';//$_POST['StateCode'];
$mytx['CheckType'] 	= '';//$_POST['CheckType'];
$mytx['Pin'] 		= '';//$_POST['Pin'];
$mytx['RegisterNum'] 	= '';//$_POST['RegisterNum'];
$mytx['SureChargeAmt'] 	= '';//$_POST['SureChargeAmt'];
$mytx['CashBackAmt'] 	= '';//$_POST['CashBackAmt'];
$mytx['ExtData'] 	= '<CustomerID>'.$_SESSION["go_orderid"].'</CustomerID><Force>T</Force>';//$_POST['ExtData']; <Force>T/Force>

//$paystring = 'https://secure.ftipgw.com/ArgoFire/transact.asmx?op=ProcessCreditCard&username='.$_SESSION["go_merchant"];

//print_r($mytx);


//Generate the string as "key=value" pairs
$mybuilder = array();
    foreach($mytx as $key=>$value) {
      $mybuilder[] = $key . '='.$value;
    }
//print_r($mybuilder);

//join the pairs into a single string
$mystring = implode("&",$mybuilder);
//echo $mystringer;
function curl_post($url, array $post = NULL, array $options = array()) 
{ 
    $defaults = array( 
        CURLOPT_POST => 1, 
        CURLOPT_HEADER => 0, 
        CURLOPT_URL => $url, 
        CURLOPT_FRESH_CONNECT => 1, 
        CURLOPT_RETURNTRANSFER => 1, 
        CURLOPT_FORBID_REUSE => 1, 
        CURLOPT_TIMEOUT => 60, 
        CURLOPT_POSTFIELDS => http_build_query($post) 
    ); 

    $ch = curl_init(); 
    curl_setopt_array($ch, ($options + $defaults)); 
    if( ! $result = curl_exec($ch)) 
    { 
        trigger_error(curl_error($ch)); 
    } 
    curl_close($ch); 
    return $result; 
}

$data = curl_post($host,$mytx);
$xml_data = simplexml_load_string($data);

	//------------------------------------------------------
	// Save Response results into global session variables
	//------------------------------------------------------
	if ($_SESSION["go_total"] > 0){	
		$_SESSION["go_resp_status"] = $xml_data->Result;
		$_SESSION["go_resp_auth_code"] = $xml_data->AuthCode;
		$_SESSION["go_resp_auth_response"] = $xml_data->RespMSG;
		$_SESSION["go_resp_code"] = $xml_data->Result;
		$_SESSION["go_resp_avs_code"] = $xml_data->GetAVSResultTXT;
		$_SESSION["go_resp_cvv2_code"] = $xml_data->GetZipMatchTXT;
		$_SESSION["go_resp_order_id"] = $xml_data->go_orderid;
		$_SESSION["go_resp_reference_number"] = $xml_data->PNRef;
		$_SESSION["go_resp_error"] = $xml_data->error;
	}
	else{
		$_SESSION["go_resp_status"] = $xml_data->Result;
		$_SESSION["go_resp_auth_code"] = $xml_data->AuthCode;
		$_SESSION["go_resp_auth_response"] = $xml_data->RespMSG;
		$_SESSION["go_resp_code"] = "Accepted";
		$_SESSION["go_resp_avs_code"] = $xml_data->GetAVSResultTXT;
		$_SESSION["go_resp_cvv2_code"] = $xml_data->GetZipMatchTXT;
		$_SESSION["go_resp_order_id"] = $xml_data->go_orderid;
		$_SESSION["go_resp_reference_number"] = $xml_data->PNRef;
		$_SESSION["go_resp_error"] = $xml_data->error;
	}


}

?>