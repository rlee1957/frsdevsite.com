<?php

function bo_confirmbkqty() 
{

# Initiate Variables
	{
	include("configuration/environment_settings.php");
	include("../".$environment_path."/includes/language_check.php");
	# html variables
		{
		$pre_module = '
<div class="row">
	<div class="span12">
		<div class="well">
';
		$post_module = '
		</div>
	</div>
</div>		
';
		$logo_options = '';
		if ($options["logo_options"])  
			{
			$logo_options = '
				<div>
					<br /><br /><br />
					<h6>'.$lang['cart_bo_logo_options_selected'].'</h6>
					<img style="img-polaroid" src="logos/'.$_SESSION["g_lilogoname"][$li].'.png" alt="'.$_SESSION["g_lilogoname"][$li].'">
				</div>
';
			}
		}
	# brick variables
		{
		$brick_name = strtoupper($_SESSION["g_bkdesc"][ $_SESSION["g_currentbktype"]]);
		$brick_image = $_SESSION["g_bkimg"][ $_SESSION["g_currentbktype"]];
		$brick_quantity = $_SESSION["g_currentpqty"];
		$brick_price = $_SESSION["g_bkpcost"][$_SESSION["g_currentbktype"]];
		$brick_subtotal = get_current_paver_total();
		}
	# other variables
		{
		$xx_ptotal = $brick_subtotal;
		$xx_rtotal = get_current_replica_total();
		$xx_r2total = get_current_replica2_total();
		$xx_dctotal = get_current_display_case_total();
		$xx_dc2total = get_current_display_case2_total();
		$xx_ptotal_tax = get_current_paver_tax_total();
		$xx_rtotal_tax = get_current_replica_tax_total();
		$xx_r2total_tax = get_current_replica2_tax_total();
		$xx_dctotal_tax = get_current_display_case_tax_total();
		$xx_dc2total_tax = get_current_display_case2_tax_total();
		$xx_ototal = 0;
		$li = $_SESSION["g_currentli"];
		$li_ship = $_SESSION["g_lishiptoqty"][$li] + 0;
		$_SESSION["g_lishiptoqty"][$li] = $li_ship;
		$lineitem_tax = $_SESSION["g_liship"][$li][$li_ship]["s_state"];
		$total_without_tax = $xx_ptotal + $xx_rtotal + $xx_r2total + $xx_dctotal + $xx_dc2total;
		$tax = $xx_ptotal_tax + $xx_rtotal_tax + $xx_r2total_tax + $xx_dctotal_tax + $xx_dc2total_tax; 
		$xx_rtaxtotal = $tax;
		$xx_subtot = $tax + $total_without_tax;	
		$xx_grandtot = $xx_subtot;
		$_SESSION["g_taxtotal"] = $xx_rtaxtotal;

		// Check if a Logo Brick was selected - check str for the word LOGO
		$x_HoldBrickName = strtoupper($_SESSION["g_bkdesc"][ $_SESSION["g_currentbktype"]]);
		}
	}

# Confirm Header
	{	
	echo '
<!-- inscription_bo_confirmbkqty2.php -->
';
	echo($pre_module);
	echo('
			<div class="pagination-centered">
				<h4>'.$lang['cart_bo_header_confirm'].'</h4>
			</div>
');
	echo('
<div style="clear: both;"></div>
');
	echo($post_module);
	}

# Brick Option Selected
	{	
	echo($pre_module);
	include("../".$environment_path."/includes/brick2.php");
	echo('
<div style="clear: both;"></div>
');
	echo($post_module);
	}

# Other Options
	{
	if(($options["enable_replica"] && $_SESSION["g_currentrqty"] > 0)||
	   ($options["enable_replica2"] && $_SESSION["g_currentr2qty"] > 0)||
	   ($options["enable_displaycase"] && $_SESSION["g_currentdcqty"] > 0)||
	   ($options["enable_displaycase2"] && $_SESSION["g_currentdc2qty"] > 0))
		{
		echo($pre_module);
		
		# Replica 1
			{
			if ($options["enable_replica"] && $_SESSION["g_currentrqty"] > 0)
				{	
				# replica 1 variables
					{
					$r1_span = $_SESSION["g_bkr_span"][$li];	
					$r1_head = '<h4>'.$lang['selected_replica'].'</h4>';
					$r1_desc = $_SESSION["g_bkrdesc"][$_SESSION["g_currentbktype"]];
					$r1_image = $_SESSION["g_bkrimg"][ $_SESSION["g_currentbktype"]];
					$r1_qty = '
						<input class="input-small" type="Text" value="'.$_SESSION["g_currentrqty"].'" readonly />
					';
					$r1_price = $_SESSION["g_bkrcost"][$_SESSION["g_currentbktype"]];
					$r1_subtotal = $xx_rtotal;
					}
				# replic 1 html
				include("../".$environment_path."/includes/replica1.php");
				}
			}
		
		# Display Case 1
			{
			if ($options["enable_displaycase"] && $_SESSION["g_currentdcqty"] > 0)
				{
				# Display Case 1 variables
					{
					$dc1_span = $_SESSION["g_bkdc_span"][$li];
					$dc1_head = '<h4>'.$lang['selected_dc'].'</h4>';	
					$dc1_desc = $_SESSION["g_bkdcdesc"][$_SESSION["g_currentbktype"]];
					$dc1_image = $_SESSION["g_bkdcimg"][ $_SESSION["g_currentbktype"]];	
					$dc1_qty = '<input class="input-small" type="Text" value="'.$_SESSION["g_currentdcqty"].'" readonly />';
					$dc1_price = $_SESSION["g_bkdccost"][$_SESSION["g_currentbktype"]];
					$dc1_subtotal = number_format((float)$xx_dctotal, 2, ".", ",");
					}
				# Display Case 1 html
					{
					include("../".$environment_path."/includes/display_case1.php");
					}
				}
			}
		
		# Replica 2
			{
			if ($options["enable_replica2"] && $_SESSION["g_currentr2qty"] > 0)
				{	
				# Replica 2 variables
					{
					$r2_span = $_SESSION["g_bkr2_span"][$li];
					$f2_head = '
					<h4>'.$lang['add_your_replica'].'</h4>';	
					$r2_desc = $_SESSION["g_bkr2desc"][$_SESSION["g_currentbktype"]];
					$r2_image = $_SESSION["g_bkr2img"][ $_SESSION["g_currentbktype"]];	
					$r2_qty = '<input class="input-small" type="Text" value="'.$_SESSION["g_currentr2qty"].'" readonly />';
					$r2_price = $_SESSION["g_bkr2cost"][$_SESSION["g_currentbktype"]];
					$r2_subtotal = "0.00";
					}
			# Display Case 1 html
					{
					include("../".$environment_path."/includes/replica2.php");
					}
				}
			}
		
		# Display Case 2
			{
			if ($options["enable_displaycase2"] && $_SESSION["g_currentdc2qty"] > 0)
				{	
				# Display Case 2 variables
					{
					$dc2_span = $_SESSION["g_bkdc2_span"][$li];
					$dc2_head = '
					<h4>
						'.$lang['selected_dc'].'
					</h4>
';	
					$dc2_desc = $_SESSION["g_bkdc2desc"][$_SESSION["g_currentbktype"]];
					$dc2_image = $_SESSION["g_bkdc2img"][ $_SESSION["g_currentbktype"]];	
					$dc2_qty = '<input class="input-small" type="Text" value="'.$_SESSION["g_currentdc2qty"].'" readonly />';
					$dc2_price = $_SESSION["g_bkdc2cost"][$_SESSION["g_currentbktype"]];
					$dc2_subtotal = $xx_dc2total;
					}
				# Display Case 2 html
					{
					include("../".$environment_path."/includes/display_case2.php");
					}
				}
			}
		
		# Close Divs
			{
			echo('
<div style="clear: both;"></div>
');
			echo ($post_module);
			}
		}
	}

# Cost Breakdown
	{
	echo($pre_module);
	
	# Sales Tax
		{
		if($options["apply_sales_tax"])
			{
			echo '
			<h4>'.$lang['cart_tax'].':</h4>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">'.$lang['published_currency_symbol'].'</strong></span>
				<input class="input-small" type="Text" id="x_rtax" name="x_rtax" value="';  printf("%2.2f", $xx_rtaxtotal); echo '" readonly />
			</div>
		';
			}
		}
	
	# Subtotal
		{
		echo '
			<h4>'.$lang['cart_subtotal'].':</h4>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">'.$lang['published_currency_symbol'].'</strong></span>
				<input class="input-small" type="Text" id="x_gtotal" name="x_gtotal" value="';  printf("%2.2f", $xx_grandtot); echo '" readonly>
			</div>
		 ';
		}
	echo($post_module);
	}

}

?>