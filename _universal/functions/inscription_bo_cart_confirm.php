<?php

function bo_cart_confirm() 
{
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
// Set order total to zero
$_SESSION["g_ordertotal"] = 0;
$_SESSION["g_taxtotal"] = 0.00;
echo '
    <TABLE CELLPADDING=0 CELLSPACING=8 BORDER=0 WIDTH="100%">
    <TR CLASS="tbl_grey" ALIGN="center">
    <TD class="tbl_head_txt"><FONT SIZE=3></FONT><BR>
    <TABLE CELLPADDING=0 CELLSPACING=2 BORDER=0 WIDTH="100%">
    <TR BGCOLOR="#FFFFFF">
    <TD>
    <TABLE CELLPADDING=0 CELLSPACING=3 BORDER=0 WIDTH="100%">
         ';
for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
    {
    if (isset($_SESSION["g_libktype"][$ii])) 
		{
		bo_cart_line_heading();
		bo_cart_paver($ii);
		if ($options["enable_replica"])
			{
            bo_cart_replica($ii);
			}
		bo_cart_blue_line();
		bo_cart_inscr($ii, $_SESSION["g_libktype"][$ii]);
        }
    }
if ($_SESSION["g_op_enable"]) 
	{
	// Display Options
	for ($ii = 1; $ii <= $_SESSION["g_op_no_li"]; $ii++)
        {
        if (isset($_SESSION["g_op_li"][$ii]["op_type"])) 
			{		
			bo_cart_line_heading();
			bo_op_cart_display($ii);                
			}
        }
	}
//Calculate Tax and add to Total
$xx_subtot = $_SESSION["g_ordertotal"];
$xx_taxrate = $_SESSION["service_objects_totaltaxrate"];
$xx_taxtot = round(($xx_taxrate * $xx_subtot),2);
$xx_grandtot = round(($xx_taxtot + $xx_subtot),2);		
if ($options["web_convenience_fee"]) 
	{		
	$_SESSION["g_ordertotal"] = $xx_grandtot + $_SESSION["convenience_fee"];
	$_SESSION["g_taxtotal"] = $xx_taxtot;
	} 
else 
	{	
	$_SESSION["g_ordertotal"] = $xx_grandtot;
	$_SESSION["g_taxtotal"] = $xx_taxtot;
	}
echo '
    </TABLE>
    </TD>
    </TR>
    </TABLE> ';
bo_cart_bottom();
echo '
     </TD>
</TR>
</TABLE>
';
}

?>