<?php
 
function bo_shipto() 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");

$_SESSION["g_currentli"] = $_SESSION["g_currentli"];

echo('
<div class="row">
	<div class="span12">
		<div id="content" class="well">
			<div class="pagination-centered">
				<h4>'.$lang['cart_shipping_header'].'</h4>
				<p>'.$lang['cart_shipping_text1'].'<span class="txt_boldred"><i>'.$lang['cart_shipping_text2'].'</i></span></p>
				<p>'.$lang['cart_shipping_text3'].'</p>
				<p>'.$lang['cart_shipping_cert_note'].'</p>
				<p><strong>'.$lang['cart_shipping_text4'].'</strong></span></p>
			</div>
');

include("../".$environment_path."/includes/inscription_shipto.php");

echo('
		</div>
	</div>
</div>
<input type="hidden" name="s_qty" value ="'.$_SESSION["g_rqtyremaining"].'">
');

include("../".$environment_path."/fields/contact_ship_fill_clear.php");

}


?>