<?php

function bo_getbkqty() 
{
	
# Initialize variables
	{	
	include("configuration/environment_settings.php");
	include("../".$environment_path."/includes/language_check.php");
	$brick_type = $_POST["bktype"];
	# html variables
		{
		$pre_module = '
<div class="row">
	<div class="span12">
		<div class="well">
';
		$post_module = '
		</div>
	</div>
</div>		
';
		$logo_options = '';
		$select_options = '
						<option value="0" selected="selected">0</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>	
';
		}
	# brick variables
		{
		$brick_name = $_SESSION["g_bkdesc"][$_POST["bktype"]];
		$brick_image = $_SESSION["g_bkimg"][$_POST["bktype"]];
		$brick_quantity = "1";
		$brick_price = $_SESSION["g_bkpcost"][$_POST["bktype"]];
		$brick_subtotal = $_SESSION["g_bkpcost"][$_POST["bktype"]];
		$bst = $brick_subtotal;
		if((strpos($bst, ".") === false))
			{
			$bst .= ".00";
			}
		}
	# other variables
		{
		
		$_SESSION["g_logo_name"] = "";  // Clear
		$_SESSION["g_logo_no"] = "";    // Clear
		$x_HoldBrickName = strtoupper($_SESSION["g_bkdesc"][$brick_type]);
		$note = '
						<hr />
						<strong>'.$lang['cart_note1'].'</strong>
						<span class="txt_boldred">'.$lang['cart_note2'].'</span>
						'.$lang['cart_note3'].'
		';
		if ($options["enable_replica"])
			{
			$note .= '
						<em>'.$lang['cart_replica_note'].'</em>
		';
			}
		}
	}

# header
	{
	echo '
<!-- inscription_bo_getbkqty2.php -->
'.$pre_module.'
			<div class="pagination-centered">
				<h2>' . $lang['cart_bo_header'] . '</h2>
				<h4>' . $lang['customize_options'] . '</h4>
			</div>
	'.$post_module;
	}
	
# Brick Selected
	{
	# html
		{
		echo $pre_module;
		include("../".$environment_path."/includes/brick2.php");
		echo('
<div style="clear: both;"></div>
');	
		}
	# note
		{
		if($options["options_format2"] == 1)
			{
			echo('
	<div style="clear: both;">
		'.trim($note).'
	</div>
	');
				
			}
		}
	echo $post_module;		
	}		
# Replica and Display Case Options
	{
	if ($options["enable_replica"] || $options["enable_replica2"] || $options["enable_displaycase"] || $options["enable_displaycase2"]) 
		{
		# Options Container START
			{
			echo $pre_module;
			}
			
		# Replica 1
			{
			if ($options["enable_replica"]) 
				{
				# replica 1 variables
					{
					$r1_span = $_SESSION["g_bkr_span"][$_POST["bktype"]];
					$r1_head = '
				<div>
					<strong>' . $lang['add_your_replica'] . '</strong>
				</div>';	
					$r1_desc = $_SESSION["g_bkrdesc"][$_POST["bktype"]];
					$r1_image = $_SESSION["g_bkrimg"][$_POST["bktype"]];	
					$r1_qty = '
				<select class="selectpicker show-tick show-menu-arrow qty input-small" 
							id="x_rqty" 
							name="x_rqty" 
							data-width="auto">
					'.$select_options.'
				</select>
';
					$r1_price = $_SESSION["g_bkrcost"][$_POST["bktype"]];
					$r1_subtotal = "0.00";
					}
				# replic 1 html
					{
					if(($r1_desc != "")&&(strtolower($r1_desc) != "not offered"))
						{
						include("../".$environment_path."/includes/replica1.php");
						}
					}
				}
			}
			
		# Display Case 1 
			{
			if ($options["enable_displaycase"]) 
				{
				# Display Case 1 variables
					{
					$dc1_span = $_SESSION["g_bkdc_span"][$_POST["bktype"]];
					$dc1_head = '
				<div>
					<strong>'.$lang['add_your_dc'].'</strong>
				</div>
';	
					$dc1_desc = $_SESSION["g_bkdcdesc"][$_POST["bktype"]];
					$dc1_image = $_SESSION["g_bkdcimg"][$_POST["bktype"]];	
					$dc1_qty = '
				<select class="selectpicker show-tick show-menu-arrow qty input-small" 
							id="x_dcqty" 
							name="x_dcqty" 
							data-width="auto" 
							data-style="btn-custom" >
					'.$select_options.'
				</select>
';
					$dc1_price = $_SESSION["g_bkdccost"][$_POST["bktype"]];
					$dc1_subtotal = "0.00";
					}
				# Display Case 1 html
					{
					if(($dc1_desc != "")&&(strtolower($dc1_desc) != "not offered"))
						{
						include("../".$environment_path."/includes/display_case1.php");
						}
					}
				}
			}
		
		# Replica 2
			{
			if ($options["enable_replica2"]) 
				{
				# Replica 2 variables
					{
					$r2_span = $_SESSION["g_bkr2_span"][$_POST["bktype"]];
					$f2_head = '
				<div>
					<strong>'.$lang['add_your_replica'].'</strong>
				</div>
';	
					$r2_desc = $_SESSION["g_bkr2desc"][$_POST["bktype"]];
					$r2_image = $_SESSION["g_bkr2img"][$_POST["bktype"]];	
					$r2_qty = '
				<select class="selectpicker show-tick show-menu-arrow qty input-small" 
							id="x_r2qty" 
							name="x_r2qty" 
							data-width="auto">
					'.$select_options.'
				</select>
';
					$r2_price = $_SESSION["g_bkr2cost"][$_POST["bktype"]];
					$r2_subtotal = "0.00";
					}
				# Replica 2 html
					{
					if(($r2_desc != "")&&(strtolower($r2_desc) != "not offered"))
						{
						include("../".$environment_path."/includes/replica2.php");
						}
					}
				}
			}
		
		# Display Case 2
			{
			if ($options["enable_displaycase2"]) 
				{
				# Display Case 2 variables
					{
					$dc2_span = $_SESSION["g_bkdc2_span"][$_POST["bktype"]];
					$dc2_head = '
				<div>
					<strong>'.$lang['add_your_dc'].'</strong>
				</div>
';	
					$dc2_desc = $_SESSION["g_bkdc2desc"][$_POST["bktype"]];
					$dc2_image = $_SESSION["g_bkdc2img"][$_POST["bktype"]];	
					$dc2_qty = '
				<select class="selectpicker show-tick show-menu-arrow qty input-small" 
							id="x_dc2qty" 
							name="x_dc2qty" 
							data-width="auto" 
							data-style="btn-custom">
					'.$select_options.'
				</select>
';
					$dc2_price = $_SESSION["g_bkdc2cost"][$_POST["bktype"]];
					$dc2_subtotal = "0.00";
					}
				# Display Case 2 html
					{
					if(($dc2_desc != "")&&(strtolower($dc2_desc) != "not offered"))
						{
						include("../".$environment_path."/includes/display_case2.php");
						}
					}
				}
			}
		
		# Options Container END
			{
			echo('
<div style="clear: both;"></div>
');
			echo ($post_module);
			}
		} 
	}
	
# Logo Picker Options
	{
	echo '<!-- Logo Picker -->';
	if ($options["logo_options"]) 
		{
		echo '
	<div id="logoPicker" class="row">
		<div class="span12">
			<div class="well">
				<h4>' . $lang['cart_bo_logo_options'] . '</h4>
				<div>
					<select class="image-picker" name="logo_name" id="logo_name">';
		foreach (glob('logos/*.png*') as $filename) 
			{
			$name1 = explode('/', $filename);
			$name2 = explode('.', $name1[1]);
			$name3 = $name2[0];
			if ($name3 == $_SESSION["g_logo_display_default"]) 
				{
				echo '
						<option selected data-img-src="'.$filename.'" value="'.$name3.'" label="'.$name3.'">
							'. $name3 . '
						</option>
	';
				} 
			else 
				{
				echo '
						<option data-img-src="'.$filename.'" value="'.$name3.'" label="'.$name3.'">
							'.$name3. '
						</option>
	';
				}
			}
		echo'
					</select>
				</div>
			</div>
		
		</div>
	</div>
	';
		} // End - Logo Picker    
	}

# Subtotal Display
	{
	echo '
<div class="row">
	<div class="span12">
		<div class="well">
			<div class="sidebar-nav sidebar-nav-fixed">
				<h4>' . $lang['cart_subtotal'] . ':</h4>
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">' . $lang['published_currency_symbol'] . '</strong>
					</span>
					<input class="input-small" 
						   type="Text" 
						   id="x_gtotal" 
						   name="x_gtotal" 
						   value="'.$bst.'"
						   readonly />
				</div>
			</div>
';
	echo($post_module);
	}	

}

?>