<?php

function bo_getbillto() 
{
include("system/application/language/language_check.php");
include("configuration/misc_settings.php");
echo '
<div>
<h4>'.$lang['cart_billing_information'].'</h4>
			<div class="span12" style="text-align:center; padding:10px;">
';
if((isset($button["use_address"]))&&(!$button["use_address"])){  }
else
	{
	echo('
				<input type="button" class="btn btn-custom" id="same_as_shipping" name="same_as_shipping" value="Same As Shipping" onclick="pre_fill_address();" />
');
	}
if((isset($button["clear_address"]))&&(!$button["clear_address"])){  }
else
	{
	echo('				
				<input type="button" style="margin-left:10px;" class="btn btn-custom" id="same_as_shipping_clear" name="same_as_shipping_clear" value="Clear Form" onclick="clear_address();" />
');
	}
echo '
			</div>
							<div class="control-group">
								<label class="control-label">
								   	*'.$lang['cart_billing_first_name'].'
								   </label>
								 <div class="controls">
								   <input type="text" id="b_fname" name="b_fname" placeholder="Enter Your First Name" value="'.$_SESSION["g_liship"][1][1]["s_name"].'" required  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   *'.$lang['cart_billing_last_name'].'
								 </label>
								 <div class="controls">
								   <input type="text" id="b_lname" name="b_lname" required placeholder="Enter Your Last Name" value="'.$_SESSION["g_liship"][1][1]["s_lname"].'"  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_billing_company'].'
								 </label>
								 <div class="controls">
								   <input type="text" id="b_company" name="b_company" placeholder="Enter Your Organization Name" value="'.$_SESSION["g_liship"][1][1]["s_company"].'"  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_billing_address'].'
								 </label>
								 <div class="controls">
								   <input type="text" id="b_addr1" name="b_addr1" required placeholder="Enter Your Street Address" value="'.$_SESSION["g_liship"][1][1]["s_addr1"].'"  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								 </label>
								 <div class="controls">
								   <input type="text" id="b_addr2" name="b_addr2" placeholder="Enter Additional Address" value="'.$_SESSION["g_liship"][1][1]["s_addr2"].'"  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_billing_state'].'
								 </label>
								 <div class="controls">
								   	<select class="selectpicker show-tick show-menu-arrow required" id="b_state" name="b_state" required value="'.$_SESSION["g_liship"][1][1]["s_state"].'">
										<option value=""></option>
										<option value="AL">AL</option>
										<option value="AK">AK</option>
										<option value="AR">AR</option>
										<option value="AZ">AZ</option>
										<option value="CA">CA</option>
										<option value="CO">CO</option>
										<option value="CT">CT</option>
										<option value="DC">DC</option>
										<option value="DE">DE</option>
										<option value="FL">FL</option>
										<option value="GA">GA</option>
										<option value="HI">HI</option>
										<option value="IA">IA</option>
										<option value="ID">ID</option>
										<option value="IL">IL</option>
										<option value="IN">IN</option>
										<option value="KS">KS</option>
										<option value="KY">KY</option>
										<option value="LA">LA</option>
										<option value="MA">MA</option>
										<option value="MD">MD</option>
										<option value="ME">ME</option>
										<option value="MI">MI</option>
										<option value="MN">MN</option>
										<option value="MO">MO</option>
										<option value="MS">MS</option>
										<option value="MT">MT</option>
										<option value="NC">NC</option>
										<option value="ND">ND</option>
										<option value="NE">NE</option>
										<option value="NH">NH</option>
										<option value="NJ">NJ</option>
										<option value="NM">NM</option>
										<option value="NV">NV</option>
										<option value="NY">NY</option>
										<option value="OH">OH</option>
										<option value="OK">OK</option>
										<option value="OR">OR</option>
										<option value="PA">PA</option>
										<option value="RI">RI</option>
										<option value="SC">SC</option>
										<option value="SD">SD</option>
										<option value="TN">TN</option>
										<option value="TX">TX</option>
										<option value="UT">UT</option>
										<option value="VA">VA</option>
										<option value="VT">VT</option>
										<option value="WA">WA</option>
										<option value="WI">WI</option>
										<option value="WV">WV</option>
										<option value="WY">WY</option>
								  </select>
								   <p class="help-block"></p>
								 </div>
								</div>
							<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_billing_city'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="b_city" id="b_city" required placeholder="Enter Your City" value="'.$_SESSION["g_liship"][1][1]["s_city"].'"  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   <p class="help-block" id="city_help"></p>
								 </div>
							</div>
								 <div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_billing_zip'].'
								   </label>
								   <div class="controls">
									 <input type="text" id="b_zip" name="b_zip" class="required digits" required placeholder="Enter Your Zip Code" maxlength="5" minlength="5" value="'.$_SESSION["g_liship"][1][1]["s_zip"].'"  onchange="sanitize_sql(this); validate_zip(this, \''.$lang["validate_zip"].'\');" 
									 onblur="sanitize_sql(this); validate_zip(this, \''.$lang["validate_zip"].'\');" />
									 <p class="help-block"></p>
								   </div>
								</div>
								<div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_billing_phone'].'
								   </label>
								   <div class="controls">
									 <input type="text" id="b_phone" name="b_phone" class="required digits" required placeholder="Enter Your Phone Number" maxlength="10" minlength="10" value="'.$_SESSION["g_liship"][1][1]["s_phone"].'"  onchange="sanitize_sql(this); validate_us_phone(this, \''.$lang["validate_us_phone"].'\');" onblur="sanitize_sql(this); validate_us_phone(this, \''.$lang["validate_us_phone"].'\');" />
									 <p class="help-block"></p>
								   </div>
								</div>
								<div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_billing_email'].'
								   </label>
								   <div class="controls">
									 <input type="email" id="b_email" name="b_email" required placeholder="Enter Your Email" rel="'; echo $lang['cart_shipping_email_note'].'" value="'.$_SESSION["g_liship"][1][1]["s_email"].'"  onchange="sanitize_sql(this); validate_email(this, \''.$lang["validate_email"].'\');" onblur="sanitize_sql(this); validate_email(this, \''.$lang["validate_email"].'\');" />
									 <p class="help-block"></p>
								   </div>
								</div>
</div>
<input type="hidden" name="b_nprofit" value="'.$_SESSION["g_billing"][11].'">
<script language="javascript" type="text/javascript">
function pre_fill_address()
{
document.getElementById("b_fname").value = "'.$_SESSION["g_liship"][1][1]["s_name"].'";
document.getElementById("b_lname").value = "'.$_SESSION["g_liship"][1][1]["s_lname"].'";
document.getElementById("b_company").value = "'.$_SESSION["g_liship"][1][1]["s_company"].'";
document.getElementById("b_addr1").value = "'.$_SESSION["g_liship"][1][1]["s_addr1"].'";
document.getElementById("b_addr2").value = "'.$_SESSION["g_liship"][1][1]["s_addr2"].'";
document.getElementById("b_city").value = "'.$_SESSION["g_liship"][1][1]["s_city"].'";
document.getElementById("b_state").value = "'.$_SESSION["g_liship"][1][1]["s_state"].'";
document.getElementById("b_zip").value = "'.$_SESSION["g_liship"][1][1]["s_zip"].'";
document.getElementById("b_phone").value = "'.$_SESSION["g_liship"][1][1]["s_phone"].'";
document.getElementById("b_email").value = "'.$_SESSION["g_liship"][1][1]["s_email"].'";
}

function clear_address()
{
document.getElementById("b_fname").value = "";
document.getElementById("b_lname").value = "";
document.getElementById("b_company").value = "";
document.getElementById("b_addr1").value = "";
document.getElementById("b_addr2").value = "";
document.getElementById("b_city").value = "";
document.getElementById("b_state").selectedIndex = -1;
document.getElementById("b_zip").value = "";
document.getElementById("b_phone").value = "";
document.getElementById("b_email").value = "";
}
</script>
<hr>
';
}

?>