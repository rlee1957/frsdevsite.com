<?php


function bo_getbkqty_mobile() 
{ 

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
    
		
		// Check if a Logo Brick was selected - check str for the word LOGO
		$_SESSION["g_logo_name"] = "";  // Clear
		$_SESSION["g_logo_no"] = "";    // Clear
		$x_HoldBrickName = strtoupper($_SESSION["g_bkdesc"][$_POST["bktype"]]);
		
		if ($options["logo_options"])  {
			echo '
        	<TABLE class="table table-nonfluid table-condensed">
            <TR CLASS="tbl_grey" ALIGN="center">
            <TD class="tbl_head_txt"><FONT SIZE=3><b>'; echo $lang['cart_bo_header']; echo '</b></FONT><BR>
            <TABLE class="table">
                <TR BGCOLOR="#FFFFFF">
                <TD>
                <TABLE class="table">
                <TR ALIGN="center" CLASS="tbl_lightblue">
	        <TD WIDTH="1%"><FONT SIZE=2>'; echo $lang['cart_bo_header_type']; echo '</FONT></TD>
                 <TD WIDTH="10%"><FONT SIZE=2>'; echo $lang['cart_quantity']; echo '</FONT></TD>
	        <TD WIDTH="30%" ALIGN="left"><FONT SIZE=2>'; echo $lang['cart_description']; echo '</FONT></TD>
	        <TD WIDTH="20%"><FONT SIZE=2></FONT>'; echo $lang['cart_bo_logo_options']; echo '</TD>
	        <TD WIDTH="10%"><FONT SIZE=2>'; echo $lang['cart_price_each']; echo '</FONT></TD>
	        <TD WIDTH="10%"><FONT SIZE=2>'; echo $lang['cart_total']; echo '</FONT></TD>
                </TR>
	    	<TR ALIGN="center">
			<TD NOWRAP><FONT SIZE=2>'; echo $lang['cart_paver']; echo '</FONT></TD>
                    <TD NOWRAP><FONT SIZE=2><INPUT onfocus="this.select()" onchange="updateTotals()" TYPE="Text" NAME="x_pqty" VALUE="0" SIZE=2 MAXLENGTH=3></FONT></TD>
                    <TD NOWRAP ALIGN="left"><FONT SIZE=2>';  echo $_SESSION["g_bkdesc"][$_POST["bktype"]];  echo '
					
<!--<a style="text-decoration:none;" href="#logo_selection" rel="facebox" title="Displaying Images" ><img src="../'.$environment_path.'/img/choose_logo_16.gif" border="0" />--></a><!--Choose Logo-->
</FONT></TD>
                    <TD NOWRAP><FONT SIZE=1><input type="hidden" name="logoid" id="logoid"/><input type="text" name="logo_name" id="logo_name" style="border:none" SIZE="50" readonly="readonly" /></FONT></TD>
					<TD NOWRAP ALIGN="right"><FONT SIZE=2>'; echo $lang['published_currency_symbol']; echo '&nbsp;<INPUT TYPE="Text" NAME="x_pcost" VALUE=';  printf("%2.2f", $_SESSION["g_bkpcost"][$_POST["bktype"]]); echo ' SIZE=10 MAXLENGTH="20" DISABLED></FONT></TD>                    
                    <TD NOWRAP align="right"><FONT SIZE=1>'; echo $lang['published_currency_symbol']; echo '&nbsp;<INPUT TYPE="Text" NAME="x_ptotal" VALUE="0.00" SIZE=10 MAXLENGTH="20" DISABLED align=right></FONT></TD>
                </TR>';
		} else {
// Main Item
		    echo '
<div class="row">
<div class="span12" style="border:2px solid #CCC;">

<div class="row">
	<div class="span12" style="text-align:center; background-color:#CCC; color:#FFF;">
  			'.$lang['cart_paver'].'
	</div>
</div>
<div style="margin:5px;">
<div class="row" >
	<div class="span2">
		<h4><span class="label" style="font-size:18px;">'.$_SESSION["g_bkdesc"][$_POST["bktype"]].'</span></h4>
    </div>
    <div class="span3">
		<h5><span class="label" style="font-size:18px;">'.$lang['cart_quantity'].'</span></h5>
		<input class="input-small" onfocus="this.select()" onchange="updateTotals()" type="text" name="x_pqty" value="1" size="2" maxlength="3" READONLY>
    </div>
     <div class="span3">
	 <h5><span class="label" style="font-size:18px;">'.$lang['cart_price_each'].'</span></h5>
     	<span style="font-size:18px">'.$lang['published_currency_symbol'].'</span>&nbsp;<input class="input-small" type="text" name="x_pcost" value="'; printf("%2.2f", $_SESSION["g_bkpcost"][$_POST["bktype"]]); echo '" size="10" maxlength="20" DISABLED>
     </div>
	 <div class="span3">
	 <h5><span class="label" style="font-size:18px;">'.$lang['cart_total'].'</span></h5>
	 <span style="font-size:18px">'.$lang['published_currency_symbol'].'<span>&nbsp;<input class="input-small" type="text" name="x_ptotal" value="0.00" size="10" maxlength="20" DISABLED></p>
	 </div>
</div>
</div>
</div>
</div>';
// Replica
if ($options["enable_replica"]){
	echo '
<div class="row">
<div class="span12" style="border:2px solid #CCC;">

<div class="row">
	<div class="span12" style="text-align:center; background-color:#CCC; color:#FFF;">
  			'.$lang['cart_replica'].'
	</div>
</div>
<div style="margin:5px;">
<div class="row" >
	<div class="span2">
		<h4><span class="label" style="font-size:18px;">'.$_SESSION["g_bkrdesc"][$_POST["bktype"]].'</span></h4>
    </div>
    <div class="span3">
		<h5><span class="label" style="font-size:18px;">'.$lang['cart_quantity'].'</span></h5>
		<input class="input-small" onfocus="this.select()" onchange="updateTotals()" type="text" name="x_rqty" value="0" size="2" maxlength="3" READONLY>
    </div>
     <div class="span3">
	 <h5><span class="label" style="font-size:18px;">'.$lang['cart_price_each'].'</span></h5>
     	<span style="font-size:18px">'.$lang['published_currency_symbol'].'</span>&nbsp;<input class="input-small" type="text" name="x_rcost" value="'; printf("%2.2f", $_SESSION["g_bkrcost"][$_POST["bktype"]]); echo '" size="10" maxlength="20" DISABLED>
     </div>
	 <div class="span3">
	 <h5><span class="label" style="font-size:18px;">'.$lang['cart_total'].'</span></h5>
	 <span style="font-size:18px">'.$lang['published_currency_symbol'].'</span>&nbsp;<input class="input-small" type="text" name="x_rtotal" value="0.00" size="10" maxlength="20" DISABLED>
	 </div>
</div>
</div>
</div>
</div>';
	}
// total
echo '
<div class="row">
<div class="span12" style="border:2px solid #CCC;">

<div class="row">
	<div class="span12" style="text-align:center; background-color:#CCC; color:#FFF;">
  			'.$lang['cart_total'].'
	</div>
</div>
<div style="margin:5px;">
<div class="row" >
	<div class="span11">
		<h4><span class="label" style="font-size:18px;">'.$lang['cart_total'].'</span></h4>
		<input class="input-small" onfocus="MoveTo(2)" type="text" name="x_gtotal" value='; printf("%2.2f", $xx_grandtot); echo  '  SIZE="10" MAXLENGTH="30">
    </div>
</div>
</div>
</div>
</div>';
}
}


?>