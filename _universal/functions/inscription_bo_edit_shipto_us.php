<?php


function bo_edit_shipto($li) 
{
include("system/application/language/language_check.php");
include("configuration/misc_settings.php");
$_SESSION["g_currentli"] = $li;
$li_ship = $_SESSION["g_lishiptoqty"][$li] ;
echo '
<div class="row">
	<div class="span12">
		<div class="well">
			<h4>'.$lang['cart_shipping_replica_shipping'].'</h4>
			<p>'.$lang['cart_shipping_text1'].'</p>
        	<p><i><span class="txt_boldred">'; echo $lang['cart_shipping_text2']; echo '</span></i></p>
			';
			if ($lang['cart_shipping_replica_total'] != ""){
				echo '
            <p><strong>'; echo $lang['cart_shipping_replica_total']; 
			echo ': '; echo $_SESSION["g_lirqty"][$li];
            echo '</strong><p>';
			}
if ($_SESSION["g_liship"][1][1]["s_name"])
	{
	echo '
			<div class="span12" style="text-align:center; padding:15px;">
';
	if((isset($button["use_address"]))&&(!$button["use_address"])){  }
	else
		{
		echo('
				<input type="button" class="btn btn-custom" id="same_as_shipping" name="same_as_shipping" value="Use First Shipping Address" onclick="pre_fill_address();">
');
		}
	if((isset($button["clear_address"]))&&(!$button["clear_address"])){  }
	else
		{
		echo('				
				<input type="button" style="margin-left:10px;" class="btn btn-custom" id="same_as_shipping_clear" name="same_as_shipping_clear" value="Clear Form" onclick="clear_address();">
');
		}
	echo '
			</div>';
			}
echo '
		
			<div class="control-group">
								 <label class="control-label">
								 '.$lang['cart_shipping_prefix_name'].'
								 </label>
								 <div class="controls">
									<select class="selectpicker show-tick show-menu-arrow" name="s_prefix" id="s_prefix"
									value="'.$_SESSION["g_liship"][1][1]["s_prefix"].'" >';
$prefix = explode("|",$lang['cart_shipping_prefix_data']);
sort($prefix);
foreach ($prefix as $value)
	{
	echo '<option value="'.$value.'"> '.$value.'</option>';
	}
echo '
									</select>
									<p class="help-block"></p>
								</div>
							</div>
			<div class="control-group">
								   <label class="control-label">
								   	*'.$lang['cart_shipping_first_name'].'
								   </label>
								 <div class="controls">
								   <input type="text" name="s_name" id="s_name" placeholder="Enter Your First Name"
								   required  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);"
								   value="'.$_SESSION["g_liship"][1][1]["s_name"].'" />
								   <p class="help-block"></p>
								 </div>
			</div>
            
			<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_middle_name'].'
								 </label>
								   <div class="controls">
								   	<input type="text" name="s_mname" id="s_mname" placeholder="Enter Your Middle Name" value="'.$_SESSION["g_liship"][1][1]["s_mname"].'"  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   	<p class="help-block"></p>
								 </div>
			</div>
			
			<div class="control-group">
								 <label class="control-label">
								   *'.$lang['cart_shipping_last_name'].'
								 </label>
								 <div class="controls">
								   <input type="text" id="s_lname" name="s_lname" required placeholder="Enter Your Last Name" value="'.$_SESSION["g_liship"][1][1]["s_lname"].'"  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
			</div>
			
			<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_company'].'
								 </label>
								 <div class="controls">
								   <input type="text" id="s_company" name="s_company" placeholder="Enter Your Organization Name" value="'.$_SESSION["g_liship"][1][1]["s_company"].'"  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
			</div>

            <div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_address'].'
								 </label>
								 <div class="controls">
								   <input type="text" id="s_addr1" name="s_addr1" required placeholder="Enter Your Street Address" value="'.$_SESSION["g_liship"][1][1]["s_addr1"].'"  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
			</div>
			
			<div class="control-group">
								 <label class="control-label">
								 </label>
								 <div class="controls">
								   <input type="text" id="s_addr2" name="s_addr2" placeholder="Enter Additional Address" value="'.$_SESSION["g_liship"][1][1]["s_addr2"].'"  onchange="sanitize_sql(this);" onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
			</div>
            
            <div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_city'].'
								 </label>
								 <div class="controls">
								   <input type="text" id="s_city" name="s_city" required placeholder="Enter Your City" 
								   onchange="sanitize_sql(this);" onblur="sanitize_sql(this);"
								   value="'.$_SESSION["g_liship"][1][1]["s_city"].'" />
								   <p class="help-block"></p>
								 </div>
			</div>
			
			
			<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_state'].'
								 </label>
								 <div class="controls">
								   	<select class="selectpicker show-tick show-menu-arrow required" id="s_state" name="s_state" required
									value="'.$_SESSION["g_liship"][1][1]["s_state"].'" >
									  	<option value=""></option>
										<option value="AL">AL</option>
										<option value="AK">AK</option>
										<option value="AR">AR</option>
										<option value="AZ">AZ</option>
										<option value="CA">CA</option>
										<option value="CO">CO</option>
										<option value="CT">CT</option>
										<option value="DC">DC</option>
										<option value="DE">DE</option>
										<option value="FL">FL</option>
										<option value="GA">GA</option>
										<option value="HI">HI</option>
										<option value="IA">IA</option>
										<option value="ID">ID</option>
										<option value="IL">IL</option>
										<option value="IN">IN</option>
										<option value="KS">KS</option>
										<option value="KY">KY</option>
										<option value="LA">LA</option>
										<option value="MA">MA</option>
										<option value="MD">MD</option>
										<option value="ME">ME</option>
										<option value="MI">MI</option>
										<option value="MN">MN</option>
										<option value="MO">MO</option>
										<option value="MS">MS</option>
										<option value="MT">MT</option>
										<option value="NC">NC</option>
										<option value="ND">ND</option>
										<option value="NE">NE</option>
										<option value="NH">NH</option>
										<option value="NJ">NJ</option>
										<option value="NM">NM</option>
										<option value="NV">NV</option>
										<option value="NY">NY</option>
										<option value="OH">OH</option>
										<option value="OK">OK</option>
										<option value="OR">OR</option>
										<option value="PA">PA</option>
										<option value="RI">RI</option>
										<option value="SC">SC</option>
										<option value="SD">SD</option>
										<option value="TN">TN</option>
										<option value="TX">TX</option>
										<option value="UT">UT</option>
										<option value="VA">VA</option>
										<option value="VT">VT</option>
										<option value="WA">WA</option>
										<option value="WI">WI</option>
										<option value="WV">WV</option>
										<option value="WY">WY</option>
								  </select>
								  <div><small>'.$lang['cart_all_orders_taxed_text'].'</small></div>
								   <p class="help-block"></p>
								 </div>
				</div>

           <div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_shipping_zip'].'
								   </label>
								   <div class="controls">
									 <input type="text" id="s_zip" name="s_zip" class="required digits" required placeholder="Enter Your Zip Code" maxlength="5" minlength="5" value="'.$_SESSION["g_liship"][1][1]["s_zip"].'"  onchange="sanitize_sql(this); validate_zip(this, \''.$lang["validate_zip"].'\');" 
									 onblur="sanitize_sql(this); validate_zip(this, \''.$lang["validate_zip"].'\');" />
									 <p class="help-block"></p>
								   </div>
			</div>
			
			<div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_shipping_daytime_phone'].'
								   </label>
								   <div class="controls">
									 <input type="test" id="s_phone" name="s_phone" class="required digits" required placeholder="Enter Your Phone Number" maxlength="10" minlength="10" value="'.$_SESSION["g_liship"][1][1]["s_phone"].'"  onchange="sanitize_sql(this); validate_us_phone(this, \''.$lang["validate_us_phone"].'\');" onblur="sanitize_sql(this); validate_us_phone(this, \''.$lang["validate_us_phone"].'\');" />
									 <p class="help-block"></p>
								   </div>
			</div>

            <div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_shipping_email'].'
								   </label>
								   <div class="controls">
									 <input type="email" id="s_email" name="s_email" required placeholder="Enter Your Email" value="'.$_SESSION["g_liship"][1][1]["s_email"].'"  onchange="sanitize_sql(this); validate_email(this, \''.$lang["validate_email"].'\');" onblur="sanitize_sql(this); validate_email(this, \''.$lang["validate_email"].'\');" />
									 <div><small>'.$lang['cart_shipping_email_note'].'</small></div>
									 <p class="help-block"></p>
								   </div>
			</div>

		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
function pre_fill_address()
{
document.getElementById("s_prefix").value = "'.$_SESSION["g_liship"][1][1]["s_prefix"].'";
document.getElementById("s_name").value = "'.$_SESSION["g_liship"][1][1]["s_name"].'";
document.getElementById("s_mname").value = "'.$_SESSION["g_liship"][1][1]["s_mname"].'";
document.getElementById("s_lname").value = "'.$_SESSION["g_liship"][1][1]["s_lname"].'";
document.getElementById("s_company").value = "'.$_SESSION["g_liship"][1][1]["s_company"].'";
document.getElementById("s_addr1").value = "'.$_SESSION["g_liship"][1][1]["s_addr1"].'";
document.getElementById("s_addr2").value = "'.$_SESSION["g_liship"][1][1]["s_addr2"].'";
document.getElementById("s_city").value = "'.$_SESSION["g_liship"][1][1]["s_city"].'";
document.getElementById("s_state").value = "'.$_SESSION["g_liship"][1][1]["s_state"].'";
document.getElementById("s_zip").value = "'.$_SESSION["g_liship"][1][1]["s_zip"].'";
document.getElementById("s_phone").value = "'.$_SESSION["g_liship"][1][1]["s_phone"].'";
document.getElementById("s_email").value = "'.$_SESSION["g_liship"][1][1]["s_email"].'";
}

function clear_address()
{
document.getElementById("s_prefix").selectedIndex = -1;
document.getElementById("s_name").value = "";
document.getElementById("s_mname").value = "";
document.getElementById("s_lname").value = "";
document.getElementById("s_company").value = "";
document.getElementById("s_addr1").value = "";
document.getElementById("s_addr2").value = "";
document.getElementById("s_city").value = "";
document.getElementById("s_state").selectedIndex = -1;
document.getElementById("s_zip").value = "";
document.getElementById("s_phone").value = "";
document.getElementById("s_email").value = "";
}
</script>
';
}


?>