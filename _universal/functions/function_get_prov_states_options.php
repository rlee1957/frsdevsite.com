<?php

function get_prov_states_options($name, $placeholder, $value, $required, $onchange)
{
include("configuration/environment_settings.php");
include("../".$environment_path."/location/country_locations.php");
$value = strtoupper($value);
$reTVal = "";
if(isset($country_locations[$_SESSION["country"]]))
	{
	$sel = "";
	if($value == ""){ $sel = ' selected'; }
	$reTVal = '
					<select class="selectpicker show-tick show-menu-arrow required"
							id="'.$name.'" 
							name="'.$name.'" 
							'.$required.'
							value="'.$value.'">
						<option value=""'.$sel.'> - Please Select - </option>
';
	foreach($country_locations[$_SESSION["country"]] as $key => $ar)
		{
		$abr = $key;
		$val = "";
		if(isset($country_locations[$_SESSION["country"]][$_SESSION["language"]]))
			{
			$val = 	$ar[$_SESSION["language"]];
			}
		else
			{
			$val = 	$ar["english"];	
			}
		$sel = "";
		if(strtolower($key) == strtolower($value)){ $sel = " selected"; }
		$reTVal .= '
						<option value="'.$key.'"'.$sel.'>'.$val.'</option>';
		}
	$reTVal .= '
					</select>';
	}
else
	{
	$reTVal = '
					<input type="text" 
						   id="'.$name.'" 
						   name="'.$name.'" 
						   '.$required.' 
						   placeholder="'.$placeholder.'"
						   onchange="'.$onchange.'"
						   onblur="'.$onchange.'"
						   value="'.$value.'"
						   maxlength="255" />';	
	}
return $reTVal;
}

?>