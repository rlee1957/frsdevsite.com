<?php


function bo_confirmbkqty() 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
$xx_ptotal = get_current_paver_total();
$xx_rtotal = get_current_replica_total();
$xx_r2total = get_current_replica2_total();
$xx_dctotal = get_current_display_case_total();
$xx_dc2total = get_current_display_case2_total();
$xx_ptotal_tax = get_current_paver_tax_total();
$xx_rtotal_tax = get_current_replica_tax_total();
$xx_r2total_tax = get_current_replica2_tax_total();
$xx_dctotal_tax = get_current_display_case_tax_total();
$xx_dc2total_tax = get_current_display_case2_tax_total();
$xx_ototal = 0;
$li = $_SESSION["g_currentli"];
$li_ship = $_SESSION["g_lishiptoqty"][$li] + 0;
$_SESSION["g_lishiptoqty"][$li] = $li_ship;
$lineitem_tax = $_SESSION["g_liship"][$li][$li_ship]["s_state"];
$total_without_tax = $xx_ptotal + $xx_rtotal + $xx_r2total + $xx_dctotal + $xx_dc2total;
$tax = $xx_ptotal_tax + $xx_rtotal_tax + $xx_r2total_tax + $xx_dctotal_tax + $xx_dc2total_tax; 
$xx_rtaxtotal = $tax;
$xx_subtot = $tax + $total_without_tax;	
$xx_grandtot = $xx_subtot;
$_SESSION["g_taxtotal"] = $xx_rtaxtotal;

// Check if a Logo Brick was selected - check str for the word LOGO
$x_HoldBrickName = strtoupper($_SESSION["g_bkdesc"][ $_SESSION["g_currentbktype"]]);
	
echo '
<div class="row">
	<div class="span12">
		<div class="well">
			<div class="pagination-centered">
				<h4>'.$lang['cart_bo_header_confirm'].'</h4>
			</div>
		</div>
	</div>
</div>
<div class="well">
	<div class="row">
		<div class="'.$_SESSION["g_bk_span"][$_SESSION["g_currentbktype"]].'">
			<h4>'.$lang['selected_customize_options'].'</h4>
			<small>'.$x_HoldBrickName.'</small>
			<div>
				<img src="images/'.$_SESSION["g_bkimg"][ $_SESSION["g_currentbktype"]].'" class="img-rounded">
			</div>
			<h6>'.$lang['cart_paver'].' '.$lang['cart_quantity'].'</h6>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">#</strong>
				</span>
				<input class="input-small" type="Text" value="'.$_SESSION["g_currentpqty"].'" readonly>
			</div>';
if ($options["logo_options"])  
	{
	echo '
			<div>
				<h6>'.$lang['cart_bo_logo_options_selected'].'</h6>
				<img style="img-polaroid" src="logos/'.$_SESSION["g_lilogoname"][$li].'.png" alt="'.$_SESSION["g_lilogoname"][$li].'">
			</div>';
	}
echo '
			<h6>'.$lang['cart_paver'].' '.$lang['cart_price_each'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">'.$lang['published_currency_symbol'].'</strong>
				</span>
				<input class="input-small" type="Text" id="x_pcost" name="x_pcost" value="';  printf("%2.2f", $_SESSION["g_bkpcost"][$_SESSION["g_currentbktype"]]); echo '" readonly />
			</div>
			<h6>'.$lang['cart_paver'].' '.$lang['cart_subtotal'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small subtotal" type="Text" id="x_ptotal" name="x_ptotal" value="';
printf("%2.2f", $xx_ptotal); 
echo '" readonly />
			</div>
		</div>
';

// Replica
if ($options["enable_replica"] && $_SESSION["g_currentrqty"] > 0)
	{		
	echo '
		<div class="'.$_SESSION["g_bkr_span"][$li].'">
			<h4>'.$lang['selected_replica'].'</h4>
			<small>'.$_SESSION["g_bkrdesc"][$_SESSION["g_currentbktype"]].'</small>
			<div>
				<img src="images/'.$_SESSION["g_bkrimg"][ $_SESSION["g_currentbktype"]].'" class="img-rounded">
			</div>
			<h6>'.$lang['cart_replica'].' '.$lang['cart_quantity'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">#</strong>
				</span>
				<input class="input-small" type="Text" value="'.$_SESSION["g_currentrqty"].'" readonly />
			</div>
			<h6>'.$lang['cart_replica'].' '.$lang['cart_price_each'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">'.$lang['published_currency_symbol'].'</strong>
				</span>
				<input class="input-small" type="Text" id="x_rcost" name="x_rcost" value="';  
	printf("%2.2f", $_SESSION["g_bkrcost"][$_SESSION["g_currentbktype"]]); 
	echo '" readonly />
			</div>
			<h6>'.$lang['cart_replica'].' '.$lang['cart_subtotal'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small subtotal" type="Text" id="x_rtotal" name="x_rtotal" value="';
	printf("%2.2f", $xx_rtotal); 
	echo '" readonly />
			</div>
		</div>';
}

// Display Cases
if ($options["enable_displaycase"] && $_SESSION["g_currentdcqty"] > 0)
	{
	echo '
		<div class="'.$_SESSION["g_bkdc_span"][$li].'">
			<h4>'.$lang['selected_dc'].'</h4>
			<small>'.$_SESSION["g_bkdcdesc"][$_SESSION["g_currentbktype"]].'</small>
			<div>
				<img src="images/'.$_SESSION["g_bkdcimg"][ $_SESSION["g_currentbktype"]].'" class="img-rounded" />
			</div>
			<h6>'.$lang['cart_display_case'].' '.$lang['cart_quantity'].'</h6>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">#</strong></span>
				<input class="input-small" type="Text" value="'.$_SESSION["g_currentdcqty"].'" readonly />
			</div>
			<h6>'.$lang['cart_display_case'].' '.$lang['cart_price_each'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small" type="Text" id="x_dccost" name="x_dccost" value="';  
	printf("%2.2f", $_SESSION["g_bkdccost"][$_SESSION["g_currentbktype"]]); 
	echo '" readonly />
			</div>
			<h6>'.$lang['cart_display_case'].' '.$lang['cart_subtotal'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small subtotal" type="Text" id="x_dctotal" name="x_dctotal" value="'; 
	printf("%2.2f", $xx_dctotal); 
	echo '" readonly />
			</div>
		</div>';
	}
// Replica 2
if ($options["enable_replica2"] && $_SESSION["g_currentr2qty"] > 0)
	{	
	echo '
		<div class="'.$_SESSION["g_bkr2_span"][$li].'">
			<h4>
				'.$lang['add_your_replica'].' 
				<small>
					'.$_SESSION["g_bkr2desc"][$_SESSION["g_currentbktype"]].'
				</small>
			</h4>
			<div>
				<img src="images/'.$_SESSION["g_bkr2img"][ $_SESSION["g_currentbktype"]].'" class="img-rounded" />
			</div>
			<h6>'.$lang['cart_replica2'].' '.$lang['cart_quantity'].'</h6>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">#</strong></span>
				<input class="input-small" type="Text" value="'.$_SESSION["g_currentr2qty"].'" readonly />
			</div>
			<h6>'.$lang['cart_replica2'].' '.$lang['cart_price_each'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">'.$lang['published_currency_symbol'].'</strong>
				</span>
				<input class="input-small" type="Text" id="x_r2cost" name="x_r2cost" value="';  printf("%2.2f", $_SESSION["g_bkr2cost"][$_SESSION["g_currentbktype"]]); echo '" readonly />
			</div>
			<h6>'.$lang['cart_replica2'].' '.$lang['cart_subtotal'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small subtotal" type="Text" id="x_r2total" name="x_r2total" value="'; 
	printf("%2.2f", $xx_r2total); 
	echo '" readonly />
			</div>
		</div>
';
	}

// Display Case 2
if ($options["enable_displaycase2"] && $_SESSION["g_currentdc2qty"] > 0)
	{	
	echo '
		<div class="'.$_SESSION["g_bkdc2_span"][$li].'">
			<h4>
				'.$lang['selected_dc'].' 
				<small style="font-weight: normal; font-color: black;">
					'.$_SESSION["g_bkdc2desc"][$_SESSION["g_currentbktype"]].'
				</small>
			</h4>
			<div>
				<img src="images/'.$_SESSION["g_bkdc2img"][ $_SESSION["g_currentbktype"]].'" class="img-rounded" />
			</div>
			<h6>'.$lang['cart_display_case2'].' '.$lang['cart_quantity'].'</h6>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">#</strong></span>
				<input class="input-small" type="Text" value="'.$_SESSION["g_currentdc2qty"].'" readonly />
			</div>
			<h6>'.$lang['cart_display_case2'].' '.$lang['cart_price_each'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small" type="Text" id="x_dc2cost" name="x_dc2cost" value="';  
	printf("%2.2f", $_SESSION["g_bkdc2cost"][$_SESSION["g_currentbktype"]]); 
	echo '" readonly />
			</div>
			<h6>'.$lang['cart_display_case2'].' '.$lang['cart_subtotal'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small subtotal" type="Text" id="x_dc2total" name="x_dc2total" value="'; printf("%2.2f", $xx_dc2total); echo '" readonly />
			</div>
		</div>
';
	}
// Close out Well
echo '
	</div>
</div>
';
echo '
<div class="row">
	<div class="span12">
		<div class="well">
';
if($options["apply_sales_tax"])
	{
	echo '
			<h4>'.$lang['cart_tax'].':</h4>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">'.$lang['published_currency_symbol'].'</strong></span>
				<input class="input-small" type="Text" id="x_rtax" name="x_rtax" value="';  printf("%2.2f", $xx_rtaxtotal); echo '" readonly>
			</div>
';
	}
echo '
			<h4>'.$lang['cart_subtotal'].':</h4>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">'.$lang['published_currency_symbol'].'</strong></span>
				<input class="input-small" type="Text" id="x_gtotal" name="x_gtotal" value="';  printf("%2.2f", $xx_grandtot); echo '" readonly>
			</div>
		</div>
	</div>
</div>
 ';
}


?>