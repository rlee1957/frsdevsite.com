<?php

function bo_is_gc($enabled = 0) 
{

global $page;	
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");

if ($options["gift_certificates"]) 
	{
	$p_li   = $_SESSION["g_currentli"];
	include("../".$environment_path."/includes/inscription_is_gc_enable.php");
	echo '
	    <div class="row">
		    <div class="span12">
			    <div class="well">
				    <h4>'.$lang['cart_gift_certificate_option_header'].'</h4>
				    <p>
						<label class="checkbox">
							<input type="checkbox" 
								   id="cb_gc" 
								   name="cb_gc" 
								   value="1"
								   '.$enabled.'/> 
							'.$lang['cart_gift_certificate_option_text'].'
						</label>
					</p>
			  </div>
		  </div>
	  </div>
<script type="text/javascript">

$(\'#cb_gc\').change(function() {
$(\'#inscription\').toggle(\'fast\', function() {
$(\'#line1\').val(\'Gift Cert\');
//Animation complete.
});
$(\'#location\').toggle(\'fast\', function() {
$(\'#brick_location\').val(\'\');
//Animation complete.
});
});
</script>
';
	}
}

?>