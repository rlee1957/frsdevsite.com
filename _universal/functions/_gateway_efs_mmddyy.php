<?php
function efs_mmddyy($p1) 
{

    $p1 = trim($p1);
    $p1_mm = substr($p1,2,2);
    $p1_dd = substr($p1,4,2);
    $p1_yy = substr($p1,0,2);

    $p1_return = $p1_mm . "-". $p1_dd . "-" . $p1_yy ;
    return $p1_return;
}

?>