<?php


function bo_displaybillto() 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
    echo '
    <TABLE CELLPADDING=0 CELLSPACING=8 BORDER=0 WIDTH="100%">
    <TR CLASS="tbl_grey" ALIGN="center">
    <TD><FONT SIZE=3><b>'; echo $lang['cart_billing_information']; echo '</b></FONT><BR>
        <TABLE CELLPADDING=0 CELLSPACING=2 BORDER=0 WIDTH="100%">
        <TR BGCOLOR="#FFFFFF">
        <TD>

            <table width="555" align="center" border="1"  cellspacing="2">
            <table width="550" align="center" border="0" cellspacing="5">

            <tr><td width="50" align="right">';
			echo $lang['cart_billing_first_name']; echo ':</td>
            <td>';  echo $_SESSION["g_billing"][1];   echo ' </td>
            </tr>

            <tr><td width="50" align="right">';
			echo $lang['cart_billing_last_name']; echo ':</td>
            <td>';  echo $_SESSION["g_billing"][2];   echo ' </td>
            </tr>

            <tr><td width="50" align="right">';
			echo $lang['cart_billing_company']; echo ':</td>
            <td>';  echo $_SESSION["g_billing"][3];   echo '</td>
            </tr>
            <tr><td width="50" align="right">';
			echo $lang['cart_billing_address']; echo ':</td>
            <td>';  echo $_SESSION["g_billing"][4];   echo ' </td>

            </tr>
            <tr><td width="50" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>';  echo $_SESSION["g_billing"][5];   echo '</td>
            </tr>
            <tr><td width="50" align="right">';
			echo $lang['cart_billing_city']; echo ':</td>
            <td>';  echo $_SESSION["g_billing"][6];   echo '</td>
            </tr>

            <tr><td width="10" align="right">';
			echo $lang['cart_billing_state']; echo ':</td>
            <td>';  echo $_SESSION["g_billing"][7];   echo ' </td>
            </tr>

            <tr><td width="20" align="right">';
			echo $lang['cart_billing_zip']; echo ':</td>
            <td>';  echo $_SESSION["g_billing"][8];   echo '</td>
            </tr>

            <tr><td width="50" align="right">';
			echo $lang['cart_billing_phone']; echo ':</td>
            <td>';  echo $_SESSION["g_billing"][9];   echo ' </td>
            </tr>

            <tr><td width="50" align="right">';
			echo $lang['cart_billing_email']; echo ':</td>
            <td>';  echo $_SESSION["g_billing"][10];   echo ' </td>
            </tr>
	    
	    <tr><td width="50" align="right">NonProfit:</td>
            <td>';  echo $_SESSION["g_billing"][11];   echo ' </td>
            </tr>

            </table>
            </table>

        </TD>
        </TR>
        </TABLE>
     </TD>
    </TR>
    </TABLE>

';
}


?>