<?php

function Style_Sheets()
{
echo '
<STYLE TYPE="text/css">

body{background: url("")

background-repeat:repeat; background-size:contain;}
 
.tbl_white  { background-color: #FFFFFF; }

.tbl_grey       { background-color: #CCCCCC; }

.tbl_lightblue  { background-color: #D5DBED; }

.txt_boldred  { font-weight: bold; color: #EA1C1C; }

.txt_boldblack  { font-weight: bold; color: #000000; }

.txt_orderform { font-weight: bold; color: #937E69; }
    
td 
{
font-family: "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
font-size: 12px;
}

.copyright {font-size: 10px;}

.head_wht 
{
font-family: "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
font-size: 18px;
font-weight: bold;
color: #FFFFFF;
}

.head_sm_wht 
{
font-family: "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
font-size: 12px;
font-weight: bold;
color: #FFFFFF;
}

.head_blue 
{
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
font-weight: bold;
color: #242250;
}

.tbl_head_txt 
{
font-family: "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
font-size: 14px;
font-weight: bold;
color: #242250;
}

.tbl_reg_txt 
{
font-family: "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
font-size: 14px;
font-weight: bold;
color: #242250;
background-color: #FFFFFF;
}

.td_prices 
{
font-family: "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
font-size: 14px;
font-weight: bold;
text-align:right;
color: #242250;
background-color: #FFFFFF;
}

.txt_cost 
{
font-family: "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
font-size: 12px;
font-weight: bold;
color: #000000;
}

.taxdescription 
{
font-family: "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
font-style: italic;
font-size:9px;
text-align:right;
background:#FFFFFF;
}

.grid input {width:10px;}

.qty_items input {width:20px;}

.total_amounts input {width:30px;}

.input-mini,input.mini,textarea.mini,select.mini{width:60px;}

.input-small,input.small,textarea.small,select.small{width:90px;}

.input-medium,input.medium,textarea.medium,select.medium{width:150px;}

.input-large,input.large,textarea.large,select.large{width:210px;}

.input-xlarge,input.xlarge,textarea.xlarge,select.xlarge{width:270px;}

.input-xxlarge,input.xxlarge,textarea.xxlarge,select.xxlarge{width:330px;}

.txtSelection{font-size:12px; font-weight:bold; text-align:right;}

a.txtFooter:link {color:#000;}

a.txtFooter:visited {color:#000;}

a.txtFooter:hover {color:#FFF;}

a.txtFooter:active {color:#000;}

.table-nonfluid {width: auto;}

</STYLE>
';
}

?>