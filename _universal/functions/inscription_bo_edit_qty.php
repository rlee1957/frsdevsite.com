<?php

function bo_edit_qty($li) 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
$_SESSION["g_currentli"] = $li;
$li_ship = $_SESSION["g_lishiptoqty"][$li] ;
echo '
<div class="container">
	<div class="row">
		<div class="span12">
			<div class="well">
				<h4>'.$lang['cart_edit'].' '.$_SESSION["g_bkdesc"][$_SESSION["g_libktype"][$li]].'</h4>
				<h4>'.$lang['cart_quantity_header'].'</h4>
				<div class="control-group">
								   <label class="control-label">
								   '.$lang['cart_order_edit_paver_quantity'].'
									 </label>
				  <div class="controls">
					<select class="selectpicker show-tick show-menu-arrow" id="p_qty" name="p_qty">
						<option value="'.$_SESSION["g_lipqty"][$li].'" selected="selected">'.$_SESSION["g_lipqty"][$li].'</option>
					</select>
					  <p class="help-block"></p>
				  </div>
				</div>
					';
// Replica			
if ($options["enable_replica"])
	{
	echo '
		<div class="control-group">
			<label class="control-label">'
            	.$lang['cart_order_edit_replica_quantity']. '
			</label>
			<div class="controls">
					<select class="selectpicker show-tick show-menu-arrow" id="r_qty" name="r_qty"
							value="'.$_SESSION["g_currentrqty"].'">
';
	$sf = 0;
	if(isset($_SESSION["g_currentrqty"])){ $sf = $_SESSION["g_currentrqty"]; }
	for($x=0;$x<11;$x++)
		{
		$sel = "";
		if($sf == $x){ $sel = " selected"; }
		echo'
						<option value="'.$x.'"'.$sel.'>'.$x.'</option>
';
		}
	echo'
					</select>
			</div>
		</div>';
	}
// Display Case
if ($options["enable_displaycase"])
	{
	echo '
            <div class="control-group">
				<label class="control-label">'
					.$lang['cart_order_edit_display_case_quantity'].'
				</label>
				<div class="controls">
					<select class="selectpicker show-tick show-menu-arrow" id="dc_qty" name="dc_qty"
							value="'.$_SESSION["g_currentdcqty"].'">
';
	$sf = 0;
	if(isset($_SESSION["g_currentdcqty"])){ $sf = $_SESSION["g_currentdcqty"]; }
	for($x=0;$x<11;$x++)
		{
		$sel = "";
		if($sf == $x){ $sel = " selected"; }
		echo'
						<option value="'.$x.'"'.$sel.'>'.$x.'</option>
';
		}
	echo'
					</select>
				</div>
			</div>';
	}
// Replica 2
if ($options["enable_replica2"])
	{
	echo '
            <div class="control-group">
				<label class="control-label">'.$lang['cart_order_edit_replica_quantity'].'</label>
				<div class="controls">
					<select class="selectpicker show-tick show-menu-arrow" id="r2_qty" name="r2_qty"
							value="'.$_SESSION["g_currentr2qty"].'" required>
';
	$sf = 0;
	if(isset($_SESSION["g_currentr2qty"])){ $sf = $_SESSION["g_currentr2qty"]; }
	for($x=0;$x<11;$x++)
		{
		$sel = "";
		if($sf == $x){ $sel = " selected"; }
		echo'
						<option value="'.$x.'"'.$sel.'>'.$x.'</option>
';
		}
	echo'
					</select>
				</div>
			</div>
';
	}
// Display Case 2
if ($options["enable_displaycase2"])
	{
	echo '
            <div class="control-group">
				<label class="control-label">'.$lang['cart_order_edit_display_case2_quantity'].'</label>
					<div class="controls">
						<select class="selectpicker show-tick show-menu-arrow" id="dc2_qty" name="dc2_qty" required>
';
	$sf = 0;
	if(isset($_SESSION["g_currentdc2qty"])){ $sf = $_SESSION["g_currentdc2qty"]; }
	for($x=0;$x<11;$x++)
		{
		$sel = "";
		if($sf == $x){ $sel = " selected"; }
		echo'
						<option value="'.$x.'"'.$sel.'>'.$x.'</option>
';
		}
	echo'
					</select>
					</div>
			</div>
            ';
	}
echo '
			</div>
		</div>
	</div>
</div>
';
$x_HoldBrickName = strtoupper($_SESSION["g_bkdesc"][$_SESSION["g_libktype"][$li]]);
if ($options["logo_options"])  
	{
	echo '
<div class="row">
	<div class="span12">
		<div class="well">
		  <h4>'.$lang['cart_bo_logo_options'].'</h4>
		  <div>
			  <select class="image-picker" name="logo_name" id="logo_name">';
	foreach(glob('logos/*.png*') as $filename)
		{
		$name1 = explode('/',$filename);
		$name2 = explode('.',$name1[1]);
		$name3 = $name2[0];
		if ($_SESSION["g_lilogoname"][$li] == true)
			{
			if ($name3 == $_SESSION["g_lilogoname"][$li])
				{
				echo '<option selected data-img-src="'.$filename.'" value="'.$name3.'" label="'.$name3.'">'.$name3.'</option>';
				}
			else
				{
				echo '<option data-img-src="'.$filename.'" value="'.$name3.'" label="'.$name3.'">'.$name3.'</option>';
				}
			}

		else
			{
			if ($name3 == $_SESSION["g_logo_display_default"])
				{
				echo '<option selected data-img-src="'.$filename.'" value="'.$name3.'" label="'.$name3.'">'.$name3.'</option>';
				}
			else
				{
				echo '<option data-img-src="'.$filename.'" value="'.$name3.'" label="'.$name3.'">'.$name3.'</option>';
				}
			}
		}
	echo'
			  </select>
		  </div>
		</div>
	</div>
</div>
';
	}
}

?>