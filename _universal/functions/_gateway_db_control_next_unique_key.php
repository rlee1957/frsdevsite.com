<?php

function db_control_next_unique_key($dbcon) 
{
	
include("configuration/environment_settings.php");
$rs1 = 0;
$rs2 = 0;
$p_msg = "";
include("../".$environment_path."/sql/select_from_control.php");
pg_send_query($dbcon, $sqry);
$rs1 = pg_get_result($dbcon);
if (pg_result_error($rs1)) 
	{
	$p_msg = "db_control_next_unique_key:select";
	err_log(pg_result_error($rs1) . ": " . $p_msg);		
	echo "An Error occurred in DB_CONTROL_NEXT_UNIQUE_KEY \n";	
	exit;
    }
$current_unique_key = 0;
$current_unique_key = pg_result($rs1, 0, 1);
$current_unique_key = $current_unique_key + 1;
if($current_unique_key > 9999)
	{
	$current_unique_key = 1000;	
	}
include("../".$environment_path."/sql/update_control.php");
pg_send_query($dbcon, $sqry);
$rs2 = pg_get_result($dbcon);
if (pg_result_error($rs2)) 
	{
	$p_msg = "db_control_next_unique_key:update";
	err_log(pg_result_error($rs2) . ": " . $p_msg);	
	echo "An Error occurred in DB_CONTROL_NEXT_UNIQUE_KEY:Update control table \n";
	exit;
    }
$current_unique_key = "0000".$current_unique_key;
$current_unique_key = substr($current_unique_key, -4, 4);
return $current_unique_key;

}

?>