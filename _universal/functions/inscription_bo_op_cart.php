<?php

function bo_op_cart() 
{
	
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
// Set order total to zero
$_SESSION["g_ordertotal"] = 0;
$_SESSION["g_rordertotal"] = 0;
$_SESSION["g_op_ship_total"] = 0.00;	
// Reset Payment vars
$_SESSION["g_payplan"] = 0;
$_SESSION["g_totalbk_pay1"] = 0.00; 
$_SESSION["g_totalbk_pay2"] = 0.00;
$_SESSION["g_totalbk_pay3"]= 0.00;
$_SESSION["g_totalbkr_pay1"] = 0.00; 
$_SESSION["g_totalbkr_pay2"] = 0.00;
$_SESSION["g_totalbkr_pay3"]= 0.00;	
$_SESSION["g_totalop_pay1"] = 0.00; 
$_SESSION["g_totalop_pay2"] = 0.00;
$_SESSION["g_totalop_pay3"]= 0.00;
$_SESSION["g_totalcost_pay1"] = 0.00;    // Accumulate all totals for payment plan 1
$_SESSION["g_totalcost_pay2"] = 0.00;	// Accumulate all totals for paymnet plan 2
$_SESSION["g_totalcost_pay3"]= 0.00;	// Accumulate all totals for payment plan 3
echo '
    <TABLE CELLPADDING=0 CELLSPACING=8 BORDER=0 WIDTH="100%" style="padding-top:10px;">
    <TR CLASS="tbl_grey" ALIGN="center">
    <TD class="tbl_head_txt"><FONT SIZE=3><b>';
echo $lang['your_shopping_cart'];
echo '</b></FONT><BR>
    <TABLE CELLPADDING=0 CELLSPACING=2 BORDER=0 WIDTH="100%">
    <TR BGCOLOR="#FFFFFF">
    <TD>
    <TABLE CELLPADDING=0 CELLSPACING=3 BORDER=0 WIDTH="100%">
';
for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
	{
	if (isset($_SESSION["g_libktype"][$ii])) 
		{
		bo_cart_line_heading();
		bo_cart_paver($ii);
		if ($options["enable_replica"])
			{
            bo_cart_replica($ii);
			}
		if ($options["enable_replica2"])
			{
            bo_cart_replica2($ii);
			}
		if ($options["enable_displaycase"])
			{
            bo_cart_display_case($ii);
			}
		if ($options["enable_displaycase2"])
			{
            bo_cart_display_case2($ii);
			}
		bo_cart_blue_line();
		bo_cart_inscr($ii, $_SESSION["g_libktype"][$ii]);
        }
    }
// Display Options
for ($ii = 1; $ii <= $_SESSION["g_op_no_li"]; $ii++)
	{
	if (isset($_SESSION["g_op_li"][$ii]["op_type"])) 
		{		
		bo_cart_line_heading();
		bo_op_cart_display($ii);
        }
    }
$xx_subtot = $_SESSION["g_ordertotal"];
$xx_taxrate = $_SESSION["service_objects_totaltaxrate"];
$xx_taxtot = round(($_SESSION["g_taxtotal"]),2);
$xx_grandtot = round(($xx_taxtot + $xx_subtot),2);
if ($options["web_convenience_fee"]) 
	{		
	$_SESSION["g_ordertotal"] = $xx_grandtot + $_SESSION["convenience_fee"];
	} 
else 
	{	
	$_SESSION["g_ordertotal"] = $xx_grandtot;
	}
$_SESSION["g_ordertotal"]  =  $_SESSION["g_ordertotal"] - $_SESSION["g_gc_redeem_amount"];
$xx_shipping_tot = $_SESSION["g_op_ship_total"];
$_SESSION["g_totalcost_pay1"] = $_SESSION["g_totalbk_pay1"] + $_SESSION["g_totalbkr_pay1"] + $_SESSION["g_totalop_pay1"] + $_SESSION["convenience_fee"] + $_SESSION["g_taxtotal"];     
$_SESSION["g_totalcost_pay2"] = $_SESSION["g_totalbk_pay2"] + $_SESSION["g_totalbkr_pay2"] + $_SESSION["g_totalop_pay2"];  
$_SESSION["g_totalcost_pay3"]= $_SESSION["g_totalbk_pay3"] + $_SESSION["g_totalbkr_pay3"] + $_SESSION["g_totalop_pay3"];  	
echo '
    </TABLE>
    </TD>
    </TR>
    </TABLE> ';
bo_cart_bottom();
echo '
     </TD>
</TR>
</TABLE>
 ';
}

?>