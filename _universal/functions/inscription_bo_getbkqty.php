<?php

function bo_getbkqty() 
{
	
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
$_SESSION["g_logo_name"] = "";
$_SESSION["g_logo_no"] = "";
$x_HoldBrickName = strtoupper($_SESSION["g_bkdesc"][$_POST["bktype"]]);
$note = '
					<hr />
					<strong>'.$lang['cart_note1'].'</strong>
					<span class="txt_boldred">'.$lang['cart_note2'].'</span>
					'.$lang['cart_note3'].'
';
if ($options["enable_replica"])
	{
	$note .= '
					<em>'.$lang['cart_replica_note'].'</em>
';
	}
if (strstr($x_HoldBrickName,"XLOGOX"))  
	{
	echo '
        	<TABLE class="table table-condensed" CELLPADDING=2 CELLSPACING=8 BORDER=0 WIDTH="100%">
            <TR CLASS="tbl_grey" ALIGN="center">
            <TD class="tbl_head_txt"><FONT SIZE=3><b>'; 
	echo $lang['cart_bo_header']; 
	echo '</b></FONT><BR>
            <TABLE class="table table-nonfluid table-condensed" CELLPADDING=0 CELLSPACING=2 BORDER=0 WIDTH="100%">
                <TR BGCOLOR="#FFFFFF">
                <TD>
                <TABLE class="table table-condensed" CELLPADDING=0 CELLSPACING=3 BORDER=0 WIDTH="100%">
                <TR ALIGN="center" CLASS="tbl_lightblue">
	        <TD WIDTH="1%"><FONT SIZE=2>'; 
	echo $lang['cart_bo_header_type']; 
	echo '</FONT></TD>
                 <TD WIDTH="10%"><FONT SIZE=2>'; 
	echo $lang['cart_quantity']; 
	echo '</FONT></TD>
	        <TD WIDTH="30%" ALIGN="left"><FONT SIZE=2>'; 
	echo $lang['cart_description']; 
	echo '</FONT></TD>
	        <TD WIDTH="20%"><FONT SIZE=2>'; 
	echo $lang['cart_bo_logo_options']; 
	echo '</FONT></TD>
	        <TD WIDTH="10%"><FONT SIZE=2>'; 
	echo $lang['cart_price_each']; 
	echo '</FONT></TD>
	        <TD WIDTH="10%"><FONT SIZE=2>'; 
	echo $lang['cart_total']; 
	echo '</FONT></TD>
                </TR>
	    	<TR ALIGN="center">
			<TD NOWRAP><FONT SIZE=2>'; 
	echo $lang['cart_paver']; 
	echo '</FONT></TD>
                    <TD NOWRAP><FONT SIZE=2><INPUT onfocus="this.select()" onchange="updateTotals()" TYPE="Text" NAME="x_pqty" VALUE="0" SIZE=2 MAXLENGTH=3></FONT></TD>
                    <TD NOWRAP ALIGN="left"><FONT SIZE=2>';  
	echo $_SESSION["g_bkdesc"][$_POST["bktype"]];  
	echo '
					
<!--<a style="text-decoration:none;" href="#logo_selection" rel="facebox" title="Displaying Images" ><img src="../'.$environment_path.'/img/choose_logo_16.gif" border="0" />--></a><!--Choose Logo-->
</FONT></TD>
                    <TD NOWRAP><FONT SIZE=1><input type="hidden" name="logoid" id="logoid"/><input type="text" name="logo_name" id="logo_name" style="border:none" SIZE="50" readonly="readonly" /></FONT></TD>
					<TD NOWRAP ALIGN="right"><FONT SIZE=2>'; 
	echo $lang['published_currency_symbol']; 
	echo '&nbsp;<INPUT TYPE="Text" NAME="x_pcost" VALUE="';  
	printf("%2.2f", $_SESSION["g_bkpcost"][$_POST["bktype"]]); 
	echo '" SIZE=10 MAXLENGTH="20" DISABLED></FONT></TD>                    
                    <TD NOWRAP align="right"><FONT SIZE=1>'; 
	echo $lang['published_currency_symbol']; 
	echo '&nbsp;<INPUT TYPE="Text" NAME="x_ptotal" VALUE="0.00" SIZE=10 MAXLENGTH="20" DISABLED align=right></FONT></TD>
                </TR>';
	}
else 
	{ // Normal Brick
	echo '
<div class="row">
	<div class="span12">
		<div class="well">
			<div>
			    <h2>'.$lang['cart_bo_header'].'</h2>
			    <h4>'.$lang['customize_options'].'</h4>
			</div>
		</div>
	</div>
</div>

<div class="well">
	<div class="row">
		<div class="'.$_SESSION["g_bk_span"][$_POST["bktype"]].'" class="pagination-centered">
			<h4>'.$lang['selected_customize_options'].'</h4>
			<small>'.$_SESSION["g_bkdesc"][$_POST["bktype"]].'</small>
			<div>
				<img src="images/'.$_SESSION["g_bkimg"][$_POST["bktype"]].'" class="img-rounded">
			</div>
			<h6>'.$lang['cart_paver'].' '.$lang['cart_quantity'].'</h6>
			<div class="control-group">
				<div class="input-prepend">
					<span class="add-on"><strong style="color:black;">#</strong></span>
					<select class="input-small" id="x_pqty" name="x_pqty" data-style="btn-custom">
						  <option value="1" selected="selected">1</option>
					</select>
				</div>
			</div>
			<div>
				<h6>'.$lang['cart_paver'].' '.$lang['cart_price_each'].'</h6>
			</div>
			<div class="control-group">
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">'.$lang['published_currency_symbol'].'</strong>
					</span>
					<input class="input-small" 
					       type="Text" 
						   id="x_pcost" 
						   name="x_pcost" 
						   value="';
	printf("%2.2f", $_SESSION["g_bkpcost"][$_POST["bktype"]]); 
	echo '" 
						   readonly />
				</div>
			</div>
			<h6>'.$lang['cart_paver'].' '.$lang['cart_subtotal'].'</h6>
			<div class="control-group">
				<div class="input-prepend">
					<span class="add-on">
						<strong style="color:black;">
							'.$lang['published_currency_symbol'].'
						</strong>
					</span>
					<input class="input-small subtotal" type="Text" id="x_ptotal" name="x_ptotal" value="';  
	printf("%2.2f", $_SESSION["g_bkpcost"][$_POST["bktype"]]); 
	echo '" readonly>
				</div>
			</div>
';
	if($options["options_format2"] == 1)
		{
		echo($note);
		}
	echo '
		</div>
';
	// Replica
	if ($options["enable_replica"])
		{		
		echo '
		<div class="'.$_SESSION["g_bkr_span"][$_POST["bktype"]].'">
			<h4>'.$lang['add_your_replica'].'</h4>
			<small>'.$_SESSION["g_bkrdesc"][$_POST["bktype"]].'</small>
			<div>
				<img src="images/'.$_SESSION["g_bkrimg"][$_POST["bktype"]].'" class="img-rounded">
			</div>
			<h6>'.$lang['cart_replica'].' '.$lang['cart_quantity'].'</h6>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">#</strong></span>
				<select class="selectpicker show-tick show-menu-arrow qty input-small" id="x_rqty" name="x_rqty" data-width="auto">
					<option value="0" selected="selected">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
				</select>
			</div>
			<h6>'.$lang['cart_replica'].' '.$lang['cart_price_each'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small" type="Text" id="x_rcost" name="x_rcost" value="';  
	printf("%2.2f", $_SESSION["g_bkrcost"][$_POST["bktype"]]); 
	echo '" readonly />
			</div>
			<h6>'.$lang['cart_replica'].' '.$lang['cart_subtotal'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small subtotal" type="Text" id="x_rtotal" name="x_rtotal" value="0.00" readonly>
			</div>
		</div>';
	}
	// Display Cases
	if ($options["enable_displaycase"])
		{
		echo '
		<div class="'.$_SESSION["g_bkdc_span"][$_POST["bktype"]].'">
			<h4>'.$lang['add_your_dc'].'</h4>
			<small>'.$_SESSION["g_bkdcdesc"][$_POST["bktype"]].'</small>
			<div>
				<img src="images/'.$_SESSION["g_bkdcimg"][$_POST["bktype"]].'" class="img-rounded" />
			</div>
			<h6>'.$lang['cart_display_case'].' '.$lang['cart_quantity'].'</h6>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">#</strong></span>
				<select class="selectpicker show-tick show-menu-arrow qty input-small" id="x_dcqty" name="x_dcqty" data-width="auto" data-style="btn-custom">
					<option value="0" selected="selected">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
				</select>
			</div>
			<h6>'.$lang['cart_display_case'].' '.$lang['cart_price_each'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small" type="Text" id="x_dccost" name="x_dccost" value="';  printf("%2.2f", $_SESSION["g_bkdccost"][$_POST["bktype"]]); echo '" readonly />
			</div>
			<h6>'.$lang['cart_display_case'].' '.$lang['cart_subtotal'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small subtotal" type="Text" id="x_dctotal" name="x_dctotal" value="0.00" readonly />
			</div>
		</div>				
';
		}
	// Replica 2
	if ($options["enable_replica2"])
		{	
		echo '
<!--<div class="row">-->
		<div class="'.$_SESSION["g_bkr2_span"][$_POST["bktype"]].'">
		<!--<div class="well">-->
			<h4>
				'.$lang['add_your_replica'].' 
				<small>
					'.$_SESSION["g_bkr2desc"][$_POST["bktype"]].'
				</small>
			</h4>
			<h6>'.$lang['cart_replica2'].' '.$lang['cart_quantity'].'</h6>
			<div>
				<img src="images/'.$_SESSION["g_bkr2img"][$_POST["bktype"]].'" class="img-rounded" />
			</div>
			<div class="input-prepend">
				<span class="add-on"><strong style="color:black;">#</strong></span>
				<select class="selectpicker show-tick show-menu-arrow qty input-small" id="x_r2qty" name="x_r2qty" data-width="auto">
					<option value="0" selected="selected">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
				</select>
			</div>
			<h6>'.$lang['cart_replica2'].' '.$lang['cart_price_each'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small" type="Text" id="x_r2cost" name="x_r2cost" value="';  
		printf("%2.2f", $_SESSION["g_bkr2cost"][$_POST["bktype"]]); 
		echo '" readonly />
			</div>
			<h6>'.$lang['cart_replica2'].' '.$lang['cart_subtotal'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small subtotal" type="Text" id="x_r2total" name="x_r2total" value="0.00" readonly />
			</div>
		</div>
<!--  </div>
</div>-->
';
		}
	// Display Case 2
	if ($options["enable_displaycase2"])
		{	
		echo '
<!--<div class="row">-->
		<div class="'.$_SESSION["g_bkdc2_span"][$_POST["bktype"]].'">
		<!--<div class="well">-->
			<h4>
				'.$lang['add_your_dc'].' 
				<small>
					'.$_SESSION["g_bkdc2desc"][$_POST["bktype"]].'
				</small>
			</h4>
			<h6>'.$lang['cart_display_case2'].' '.$lang['cart_quantity'].'</h6>
			<div>
				<img src="images/'.$_SESSION["g_bkdc2img"][$_POST["bktype"]].'" class="img-rounded" />
			</div>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">#</strong>
				</span>
				<select class="selectpicker show-tick show-menu-arrow qty input-small" id="x_dc2qty" name="x_dc2qty" data-width="auto" data-style="btn-custom">
					<option value="0" selected="selected">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
				</select>
			</div>
			<h6>'.$lang['cart_display_case2'].' '.$lang['cart_price_each'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">'.$lang['published_currency_symbol'].'</strong>
				</span>
				<input class="input-small" type="Text" id="x_dc2cost" name="x_dc2cost" value="';  
		printf("%2.2f", $_SESSION["g_bkdc2cost"][$_POST["bktype"]]); 
		echo '" readonly />
			</div>
			<h6>'.$lang['cart_display_case2'].' '.$lang['cart_subtotal'].'</h6>
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small subtotal" type="Text" id="x_dc2total" name="x_dc2total" value="0.00" readonly />
			</div>
		</div>
<!--  </div>
</div>-->
';
		}
	echo '
	</div>
</div>
	';
	if ($options["logo_options"])  
		{
		echo '
<div class="row">
	<div class="span12">
		<div class="well">
		  <h4>'.$lang['cart_bo_logo_options'].'</h4>
		  <div>
			  <select class="image-picker" name="logo_name" id="logo_name">';
		foreach(glob('logos/*.png*') as $filename)
			{
			$name1 = explode('/',$filename);
			$name2 = explode('.',$name1[1]);
			$name3 = $name2[0];
			if ($_SESSION["remote"] == true)
				{
				if ($name3 == $_SESSION["logo_name"])
					{
					echo '<option selected data-img-src="'.$filename.'" value="'.$name3.'" label="'.$name3.'">'.$name3.'</option>';
					}
				else
					{
					echo '<option data-img-src="'.$filename.'" value="'.$name3.'" label="'.$name3.'">'.$name3.'</option>';
					}
				}
			else
				{
				if ($name3 == $_SESSION["g_logo_display_default"])
					{
					echo '<option selected data-img-src="'.$filename.'" value="'.$name3.'" label="'.$name3.'">'.$name3.'</option>';
					}
				else
					{
					echo '<option data-img-src="'.$filename.'" value="'.$name3.'" label="'.$name3.'">'.$name3.'</option>';
					}
				}
			}
		echo'
			  </select>
		  </div>
		</div>
	</div>
</div>
';
		}
	echo '
<div class="row">
	<div class="span12">
		<div class="well">
			<div class="sidebar-nav sidebar-nav-fixed">
			  <h4>'.$lang['cart_subtotal'].':</h4>
				<div class="input-prepend">
							<span class="add-on"><strong style="color:black;">'.$lang['published_currency_symbol'].'</strong></span>
							<input class="input-small" type="Text" id="x_gtotal" name="x_gtotal" value="';  printf("%2.2f", $_SESSION["g_bkpcost"][$_POST["bktype"]]); echo '" readonly>
				</div>
			</div>
';
	if($options["options_format2"] == 2)
		{
		echo($note);
		}
	echo '
		</div>
	</div>
';
	if($options["options_format2"] == 2)
		{
		echo($note);
		}
	echo '
</div>
	';	    
	}
}

?>