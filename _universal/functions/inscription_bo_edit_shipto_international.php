<?php

function bo_edit_shipto($li) 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
include("configuration/misc_settings.php");
$_SESSION["g_currentli"] = $li;
$li_ship = $_SESSION["g_lishiptoqty"][$li] ;
echo '
<div class="row">
	<div class="span12">
		<div class="well">
			<h4>'.$lang['cart_shipping_replica_shipping'].'</h4>
			<p>'.$lang['cart_shipping_text1'].'</p>
        	<p><i><span class="txt_boldred">'; echo $lang['cart_shipping_text2']; echo '</span></i></p>
			';
if ($lang['cart_shipping_replica_total'] != "")
	{
	echo '
            <p><strong>'; echo $lang['cart_shipping_replica_total']; 
	echo ': '; echo $_SESSION["g_lirqty"][$li];
	echo '</strong><p>';
	}
if ($_SESSION["g_liship"][1][1]["s_name"])
	{
	echo '
			<div class="span12" style="text-align:center; padding:15px;">
';
	if((isset($button["use_address"]))&&(!$button["use_address"])){  }
	else
		{
		echo('
				<input type="button" class="btn btn-custom" id="same_as_shipping" name="same_as_shipping" value="'.$lang['edshp_address_btn'].'" />
');
		}
	if((isset($button["clear_address"]))&&(!$button["clear_address"])){  }
	else
		{
		echo('				
				<input type="button" style="margin-left:10px;" class="btn btn-custom" id="same_as_shipping_clear" name="same_as_shipping_clear" value="'.$lang['edshp_clear_btn'].'" />
');
		}
				
	echo '
			</div>';
	}
echo '
		
			<div class="control-group">
								 <label class="control-label">
								 '.$lang['cart_shipping_prefix_name'].'
								 </label>
								 <div class="controls">
									<select class="selectpicker show-tick show-menu-arrow" name="s_prefix">';
$prefix = explode("|",$lang['cart_shipping_prefix_data']);
sort($prefix);
foreach ($prefix as $value)
	{
	if ($_SESSION["g_liship"][$li][1]["s_prefix"] == $value)
		{
		echo '<option selected="selected" value="'.$value.'"> '.$value.'</option>';
		}
	else
		{
		echo '<option value="'.$value.'"> '.$value.'</option>';
		}
	}
echo '
									</select>
									<p class="help-block"></p>
								</div>
							</div>
			<div class="control-group">
								   <label class="control-label">
								   	*'.$lang['cart_shipping_first_name'].'
								   </label>
								 <div class="controls">
								   <input type="text" name="s_name" 
								   placeholder="'.$lang['edshp_fname_plchldr'].'" 
								   value="'.$_SESSION["g_liship"][$li][1]["s_name"].'" required
								   onchange="sanitize_sql(this);"
								   onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
			</div>
            
			<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_middle_name'].'
								 </label>
								   <div class="controls">
								   	<input type="text" name="s_mname" 
									placeholder="'.$lang['edshp_mname_plchldr'].'" 
									value="'.$_SESSION["g_liship"][$li][1]["s_mname"].'"
								    onchange="sanitize_sql(this);"
								    onblur="sanitize_sql(this);" />
								   	<p class="help-block"></p>
								 </div>
			</div>
			
			<div class="control-group">
								 <label class="control-label">
								   *'.$lang['cart_shipping_last_name'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="s_lname" required 
								   placeholder="'.$lang['edshp_lname_plchldr'].'" 
								   value="'.$_SESSION["g_liship"][$li][1]["s_lname"].'"
								   onchange="sanitize_sql(this);"
								   onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
			</div>
			
			<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_company'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="s_company" 
										  placeholder="'.$lang['edshp_org_plchldr'].'" 
										  value="'.$_SESSION["g_liship"][$li][1]["s_company"].'"
										  onchange="sanitize_sql(this);"
									      onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
			</div>

            <div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_address'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="s_addr1" 
										  required 
										  placeholder="'.$lang['edshp_add_plchldr'].'" 
										  value="'.$_SESSION["g_liship"][$li][1]["s_addr1"].'"
										  onchange="sanitize_sql(this);"
										  onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
			</div>
			
			<div class="control-group">
								 <label class="control-label">
								 </label>
								 <div class="controls">
								   <input type="text" name="s_addr2" 
										  placeholder="'.$lang['edshp_add2_plchldr'].'" 
										  value="'.$_SESSION["g_liship"][$li][1]["s_addr2"].'"
										  onchange="sanitize_sql(this);"
										  onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
			</div>
            
            <div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_city'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="s_city" 
										  required 
										  placeholder="'.$lang['edshp_city_plchldr'].'" 
										  value="'.$_SESSION["g_liship"][$li][1]["s_city"].'"
										  onchange="sanitize_sql(this);"
										  onblur="sanitize_sql(this);" />
								   <p class="help-block"></p>
								 </div>
			</div>
			
			
			<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_state'].'
								 </label>
								 <div class="controls">';
$select = '<select class="selectpicker show-tick show-menu-arrow required"
				   id="s_state" name="s_state" required
				   value="'.$_SESSION["g_liship"][$li][1]["s_state"].'>';
$placeholder = $lang['cart_shipping_state_placeholder'];
$value = $_SESSION["g_liship"][$li][1]["s_state"];
echo(get_prov_states_options($select, $placeholder, $value));
echo('
								  <div><small>'.$lang['cart_all_orders_taxed_text'].'</small></div>
								   <p class="help-block"></p>
								 </div>
				</div>

           <div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_shipping_zip'].'
								   </label>
								   <div class="controls">');
$placeholder = $lang['edshp_pc_plchldr'];
$value = $_SESSION["g_liship"][$li][1]["s_zip"];
$id = "s_zip";
echo(get_postal_code_input($placeholder, $value, $id));
echo('								
									 <p class="help-block"></p>
								   </div>
			</div>
								<div class="control-group">
								   <label class="control-label">
									 *'.$lang['cart_shipping_country'].'
								   </label>
								   <div class="controls">');
$placeholder = $lang['cart_shipping_country_placeholder'];
$value = $_SESSION["country"];
$id = "s_country";
echo(get_country_options($placeholder, $value, $id));
echo('
									 <p class="help-block"></p>
								   </div>
								</div>
			<div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_shipping_daytime_phone'].'
								   </label>
								   <div class="controls">');
$placeholder = $lang['edshp_phone_plchldr'];
$value = $_SESSION["g_liship"][$li][1]["s_phone"];
$id = "s_phone";
echo(get_phone_input($placeholder, $value, $id));
echo('									 
									 <p class="help-block"></p>
								   </div>
			</div>
								

            <div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_shipping_email'].'
								   </label>
								   <div class="controls">
									 <input type="email" name="s_email" required placeholder="Enter Your Email" value="'.$_SESSION["g_liship"][$li][1]["s_email"].'">
									 <div><small>'.$lang['cart_shipping_email_note'].'</small></div>
									 <p class="help-block"></p>
								   </div>
			</div>

		</div>
	</div>
</div>
');
}

?>