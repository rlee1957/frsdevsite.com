<?php

function bo_cart() 
{
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
// Set order total to zero
$_SESSION["g_ordertotal"] = 0;
$_SESSION["g_rordertotal"] = 0;
echo '
    <TABLE CELLPADDING=0 CELLSPACING=8 BORDER=0 WIDTH="100%">
    <TR CLASS="tbl_grey" ALIGN="center">
    <TD class="tbl_head_txt"><FONT SIZE=3><b>Your Shopping Cart</b></FONT><BR>
    <TABLE CELLPADDING=0 CELLSPACING=2 BORDER=0 WIDTH="100%">
    <TR BGCOLOR="#FFFFFF">
    <TD>
    <TABLE CELLPADDING=0 CELLSPACING=3 BORDER=0 WIDTH="100%">
         ';
for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
    {
    if (isset($_SESSION["g_libktype"][$ii])) 
		{
		bo_cart_line_heading();
		bo_cart_paver($ii);
		if ($options["enable_replica"])
			{
            bo_cart_replica($ii);
			}
		if ($options["enable_replica2"])
			{
            bo_cart_replica2($ii);
			}
		if ($options["enable_displaycase"])
			{
            bo_cart_display_case($ii);
			}
		if ($options["enable_displaycase2"])
			{
            bo_cart_display_case2($ii);
			}
		bo_cart_blue_line();
		bo_cart_inscr($ii, $_SESSION["g_libktype"][$ii]);
        }
    }
//Calculate Tax and add to Total
$xx_subtot = $_SESSION["g_ordertotal"];
$xx_taxrate = $_SESSION["service_objects_totaltaxrate"];
$xx_taxtot = round(($_SESSION["g_taxtotal"]),2);
$xx_grandtot = round(($xx_taxtot + $xx_subtot),2);
// If handling fee 
if ($options["web_convenience_fee"]) 
	{		
	$_SESSION["g_ordertotal"] = $xx_grandtot + $_SESSION["convenience_fee"];
	} 
else 
	{	
	$_SESSION["g_ordertotal"] = $xx_grandtot;
	}
echo '
    </TABLE>
    </TD>
    </TR>
    </TABLE> ';
bo_cart_bottom();
echo '
     </TD>
</TR>
</TABLE>
 ';
}

?>