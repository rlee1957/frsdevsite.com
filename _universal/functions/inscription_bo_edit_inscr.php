<?php

function bo_edit_inscr($p_li) 
{

global $page;
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
$p_bktype =  $_SESSION["g_libktype"][$p_li]  ;
$p_bkchar = $_SESSION["g_bkchar"][$p_bktype] ;
$p_bklines = $_SESSION["g_bklines"][$p_bktype] ;
include("../".$environment_path."/includes/inscription_is_gc_show_inscr.php");

echo '
<div id="inscription"'.$style.'>
	<div class="row">
	<div class="span12">
	<div class="well pagination-centered">
    	<!-- Begin Changes -->
		  <div class="row">
			  <div class="span10">
				  <h6>'.$p_bkchar.' allowed characters per line</h6>
			  </div>
		  </div>
					';
for ($ll = 1; $ll <=  $p_bklines; $ll++)  
	{
	$line = "line".$ll;
	echo '
					<div class="row">
						<div class="span10">
							<strong style="font-size:18px;">
								'.$lang['cart_inscription_line'].' '.$ll.'
							</strong> 
							<input id="'.$line.'" 
								   style="font-size:20px;text-transform:uppercase;" 
								   class="xlarge" name="'.$line.'" 
								   maxlength="'.$p_bkchar.'" 
								   value="'.trim($_SESSION["g_liins"][$p_li][$ll]).'"
								   onchange="sanitize_sql(this);"
								   onblur="sanitize_sql(this);" />';
	if ($options["greek_lettering"])
		{
		echo '
							<script type="text/javascript">
								$(function () {
									$(\'#'.$line.'\').keypad({
									showOn: \'button\',
									buttonImageOnly: true,
									keypadOnly: false,
									buttonImage: \'../'.$environment_path.'/images/omega.png\',separator: \'|\',
									layout: [\'Α|Β|Γ|Δ|Ε\',
									\'Ζ|Η|Θ|Ι|Κ\',
									\'Λ|Μ|Ν|Ξ|Ο\',
									\'Π|Ρ|Σ|Τ|Υ\',
									\'Φ|Χ|Ψ|Ω\',
									$.keypad.CLOSE +\'|\'+ $.keypad.CLEAR],
									prompt: \'Add these Greek letters\'});
								});
                			</script>';
		}
	echo'
							<div id="'.$ll.'_num"></div>
						</div>
					</div>';
	}
echo '


			  <div class="row">
				<div class="span10"><strong>'.$lang['cart_order_confirmation_note'].'</strong>
				</div>
			  </div>
			</div>
		</div>
	</div>
</div>
 ';
echo'
	<script type="text/javascript">       
	$(document).ready(function(){';
for ($ll = 1; $ll <=  $p_bklines; $ll++)  
	{
	$line = "line".$ll;
	echo '
		$("#'.$line.'").keyup(function () {
			var ta = '.$p_bkchar.';
			$("#'.$ll.'_num").html(\'Spaces Used: (\' + $(\'#'.$line.'\').val().length + \') Remaining: (\' + (ta - $(\'#'.$line.'\').val().length) +\')\');
		});
		
		$("#'.$line.'").change(function () {
    		var ta = '.$p_bkchar.';
    		$("#'.$ll.'_num").html(\'Spaces Used: (\' + $(\'#'.$line.'\').val().length + \') Remaining: (\' + (ta - $(\'#'.$line.'\').val().length) +\')\');
		});
		';
	}
echo '
	});
	</script>';
}

?>