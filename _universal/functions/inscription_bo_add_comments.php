<?php

function bo_add_comments() 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
$brick_images = explode("|",$lang['brick_image_path']);
$image_count = count($brick_images);
$ni = '
				<a class="carousel-control left" href="#brickImages" data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#brickImages" data-slide="next">&rsaquo;</a>';
if($image_count == 1)
	{ 
	$ni = ""; 
	}
echo '
	<div class="row">
		<div class="span12">
			<div id="brickImages" class="carousel slide well">
				<ol class="carousel-indicators">
			  ';
//var_dump($brick_images);
foreach ($brick_images as $key1 => $value1)
	{
	if ($key1 == 0)
		{
		$active = "class=\"active\"";
		}
	else 
		{
		$active = "";
		}
	echo '
					<li data-target="#brickImages" data-slide-to="'.$key1.'" '.$active.'></li>
		';
	}
echo '
				</ol>
				<!-- Carousel items -->
				<div class="carousel-inner">';
foreach ($brick_images as $key => $value)
	{
	if ($key == 0)
		{
		$active = "item active";
		}
	else 
		{
		$active = "item";
		}
	echo '
					<div class="'.$active.'">';
	echo '
						<img src="'.$value.'" alt="'.$key.'">
					</div>';
	}
echo '
				</div>
				<!-- Carousel nav -->
				'.$ni.'
			</div>
		</div>
</div>
<script type="text/javascript">
$(document).ready(function() 
	{
	$(\'#brickImages\').carousel(
		{
		interval: 8000
		}
	);
	}
);
</script>
';
}

?>