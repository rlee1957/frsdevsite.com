<?php

function bo_getlocation($bktype)
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
if(isset($_SESSION["g_bk_location_data"][$bktype]))
	{
	$brick_location = explode("|",$_SESSION["g_bk_location_data"][$bktype]);
	if($brick_location[0] != "")
		{
		echo '
		  <div id="location">
			<div class="row">
				<div class="span12">
					<div class="well">
						<h4>'.$_SESSION["g_bk_location"][$bktype].'</h4>
						<select id="brick_location" name="brick_location" class="required">
							<option value=""></option>';
		foreach ($brick_location as $key1 => $value1)
			{
			echo '
							<option value="'.$value1.'">'.$value1.'</option>';
			}
		echo '
						</select>
						<p><em>'.$lang['cart_location_footer'].'</em></p>
							<input type="text"  id="brick_location_other" class="required" maxlength="25" name="brick_location_other" placeholder="Type Other Organization..">

					</div>
				</div>
			</div>
		</div>
		';
		}
	}
}

?>