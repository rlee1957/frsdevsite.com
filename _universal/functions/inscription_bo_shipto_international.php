<?php
 
function bo_shipto() 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
include("configuration/misc_settings.php");
echo '
<div class="row">
	<div class="span12">
		<div id="content" class="well">
			<div class="pagination-centered">
				<h4>'.$lang['cart_shipping_header'].'</h4>
				<p>'.$lang['cart_shipping_text1'].'<span class="txt_boldred"><i>'.$lang['cart_shipping_text2'].'</i></span></p>
				<p>'.$lang['cart_shipping_text3'].'</p>
				<p>'.$lang['cart_shipping_cert_note'].'</p>
				<p><strong>'.$lang['cart_shipping_text4'].'</strong></span></p>
			</div>';
if(isset($_SESSION["g_liship"][1][1]))
	{
	if ($_SESSION["g_liship"][1][1]["s_name"])
		{
		echo '
			<div class="span12" style="text-align:center; padding:15px;">
';
		if((isset($button["use_address"]))&&(!$button["use_address"])){  }
		else
			{
			echo('
				<input type="button" class="btn btn-custom" id="same_as_shipping" name="same_as_shipping" value="'.$lang['cart_shipping_addr_btn'].'">
');
			}
		if((isset($button["clear_address"]))&&(!$button["clear_address"])){  }
		else
			{
			echo('
				<input type="button" style="margin-left:10px;" class="btn btn-custom" id="same_as_shipping_clear" name="same_as_shipping_clear" value="'.$lang['cart_shipping_cancel_btn'].'">
');
			}
		echo('
			</div>
');
		}
	}
echo'
			  				<div class="control-group">
								 <label class="control-label">
								 '.$lang['cart_shipping_prefix_name'].'
								 </label>
								 <div class="controls">
									<select class="selectpicker show-tick show-menu-arrow" name="s_prefix">';
$prefix = explode("|",$lang['cart_shipping_prefix_data']);
sort($prefix);
foreach ($prefix as $value)
	{
	echo '<option value="'.$value.'"> '.$value.'</option>';
	}
echo '
									</select>
									<p class="help-block"></p>
								  </div>
							</div>
							<div class="control-group">
								   <label class="control-label">
								   	*'.$lang['cart_shipping_first_name'].'
								   </label>
								 <div class="controls">
								   <input type="text" name="s_name" 
										  placeholder="'.
										  $lang['cart_shipping_first_name_placeholder'].
										  '" required
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_middle_name'].'
								 </label>
								   <div class="controls">
								   	<input type="text" name="s_mname" 
										   placeholder="'.
										  $lang['cart_shipping_middle_name_placeholder'].
										  '"
										   onchange="sanitize_sql(this)"
										   onblur="sanitize_sql(this)" />
								   	<p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   *'.$lang['cart_shipping_last_name'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="s_lname" 
										  required 
										  placeholder="'.
										  $lang['cart_shipping_last_name_placeholder'].
										  '"
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_shipping_company'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="s_company" 
										  placeholder="'.
										  $lang['cart_shipping_company_placeholder'].
										  '"
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   *'.$lang['cart_shipping_address'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="s_addr1" 
										  required 
										  placeholder="'.
										  $lang['cart_shipping_address_placeholder'].
										  '"
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								 </label>
								 <div class="controls">
								   <input type="text" name="s_addr2" 
										  placeholder="'.
										  $lang['cart_shipping_address2_placeholder'].
										  '"
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   *'.$lang['cart_shipping_city'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="s_city" 
										  required placeholder="'.
										  $lang['cart_shipping_city_placeholder'].
										  '"
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   *'.$lang['cart_shipping_state'].'
								 </label>
								 <div class="controls">';
$select = '<select class="selectpicker show-tick show-menu-arrow required" 
							id="s_state" name="s_state" required>';
$placeholder = $lang['cart_shipping_state_placeholder'];
$value = "";
echo(get_prov_states_options($select, $placeholder, $value));
echo('
								  <div><small>'.$lang['cart_all_orders_taxed_text'].'</small></div>
								   <p class="help-block"></p>
								 </div>
								</div>
								 <div class="control-group">
								   <label class="control-label">
									 *'.$lang['cart_shipping_zip'].'
								   </label>
								   <div class="controls">');
$placeholder = $lang['cart_shipping_zip_placeholder'];
$value = "";
$id = "s_zip";
echo(get_postal_code_input($placeholder, $value, $id));
echo('
									 <p class="help-block"></p>
								   </div>
								</div>
								<div class="control-group">
								   <label class="control-label">
									 *'.$lang['cart_shipping_country'].'
								   </label>
								   <div class="controls">');
$placeholder = $lang['cart_shipping_country_placeholder'];
$value = $_SESSION["country"];
$id = "s_country";
echo(get_country_options($placeholder, $value, $id));
echo('
									 <p class="help-block"></p>
								   </div>
								</div>
								
								<div class="control-group">
								   <label class="control-label">
									 *'.$lang['cart_shipping_daytime_phone'].'
								   </label>
								   <div class="controls">');
$placeholder = $lang['cart_shipping_daytime_phone_placeholder'];
$value = '';
$id = "s_phone";
echo(get_phone_input($placeholder, $value, $id));
echo('
									 <p class="help-block"></p>
								   </div>
								</div>
								<div class="control-group">
								   <label class="control-label">
									 *'.$lang['cart_shipping_email'].'
								   </label>
								   <div class="controls">
									 <input type="email" name="s_email" 
											required placeholder="'.
										    $lang['cart_shipping_email_placeholder'].
										    '"
										    onchange="sanitize_sql(this)"
										    onblur="sanitize_sql(this)" />
									 <div><small>'.$lang['cart_shipping_email_note'].'</small></div>
									 <p class="help-block"></p>
								   </div>
								</div>
		</div>
	</div>
</div>
<input type="hidden" name="s_qty" value ="'.$_SESSION["g_rqtyremaining"].'">

');
}


?>