<?php


function bo_shiplist($li) {
    /*------------------------------------------------------------------------------------------
     - capture current line item
     - capture how many shipping addresses have been added
     - Loop thru all captured shipping addresses and display in table
    ------------------------------------------------------------------------------------------*/

    //$li = $_SESSION["g_currentli"];
    $li_ship = $_SESSION["g_lishiptoqty"][$li] ;

echo '
<TABLE CELLPADDING=0 CELLSPACING=8 BORDER=0 WIDTH="100%">
<TR CLASS="tbl_grey" ALIGN="center">
    <TD><FONT SIZE=3>Ship To List</FONT><BR>
    <TABLE CELLPADDING=3 CELLSPACING=2 BORDER=0 WIDTH="100%">
    <TR BGCOLOR="#FFFFFF">
    <TD>
    <TABLE CELLPADDING=0 CELLSPACING=3 BORDER=0 WIDTH="100%">
        <TR ALIGN="center" CLASS="tbl_lightblue">
            <TD WIDTH="2%"><FONT SIZE=1>Replica Qty</FONT></TD>
            <TD WIDTH="10%"><FONT SIZE=1>First Name</FONT></TD>
			<TD WIDTH="10%"><FONT SIZE=1>Last Name</FONT></TD>
            <TD WIDTH="20%" ALIGN="left"><FONT SIZE=1>Address</FONT></TD>
            <TD WIDTH="10%"><FONT SIZE=1>City</FONT></TD>
            <TD WIDTH="3%"><FONT SIZE=1>State</FONT></TD>
            <TD WIDTH="5%"><FONT SIZE=1>Zip</FONT></TD>
            <TD WIDTH="7%"><FONT SIZE=1>Phone</FONT></TD>
            <TD WIDTH="10%"><FONT SIZE=1>Email</FONT></TD>
        </TR>';

         for ($ss = 1; $ss <=  $li_ship; $ss++)  {
              echo '
              <TR ALIGN="center" CLASS="tbl_white">
                <TD WIDTH="2%"><FONT SIZE=1>'; echo $_SESSION["g_liship"][$li][$ss]["s_qty"];    echo '</FONT></TD>
                <TD WIDTH="10%"><FONT SIZE=1>'; echo $_SESSION["g_liship"][$li][$ss]["s_name"];    echo '</FONT></TD>
				<TD WIDTH="10%"><FONT SIZE=1>'; echo $_SESSION["g_liship"][$li][$ss]["s_lname"];    echo '</FONT></TD>
                <TD WIDTH="20%" ALIGN="left"><FONT SIZE=1>'; echo $_SESSION["g_liship"][$li][$ss]["s_addr1"];    echo '</FONT></TD>
                <TD WIDTH="10%"><FONT SIZE=1>'; echo $_SESSION["g_liship"][$li][$ss]["s_city"];    echo '</FONT></TD>
                <TD WIDTH="3%"><FONT SIZE=1>'; echo $_SESSION["g_liship"][$li][$ss]["s_state"];    echo '</FONT></TD>
                <TD WIDTH="5%"><FONT SIZE=1>'; echo $_SESSION["g_liship"][$li][$ss]["s_zip"];    echo '</FONT></TD>
                <TD WIDTH="7%"><FONT SIZE=1>'; echo $_SESSION["g_liship"][$li][$ss]["s_phone"];    echo '</FONT></TD>
                <TD WIDTH="10%"><FONT SIZE=1>'; echo $_SESSION["g_liship"][$li][$ss]["s_email"];    echo '</FONT></TD>
            </TR>';

         }

echo '
    </TABLE>
    </TD>
    </TR>
    </TABLE>
     </TD>
</TR>
</TABLE>

';

}


?>