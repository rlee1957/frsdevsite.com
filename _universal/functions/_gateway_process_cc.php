<?php

function process_cc()
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
$social_media = "";
if($options["social_media_confirm"])
	{ 
	$social_media = '
	<br />	
	<div>
		'.show_social_media().'
	</div>
	<br />
'; 
	}

$etransid = $_SESSION["go_orderid"];

$amt = $_SESSION["go_total"];

$vkey=valkey($etransid,$amt);

$o_bfname = $_SESSION["g_billing"][1];
$o_blname = $_SESSION["g_billing"][2];
$bill_name = $o_bfname." ".$o_blname;
$o_bcompany = $_SESSION["g_billing"][3];
$o_baddr1 = $_SESSION["g_billing"][4];
$o_baddr2 = $_SESSION["g_billing"][5];
$o_bcity = $_SESSION["g_billing"][6];
$o_bstate = $_SESSION["g_billing"][7];
$o_bzip = $_SESSION["g_billing"][8];
$o_bphone = $_SESSION["g_billing"][9];
$o_bemail = $_SESSION["g_billing"][10];

if($options["tell_your_story"])
	{
	include("../".$environment_path."/includes/tell_your_story.php");	
	}
else
	{
	include("../".$environment_path."/includes/process_confirmation.php");
	}
}

?>