<?php

# ../_common/functions/inscription_bo_cart_bottom.php

function bo_cart_bottom() 
{
include("configuration/environment_settings.php");
include("../_common/includes/language_check.php");
#include("../".$environment_path."/includes/test_var.php");
#exit();
$xx_ptotal = get_paver_total();
$xx_rtotal = get_replica_total();
$xx_r2total = get_replica2_total();
$xx_dctotal = get_display_case_total();
$xx_dc2total = get_display_case2_total();
$xx_ptotal_tax = get_paver_tax_total();
$_SESSION["paver_tax_total"] = $xx_ptotal_tax;
$xx_rtotal_tax = get_replica_tax_total();
$_SESSION["replica_tax_total"] = $xx_rtotal_tax;
$xx_r2total_tax = get_replica2_tax_total();
$_SESSION["replica2_tax_total"] = $xx_r2total_tax;
$xx_dctotal_tax = get_display_case_tax_total();
$_SESSION["display_case_tax_total"] = $xx_dctotal_tax;
$xx_dc2total_tax = get_display_case2_tax_total();
$_SESSION["display_case2_tax_total"] = $xx_dc2total_tax;
# ------------NEW 2015-09-09------------
$xx_shiptotal = get_shipping_total();
$xx_shiptotal_tax = get_shipping_tax_total();
$xx_validshipto = valid_shipto();
$xx_shipto_text = get_shipto_label();

# ------------end NEW------------
$convenience_fee = get_convenience_fee();
$convenience_fee_tax = get_convenience_fee_tax();

$greek_character_fee = get_greek_character_fee();

$_SESSION["g_subordertotal"] = $xx_ptotal + $xx_rtotal + $xx_r2total + $xx_dctotal + $xx_dc2total;
$_SESSION["g_taxtotal"] = $xx_ptotal_tax + $xx_rtotal_tax + $xx_r2total_tax + $xx_dctotal_tax + $xx_dc2total_tax + $convenience_fee_tax + $xx_shiptotal_tax;
$_SESSION["g_ordertotal"] = $_SESSION["g_subordertotal"] + $convenience_fee + $_SESSION["g_taxtotal"] + $xx_shiptotal + $greek_character_fee;
$_SESSION["g_ordertotal"] = $_SESSION["g_ordertotal"] - $_SESSION["g_gc_redeem_amount"];
$_SESSION["g_ordertotal_backup"] = $_SESSION["g_ordertotal"];
$_SESSION["g_taxtotal_backup"] = $_SESSION["g_taxtotal"];
# Subtotal Field
echo '
<div class="row">
	<div class="span12">
		<span id="bo_bottom">
		<div class="well">
			<table class="table table-condensed">
				<tr>
					<td class="pull-right" style="border-top:0px;">
						<div class="input-prepend">
							<span class="add-on">
								<strong style="color:black;">
									'.$lang['cart_subtotal'].': '.$lang['published_currency_symbol'].'
								</strong>
							</span>
							<input class="input-small" type="Text" name="x_gsubtotal" id="x_gsubtotal" value="'; 
printf("%2.2f", $_SESSION["g_subordertotal"]); 
echo '"  readonly style="height: 100%;" />
						</div>
					</td>
				</tr>
';
# Tax Field
if ($options["apply_sales_tax"])
	{
	echo '
				<tr>
					<td class="pull-right" style="border-top:0px;">
						<div class="input-prepend">
							<span class="add-on">
								<strong style="color:black;">
									'.$lang['cart_tax'].': '.$lang['published_currency_symbol'].'
								</strong>
							</span>
							<input class="input-small" type="Text" name="x_gtaxtotal" id="x_gtaxtotal" value="'; 
	printf("%2.2f", $_SESSION["g_taxtotal"]); 
	echo '"  readonly style="height: 100%;" />
						</div>
					</td>
				</tr>';
}
# Replica Shipping Field
if ($options["enable_replica"])
	{
	
	echo '
				<tr>
					<td class="pull-right" style="border-top:0px;">
					<div class="input-prepend">
					  <span class="add-on">
						<strong style="color:black;">'.$lang['cart_replica_shipping'].': '.$lang['published_currency_symbol'].'</strong>
					  </span>
						<input class="input-small" type="Text" name="x_gregfee" id="x_gregfee" value="'.$xx_shipto_text.'"  readonly style="height: 100%;" />
					  </div>
					</td>
				</tr>
';
	}

# Handling Fee Field
if ($options["web_convenience_fee"]) 
	{	
	echo '
				<tr>
					<td class="pull-right" style="border-top:0px;">
						<div class="input-prepend">
							<span class="add-on">
								<strong style="color:black;">
									'.$lang['cart_handling_fee'].': '.$lang['published_currency_symbol'].'
								</strong>
							</span>
							<input class="input-small" type="Text" name="x_ghandfee" id="x_ghandfee" value="'; 
	printf("%2.2f", $convenience_fee); 
	echo '"  readonly style="height: 100%;" />
						</div>
					</td>
				</tr>
';
	}
# Greek Character Fee
if($greek_character_fee > 0)
	{	
	echo('
	<tr>
		<td class="pull-right" style="border-top:0px;">
			<div class="input-prepend">
				<span class="add-on">
					<strong style="color:black;">
						'.$lang['greek_char_fee'].': '.$lang['published_currency_symbol'].'
					</strong>
				</span>
				<input class="input-small" type="Text" name="x_ghandfee" id="x_ghandfee" value="'); 
	printf("%2.2f", $greek_character_fee); 
	echo('"  readonly style="height: 100%;" />
			</div>
		</td>
	</tr>
');
				}
	
# Grand Total Field
echo '
				<tr>
					<td class="pull-right" style="border-top:0px;">
';
if ($_SESSION["g_ordertotal"] < 0)
	{
	
	echo '
						<div class="alert alert-error">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<h4>'.$lang['cart_attention'].'</h4>
							'.$lang['cart_attention_pre'].' ('.$_SESSION["g_ordertotal"]*(-1).')'.$lang['published_currency_symbol'].$lang['cart_attention_suf'].'
						</div>';
						//echo $_SESSION["g_ordertotal"].' ';
						$_SESSION["g_ordertotal"] = 0.00;
	}
echo'	
						<div class="input-prepend">
							<span class="add-on">
								<strong style="color:black;">
									'.$lang['cart_total'].': '.$lang['published_currency_symbol'].'
								</strong>
							</span>
							<input class="input-small" type="Text" name="x_gtotal" id="x_gtotal" value="'; 
echo number_format($_SESSION["g_ordertotal"] , 2, '.', ''); 
echo '"  readonly style="height: 100%;" />
						</div>
					</td>
				</tr>
';

# Web Convenience Fee
if ($options["web_convenience_fee"])
	{
	echo '
				<tr>
					<td class="pull-right" style="border-top:0px;">
						'.$lang['cart_all_orders_taxed'].'
					</td>
				</tr>
';
	}
	
# Tax
if ($options["apply_sales_tax"])
	{
	echo '
				<tr>
					<td class="pull-right" style="border-top:0px;">
						'.$lang['cart_all_orders_taxed_text'].'
					</td>
				</tr>
';
	}
echo '
				<tr>
					<td>'.$lang['shipping_disclaimer'].'</td>
				</tr>
			</table>
		</div>
		</span>
	</div>
</div>
';
/*
echo('
<textarea style="width: 99%; height: 250px;">
');
print_r($_SESSION);
echo('
</textarea>
');*/
}

?>