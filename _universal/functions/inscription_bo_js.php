<?php

function bo_js() 
{
	
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
echo '
<script type="text/javascript">
<!--

function roundNumber(num, dec) {
	var result = Math.round(num * Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}

function MoveTo(x) {
	if (x == 1) {
		document.frmInscription.x_pqty.focus();
	}
	if (x == 2) {
		document.frmInscription.ins_conf_cont.focus();
	}
	if (x == 3) {
		document.frmcart.cart_checkout.focus();
	}
	if (x == 4) {
		document.frmOptions.x_op_qty.focus();
	}
	if (x == 5) {
		document.frmOptions.but_op_get_cont.focus();
	}
	
}

function updateTotals(){
                    if (document.frmInscription.x_rqty.value > 0) {
                         if (document.frmInscription.x_pqty.value <= 0) {
                            alert("You must have at least one paver!");
                            document.frmInscription.x_rqty.value = 0;
                            document.frmInscription.x_pqty.focus();
                         }
                    }
		  
		// -------------------------------------------------     
		// Put values into local variables for calculation
		// ------------------------------------------------
				
		l_pqty = 0;
		l_rqty = 0;
		l_dcqty = 0;
		l_pcost = 0.00;
		l_rcost = 0.00;
		l_r2cost = 0.00;
		l_dccost = 0.00;
		l_dc2cost = 0.00;
		l_ptotcost = 0.00;
		l_rtotcost = 0.00;
		l_r2totcost = 0.00;
		l_dctotcost = 0.00;
		l_dc2totcost = 0.00;
		l_subtot = 0.00;
		l_grandtot = 0.00;
		
		l_pqty = document.frmInscription.x_pqty.value;
		l_rqty = document.frmInscription.x_rqty.value;
		l_dcqty = document.frmInscription.x_dcqty.value;
		';
if($options["enable_replica2"])
	{
	echo '
		l_r2qty = document.frmInscription.x_r2qty.value;
		l_r2cost = document.frmInscription.x_r2cost.value;
		l_r2totcost = l_r2qty * l_r2cost;
		document.frmInscription.x_r2total.value = l_r2totcost);';
		
	}
if($options["enable_displaycase2"])
	{
	echo '
		l_dc2qty = document.frmInscription.x_dc2qty.value;
		l_dc2cost = document.frmInscription.x_dc2cost.value;
		l_dc2totcost = l_dc2qty * l_dc2cost;
		document.frmInscription.x_dc2total.value = l_dc2totcost;';
	}
echo '
		l_pcost = document.frmInscription.x_pcost.value;
		l_rcost = document.frmInscription.x_rcost.value;
		l_dccost = document.frmInscription.x_dccost.value;
		
		
		l_ptotcost = l_pqty * l_pcost;
		l_rtotcost = l_rqty * l_rcost;
		l_dctotcost = l_dcqty * l_dccost;
		
		
		
		l_subtot = l_ptotcost + l_rtotcost + l_dctotcost + l_r2totcost + l_dc2totcost;
		l_grandtot = l_subtot;
		
		document.frmInscription.x_ptotal.value = l_ptotcost.toFixed(2);
		document.frmInscription.x_rtotal.value = l_rtotcost.toFixed(2);
		document.frmInscription.x_dctotal.value = l_dctotcost.toFixed(2);
		
		//document.getElementById(\'x_gtotal\').value=l_grandtot.toFixed(2);
		document.frmInscription.x_gtotal.value = l_grandtot.toFixed(2);
		//document.frmInscription.xx_gtotal.value = l_grandtot.toFixed(2);
			
}
function update_donation_total(){
		document.frmOptions.x_op_total.value = document.frmOptions.x_op_cost.value;
		document.frmOptions.x_op_gtotal.value = document.frmOptions.x_op_cost.value;
	 }

function updateOpTotals(){		  
		// -------------------------------------------------     
		// Put values into local variables for calculation
		// ------------------------------------------------
		l_qty = 0;
		l_cost = 0.00;
		l_taxrate = 0.00;
		l_totcost = 0.00;
		
		l_gtotcost = 0.00;
		
		l_rqty = 0;
		l_rcost = 0.00;
		l_rtotcost = 0.00;
		
		
		l_qty = document.frmOptions.x_op_qty.value;
		l_cost = document.frmOptions.x_op_cost.value;		
		l_taxrate = '; echo $_SESSION["service_objects_totaltaxrate"]; echo ';
		l_totcost = l_qty * l_cost;	

		l_rqty = document.frmOptions.x_op_rqty.value;
		l_rcost = document.frmOptions.x_op_rcost.value;		
		l_rtotcost = l_rqty * l_rcost;	
		
		l_gtotcost = l_rtotcost + l_totcost;
				
		document.frmOptions.x_op_rtotal.value = l_rtotcost.toFixed(2);		
		document.frmOptions.x_op_total.value = l_totcost.toFixed(2);
		document.frmOptions.x_op_gtotal.value = l_gtotcost.toFixed(2);
		
		
}


function js_debug() {
        mywindow = window.open("custom/debug.html", "MyWindow",
            "toolbar=no, location=no, directories=no, status=no," +
            "menubar=no, scrollbars=no, resizable=no," +
            "width=500, height=400");
}

function selectcontents(fieldobject) {
        fieldobject.select();
}

function chk_for_tab(fieldobject) {
        alert(fieldobject.style.color);
        fieldobject.value = "";
        fieldobject.style.background = "red";
}


function autoTab(input, len) {
    if(input.value.length >= len) {
        input.value = input.value.slice(0, len);
        input.form[(getIndex(input)+1) % input.form.length].focus();
}

function getIndex(input) {
var index = -1, i = 0, found = false;
while (i < input.form.length && index == -1)
     if (input.form[i] == input) index = i;
    else i++;
    return index;
}

return true;
}


//-->
</script>
';
}

?>