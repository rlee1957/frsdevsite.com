<?php

function bo_cart_empty() 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
// Set order total to zero
$_SESSION["g_ordertotal"] = 0;
echo '
    <TABLE CELLPADDING=0 CELLSPACING=8 BORDER=0 WIDTH="100%">
        <TR CLASS="tbl_grey" ALIGN="center">
        <TD class="tbl_head_txt"><FONT SIZE=3><b>';
echo $lang['your_shopping_cart'];
echo '</b></FONT><BR>
            <TABLE CELLPADDING=0 CELLSPACING=2 BORDER=0 WIDTH="100%">
                <TR BGCOLOR="#FFFFFF">
                <TD>
                 <TABLE CELLPADDING=0 CELLSPACING=3 BORDER=0 WIDTH="100%">     ';
bo_cart_line_heading();
echo '
                    <TR ALIGN="center">
                    <TD NOWRAP><FONT SIZE=2></FONT></TD>
                    <TD NOWRAP><FONT SIZE=2>';  
echo  $xx_qty;  
echo '</FONT></TD>
                    <TD NOWRAP ALIGN="left"><FONT SIZE=2>';
echo $lang['empty_cart'];
echo '</FONT> </TD>
		    <TD NOWRAP> </TD>
                    <TD class="txt_cost" NOWRAP ALIGN="right"><FONT SIZE=2>$&nbsp;<INPUT TYPE="Text" NAME="x_rcost" VALUE=';
echo '"' . $xx_cost . '.00"'; 
echo ' SIZE=10 MAXLENGTH="20" DISABLED></FONT></TD>
                    <TD class="txt_cost" NOWRAP align="right"><FONT SIZE=1><INPUT TYPE="HIDDEN" NAME="x_rship" VALUE='; 
echo '"' . $xx_ship . '.00"'; 
echo ' SIZE=10 MAXLENGTH="20" DISABLED></FONT></TD>
                    <TD class="txt_cost" NOWRAP align="right"><FONT SIZE=1>$&nbsp;<INPUT TYPE="Text" NAME="x_rtotal"  VALUE=';
printf("%2.2f", $xx_total); 
echo ' SIZE=10 MAXLENGTH="20" DISABLED></FONT></TD>
                    </TR>
                </TABLE>
                </TD>
                </TR>
            </TABLE>
            </TD>
            </TR>
    </TABLE>
	<TABLE CELLPADDING=2 CELLSPACING=3 BORDER=0 WIDTH="100%">

            <TR ALIGN="left"><TD COLSPAN=3 ><FONT SIZE=2>';
echo $lang['footer_note'];
echo ' ';
echo $lang['published_telephone'];
echo '.<br>
            ';  
echo '</FONT>
            </TD></TR>
    </TABLE>
 ';
}

?>