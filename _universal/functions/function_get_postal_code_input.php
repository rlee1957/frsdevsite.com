<?php

function get_postal_code_input($placeholder, $value, $id)
{
include("configuration/environment_settings.php");
include("configuration/countries_offered.php");
$reTVal = "";
if(!isset($countries_offered[strtolower($_SESSION["country"])]["postal_code"]))
	{
	$pc = $countries_offered["ot"]["postal_code"];
	}
else
	{
	$pc = $countries_offered[strtolower($_SESSION["country"])]["postal_code"];
	}
switch($pc)
	{
	case "zip":
		{
		$required = ' required';
		$maxlength = ' maxlength="5"';
		$minlength = ' minlength="5"';
		$class = ' class="required digits"';
		break;
		}
	case "canada":
		{
		$required = ' required';
		$maxlength = ' maxlength="7"';
		$minlength = ' minlength="7"';
		$class = ' class="required canada"';
		break;
		}
	default:
		{
		$required = '';
		$maxlength = ' maxlength="20"';
		$minlength = '';
		$class = '';
		break;
		}
	}
$reTVal = '
					<input type="text" 
						   id="'.$id.'"
						   name="'.$id.'" '.
						   $class. 
						   $required. 
						   ' placeholder="'.$placeholder.'" '. 
						   $maxlength.$minlength.
						   ' value="'.$value.'" />';
return $reTVal;
}

?>