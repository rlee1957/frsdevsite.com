<?php
function Customer_Footer()
{

include("configuration/environment_settings.php");
echo '
<div class="row pagination-centered">
	<div class="span12 pagination-centered">
	  <div class="row-fluid pagination-centered">
		  <div class="span11 badge" style="margin-bottom:10px; float:none; margin-left: auto; margin-right: auto;">
				  <a class="btn btn-link txtFooter" href="start_over.php">Start Over</a>
				  &nbsp;|&nbsp;
				  <a class="btn btn-link txtFooter" target="_blank" href="configuration/faq.php">FAQ</a>
		  </div>
	  </div>
	</div>
</div>	
<div class="row pagination-centered">
	<div class="span12">
		&copy; Copyright 2015, <a href="http://www.fundraisersltd.net">Fund Raisers Ltd.</a> All rights reserved.
	</div>
</div>
';
include("../".$environment_path."/includes/footer.php");
}

?>