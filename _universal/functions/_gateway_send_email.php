<?php
function send_email()
{

include("configuration/environment_settings.php");
include("../_common/includes/language_check.php");
include("configuration/image_settings.php");
// Message Body that will be received when approved ($_SESSION["go_resp_code"] = "0")
if($force_success){ $_SESSION["go_resp_code"] = 0; }
$social_media = "";
if($options["social_media_email"])
	{ 
	$social_media = '	
	<div>
		'.show_social_media().'
	</div>
'; 
	}
$approvedBody = '
<body style="margin-left:150px; margin-right:150px;margin-top:10px;">
	<div style="background-color:'.$lang["published_background_color"].';width:600px">
		<center>
			<img src="'.$lang["full_published_logo"].'" 
				 width="600" 
				 alt="'.$lang["published_campaign_name"].'" />
		</center>
	</div>
	<div align="center" style="width:600px">
		<p align="center">
			<strong>
				'.$lang["confirmation_payment"].' '.$lang["confirmation_payment_successful"].'
			</strong>
		</p>
		<p style="color:#F00; font-style:italic; font-size:11px;" align="center">
			('.strtoupper($lang["confirmation_payment_please_print"]).')
		</p>
		<div align="left" style="width:600px">
			<p>
				'.$lang["confirmation_thank_you_text1_approved"].'
			</p>
			<p>
				'.$lang["confirmation_processed_success"].'
			</p>
		</div>
	</div>
	<DIV align="center" style="width:600px">
		<TABLE border="1" cellPadding="0" width="587">
			<TBODY>
				<TR>
					<TD width="484">
						<p align="right">
							'.$lang["confirmation_order_date"].':
						</p>
					</TD>
					<TD bgcolor="#cfc5b1">
						<p>
							'.date("F j, Y, g:i a").'
						</p>
					</TD>
				</TR>
				<TR>
					<TD width="484">
						<p align="right">
							'.$lang["confirmation_order_number"].':
						</p>
					</TD>
					<TD bgcolor="#cfc5b1">
						<p>
							'.$_SESSION["go_orderid"].'
						</p>
					</TD>
				</TR>
				<TR>
					<TD width="484">
						<p align="right">
							'.$lang["confirmation_auth_code"].':
						</p>
					</TD>
					<TD bgcolor="#cfc5b1">
						<p>
							'.$_SESSION["go_resp_auth_code"].'
						</p>
					</TD>
				</TR>
				<TR>
					<TD width="484">
						<p align="right">
							'.$lang["confirmation_auth_response"].':
						</p>
					</TD>
					<TD bgcolor="#cfc5b1">
						<p>
							'.strtoupper($lang['confirmation_approved']).'
						</p>
					</TD>
				</TR>
				<TR>
					<TD width="484">
						<p align="right">
							'.$lang["confirmation_payment_amount"].':
						</p>
					</TD>
					<TD bgcolor="#cfc5b1">
						<p>
							'.$_SESSION["go_total"].'
						</p>
					</TD>
				</TR>
				<TR>
					<TD width="484"><p align="right">'.$lang["confirmation_customer_name"].':</p></TD>
					<TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_oname"].'</p></TD>
				</TR>
				<TR>
					<TD width="484"><p align="right">'.$lang["email_status_code"].':</p></TD>
					<TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_status"].'</p></TD>
				</TR>
			</TBODY>
		</TABLE>
	</DIV>
	<div style="width:600px">'.$lang["confirmation_auth_message"].'</div>
	'.$social_media.'
</body>';

// Free Brick Body - Used For All campaigns offering free brick (Offering Free brick with code)
$freeBody = 
'<body style="margin-left:150px; margin-right:150px;margin-top:10px;">
<div style="background-color:'.$lang["published_background_color"].'">
<center><img src="'.$lang["full_published_logo"].'" width="600" height="65" alt="'.$lang["published_campaign_name"].'"></center>
<div style="color:#FFF" align="center"><strong>'.$lang["published_telephone_acronym"].' '.$lang["published_telephone_short"].' :   '.$lang["published_hours_of_operation"].'</strong></div>

</div>
<p align="center"><strong>'.$lang["confirmation_order_confirmation"].'  '.$lang["confirmation_order_received"].'</strong></p>
<p style="color:#F00; font-style:italic; font-size:11px;" align="center">('.strtoupper($lang["confirmation_payment_please_print"]).')</p>
<p>'.$lang["confirmation_thank_you_text1"].'</p>
<p>'.$lang['confirmation_processed_received'].'</p>
<DIV align="center">
  <TABLE border="1" cellPadding="0" width="587">
    <TBODY>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_date"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.date("F j, Y, g:i a").'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_number"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_orderid"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_code"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_auth_code"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_response"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.strtoupper($lang['confirmation_approved']).'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_payment_amount"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_total"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_customer_name"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_oname"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_message"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_status"].'</p></TD>
      </TR>
    </TBODY>
  </TABLE>
</DIV>
<div style="width:600px">'.$lang["confirmation_auth_body_accepted"].'</div>
'.$social_media.'
</body>';

// Received without processing
$ReceivedBody = 
'<body style="margin-left:150px; margin-right:150px;margin-top:10px;">
<div style="background-color:'.$lang["published_background_color"].'">
<center><img src="'.$lang["full_published_logo"].'" width="600" height="65" alt="'.$lang["published_campaign_name"].'"></center>
<div style="color:#FFF" align="center"><strong>'.$lang["published_telephone_acronym"].' '.$lang["published_telephone_short"].' :   '.$lang["published_hours_of_operation"].'</strong></div>

</div>
<p align="center"><strong>'.$lang["confirmation_order_confirmation"].'  '.$lang["confirmation_order_received"].'</strong></p>
<p style="color:#F00; font-style:italic; font-size:11px;" align="center">('.strtoupper($lang["confirmation_payment_please_print"]).')</p>
<p>'.$lang["confirmation_thank_you_text1"].'</p>
<p>'.$lang["confirmation_processed_success"].'</p>
<DIV align="center">
  <TABLE border="1" cellPadding="0" width="587">
    <TBODY>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_date"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.date("F j, Y, g:i a").'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_number"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_orderid"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_code"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_auth_code"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_response"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.strtoupper($lang['confirmation_pending']).'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_payment_amount"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_total"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_customer_name"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_oname"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_message"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_status"].'</p></TD>
      </TR>
    </TBODY>
  </TABLE>
</DIV>
<div style="width:600px">'.$lang["confirmation_web_receipt"].'</div>
'.$social_media.'
</body>';


//Declined Body
$declinedBody = '<body style="margin-left:150px; margin-right:150px;margin-top:10px;">
<div style="background-color:'.$lang["published_background_color"].';width:600px">
<center><img src="'.$lang["full_published_logo"].'" width="600" alt="'.$lang["published_campaign_name"].'"></center>
</div>
<div align="center" style="width:600px">
<p align="center"><strong>'.strtoupper($lang["confirmation_payment"]).' '.strtoupper($lang["confirmation_payment_failed"]).'</strong></p>
<p style="color:#F00; font-style:italic; font-size:11px;" align="center">('.strtoupper($lang["confirmation_payment_please_print"]).')</p>
</div>
<p>'.$lang["confirmation_processed_failed"].'</p>
<DIV align="center" style="width:600px">
  <TABLE border="1" cellPadding="0" width="587">
    <TBODY>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_date"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.date("F j, Y, g:i a").'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_order_number"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_orderid"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_code"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_auth_code"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_auth_response"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.strtoupper($lang["confirmation_declined"]).'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_payment_amount"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_total"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["confirmation_customer_name"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_oname"].'</p></TD>
      </TR>
      <TR>
        <TD width="484"><p align="right">'.$lang["email_status_code"].':</p></TD>
        <TD bgcolor="#cfc5b1"><p>'.$_SESSION["go_resp_status"].'</p></TD>
      </TR>
    </TBODY>
  </TABLE>
</DIV>
<div style="width:600px">'.$lang["confirmation_auth_body_declined"].'</div>
'.$social_media.'
</body>';

$path = "";	
include($path."../_common/includes/determine_body.php");
include($path."../_common/includes/sendgrid_module.php");
	
echo($retval);

}

?>