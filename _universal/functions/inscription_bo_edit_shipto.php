<?php

function bo_edit_shipto($li) 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");

$_SESSION["g_currentli"] = $li;
$li_ship = $_SESSION["g_lishiptoqty"][$li];

echo('
<div class="row">
	<div class="span12">
		<div class="well">
			<h4>'.$lang['cart_shipping_replica_shipping'].'</h4>
			<p>'.$lang['cart_shipping_text1'].'</p>
        	<p><i><span class="txt_boldred">'.$lang['cart_shipping_text2'].'</span></i></p>
');

if ($lang['cart_shipping_replica_total'] != "")
	{
	echo('
            <p>
				<strong>
					'.$lang['cart_shipping_replica_total'].': '.$_SESSION["g_lirqty"][$li].'
				</strong>
			<p>
');
	}
	
include("../".$environment_path."/includes/inscription_shipto.php");

echo('
		</div>
	</div>
</div>
');

include("../".$environment_path."/fields/contact_ship_fill_clear.php");

}

?>