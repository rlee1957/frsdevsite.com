<?php

function bo_inscr() 
{
	
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
$p_bktype = $_SESSION["g_currentbktype"];
$p_bkchar = $_SESSION["g_bkchar"][$p_bktype] ;
$p_bklines = $_SESSION["g_bklines"][$p_bktype];
$ca = $lang['cart_inscription_guide_body'];

if($options["show_allowed_characters1"])
	{ 
	$ca = $lang["pre_allowed_characters"].$p_bkchar.$lang['cart_inscription_guide_body'];
	}
if($options["show_allowed_characters2"])
	{ 
	$ca = $lang['cart_inscription_guide_body'];;
	}
echo '
	<div id="inscription">
	  <div class="row">
		<div class="span12">
		  <div class="well">
		  	<h4>'.$lang['cart_inscription_header'].'</h4>
			<p>'.$lang['cart_inscription_guide_title'].'</p>
			<p>'.$ca.'</p>
			<p>'.$lang['cart_inscription_as_follows'].'</p>
			<div class="pagination-centered">
				<div class="row">
					<div class="span10">
<center><table cellspacing=0 cellpadding=0 border=0>
	<tr>
		<td>&nbsp;</td>
		<td align=center>
			<strong>'.$p_bkchar.$lang["allowed_chrs_line"].'</strong>
		</td>
		<td>&nbsp;</td>
	</tr>
';
			  
for ($ll = 1; $ll <=  $p_bklines; $ll++)  
	{
	$line = "line".$ll;
	if(isset($_POST[$line]))
		{
		$r_line = htmlspecialchars(trim($_POST[$line]));	
		}
	else
		{
		$r_line = "";	
		}
	echo '
	<tr>
		<td>
			<strong style="font-size:20px;">'.$lang['cart_inscription_line'].' '.$ll.':&nbsp;</strong>
		</td>
		<td>
			<input id="'.$line.'" style="font-size: 19px; text-transform:uppercase;" class="large" name="'.$line.'" maxlength="'.$p_bkchar.'" value="'.$r_line.'"
			onblur="sanitize_sql(this);"
			onchange="sanitize_sql(this);" />
		</td>
';
//	if ($_SESSION["g_bkkeyboard"][$p_bktype] != "")
	if($options["greek_lettering"])
		{
		echo'
		<td>
			<script type="text/javascript">
			 $(function () {$(\'#'.$line.'\').keypad({showOn: \'button\',
			 buttonImageOnly: true, keypadOnly: false, buttonImage: \'img/omega.png\',separator: \'|\', layout: [\'Α|Β|Γ|Δ|Ε\',
			 \'Ζ|Η|Θ|Ι|Κ\', \'Λ|Μ|Ν|Ξ|Ο\', \'Π|Ρ|Σ|Τ|Υ\', \'Φ|Χ|Ψ|Ω\',
			 $.keypad.CLOSE +\'|\'+ $.keypad.CLEAR], 											prompt: \'Add these Greek letters\'});
			 });
			 </script>&nbsp;
		</td>';
		}
	echo'
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td align=center><div id="'.$ll.'_num"></div></td>
		<td>&nbsp;</td>
	</tr>
	';
	}
echo '
</table></center>
				</div>
			  </div>
			</div>
 			<i>'.$lang['cart_inscription_footer'].'</i>
		  </div>
		</div>
	  </div>
	</div>
 ';
// Javascript
echo '
<script type="text/javascript">       
$(document).ready(function(){';
for ($ll = 1; $ll <=  $p_bklines; $ll++)  
	{
	$line = "line".$ll;
	echo '
		$("#'.$line.'").keyup(function () {
			var ta = '.$p_bkchar.';
			$("#'.$ll.'_num").html(\''.$lang["spaces_used"].' (\' + $(\'#'.$line.'\').val().length + \') '.$lang["spaces_remaining"].' (\' + (ta - $(\'#'.$line.'\').val().length) +\')\');
		});
		
		$("#'.$line.'").change(function () {
    		var ta = '.$p_bkchar.';
    		$("#'.$ll.'_num").html(\''.$lang["spaces_used"].' (\' + $(\'#'.$line.'\').val().length + \') '.$lang["spaces_remaining"].' (\' + (ta - $(\'#'.$line.'\').val().length) +\')\');
		});
	';
	}
echo '
});
</script>';
}
?>