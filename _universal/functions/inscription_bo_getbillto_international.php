<?php

function bo_getbillto() 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
include("configuration/misc_settings.php");
echo '
<div>
<h4>'.$lang['cart_billing_information'].'</h4>
			<div class="span12" style="text-align:center; padding:10px;">
';
if((isset($button["use_address"]))&&(!$button["use_address"])){  }
else
	{
	echo('
				<input type="button" class="btn btn-custom" id="same_as_shipping" name="same_as_shipping" value="'.$lang['bill_addr_btn'].'">
');
	}
if((isset($button["clear_address"]))&&(!$button["clear_address"])){  }
else
	{
	echo('				
				<input type="button" style="margin-left:10px;" class="btn btn-custom" id="same_as_shipping_clear" name="same_as_shipping_clear" value="'.$lang['bill_clear_btn'].'">
');
	}
echo '				
			</div>
							<div class="control-group">
								<label class="control-label">
								   	*'.$lang['cart_billing_first_name'].'
								   </label>
								 <div class="controls">
								   <input type="text" name="b_fname" 
								   placeholder="'.$lang['cart_shipping_first_name_placeholder'].
										  '" value="'.$_SESSION["g_liship"][1][1]["s_name"].'" required
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)"										  />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   *'.$lang['cart_billing_last_name'].'
								 </label>
								 <div class="controls">
								   <input type="text" 
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)"
										  name="b_lname" 
										  required placeholder="'.
										  $lang['cart_shipping_last_name_placeholder'].
										  '" value="'.$_SESSION["g_liship"][1][1]["s_lname"].'">
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_billing_company'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="b_company" 
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)"
										  placeholder="'.
										  $lang['cart_shipping_company_placeholder'].
										  '" value="'.$_SESSION["g_liship"][1][1]["s_company"].'" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_billing_address'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="b_addr1" required 
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)"
										  placeholder="'.
										  $lang['cart_shipping_address_placeholder'].
										  '" value="'.$_SESSION["g_liship"][1][1]["s_addr1"].'" />
								   <p class="help-block"></p>
								 </div>
							</div>
							<div class="control-group">
								 <label class="control-label">
								 </label>
								 <div class="controls">
								   <input type="text" name="b_addr2" 
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)"
										  placeholder="'.
										  $lang['cart_shipping_address2_placeholder'].
										  '" value="'.$_SESSION["g_liship"][1][1]["s_addr2"].'">
								   <p class="help-block"></p>
								 </div>
							</div>
							
							<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_billing_city'].'
								 </label>
								 <div class="controls">
								   <input type="text" name="b_city" id="b_city" required 
										  onchange="sanitize_sql(this)"
										  onblur="sanitize_sql(this)"
										  placeholder="'.
										  $lang['cart_shipping_city_placeholder'].
										  '" value="'.$_SESSION["g_liship"][1][1]["s_city"].'">
								   <p class="help-block" id="city_help"></p>
								 </div>
							</div>
							
								<div class="control-group">
								 <label class="control-label">
								   '.$lang['cart_billing_state'].'
								 </label>
								 <div class="controls">';
$placeholder = $lang['edshp_prov_plchldr'];
$value = $_SESSION["g_liship"][1][1]["s_state"];								 
$select = '<select class="selectpicker show-tick show-menu-arrow required" id="b_state" name="b_state" required>';								 
echo(get_prov_states_options($select, $placeholder, $value));
echo('
								   <p class="help-block"></p>
								 </div>
								</div>
							
								 <div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_billing_zip'].'
								   </label>
								   <div class="controls">');
$placeholder = $lang['cart_shipping_zip_placeholder'];
$value = $_SESSION["g_liship"][1][1]["s_zip"];;
$id = "b_zip";
echo(get_postal_code_input($placeholder, $value, $id));
echo('								
									 <p class="help-block"></p>
								   </div>
								</div>');
echo('
								<div class="control-group">
								   <label class="control-label">
									 *'.$lang['cart_shipping_country'].'
								   </label>
								   <div class="controls">');
$placeholder = $lang['cart_shipping_country_placeholder'];
$value = $_SESSION["country"];
$id = "b_country";
echo(get_country_options($placeholder, $value, $id));
echo('
									 <p class="help-block"></p>
								   </div>
								</div>
								<div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_billing_phone'].'
								   </label>
								   <div class="controls">');
$placeholder = $lang['cart_shipping_daytime_phone_placeholder'];
$value = $_SESSION["g_liship"][1][1]["s_phone"];
$id = "b_phone";
echo(get_phone_input($placeholder, $value, $id));
echo('								   
									 <p class="help-block"></p>
								   </div>
								</div>
								<div class="control-group">
								   <label class="control-label">
									 '.$lang['cart_billing_email'].'
								   </label>
								   <div class="controls">
									 <input type="email" name="b_email" required 
											onchange="sanitize_sql(this)"
										    onblur="sanitize_sql(this)"
											placeholder="'.
										    $lang['cart_shipping_email_placeholder'].
										    '" rel="'.$lang['cart_shipping_email_note'].'" value="'.$_SESSION["g_liship"][1][1]["s_email"].'">
									 <p class="help-block"></p>
								   </div>
								</div>
</div>
<input type="hidden" name="b_nprofit" value="'.$_SESSION["g_billing"][11].'">
<hr>
');
}


?>