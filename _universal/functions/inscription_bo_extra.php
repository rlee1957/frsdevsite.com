<?php


function bo_extra()
{

include("configuration/environment_settings.php");
include("../_common/includes/language_check.php");
echo('
<div>
	<h4>'.$lang['cart_extra_information'].'</h4>
	<!-- Buying As Options -->
	<div class="control-group">
		<label class="control-label">
			'.$lang['cart_extra_referral'].'
		</label>
		<div class="controls">
			<select class="selectpicker span6 show-tick show-menu-arrow"
					required
					id="b_referral" 
					name="b_referral"
					onchange="show_result(this);">
				<option value="" selected disabled> '.$lang['cart_extra_dropdown_prompt'].'</option>
');
$heard_from = explode("|",$lang['cart_extra_referral_data']);
sort($heard_from);
foreach ($heard_from as $value)
	{
	echo('
				<option value="'.$value.'"> '.$value.'</option>
');
	}
echo('
			</select>
');
foreach($cer_special as $value)
	{
	$arr = array();
	$a = explode(",", $value);
	foreach($a as $key => $val)
		{
		$ar = explode(":", trim($val));
		$arr[trim($ar[0])] = trim($ar[1]);
		}
	$name = str_replace("'", "", trim($arr["name"])); 
	$required = str_replace("'", "", trim($arr["required"])); 
	$label = str_replace("'", "", trim($arr["label"])); 
	$placeholder = str_replace("'", "", trim($arr["placeholder"])); 
	$display_element_id = str_replace("'", "", trim($arr["display_element_id"])); 
	$text_element_id = str_replace("'", "", trim($arr["text_element_id"]));
	$max_length = str_replace("'", "", trim($arr["max_length"]));
	echo('
			<div id="'.$display_element_id.'" style="display:none;">
				<br />
				'.$label.'
				<input class="small" 
					   type="text" 
					   id="'.$text_element_id.'" 
					   name="do_nothing" 
					   maxlength="'.$max_length.'" 
					   value=""
					   placeholder="'.$placeholder.'"
					   onchange="sanitize_sql(this);"
				       onblur="sanitize_sql(this);"
					   style="width: 200px;" />
			</div>
');
	}
echo('
			<p class="help-block"></p>
		</div>
	</div>
');
if($lang['cart_extra_buyer_as_data'] != "")
	{
	echo('
	<!-- Opt In Options -->
		<div class="control-group">
			<label class="control-label">
				'.$lang['cart_extra_buyer_as'].'
			</label>
			<div class="controls">');
	$values = explode("|",$lang['cart_extra_buyer_as_data']);
	sort($values); //sort the values
	foreach ($values as $value)
		{
		if ($value == "Alumni")
			{
			echo('
				<div>
					<input type="checkbox" 
						   required
						   name="b_buying_as[]" 
						   id="show_'.$value.'" 
						   value="'.$value.'" /> 
					'.$value.'
				</div>
				<div id="container_'.$value.'" style="display:none;">
					<select id="select_'.$value.'" name="b_buying_as[]">
						<option name="" value="none"></option>');
						
			for ($i=1935; $i<=2013; $i++)
				{
				echo('
						<option value="'.$i.'">'.$i.'</option>');
				
				}
			echo('
					</select>
				</div>');
			}
		elseif($value == "Other")
			{
			echo('
				<div>
					<input type="checkbox" 
						   required
						   name="b_buying_as[]" 
						   value="'.$value.'" /> 
						   '.$value.' 
						   '.$lang['cart_extra_other2'].'
					<input type="text" 
						   placeholder="'.$lang['cart_extra_other2_placeholder'].'" 
						   name="b_buying_as[]" 
						   id="show_'.$value.'"
						   size=40
						   onchange="sanitize_sql(this);"
						   onblur="sanitize_sql(this);" >
				</div>');
			}
		elseif($value == "University Affilate")
			{
			echo('
				<div>
					<input type="checkbox" 
						   required
						   name="b_buying_as[]" 
						   value="'.$value.'" /> 
					'.$value.' 
					'.$lang['cart_extra_university'].'
					<input type="text" 
						   placeholder="Name of University"  
						   name="b_buying_as[]" 
						   id="show_'.$value.'"
						   size=40
						   onchange="sanitize_sql(this);"
						   onblur="sanitize_sql(this);					   />
				</div>');
			}
		
		else
			{
			echo('
				<div>
					<input type="checkbox" 
						   required
						   name="b_buying_as[]" 
						   id="b_buying_as[]" 
						   value="'.$value.'" /> 
					'.$value.'
				</div>');
			}
		}
	echo('
				<p class="help-block"></p>
			</div>
		</div>');
	}
if($lang['cart_collect_marketing_enable'])
	{
    bo_collect_email();
    }
echo('
</div>
<input type=hidden value="'.$javascript_cer_special.'" id="javascript_cer_special" />
<hr>
');
}

?>