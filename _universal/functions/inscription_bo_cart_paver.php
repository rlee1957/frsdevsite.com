<?php

function bo_cart_paver($line_item) 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
$xx_cost =  $_SESSION["g_bkpcost"][ $_SESSION["g_libktype"][$line_item]];
$xx_qty =  $_SESSION["g_lipqty"][$line_item];
$xx_total = $xx_qty * $xx_cost;
$_SESSION["g_ordertotal"] =  $_SESSION["g_ordertotal"] + $xx_total;
// Accumulate Payment Plan
if(isset($_SESSION["g_bkpcost_pay1"]))
	{
	$_SESSION["g_totalbk_pay1"] = ($_SESSION["g_totalbk_pay1"] + $_SESSION["g_bkpcost_pay1"][$_SESSION["g_libktype"][$line_item]] * $_SESSION["g_lipqty"][$line_item] ); 
	}
else
	{
	$_SESSION["g_totalbk_pay1"] = 0;	
	}
if(isset($_SESSION["g_bkpcost_pay2"]))
	{
	$_SESSION["g_totalbk_pay2"] = ($_SESSION["g_totalbk_pay2"] + $_SESSION["g_bkpcost_pay2"][$_SESSION["g_libktype"][$line_item]] * $_SESSION["g_lipqty"][$line_item] ); 
	}
else
	{
	$_SESSION["g_totalbk_pay2"] = 0;	
	}
if(isset($_SESSION["g_bkpcost_pay2"]))
	{
	$_SESSION["g_totalbk_pay3"]=  ($_SESSION["g_totalbk_pay3"] + $_SESSION["g_bkpcost_pay3"][$_SESSION["g_libktype"][$line_item]] * $_SESSION["g_lipqty"][$line_item] ); 
	}
else
	{
	$_SESSION["g_totalbk_pay3"]=  0;	
	}
echo '
			<tr>
				';
if  ($_SESSION["show_buttons"] == 1) 
	{ 
	echo '
				  <td>
				  	<a class="btn btn-link" href="proc_ins.html?selected_line_item='.$line_item.'">'.
					$lang['cart_edit'].'
				  </td>
				  <td>
				  	<a class="btn btn-link" href="proc_ins.html?selected_line_item_del='.$line_item.'">'.
					$lang['cart_delete'].'
				  </td>
				';
	}
else 
	{
	echo '
				  	<td></td>
				  	<td></td>';
	}
echo '
					<td>
						<h5>'.$xx_qty.'</h5>
					</td>
					<td>';
if($_SESSION["g_lilogoname"][$line_item] != "")
	{
	echo '<h5>'.$_SESSION["g_bkdesc"][$_SESSION["g_libktype"][$line_item]].'</h5>
							<img  src="logos/'.$_SESSION["g_lilogoname"][$line_item].'.png">
							';
	}
else
	{
	echo '<h5>'.$_SESSION["g_bkdesc"][ $_SESSION["g_libktype"][$line_item]].'</h5>';
	}
echo '
					</td>
					<td>
						<div class="input-prepend">
							<span class="add-on"><strong style="color:black;">'.$lang['published_currency_symbol'].'</strong></span>
							  <input class="input-small" type="Text" name="x_pcost" value="';  
printf("%2.2f", $xx_cost); 
echo '" readonly>
						</div>
					</td>
					<td>
						<div class="input-prepend">
							<span class="add-on"><strong style="color:black;">'.$lang['published_currency_symbol'].'</strong></span>
							  <input class="input-small" type="Text" name="x_ptotal" value="';  
printf("%2.2f", $xx_total); 
echo '" readonly>
						</div>
					</td>
			</tr>
     ';
}

?>