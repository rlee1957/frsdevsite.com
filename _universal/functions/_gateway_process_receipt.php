<?php
function process_receipt() 
{

include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
/*
$emheaders .= "From: " . $webmaster . "\n";
$emheaders .= "X-Mailer: PHP/" . phpversion() . "\n"; 
$emheaders .= "Content-Type: text/html; charset=iso-8859-1\n"; 
$emheaders .= "bcc:${webmaster}\n";
$title = "RECEIPT";
*/

$emheaders .= "From: " . $webmaster . "\n";
$emheaders .= "X-Mailer: PHP/" . phpversion() . "\n"; 
$emheaders .= "Content-Type: text/html; charset=iso-8859-1\n"; 
$emheaders .= "bcc:";
$title = "RECEIPT";


$content = '
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<title>Confirmation Page</title>
		<style>
	#ccdata table {
	 	background-color: #FFFFFF;
		color: #000000;
		width: 500px;
		border: 1px solid #D7E5F2;
		border-collapse: collapse;
	} 
	
	#ccdata td {
		border: 1px solid #D7E5F2;
		padding-center: 4px;
	}
	
	.labelcell {
		font: 12px "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
		color: #3670A7;
		text-align: right;
		background-color: transparent;
		width: 220px;
	}

	.fieldcell {
		background-color: #F2F7FB;
		color: #000000;
		text-align: left;
		margin-right: 0px;
		padding-right: 0px;
	}

	.txtl {
		background-color: #FFFFFF;
		color: #000000;
		text-align: left;
		margin-right: 0px;
		padding-right: 0px;
	}
	
	.headingcc {
		font: 13px "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
		background-color: #f0e68c;
		text-align: justify;
	}
	
	.headingcenter {
		font: 13px "Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
		background-color: #bdb76b;
		color: #ffffff;
		text-align: center;
	}	
	</style>
	</head>
	<body><div align="center" style="display:block"><img src="images/'.$lang['published_logo'].'" alt="LOGO" border="0"><br>

<div align="center">
	<TABLE>
	<!-- Display Phone Number
		<tr>
			<td align="center">
			'.$lang["published_telephone_acronym"].' '.$lang["published_telephone_short"].'
			</td>
		</tr>
		-->
		<tr>
			<td align="center">
			<br>
			<b>'.$lang['cart_order_confirmation_header'].'</b>
			</td>
		</tr>
		<tr>
			<td align="center">
			<i>('.strtoupper($lang['confirmation_payment_please_print']).')</i>
			</td>
		</tr>
		<tr>
			<td class="txt1" align="left">
			<br>'.
			$lang['confirmation_thank_you_text1'].
			'&nbsp;'.
			$lang['confirmation_thank_you_text2'].
			'
			</td>
		</tr>
	</table>
</div>';

echo $content;
bo_combined_cart();
// Added May 15
//if ($_SESSION["g_op_enable"] || $_SESSION["g_gc_enable"]) {
///		bo_op_cart();
//	} else {
//	    bo_cart();
//}
 /// bo_cart_confirm();

//  Testing for payplan
// $_SESSION["g_payplan"] = 1;
// echo $_SESSION["g_payplan"];
// Added 10-1-08 Dustin Blad - Add Payment Plan Table if Payment plan was selected
if ($_SESSION["g_payplan"]) {
		echo '
		   <div id="payplan_grid" >
   <div align="right" class="tbl_reg_txt"><strong>Payment Breakdown:</strong></div>
<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH="100%">
        <TR BGCOLOR="#FFFFFF" ALIGN="center">
        <TD NOWRAP WIDTH="60%" ALIGN="left">        </TD>
        <TD WIDTH="40%">
            <TABLE CELLPADDING=0 CELLSPACING=3 BORDER=1 WIDTH="100%">

	    		<TR ALIGN="center">
                <TD NOWRAP WIDTH="20%" ALIGN="right"><strong><FONT SIZE=3>Payment 1:</FONT></strong></TD>
                <TD NOWRAP WIDTH="20%" ALIGN="right" BGCOLOR="#FFFFFF"><FONT SIZE=2>$&nbsp;
                  <INPUT TYPE="Text" id="payment1" NAME="payment1" VALUE="'; echo number_format($_SESSION["g_totalcost_pay1"],2); echo '" SIZE=10 MAXLENGTH="30" align=right>
                </FONT></TD>
                </TR>
				<TR ALIGN="center">
                <TD NOWRAP WIDTH="20%" ALIGN="right"><FONT SIZE=3><strong>Payment 2</strong>:</FONT></TD>
                <TD NOWRAP WIDTH="20%" ALIGN="right" BGCOLOR="#FFFFFF"><FONT SIZE=2>$&nbsp;
                  <INPUT TYPE="Text" id="payment2" NAME="payment2" VALUE="'; echo number_format($_SESSION["g_totalcost_pay2"],2); echo '" SIZE=10 MAXLENGTH="30" align=right>
                </FONT></TD>
                </TR>				
				<TR ALIGN="center">
                <TD NOWRAP WIDTH="20%" ALIGN="right"><strong><FONT SIZE=3>Payment 3:</FONT></strong></TD>
                <TD NOWRAP WIDTH="20%" ALIGN="right" BGCOLOR="#FFFFFF"><FONT SIZE=2>$&nbsp;
                  <INPUT TYPE="Text" id="payment3" NAME="payment3" VALUE="'; echo number_format($_SESSION["g_totalcost_pay3"],2); echo '" SIZE=10 MAXLENGTH="30" align=right>
                </FONT></TD>
                </TR>
                <TR ALIGN="center">
                <TD NOWRAP WIDTH="20%" ALIGN="right"><strong><FONT SIZE=3>Total:</FONT></strong></TD>
                <TD NOWRAP WIDTH="20%" ALIGN="right" BGCOLOR="#FFFFFF"><FONT SIZE=2>$&nbsp;
                  <INPUT TYPE="Text" id="payment_total" NAME="payment_total" VALUE="'; echo number_format($_SESSION["g_totalcost_pay1"] + $_SESSION["g_totalcost_pay2"] + $_SESSION["g_totalcost_pay3"],2); echo '" SIZE=10 MAXLENGTH="30" align=right>
                </FONT></TD>
                </TR>
            </TABLE>        </TD>
        </TR>
     </TABLE>
     
     </TD>
</TR>
</table>
  </div>';
	
}

$content = $lang['confirmation_web_receipt'];
$gacode1 = '
<!-- GA Tracking -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("'.$lang['set_analytics'].'");
pageTracker._initData();
pageTracker._trackPageview();

pageTracker._addTrans(
      "'. $_SESSION["go_orderid"] .'",
      "'.$lang['published_campaign_name'].'",
      "'. $_SESSION["go_total"] .'",
      "'. $_SESSION["g_taxtotal"] .'",
      "'. $_SESSION["g_op_ship_total"] .'",
      "'. $_SESSION["go_ocity"] .'",
      "'. $_SESSION["go_ostate"] .'",
      "US"
);
';

echo $content;
?>
<div id="fb-root"></div>
    <script src='https://connect.facebook.net/en_US/all.js'></script>
	<div style="width:130; margin-left: auto; margin-right: auto; margin-top:10px; ">
		<a href="#" onclick='postToFeed(); return false;'><img src="../<?php echo($environment_path); ?>/img/facebook-share-button.png" alt="Post To Feed"></a>
	</div>

    <p id='msg'></p>
    <script>
        FB.init({ appId: "1470139389866062", status: false, cookie: true });

        function postToFeed() {

            // calling the API ...
            var obj = {
                method: 'feed',
                name: '<?php echo $lang['facebook_name']; ?>',
                link: 'https://www.brickorder.com/rockhall/',
                picture: 'https://www.brickorder.com/rockhall/img/RRHOF_155x100_logo_03-07-14.jpg',
                caption: '<?php echo $lang['facebook_caption']; ?>',
                description: '<?php echo $lang['facebook_description']; ?>',
            };

            function callback(response) {
                document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
                if (response && response.post_id) {
                    alert('Post was published.');
                } else {
                    alert('Post was not published.');
                }
            }

            FB.ui(obj, callback);

        }

		//window.onload=function(){postToFeed()};
    </script>
<?php

if ($_SESSION["go_resp_status"] == 1) {
	echo '
	<div align="center">
		<TABLE id="ccdata">
			<tr><td>
				<a href="index.html">'.$lang["home"].'</a>
			</td></tr>		
		</TABLE>
	</div>	
	';
} else {
	echo '
	<div align="center">
		<TABLE id="ccdata">
			<tr>
				<td>

				-
				<a href="index.html">'.$lang["home"].'</a> - <a href="configuration/faq.php">FAQ</a></td>
			</tr>		
		</TABLE>
	</div>	
	';
}

// echo "Status: " . $_SESSION["go_resp_status"] ;
echo $gacode1;
// GA Loop Through Each Product and add here

                 for ($ii = 1; $ii <= $_SESSION["g_litotals"]; $ii++)
                {
                if (isset($_SESSION["g_libktype"][$ii])) {
                     $tmp_bktype = $_SESSION["g_libktype"][$ii];
                     $tmp_bkdesc = $_SESSION["g_bkdesc"][ $_SESSION["g_libktype"][$ii]];
                     $tmp_bklines = $_SESSION["g_bklines"][ $_SESSION["g_libktype"][$ii]];
                     $tmp_bkchar = $_SESSION["g_bkchar"][ $_SESSION["g_libktype"][$ii]];
                     $tmp_pcost =  $_SESSION["g_bkpcost"][ $_SESSION["g_libktype"][$ii]];
                     $tmp_rcost =  $_SESSION["g_bkrcost"][ $_SESSION["g_libktype"][$ii]];
                     $tmp_rship =  $_SESSION["g_bkrship"][ $_SESSION["g_libktype"][$ii]];
                     $tmp_pqty =  $_SESSION["g_lipqty"][$ii];
                     $tmp_rqty =  $_SESSION["g_lirqty"][$ii];
					 $tmp_logoname = trim($_SESSION["g_lilogoname"][$ii]);
					 $tmp_logono = $_SESSION["g_lilogono"][$ii];
// GA add Brick product
echo 'pageTracker._addItem(
      "'. $_SESSION["go_orderid"] .'",
      "'. $tmp_bkdesc .' '. $tmp_logoname .'",
      "'. $tmp_bkdesc .'",
      "Brick",
      "'. $tmp_pcost .'", 
      "'. $tmp_pqty .'");
';
// GA add Replica product

echo 'pageTracker._addItem(
      "'. $_SESSION["go_orderid"] .'",
      "'. $tmp_bkdesc .' '. $tmp_logoname .' Replica",
      "'. $tmp_bkdesc .' Replica",
      "Replica",
      "'. $tmp_rcost .'", 
      "'. $tmp_rqty .'");
';

                    } // end if isset
                } // end for
// End Loop


// Loop through Options
	   if ($_SESSION["g_op_enable"]) {
        for ($ii = 1; $ii <= $_SESSION["g_op_no_li"]; $ii++)
                {
                if (isset($_SESSION["g_op_li"][$ii]["op_type"])) {	
				
						$tmp_iii =  $_SESSION["g_op_li"][$ii]["op_type"];
  						$tmp_xx_qty = $_SESSION["g_op_li"][$ii]["op_qty"];
  						$tmp_xx_cost =  $_SESSION["g_op_cost"][$tmp_iii];
						$tmp_xx_ship =  $_SESSION["g_op_ship"][$tmp_iii];
						$tmp_xx_ship_total =  $tmp_xx_ship * $tmp_xx_qty;
  						$tmp_xx_cost_total = $_SESSION["g_op_cost"][$tmp_iii] * $tmp_xx_qty;
						$tmp_xx_total = $tmp_xx_cost_total + $tmp_xx_ship_total;

echo 'pageTracker._addItem(
	"'. $_SESSION["go_orderid"] .'",
	"'. $_SESSION["g_op_desc"][$tmp_iii] .'",
	"'. $_SESSION["g_op_desc"][$tmp_iii] .'",
	"Gift Certificate",
	"'. $tmp_xx_cost .'", 
	"'. $tmp_xx_qty .'");
';
				}
			}
		}

// Run GA Track Trans
echo '
pageTracker._addItem(
    "'. $_SESSION["go_orderid"] .'",
    "Handling",
    "Handling",
    "Fee",
    "'.$_SESSION["g_handfee"].'",
    "1"
  );

pageTracker._trackTrans();
} catch(err) {}</script>
</BODY></HTML>';


// send_email();  // Send Emails after processing the credit card

}


?>