<?php

function ses_del_line_item()
{
//------------------------------------------------------------------------
// Delete the selected line item from array
// It only emptys the array, it does not remove it
// Example: if you had 3 array elements and called
// this function to delete array element 2 you will
// still have elements 1 and 3
//------------------------------------------------------------------------
$ii = $_GET['selected_line_item_del'];
unset($_SESSION["g_libktype"][$ii]);
}

?>