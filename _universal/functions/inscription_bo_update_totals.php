<?php


function bo_update_totals()
{
	
include("configuration/environment_settings.php");
include("../".$environment_path."/includes/language_check.php");
echo '
	<script type="text/javascript">
	function updateTotals(){
                    if (document.frmInscription.x_rqty.value > 0) {
                         if (document.frmInscription.x_pqty.value <= 0) {
                            alert("You must have at least one paver!");
                            document.frmInscription.x_rqty.value = 0;
                            document.frmInscription.x_pqty.focus();
                         }
                    }
		  
		// -------------------------------------------------     
		// Put values into local variables for calculation
		// ------------------------------------------------
				
		l_pqty = 0;
		l_rqty = 0;
		l_dcqty = 0;
		l_pcost = 0.00;
		l_rcost = 0.00;
		l_r2cost = 0.00;
		l_dccost = 0.00;
		l_dc2cost = 0.00;
		l_ptotcost = 0.00;
		l_rtotcost = 0.00;
		l_r2totcost = 0.00;
		l_dctotcost = 0.00;
		l_dc2totcost = 0.00;
		l_subtot = 0.00;
		l_grandtot = 0.00;
		
		l_pqty = document.frmInscription.x_pqty.value;
		l_rqty = document.frmInscription.x_rqty.value;
		l_dcqty = document.frmInscription.x_dcqty.value;
		';
		if($options["enable_replica2"]){
		echo '
		l_r2qty = document.frmInscription.x_r2qty.value;
		l_r2cost = document.frmInscription.x_r2cost.value;
		l_r2totcost = l_r2qty * l_r2cost;
		document.frmInscription.x_r2total.value = l_r2totcost.toFixed(2);';
		
		}
		if($options["enable_displaycase2"]){
		echo '
		l_dc2qty = document.frmInscription.x_dc2qty.value;
		l_dc2cost = document.frmInscription.x_dc2cost.value;
		l_dc2totcost = l_dc2qty * l_dc2cost;
		document.frmInscription.x_dc2total.value = l_dc2totcost.toFixed(2);';
		}
		echo '
		l_pcost = document.frmInscription.x_pcost.value;
		l_rcost = document.frmInscription.x_rcost.value;
		l_dccost = document.frmInscription.x_dccost.value;
		
		
		l_ptotcost = l_pqty * l_pcost;
		l_rtotcost = l_rqty * l_rcost;
		l_dctotcost = l_dcqty * l_dccost;
		
		
		
		l_subtot = l_ptotcost + l_rtotcost + l_dctotcost + l_r2totcost + l_dc2totcost;
		l_grandtot = l_subtot;
		
		document.frmInscription.x_ptotal.value = l_ptotcost.toFixed(2);
		document.frmInscription.x_rtotal.value = l_rtotcost.toFixed(2);
		document.frmInscription.x_dctotal.value = l_dctotcost.toFixed(2);
		
		//document.getElementById(\'x_gtotal\').value=l_grandtot.toFixed(2);
		document.frmInscription.x_gtotal.value = l_grandtot.toFixed(2);
		//document.frmInscription.xx_gtotal.value = l_grandtot.toFixed(2);
			
}
	</script>
';
}


?>