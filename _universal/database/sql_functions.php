<?php

function exe($creds, $sql, $params, $driver)
{
	
$res = array();
$message = "Success";
$res["sql"] = $sql;
$res["params"] = $params;
# $sql = "INSERT INTO xyz (x, y, z) VALUES (?, ?, ?)"
# $params = array($x, $y, $z)
# see - http://wiki.hashphp.org/PDO_Tutorial_for_MySQL_Developers
$pdo = get_psql_pdo($creds);
$res["error"] = get_connection(0, $message, $pdo);
$stmt = $pdo->prepare($sql);
$stmt->execute($params);
$affected_rows = $stmt->rowCount();
$res["affected_rows"] = $affected_rows;
return $res;

}

function get_psql_pdo($creds, $driver)
{	

include("../_common/includes/_".$creds."_credentials.php");
if($driver == "mysql")
	{
	$driver = "mysql:host=".$host.";dbname=".$dbname;
	}
if($driver == "postgres")
	{
	$driver = "pgsql:host=".$host.";dbname=".$dbname;	
	}
if($driver == "mssql")
	{
	$driver = "mysql:host=".$host.";dbname=".$dbname;	
	}
$pdo = new PDO($driver,$user,$pwd);	
return $pdo;

}

?>