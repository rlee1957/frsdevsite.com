<?php

$sql = "
insert into stories
	(
	campaign_name,
	campaign_db_name,
	order_id,
	datetimestamp,
	story_type,
	story_category,
	story_details,
	has_photo,
	photo_location,
	story_text,
	story_html,
	story_other
	)
values
	(
	'".sanitize_sql($lang['published_campaign_name_short'])."',
	'".sanitize_sql($dbname)."',
	'".sanitize_sql($order_id)."',
	'".sanitize_sql($dts)."',
	'".sanitize_sql($story_type)."',
	'".sanitize_sql($story_category)."',
	'',
	".$has_photo.",
	'".$story_file."',
	'".sanitize_sql($story_text)."',
	'',
	''
	)
";

?>