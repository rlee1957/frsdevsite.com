function sanitize_sql(obj)
{
var tmp = obj.value;
var reTVal = "";
var len = tmp.length;
var chr = "";
var asc = 0;
var x = 0;
for(x;x<len;x++)
	{
	asc = tmp.charCodeAt(x);
	chr = String.fromCharCode(asc);
	if(asc == 39){chr = "`";}
	if(asc == 92){chr = "";}
	reTVal += chr;	
	}
obj.value = reTVal;
}

function only_printable(obj)
{
var tmp = obj.value;
var reTVal = "";
var len = tmp.length;
var chr = "";
var asc = 0;
var x = 0;
for(;x<len;x++)
	{
	asc = tmp.charCodeAt(x);
	chr = String.fromCharCode(asc);
	if((asc < 33)||(asc > 126)){ chr = ""; }
	reTVal += chr;	
	}
return reTVal;
}

function allowed_characters(evt)
{
	
var code = (evt.which) ? evt.which : evt.keyCode;
var shift = evt.shiftKey;
if(document.getElementById("inscription_restricted_codes"))
	{
	var tst	= document.getElementById("inscription_restricted_codes").value;
	var ar = tst.split("|");
	var ln = ar.length;
	if(ln > 0)
		{
		for(x=0;x<ln;x++)
			{
			var cd = ar[x].split("~");
			var testshift = false;
			var tshift = cd[0];
			if(tshift > 0){ testshift = true; }
			var tcode = cd[1];
			if((testshift == shift )&&(tcode == code))
				{  
				return false;
				}
			}	
		}
	}
return true;	

}

function only_allowed(obj)
{

var retval = obj.value;
if(retval.length > 0)
	{
	if(document.getElementById("inscription_allowed_characters"))
		{
		var tst	= document.getElementById("inscription_allowed_characters").value;
		var ln = tst.length;
		if(ln > 0)
			{
			var len = obj.value.length;
			retval = "";
			for(x=0;x<len;x++)
				{
				var nxt = "";	
				for(y=0;y<ln;y++)
					{
					if(obj.value.substr(x, 1) == tst.substr(y, 1))
						{
						nxt = obj.value.substr(x, 1);
						break;
						}
					}
				retval += nxt
				}	
			}
		}
	}
obj.value = retval;
	
}


